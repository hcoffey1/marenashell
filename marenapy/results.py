import os
from shutil import *
from marenapy.paths import *
from marenapy.tools import *
from marenapy.benches.bench import *
from marenapy.utils import *
from functools import reduce
from math import sqrt, isnan, isinf
from statistics import mean, median
from operator import itemgetter
import numpy as np
import operator
import pickle
import gc
import itertools

from statistics import mean,stdev,median

def peekline(f):
  pos = f.tell()
  line = f.readline()
  f.seek(pos)
  return line

def update_scalar_stat(stats, x):
  mean     = stats[0]
  variance = stats[1]
  minval   = stats[2]
  maxval   = stats[3]
  rawval   = stats[4]
  n        = stats[5]

  n += 1

  if (n > 1):
    variance += ( (((float(x) - mean) ** 2) / n) - (variance / (n-1)) )

  mean += ( (x - mean) / n )
  if (n == 1):
    minval = maxval = x;
  else:
    maxval = x if x > maxval else maxval
    minval = x if x < minval else minval
  
  rawval += x

  stats[0] = mean
  stats[1] = variance
  stats[2] = minval
  stats[3] = maxval
  stats[4] = rawval
  stats[5] = n

def update_ratio_stat(stats, x, num, den):
  mean     = stats[0]
  variance = stats[1]
  minval   = stats[2]
  maxval   = stats[3]
  rawnum   = stats[4]
  rawden   = stats[5]
  n        = stats[6]

  n += 1

  if (n > 1):
    variance += ( (((float(x) - mean) ** 2) / n) - (variance / (n-1)) )

  mean += ( (x - mean) / n )
  if (n == 1):
    minval = maxval = x;
  else:
    maxval = x if x > maxval else maxval
    minval = x if x < minval else minval
  
  rawnum += num
  rawden += den

  stats[0] = mean
  stats[1] = variance
  stats[2] = minval
  stats[3] = maxval
  stats[4] = rawnum
  stats[5] = rawden
  stats[6] = n

def new_int_stat():
  return [0.0, 0.0, 0, 0, 0, 0]

def new_float_stat():
  return [0.0, 0.0, 0.0, 0.0, 0, 0, 0]

def get_int_info(line):
  pts = line.split()
  return ( [ float(pts[0]), float(pts[1]) ] + \
           [ int(x) for x in pts[2:] ]        \
         )

def get_float_info(line):
  pts = line.split()
  return ( [ float(x) for x in pts ] )

def get_agg_stats(vals, objects, maxval=False, ratval=False, pp=False):

  agg_mean  = 0.0
  agg_stdev = 0.0
  agg_min   = 0
  agg_max   = 0
  agg_val   = 0

  n = 0
  for val,m in zip(vals,objects):
    if m == 0:
      continue

    prev_usq = (agg_mean ** 2)
    prev_var = (agg_stdev ** 2)

    agg_mean = ( (agg_mean * (float(n) / (n+m))) + \
                 (val[0]   * (float(m) / (n+m))) )

    cur_usq = (val[0] ** 2)
    cur_var = (val[1] ** 2)
    t1 = (n * (prev_var + prev_usq))
    t2 = (m * (cur_var + cur_usq))
    agg_var = ( (( t1 + t2 ) / (m + n)) - (agg_mean**2) )
    if (agg_var < 0.0):
      if (agg_var < -0.1):
        print("error: bad var: " + str(agg_var))
        raise (SystemExit(1))
      else:
        agg_stdev = 0.0
    else:
      agg_stdev = (agg_var ** 0.5)

    agg_min = min([agg_min, val[2]])
    agg_max = max([agg_max, val[2]])

    if maxval:
      agg_val = max([agg_val, val[4]])
    elif ratval:
      agg_val = ( (agg_val * (float(n) / (n+m))) + \
                  (val[4]  * (float(m) / (n+m))) )
    else:
      agg_val += val[4]

    n += m

  return [ agg_mean, agg_stdev, agg_min, agg_max, agg_val ]

def get_object_tzstr_info(bench, cfg_name, cutstyle, tzstr):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%s.object_%s-%s.pkl' % (results_dir, cutstyle.lower(), tzstr)), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_object_cut_info(bench, cfg_name, cutstyle=BANDWIDTH_PER_BYTE):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sobject_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_feature_cut_info(bench, cfg_name, cutstyle, train=False):
  results_dir = get_results_dir(bench, cfg_name)
  if train:
    cut_pkl_fd = open(('%sfeature_train_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  else:
    cut_pkl_fd = open(('%sfeature_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_alt_site_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%salt_site_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_bench_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sbench_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  btdict = pickle.load(cut_pkl_fd)
  return btdict


def get_agg_info_line(info_id, agg_list):
  objects  = sum ( [ x[0] for x in agg_list ]  )
  size     = sum ( [ x[1] for x in agg_list ]  )
  touched  = sum ( [ x[2] for x in agg_list ]  )
  page_rss = max ( [ x[3] for x in agg_list ]  ) 
  line_rss = max ( [ x[4] for x in agg_list ]  ) 
  agpg_rss = max ( [ x[5] for x in agg_list ]  ) 
  agcl_rss = max ( [ x[6] for x in agg_list ]  ) 
  szinfo   = [ sum( [ x[i] for x in agg_list ] ) \
               for i in range(7, len( agg_list[0] )) ]
  pts = [info_id, objects, size, touched, page_rss, line_rss, \
         agpg_rss, agcl_rss] + szinfo
  aggline = " ".join([("%-16d" % pts[0])] + [ ("%16d" % x) for x in pts[1:]])
  return aggline

def get_output_files(benches, configs, its=[0], copies=[0],
  files=pinfiles):
  for bench in benches:
    for cfg_name in configs:
      for it in its:
        for copy in copies:
          get_bench_cfg_files(bench, cfg_name, it, copy, files=files)

def get_bench_cfg_files(bench, cfg_name, it, copy, files=pinfiles): 
  cfg.read_cfg(cfg_name)

  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for run in runs:
    for fname in files:
      run_dir     = get_run_dir(bench, cfg_name, it, copy, run)
      results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
      cmd = ("cp %s %s" % ((run_dir+fname), (results_dir+fname)))
      os.system(cmd)

def remove_staged_files(benches, configs, its=[0], copies=[0],\
  files=['obj_info.out']):
  for bench in benches:
    for cfg_name in configs:
      for it in its:
        for copy in copies:
          remove_bench_cfg_files(bench, cfg_name, it, copy, files=files)

def remove_bench_cfg_files(bench, cfg_name, it, copy,\
  files=['obj_info.out']): 

  cfg.read_cfg(cfg_name)
  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for run in runs:
    for fname in files:
      run_dir = get_run_dir(bench, cfg_name, it, copy, run)
      cmd = ("/usr/bin/sudo rm -f %s" % ((run_dir+fname)))
      print(cmd)
      os.system(cmd)

def manhattan_distance(a, b):
  dist = 0
  for x,y in zip(a,b):
    dist += (abs(x - y))
  return dist

def get_agg_phase_id(bbv, agg_bbvs, top_id):

  min_dist = None
  agg_id = None
  for cur_id,cur_bbv in agg_bbvs.items():
    dist = manhattan_distance(bbv, cur_bbv)
    if min_dist == None or dist < min_dist:
      min_dist = dist
      agg_id = cur_id

  if (min_dist == None) or (min_dist >= UNIQUE_PHASE_THRESHOLD):
    agg_id = top_id
    top_id += 1
    agg_bbvs[agg_id] = bbv

  return (agg_id, top_id)

def get_closest_phase(bbv, tid2phase):

  min_dist = None
  close_id = None
  for tid,cur_bbv in tid2phase.items():
    dist = manhattan_distance(bbv, cur_bbv)
    if min_dist == None or dist < min_dist:
      min_dist = dist
      close_id = tid

  return close_id

def get_mc_stats_agg(bench, cfg_name):
  cfg.read_cfg(cfg_name)
  #print("%-20s %-30s ... " % (bench, cfg_name), end="")

  mc_stats = [0 for _ in range(10)]

  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for run in runs:
    results_dir = get_run_results_dir(bench, cfg_name, 0, 0, run)
    mc_statsf = open(("%smc_stats.out" % results_dir), 'r')

    next(mc_statsf)
    for line in mc_statsf:
      for i,v in enumerate(line.split()):
        mc_stats[i] += int(v)

  return mc_stats

def create_agg_object_infos(benches, configs, do_objects=False, \
  do_cachelines=False):
  for bench in benches:
    for cfg_name in configs:
      make_agg_object_info(bench, cfg_name, do_objects=do_objects)

def make_agg_object_info(bench, cfg_name, it=0, do_objects=False):

  cfg.read_cfg(cfg_name)
  print("%-20s %-30s ... " % (bench, cfg_name), end="")

  curid   = 0
  clsum   = 0
  totobjs = 0

  imap    = {}
  clmap   = {}
  clstats = {}
  objlist = []

  fncfg = False
  if cfg_name == 'pin_fnlpc_9cxt':
    fncfg = True

  phase_cfg = False
  if cfg_name in phase_cfgs:
    phase_cfg = True


  runs    = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  err     = False

  tmpdir  = get_run_results_dir(bench, cfg_name, it, 0, 0)

  if do_objects:
    agg_obj_fd = open(('%sagg_obj_info.out' % \
                     (get_results_dir(bench, cfg_name))), 'w')

  site_strs        = {}
  type_strs        = {}
  acc_strs         = {}
  agg_size_map     = {}
  agg_szbucket_map = {}
  agg_site_map     = {}
  agg_type_map     = {}
  agg_acc_map      = {}
  top_site_id      = {}

  top_type_id      = 0
  top_type_str_id  = 0
  top_acc_id       = 0
  top_acc_str_id   = 0

  if phase_cfg:
    agg_bbvs            = {}
    run_agg_phase_id    = {}
    agg_alloc_phase_map = {}
    agg_phase_sig_map   = {}
    agg_typesz_map      = {}
    agg_phasesz_map     = {}
    agg_phase_site_map  = {}
    agg_phsz_site_map   = {}

    top_alloc_phase_id  = 0
    top_phase_sig_id    = 0
    top_typesz_id       = 0
    top_phasesz_id      = 0
    top_phase_site_id   = {}
    top_phsz_site_id    = {}


  for copy in range(cfg.current_cfg['runcfg']['copies']):

    for run in runs:
#      try:
        run_site_map = {}
        run_type_map = {}
        run_acc_map  = {}
        run_types    = {}

        if phase_cfg:
          run_agg_phase_id[run] = {}
          run_phase_map         = {}
          run_phase_sig_map     = {}
          run_typesz_map        = {}
          run_phasesz_map       = {}
          run_phase_site_map    = {}
          run_phsz_site_map     = {}


        results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)


        #print("size-%d" % run)
        size_info_fd = open(('%ssize_info.out' % (results_dir)), 'r')
        for line in size_info_fd:
          if line.isspace():
            break

          szinfo = [int(x) for x in line.split()]
          szkey  = szinfo[0]
          if not szkey in agg_size_map:
            agg_size_map[szkey] = []
          agg_size_map[szkey].append(szinfo[1:])
        size_info_fd.close()


        #print("szbucket-%d" % run)
        size_bucket_info_fd = open(('%ssize_bucket_info.out' % (results_dir)), 'r')
        for line in size_bucket_info_fd:
          if line.isspace():
            break

          szbinfo = [int(x) for x in line.split()]
          szbkey  = szbinfo[0]
          if not szbkey in agg_szbucket_map:
            agg_szbucket_map[szbkey] = []
          agg_szbucket_map[szbkey].append(szbinfo[1:])
        size_bucket_info_fd.close()



        #print("types-%d" % run)
        type_strs_fd = open(('%stype_strs.out' % (results_dir)), 'r', \
                             encoding="ISO-8859-1")
        for line in type_strs_fd:
          if line.isspace():
            break

          run_type_id = int(line.split()[0])
          type_str    = " ".join( line.split()[1:] )
          if not type_str in type_strs:
            type_strs[type_str] = top_type_str_id
            top_type_str_id += 1
          run_types[run_type_id] = type_strs[type_str]
        type_strs_fd.close()

        type_sigs_fd = open(('%stype_sigs.out' % (results_dir)), 'r')
        for line in type_sigs_fd:
          if line.isspace():
            break

          pts = line.split()
          type_id  = int(pts[0])
          type_sig = tuple([ run_types[int(x)] for x in pts[1:] ])
          run_type_map[type_id] = type_sig
        type_sigs_fd.close()

        type_info_fd = open(('%stype_sig_info.out' % (results_dir)), 'r')
        for line in type_info_fd:
          if line.isspace():
            break

          tinfo = [int(x) for x in line.split()]
          tkey  = run_type_map[tinfo[0]]
          if not tkey in agg_type_map:
            agg_type_map[tkey] = (top_type_id, [])
            top_type_id += 1
          agg_type_map[tkey][1].append(tinfo[1:])
        type_info_fd.close()



        #print("accs-%d" % run)
        acc_strs_fd = open(('%sacc_strs.out' % (results_dir)), 'r', \
                             encoding="ISO-8859-1")
        for line in acc_strs_fd:
          if line.isspace():
            break

          if len(line.split()) == 2:
            addr_part, str_part = line.split()
          else:
            #print(line)
            if len(line.split(':')) == 2:
              addr_part, str_part = line.split(':')
              str_part += ':'
            elif len(line.split('/')) == 2:
              addr_part, str_part = line.split('/')
              str_part += '/'
            else:
              raise (SystemExit(1))

          addr = int(addr_part,base=16)
          if not addr in acc_strs:
            acc_strs[addr] = str_part
        acc_strs_fd.close()

        acc_sigs_fd = open(('%sacc_sigs.out' % (results_dir)), 'r')
        for line in acc_sigs_fd:
          if line.isspace():
            break

          pts = line.split()
          acc_id  = int(pts[0])
          acc_sig = tuple([ int(x,base=16) for x in pts[1:] ])
          run_acc_map[acc_id] = acc_sig
        acc_sigs_fd.close()

        acc_info_fd = open(('%sacc_sig_info.out' % (results_dir)), 'r')
        for line in acc_info_fd:
          if line.isspace():
            break

          accinfo = [int(x) for x in line.split()]
          acckey  = run_acc_map[accinfo[0]]
          if not acckey in agg_acc_map:
            agg_acc_map[acckey] = (top_acc_id, [])
            top_acc_id += 1
          agg_acc_map[acckey][1].append(accinfo[1:])
        acc_info_fd.close()



        #print("sites-%d" % run)
        site_strs_fd = open(('%ssite_cxt_strs.out' % (results_dir)), 'r', \
                             encoding="ISO-8859-1")
        for line in site_strs_fd:
          if line.isspace():
            break

          addr_part, str_part = line.split()
          addr = int(addr_part,base=16)
          if not addr in site_strs:
            site_strs[addr] = str_part
        site_strs_fd.close()

        for clen in pin_cxt_lengths:
          #print("site_cxt-%d-%d" % (clen,run))

          if not clen in run_site_map:
            run_site_map[clen] = {}
          if not clen in agg_site_map:
            agg_site_map[clen] = {}
          if not clen in top_site_id:
            top_site_id[clen] = 0

          site_cxts_fd = open(('%ssite_cxt%d_cxts.out' % (results_dir, clen)), 'r')
          for line in site_cxts_fd:
            if line.isspace():
              break

            pts = line.split()
            site_id    = int(pts[0])
            site_addrs = tuple([ int(x,base=16) for x in pts[1:] ])
            run_site_map[clen][site_id] = site_addrs
          site_cxts_fd.close()


          site_info_fd = open(('%ssite_cxt%d_info.out' % (results_dir, clen)), 'r')
          for line in site_info_fd:
            if line.isspace():
              break

            sinfo = [int(x) for x in line.split()]
            skey  = run_site_map[clen][sinfo[0]]
            if not skey in agg_site_map[clen]:
              agg_site_map[clen][skey] = (top_site_id[clen], [])
              top_site_id[clen] += 1
            agg_site_map[clen][skey][1].append(sinfo[1:])
          site_info_fd.close()

        if phase_cfg:
          phase_bbvs_fd = open(('%sphase_bbvs.out' % (results_dir)), 'r')
          for line in phase_bbvs_fd:
            if line.isspace():
              break

            pts = line.split()
            phase_id  = int(pts[0])
            phase_bbv = tuple([ int(x) for x in pts[1:] ])
            run_phase_map[phase_id] = phase_bbv
            (agg_phase_id, top_alloc_phase_id) = get_agg_phase_id(phase_bbv, \
                                                 agg_bbvs, top_alloc_phase_id)
            run_agg_phase_id[run][phase_id] = agg_phase_id
          phase_bbvs_fd.close()

          alloc_phase_info_fd = open(('%salloc_phase_info.out' % (results_dir)), 'r')
          for line in alloc_phase_info_fd:
            if line.isspace():
              break

            aph_info = [int(x) for x in line.split()]
            aph_id   = run_agg_phase_id[run][aph_info[0]]
            if not aph_id in agg_alloc_phase_map:
              agg_alloc_phase_map[aph_id] = []
            agg_alloc_phase_map[aph_id].append(aph_info[1:])
          alloc_phase_info_fd.close()

          phase_sigs_fd = open(('%sphase_sigs.out' % (results_dir)), 'r')
          for line in phase_sigs_fd:
            if line.isspace():
              break

            pts = line.split()
            phase_sig_id = int(pts[0])
            phase_sig    = tuple([ run_agg_phase_id[run][int(x)] for x in pts[1:] ])
            run_phase_sig_map[phase_sig_id] = phase_sig
          phase_sigs_fd.close()

          phase_sig_info_fd = open(('%sphase_sig_info.out' % (results_dir)), 'r')
          for line in phase_sig_info_fd:
            if line.isspace():
              break


            phase_sig_info = [int(x) for x in line.split()]
            phase_sig_key  = run_phase_sig_map[phase_sig_info[0]]
            #print(phase_sig_info[0])
            #print(run_phase_sig_map[phase_sig_info[0]])
            if not phase_sig_key in agg_phase_sig_map:
              agg_phase_sig_map[phase_sig_key] = (top_phase_sig_id, [])
              top_phase_sig_id += 1
            agg_phase_sig_map[phase_sig_key][1].append(phase_sig_info[1:])
          phase_sig_info_fd.close()


          size_tsigs_fd = open(('%ssize_typesigs.out' % (results_dir)), 'r')
          for line in size_tsigs_fd:
            if line.isspace():
              break

            # these are in hex due to an error in the pintool
            pts = line.split()
            typesz_id  = int(pts[0])
            typesz_sig = tuple( ( [int(pts[1],base=16)] + \
                                  [ run_types[int(x,base=16)] for x in pts[2:] ] \
                              ) )
            run_typesz_map[typesz_id] = typesz_sig
          size_tsigs_fd.close()

          size_tsigs_info_fd = open(('%ssize_typesig_info.out' % (results_dir)), 'r')
          for line in size_tsigs_info_fd:
            if line.isspace():
              break

            tinfo = [int(x) for x in line.split()]
            tkey  = run_typesz_map[tinfo[0]]
            if not tkey in agg_typesz_map:
              agg_typesz_map[tkey] = (top_typesz_id, [])
              top_typesz_id += 1
            agg_typesz_map[tkey][1].append(tinfo[1:])
          size_tsigs_info_fd.close()


          phase_size_fd = open(('%sphase_size.out' % (results_dir)), 'r')
          for line in phase_size_fd:
            if line.isspace():
              break

            # the size is in hex due to an error in the pintool
            pts = line.split()
            phasesz_id  = int(pts[0])
            phasesz_sig = ( run_agg_phase_id[run][int(pts[1])], int(pts[2]) )
            run_phasesz_map[phasesz_id] = phasesz_sig
          phase_size_fd.close()

          phase_size_info_fd = open(('%sphase_size_info.out' % (results_dir)), 'r')
          for line in phase_size_info_fd:
            if line.isspace():
              break

            phsz_info = [int(x) for x in line.split()]
            phsz_key  = run_phasesz_map[phsz_info[0]]
            if not phsz_key in agg_phasesz_map:
              agg_phasesz_map[phsz_key] = (top_phasesz_id, [])
              top_phasesz_id += 1
            agg_phasesz_map[phsz_key][1].append(phsz_info[1:])
          phase_size_info_fd.close()


          for clen in pin_cxt_lengths:
            #print("site_cxt-%d-%d" % (clen,run))

            if not clen in run_phase_site_map:
              run_phase_site_map[clen] = {}
            if not clen in agg_phase_site_map:
              agg_phase_site_map[clen] = {}
            if not clen in top_phase_site_id:
              top_phase_site_id[clen] = 0

            phase_site_fd = open(('%sphase_site%d.out' % (results_dir, clen)), 'r')
            for line in phase_site_fd:
              if line.isspace():
                break

              pts = line.split()
              phase_site_id    = int(pts[0])
              phase_site_addrs = ( run_agg_phase_id[run][int(pts[1])],
                                   run_site_map[clen][int(pts[2])] )
              run_phase_site_map[clen][phase_site_id] = phase_site_addrs
            phase_site_fd.close()


            phase_site_info_fd = open(('%sphase_site%d_info.out' % (results_dir, clen)), 'r')
            for line in phase_site_info_fd:
              if line.isspace():
                break

              sinfo = [int(x) for x in line.split()]
              skey  = run_phase_site_map[clen][sinfo[0]]
              if not skey in agg_phase_site_map[clen]:
                agg_phase_site_map[clen][skey] = (top_phase_site_id[clen], [])
                top_phase_site_id[clen] += 1
              agg_phase_site_map[clen][skey][1].append(sinfo[1:])
            phase_site_info_fd.close()

            if not clen in run_phsz_site_map:
              run_phsz_site_map[clen] = {}
            if not clen in agg_phsz_site_map:
              agg_phsz_site_map[clen] = {}
            if not clen in top_phsz_site_id:
              top_phsz_site_id[clen] = 0

            phsz_site_fd = open(('%sphsz_site%d.out' % (results_dir, clen)), 'r')
            for line in phsz_site_fd:
              if line.isspace():
                break

              pts = line.split()
              phsz_site_id    = int(pts[0])
              phsz_site_addrs = ( run_agg_phase_id[run][int(pts[1])],
                                  int(pts[2]),
                                  run_site_map[clen][int(pts[3])] )
              run_phsz_site_map[clen][phsz_site_id] = phsz_site_addrs
            phsz_site_fd.close()


            phsz_site_info_fd = open(('%sphsz_site%d_info.out' % (results_dir, clen)), 'r')
            for line in phsz_site_info_fd:
              if line.isspace():
                break

              sinfo = [int(x) for x in line.split()]
              skey  = run_phsz_site_map[clen][sinfo[0]]
              if not skey in agg_phsz_site_map[clen]:
                agg_phsz_site_map[clen][skey] = (top_phsz_site_id[clen], [])
                top_phsz_site_id[clen] += 1
              agg_phsz_site_map[clen][skey][1].append(sinfo[1:])
            phsz_site_info_fd.close()


        if do_objects:
          #print("objects-%d" % run)
          cur_obj_fd = open(('%sobj_info.out' % results_dir), 'r')
          for line in cur_obj_fd:
            pts = line.split()

            idx = 0
            site_ids = []
            for i in pin_cxt_lengths:
              site_ids.append(agg_site_map[i][run_site_map[i][int(pts[idx])]][0])
              idx += 1

            size     = int(pts[idx])
            idx     += 1

            szbucket = int(pts[(idx)])
            idx     += 1

            type_id  = agg_type_map[run_type_map[int(pts[(idx)])]][0]
            idx     += 1

            acc_id   = agg_acc_map[run_acc_map[int(pts[(idx)])]][0]
            idx     += 1

            aph_id   = run_agg_phase_id[run][int(pts[(idx)])]
            idx     += 1

            if phase_cfg:
              phase_site_ids = []
              for i in pin_cxt_lengths:
                phase_site_ids.append(agg_phase_site_map[i][run_phase_site_map[i][int(pts[idx])]][0])
                idx += 1

              phsz_site_ids = []
              for i in pin_cxt_lengths:
                phsz_site_ids.append(agg_phsz_site_map[i][run_phsz_site_map[i][int(pts[idx])]][0])
                idx += 1

              phase_sig_id  = agg_phase_sig_map[run_phase_sig_map[int(pts[idx])]][0]
              idx          += 1

              typesz_id     = agg_typesz_map[run_typesz_map[int(pts[(idx)])]][0]
              idx          += 1

              phasesz_id    = agg_phasesz_map[run_phasesz_map[int(pts[(idx)])]][0]
              idx          += 1

            agg_pts = site_ids + [size, szbucket, type_id, acc_id, aph_id]
            if phase_cfg:
              agg_pts += phase_site_ids + phsz_site_ids + \
                         [phase_sig_id, typesz_id, phasesz_id]

            agg_pts   += [int(x) for x in pts[(idx):(idx+5)]]
            idx       += 5

            agg_pts   += [float(x) for x in pts[(idx):]]

            str_pts   = [ ("%16d"   % x) for x in agg_pts[:idx] ] + \
                        [ ("%16.4f" % x) for x in agg_pts[idx:] ]
            objline   = " ".join(str_pts)
            print (objline, file=agg_obj_fd)
          cur_obj_fd.close()

  if do_objects:
    agg_obj_fd.close()

#      except:
#        print (("error parsing run-%d."%run))
#        err = True
#        break


  if not err:
#    try: 
      results_dir = get_results_dir(bench, cfg_name)


      agg_type_strs_fd = open(('%sagg_type_strs.out' % (results_dir)), 'w')
      for type_str, type_id in type_strs.items():
        aggline = " ".join( ( [ ("%-10d" % type_id) ] + [ type_str ] ) )
        print (aggline, file=agg_type_strs_fd)
      print ("", file=agg_type_strs_fd)
      agg_type_strs_fd.close()

      agg_acc_strs_fd = open(('%sagg_acc_strs.out' % (results_dir)), 'w')
      for acc_id, acc_str in acc_strs.items():
        aggline = " ".join( ( [ ("%-16s" % hex(acc_id)) ] + [ acc_str ] ) )
        print (aggline, file=agg_acc_strs_fd)
      print ("", file=agg_acc_strs_fd)
      agg_acc_strs_fd.close()

      agg_site_strs_fd = open(('%sagg_site_cxt_strs.out' % (results_dir)), 'w')
      for addr in site_strs:
        aggline = " ".join( ( [ ("%-16s" % hex(addr)) ] + [ site_strs[addr] ] ) )
        print (aggline, file=agg_site_strs_fd)
      print ("", file=agg_site_strs_fd)
      agg_site_strs_fd.close()


      agg_type_sigs_fd = open(('%sagg_type_sigs.out' % (results_dir)), 'w')
      for tsig in agg_type_map:
        aggline = " ".join( ( [ ("%-10d" % agg_type_map[tsig][0]) ] + \
                              [ ("%10d" % tnum) for tnum in tsig ] ) )
        print (aggline, file=agg_type_sigs_fd)
      print ("", file=agg_type_sigs_fd)
      agg_type_sigs_fd.close()

      agg_acc_sigs_fd = open(('%sagg_acc_sigs.out' % (results_dir)), 'w')
      for asig in agg_acc_map:
        aggline = " ".join( ( [ ("%-10d" % agg_acc_map[asig][0]) ] + \
                              [ ("%16x" % addr) for addr in asig ] ) )
        print (aggline, file=agg_acc_sigs_fd)
      print ("", file=agg_acc_sigs_fd)
      agg_acc_sigs_fd.close()


      agg_size_info_fd  = open(('%sagg_size_info.out' % (results_dir)), 'w')
      for size in agg_size_map:
        print (get_agg_info_line(size, agg_size_map[size]), file=agg_size_info_fd)
      print ("", file=agg_size_info_fd)
      agg_size_info_fd.close()

      agg_szbucket_info_fd  = open(('%sagg_size_bucket_info.out' % (results_dir)), 'w')
      for szbucket in agg_szbucket_map: 
        print (get_agg_info_line(szbucket, agg_szbucket_map[szbucket]),
               file=agg_szbucket_info_fd)
      print ("", file=agg_szbucket_info_fd)
      agg_szbucket_info_fd.close()

      agg_type_info_fd  = open(('%sagg_type_sig_info.out' % (results_dir)), 'w')
      for tsig in agg_type_map:
        print (get_agg_info_line(agg_type_map[tsig][0], \
                                 agg_type_map[tsig][1]),
               file=agg_type_info_fd)
      print ("", file=agg_type_info_fd)
      agg_type_info_fd.close()

      agg_acc_info_fd  = open(('%sagg_acc_sig_info.out' % (results_dir)), 'w')
      for asig in agg_acc_map:
        print (get_agg_info_line(agg_acc_map[asig][0], \
                                 agg_acc_map[asig][1]),
               file=agg_acc_info_fd)
      print ("", file=agg_acc_info_fd)
      agg_acc_info_fd.close()


      for clen in pin_cxt_lengths:

        agg_cxts_fd  = open(('%sagg_site_cxt%d_cxts.out' % (results_dir,clen)), 'w')
        for site in agg_site_map[clen]:
          aggline = " ".join( ( [ ("%-10d" % agg_site_map[clen][site][0]) ] + \
                                [ ("%16s" % hex(addr)) for addr in site ] ) )
          print (aggline, file=agg_cxts_fd)
        print ("", file=agg_cxts_fd)
        agg_cxts_fd.close()

        agg_site_info_fd  = open(('%sagg_site_cxt%d_info.out' % (results_dir,clen)), 'w')
        for site in agg_site_map[clen]:
          print (get_agg_info_line( agg_site_map[clen][site][0],   \
                                    agg_site_map[clen][site][1] ), \
                 file=agg_site_info_fd )
        print ("", file=agg_site_info_fd)
        agg_site_info_fd.close()


      if phase_cfg:
        agg_phases_fd = open(('%sagg_phases.out' % (results_dir)), 'w')
        #for phase_id in agg_alloc_phase_map:
        #  aggline = " ".join( ( [ ("%-10d" % phase_id) ] + \
        #                        [ ("%16d"  % val) for val in agg_bbvs[phase_id] ] ) )
        for phase_id,phase_bbv in agg_bbvs.items():
          aggline = " ".join( ( [ ("%-10d" % phase_id) ] + \
                                [ ("%16d"  % val) for val in phase_bbv ] ) )
          print (aggline, file=agg_phases_fd)
        print ("", file=agg_phases_fd)
        agg_phases_fd.close()

        agg_phase_sigs_fd = open(('%sagg_phase_sigs.out' % (results_dir)), 'w')
        for phase_sig in agg_phase_sig_map:
          aggline = " ".join( ( [ ("%-10d" % agg_phase_sig_map[phase_sig][0]) ] + \
                                [ ("%16d"  % val) for val in phase_sig ] ) )
          print (aggline, file=agg_phase_sigs_fd)
        print ("", file=agg_phase_sigs_fd)
        agg_phase_sigs_fd.close()


        agg_typesz_fd = open(('%sagg_size_typesigs.out' % (results_dir)), 'w')
        for typesz in agg_typesz_map:
          aggline = " ".join( ( [ ("%-10d" % agg_typesz_map[typesz][0]) ] + \
                                [ ("%16d" % val) for val in typesz ] ) )
          print (aggline, file=agg_typesz_fd)
        print ("", file=agg_typesz_fd)
        agg_typesz_fd.close()

        agg_phasesz_fd = open(('%sagg_phase_size_sigs.out' % (results_dir)), 'w')
        for phasesz in agg_phasesz_map:
          aggline = " ".join( ( [ ("%-10d" % agg_phasesz_map[phasesz][0]) ] + \
                                [ ("%16d"  % phasesz[0]) ] + \
                                [ ("%16d"  % phasesz[1]) ] ) )
          print (aggline, file=agg_phasesz_fd)
        print ("", file=agg_phasesz_fd)
        agg_phasesz_fd.close()


        agg_alloc_phase_info_fd  = open(('%sagg_alloc_phase_info.out' % (results_dir)), 'w')
        for phid in agg_alloc_phase_map:
          print (get_agg_info_line(phid, agg_alloc_phase_map[phid]),
                 file=agg_alloc_phase_info_fd)
        print ("", file=agg_alloc_phase_info_fd)
        agg_alloc_phase_info_fd.close()

        agg_phase_sig_info_fd  = open(('%sagg_phase_sig_info.out' % (results_dir)), 'w')
        for phase_sig in agg_phase_sig_map:
          print (get_agg_info_line(agg_phase_sig_map[phase_sig][0], \
                                   agg_phase_sig_map[phase_sig][1]),
                 file=agg_phase_sig_info_fd)
        print ("", file=agg_phase_sig_info_fd)
        agg_phase_sig_info_fd.close()

        agg_typesz_info_fd  = open(('%sagg_typesz_info.out' % (results_dir)), 'w')
        for typesz in agg_typesz_map:
          print (get_agg_info_line(agg_typesz_map[typesz][0], \
                                   agg_typesz_map[typesz][1]),
                 file=agg_typesz_info_fd)
        print ("", file=agg_typesz_info_fd)
        agg_typesz_info_fd.close()

        agg_phasesz_info_fd  = open(('%sagg_phase_size_info.out' % (results_dir)), 'w')
        for phasesz in agg_phasesz_map:
          print (get_agg_info_line(agg_phasesz_map[phasesz][0], \
                                   agg_phasesz_map[phasesz][1]),
                 file=agg_phasesz_info_fd)
        print ("", file=agg_phasesz_info_fd)
        agg_phasesz_info_fd.close()

        for clen in pin_cxt_lengths:

          agg_phase_site_fd  = open(('%sagg_phase_site%d.out' % (results_dir,clen)), 'w')
          for phsite in agg_phase_site_map[clen]:
            aggline = " ".join( ( [ ("%-10d" % agg_phase_site_map[clen][phsite][0]) ] + \
                                  [ ("%16d"  % phsite[0]) ] + \
                                  [ ("%16s"  % hex(addr)) for addr in phsite[1] ] ) )
            print (aggline, file=agg_phase_site_fd)
          print ("", file=agg_phase_site_fd)
          agg_phase_site_fd.close()

          agg_phase_site_info_fd  = open(('%sagg_phase_site%d_info.out' % (results_dir,clen)), 'w')
          for phsite in agg_phase_site_map[clen]:
            print (get_agg_info_line( agg_phase_site_map[clen][phsite][0],   \
                                      agg_phase_site_map[clen][phsite][1] ), \
                   file=agg_phase_site_info_fd )
          print ("", file=agg_phase_site_info_fd)
          agg_phase_site_info_fd.close()


          agg_phsz_site_fd  = open(('%sagg_phsz_site%d.out' % (results_dir,clen)), 'w')
          for phsite in agg_phsz_site_map[clen]:
            aggline = " ".join( ( [ ("%-10d" % agg_phsz_site_map[clen][phsite][0]) ] + \
                                  [ ("%16d"  % phsite[0]) ] + [ ("%16d" % phsite[1]) ] + \
                                  [ ("%16d"  % val) for val in phsite[2] ] ) )
            print (aggline, file=agg_phsz_site_fd)
          print ("", file=agg_phsz_site_fd)
          agg_phsz_site_fd.close()

          agg_phsz_site_info_fd  = open(('%sagg_phsz_site%d_info.out' % (results_dir,clen)), 'w')
          for phsite in agg_phsz_site_map[clen]:
            print (get_agg_info_line( agg_phsz_site_map[clen][phsite][0],   \
                                      agg_phsz_site_map[clen][phsite][1] ), \
                   file=agg_phsz_site_info_fd )
          print ("", file=agg_phsz_site_info_fd)
          agg_phsz_site_info_fd.close()

      print ("ok.")

#    except:
#      print ("error printing agg files.")


def get_cid2uid(bench, cfg_name, sigsfile, strsfile=None,
  sigbase=16, strbase=16, startidx=1):

  cid2uid = {}

  results_dir = get_results_dir(bench, cfg_name)
  if strsfile:
    sig2ustr = {}
    strs_fd = open(('%s%s' % (results_dir, strsfile)), 'r')
    for line in strs_fd:
      if line.isspace():
        break
      pts  = line.split()
      sig  = int(pts[0],base=strbase)
      ustr = " ".join(pts[1:])
      sig2ustr[sig] = ustr
    strs_fd.close()

  sigs_fd = open(('%s%s' % (results_dir, sigsfile)), 'r')
  for line in sigs_fd:
    if line.isspace():
      break
    pts  = line.split()
    cid  = int(pts[0])

    idx  = 1
    uid  = []
    while idx < startidx:
      uid.append(int(pts[idx]))
      idx += 1

    if strsfile:
      uid += [ sig2ustr[int(x,base=sigbase)] for x in pts[startidx:] ]
    else:
      uid += [ int(x,base=sigbase) for x in pts[startidx:] ]

    cid2uid[cid] = tuple(uid)
  sigs_fd.close()

  return cid2uid

def get_uid2cid(bench, cfg_name, sigsfile, strsfile=None,
  sigbase=16, strbase=16, startidx=1):

  uid2cid = {}

  results_dir = get_results_dir(bench, cfg_name)
  if strsfile:
    sig2ustr = {}
    strs_fd = open(('%s%s' % (results_dir, strsfile)), 'r')
    for line in strs_fd:
      if line.isspace():
        break
      pts  = line.split()
      sig  = int(pts[0],base=strbase)
      ustr = " ".join(pts[1:])
      sig2ustr[sig] = ustr
    strs_fd.close()

  sigs_fd  = open(('%s%s' % (results_dir, sigsfile)), 'r')
  for line in sigs_fd:
    if line.isspace():
      break
    pts  = line.split()
    cid  = int(pts[0])

    idx  = 1
    uid  = []
    while idx < startidx:
      uid.append(int(pts[idx]))
      idx += 1

    if strsfile:
      for x in pts[startidx:]:
        if not int(x,base=sigbase) in sig2ustr:
          print(x)
      uid += [ sig2ustr[int(x,base=sigbase)] for x in pts[startidx:] ]
    else:
      uid += [ int(x,base=sigbase) for x in pts[startidx:] ]

    uid2cid[tuple(uid)] = cid
  sigs_fd.close()

  return uid2cid

def create_train2ref_maps(benches, ref_cfg, train_cfg):
  for bench in benches:
    make_train2ref_map(bench, ref_cfg, train_cfg)

def get_train2ref_map(bench, ref_cfg, train_cfg):
  results_dir  = get_results_dir(bench, ref_cfg)
  t2r_pkl_file = ('%strain2ref_%s.pkl' % (results_dir, train_cfg))

  if not os.path.isfile(t2r_pkl_file):
    print ("making train2ref: %s --> %s ... " % (train_cfg, ref_cfg),end='')
    make_train2ref_map(bench, ref_cfg, train_cfg)
    print ("done.")

  t2r_pkl_fd  = open(t2r_pkl_file, 'rb')
  train2ref   = pickle.load(t2r_pkl_fd)
  t2r_pkl_fd.close()

  return train2ref

def get_ref2train_map(bench, ref_cfg, train_cfg):
  results_dir  = get_results_dir(bench, ref_cfg)
  r2t_pkl_file = ('%sref2train_%s.pkl' % (results_dir, train_cfg))

  if not os.path.isfile(r2t_pkl_file):
    print ("making ref2train: %s --> %s ... " % (train_cfg, ref_cfg),end='')
    make_train2ref_map(bench, ref_cfg, train_cfg)
    print ("done.")

  r2t_pkl_fd  = open(r2t_pkl_file, 'rb')
  ref2train   = pickle.load(r2t_pkl_fd)
  r2t_pkl_fd.close()

  return ref2train

def make_train2ref_map(bench, ref_cfg, train_cfg):

  train2ref = {}
  ref2train = {}

  train_otdict = get_object_cut_info(bench, train_cfg)

  train2ref[SIZE] = {}
  ref2train[SIZE] = {}
  for key in train_otdict[ALL][SIZE]:
    train2ref[SIZE][key] = key
    ref2train[SIZE][key] = key

  train2ref[SIZE_BUCKET] = {}
  ref2train[SIZE_BUCKET] = {}
  for key in train_otdict[ALL][SIZE_BUCKET]:
    train2ref[SIZE_BUCKET][key] = key
    ref2train[SIZE_BUCKET][key] = key

  train2ref[TYPE_SIG] = {}
  ref2train[TYPE_SIG] = {}
  tsig2rid = get_uid2cid(bench, ref_cfg, 'agg_type_sigs.out',
                         'agg_type_strs.out', sigbase=10, strbase=10)
  tid2tsig = get_cid2uid(bench, train_cfg, 'agg_type_sigs.out',
                         'agg_type_strs.out', sigbase=10, strbase=10)
  for tid,tsig in tid2tsig.items():
    if tsig in tsig2rid:
      train2ref[TYPE_SIG][tid] = tsig2rid[tsig]
      ref2train[TYPE_SIG][tsig2rid[tsig]] = tid


  train2ref[ACC_SIG] = {}
  ref2train[ACC_SIG] = {}
  asig2rid = get_uid2cid(bench, ref_cfg, 'agg_acc_sigs.out', sigbase=16)
  tid2asig = get_cid2uid(bench, train_cfg, 'agg_acc_sigs.out', sigbase=16)
  for tid,asig in tid2asig.items():
    if asig in asig2rid:
      train2ref[ACC_SIG][tid] = asig2rid[asig]
      ref2train[ACC_SIG][asig2rid[asig]] = tid


  train2ref[TYPE_SIZE] = {}
  ref2train[TYPE_SIZE] = {}
  tsz2rid = get_uid2cid(bench, ref_cfg, 'agg_size_typesigs.out',
                        'agg_type_strs.out', sigbase=10, strbase=10, startidx=2)
  tid2tsz = get_cid2uid(bench, train_cfg, 'agg_size_typesigs.out',
                         'agg_type_strs.out', sigbase=10, strbase=10, startidx=2)
  for tid,tsz in tid2tsz.items():
    if tsz in tsz2rid:
      train2ref[TYPE_SIZE][tid] = tsz2rid[tsz]
      ref2train[TYPE_SIZE][tsz2rid[tsz]] = tid

  # For the phase-based features, the ref phases map to the set of phases in
  # the train run that are closer than any other phase
  #
  # There can only be one train phase for each ref phase. The algorithm maps
  # from ref --> train
  #
  phase_ref2train = {}
  phase_train2ref = {}
  rid2phase = get_cid2uid(bench, ref_cfg, 'agg_phases.out',
                          sigbase=10)
  tid2phase = get_cid2uid(bench, train_cfg, 'agg_phases.out',
                          sigbase=10)
  for rid,bbv in rid2phase.items():
    tid = get_closest_phase(bbv, tid2phase)
    phase_ref2train[rid] = tid

    if not tid in phase_train2ref:
      phase_train2ref[tid] = set([])
    phase_train2ref[tid].add(rid)

#  for tid,bbv in tid2phase.items():
#    rid = get_closest_phase(bbv, rid2phase)
#
#    if not tid in phase_train2ref:
#      phase_train2ref[tid] = set([])
#    phase_train2ref[tid].add(rid)


#  print("phase_ref2train")
#  for rid in phase_ref2train:
#    print ("%d --> "%rid, end='')
#    print (phase_ref2train[rid])
#
#  print("phase_train2ref")
#  for tid in phase_train2ref:
#    print ("%d --> "%tid, end='')
#    print (phase_train2ref[tid])

  train2ref[ALLOC_PHASE] = {}
  ref2train[ALLOC_PHASE] = {}
  for tid in tid2phase:
#    print(tid)
    if tid in phase_train2ref:
      train2ref[ALLOC_PHASE][tid] = phase_train2ref[tid]
      for rid in phase_train2ref[tid]:
        ref2train[ALLOC_PHASE][rid] = tid
#        print("r2t: %d --> %d" % (rid, tid))


#  for tid in phase_train2ref:
#    print ("%d --> " % tid, end='')
#    print (phase_train2ref[tid])


  train2ref[PHASE_SIZE] = {}
  ref2train[PHASE_SIZE] = {}
  phsz2rid = get_uid2cid(bench, ref_cfg, 'agg_phase_size_sigs.out',
                         sigbase=10)
  tid2phsz = get_cid2uid(bench, train_cfg, 'agg_phase_size_sigs.out',
                         sigbase=10)
  for tid,phsz in tid2phsz.items():
    #ref_phsz = ( phase_train2ref[phsz[0]], phsz[1] )
    #ref_phszs = [ (x, phsz[1]) for x in phase_train2ref[phsz[0]] ]
    if phsz[0] in phase_train2ref:
      ref_phszs = [ (x, phsz[1]) for x in phase_train2ref[phsz[0]] ]
      rids = []
      for ref_phsz in ref_phszs:
        if ref_phsz in phsz2rid:
          rids.append(phsz2rid[ref_phsz])

      train2ref[PHASE_SIZE][tid] = set(rids)
      for rid in rids:
        ref2train[PHASE_SIZE][rid] = tid

#  for phsz in phsz2rid:
#    print (phsz,end='')
#    print (" --> %d" % phsz2rid[phsz])
#
#  for tid in train2ref[PHASE_SIZE]:
#    print ("%d --> " % tid, end='')
#    print (train2ref[PHASE_SIZE][tid])


  train2ref[PHASE_SIG] = {}
  ref2train[PHASE_SIG] = {}
  psig2rid = get_uid2cid(bench, ref_cfg, 'agg_phase_sigs.out',
                         sigbase=10)
  tid2psig = get_cid2uid(bench, train_cfg, 'agg_phase_sigs.out',
                         sigbase=10)

  ref_psigs = []
  for tid,psig in tid2psig.items():
    ref_psigs.append( ( tid, [ y for y in \
      itertools.chain.from_iterable([ phase_train2ref[x] for x in psig \
                                      if x in phase_train2ref ])] ) )

  candidates = [ (t, set(y)) for l,t,y in \
                  sorted([ (len(x), tid, x) for tid,x in ref_psigs ]) ]

#  for t,c in candidates:
#    print ("%d --> " % t, end='')
#    print (c)
#
#  for ref_psig in psig2rid:
#    print (ref_psig, end='')
#    print (" --> %d" % psig2rid[ref_psig])

  for ref_psig in psig2rid:
    ref_pset = set(ref_psig)
    for tid,c in candidates:
      if ref_pset.issubset(c):
        if not tid in train2ref[PHASE_SIG]:
          train2ref[PHASE_SIG][tid] = set([])
        train2ref[PHASE_SIG][tid].add(psig2rid[ref_psig])
        ref2train[PHASE_SIG][psig2rid[ref_psig]] = tid
        break

#  for tid in train2ref[PHASE_SIG]:
#    print("%d --> " % tid, end='')
#    print(train2ref[PHASE_SIG][tid])
    #ref_psigs  = set([ frozenset(x) for x in list(itertools.product(*candidates)) ])

#    rids = []
#    for ref_psig in ref_psigs:
#      if ref_psig in psig2rid:
#        rids.append(psig2rid[ref_psig])
#    train2ref[PHASE_SIG][tid] = set(rids)



  train2ref[SITES] = {}
  ref2train[SITES] = {}
  for clen in pin_cxt_lengths:
    train2ref[SITES][clen] = {}
    ref2train[SITES][clen] = {}

#    site2rid = get_uid2cid(bench, ref_cfg, ('agg_site_cxt%d_cxts.out'%clen),
#                           'agg_site_cxt_strs.out', sigbase=16)
#    tid2site = get_cid2uid(bench, train_cfg, ('agg_site_cxt%d_cxts.out'%clen),
#                           'agg_site_cxt_strs.out', sigbase=16)
#
    site2rid = get_uid2cid(bench, ref_cfg, ('agg_site_cxt%d_cxts.out'%clen),
                           sigbase=16)
    tid2site = get_cid2uid(bench, train_cfg, ('agg_site_cxt%d_cxts.out'%clen),
                           sigbase=16)

    for tid,site in tid2site.items():
      if site in site2rid:
        train2ref[SITES][clen][tid] = site2rid[site]
        ref2train[SITES][clen][site2rid[site]] = tid
 
#    if clen == 0:
#      print ("ref2train")
#      matched_rkeys = train2ref[SITES][clen].values()
#      cnt = 0
#      for site,rid in site2rid.items():
#        if rid in matched_rkeys:
#          print ("mat: %d --> %d" % (rid, mjtmp[rid]),end='')
#          print (site)
#          cnt += 1
#        else:
#          print ("unm: %d --> %d" % (rid, mjtmp[rid]),end='')
#          print (site)
#      print("%d matches" % cnt)
#
#      print ("train2ref")
#      cnt = 0
#      for tid,site in tid2site.items():
#        if site in site2rid:
#          print ("mat: %d --> %d" % (tid, train2ref[SITES][clen][tid]),end='')
#          print (site)
#          cnt += 1
#        else:
#          print ("unm: %d --> %d" % (tid, train2ref[SITES][clen][tid]),end='')
#          print (site)
#      print("%d matches, %d keys" % (cnt, len(train2ref[SITES][clen].keys())))


  train2ref[PHASE_SITES] = {}
  ref2train[PHASE_SITES] = {}
  for clen in pin_cxt_lengths:

    train2ref[PHASE_SITES][clen] = {}
    ref2train[PHASE_SITES][clen] = {}
    phsite2rid = get_uid2cid(bench, ref_cfg, ('agg_phase_site%d.out'%clen),
                             sigbase=16, startidx=2)
    tid2phsite = get_cid2uid(bench, train_cfg, ('agg_phase_site%d.out'%clen),
                             sigbase=16, startidx=2)
    for tid,phsite in tid2phsite.items():
      #ref_phsite = tuple( [ phase_train2ref[phsite[0]] ] + list(phsite[1:]) )
      if phsite[0] in phase_train2ref:
        ref_phsites = [ tuple([x] + list(phsite[1:])) \
                        for x in phase_train2ref[phsite[0]] ]

        rids = []
        for ref_phsite in ref_phsites:
          if ref_phsite in phsite2rid:
            rids.append(phsite2rid[ref_phsite])
        train2ref[PHASE_SITES][clen][tid] = set(rids)
        for rid in rids:
          ref2train[PHASE_SITES][clen][rid] = tid


  train2ref[PHSZ_SITES] = {}
  ref2train[PHSZ_SITES] = {}
  for clen in pin_cxt_lengths:

    train2ref[PHSZ_SITES][clen] = {}
    ref2train[PHSZ_SITES][clen] = {}
    phszst2rid = get_uid2cid(bench, ref_cfg, ('agg_phsz_site%d.out'%clen),
                             sigbase=10, startidx=3)
    tid2phszst = get_cid2uid(bench, train_cfg, ('agg_phsz_site%d.out'%clen),
                             sigbase=10, startidx=3)
    for tid,phszst in tid2phszst.items():
      #ref_phszst = tuple( [ phase_train2ref[phszst[0]] ] + [phszst[1]] + list(phszst[2:]) )
      if phszst[0] in phase_train2ref:
        ref_phszsts = [ tuple([x] + [phszst[1]] + list(phszst[2:])) \
                        for x in phase_train2ref[phszst[0]] \
                      ]

        rids = []
        for ref_phszst in ref_phszsts:
          if ref_phszst in phszst2rid:
            rids.append(phszst2rid[ref_phszst])
        train2ref[PHSZ_SITES][clen][tid] = set(rids)
        for rid in rids:
          ref2train[PHSZ_SITES][clen][rid] = tid

  print("pickling train2ref %-24s ... " % bench, end='')
  results_dir = get_results_dir(bench, ref_cfg)
  t2r_pkl_fd = open(('%strain2ref_%s.pkl' % (results_dir, train_cfg)), 'wb')
  pickle.dump(train2ref, t2r_pkl_fd)
  t2r_pkl_fd.close()
  print("done.")

  print("pickling ref2train %-24s ... " % bench, end='')
  results_dir = get_results_dir(bench, ref_cfg)
  r2t_pkl_fd = open(('%sref2train_%s.pkl' % (results_dir, train_cfg)), 'wb')
  pickle.dump(ref2train, r2t_pkl_fd)
  r2t_pkl_fd.close()
  print("done.")
  
  return train2ref

def make_otdict_pickle(bench, cfg_name, tsizes=def_tsizes, \
  cutstyle=THERMOS):

  otdict = {}

  otzstrs = [cut_str(cutstyle, tz) for tz in tsizes]
  for tzstr in ( [ ALL ] + otzstrs ):
    otdict[tzstr] = get_object_tzstr_info(bench, cfg_name, cutstyle, tzstr)

    results_dir = get_results_dir(bench, cfg_name)
    cmd = ("rm -f %s" % ('%s.object_%s-%s.pkl' % (results_dir, cutstyle.lower(), tzstr)))
    print(cmd)

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sobject_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(otdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

def get_otdict_object(objlist, objidxs):
  if objidxs == None:
    for obj in objlist:
      yield obj
  else:
    for idx in objidxs:
      yield objlist[idx]

def get_otdict(bench, cfg_name, objlist, phase_cfg, foff,
  cutstyle=None, tzstr=None, objidxs=None, lowmem=False):

  otdict = {}
  otdict[AGG]         = [ 0 for _ in range(10) ]
  otdict[SIZE]        = {}
  otdict[SIZE_BUCKET] = {}
  otdict[TYPE_SIG]    = {}
  otdict[ACC_SIG]     = {}
  otdict[SITES]       = {}
  for cxt in pin_cxt_lengths:
    otdict[SITES][cxt] = {}

  if phase_cfg:
    otdict[ALLOC_PHASE] = {}
    otdict[TYPE_SIZE]   = {}
    otdict[PHASE_SIZE]  = {}
    otdict[PHASE_SIG]   = {}
    otdict[PHASE_SITES] = {}
    otdict[PHSZ_SITES]  = {}
    for cxt in pin_cxt_lengths:
      otdict[PHASE_SITES][cxt] = {}
      otdict[PHSZ_SITES][cxt]  = {}

  otdict[LIFETIME] = get_ins_count(bench, cfg_name)

  for k,obj in enumerate(get_otdict_object(objlist,objidxs)):
    clsize      = obj[(lines_idx    + foff)]
    pc_accs     = obj[(pcaccs_idx   + foff)]
    mc_accs     = obj[(mcaccs_idx   + foff)]
    bw          = obj[(bw_idx       + foff)]
    lifetime    = obj[(lifetime_idx + foff)]
    pc_hit_rate = obj[(pchits_idx   + foff)]
    mc_hit_rate = obj[(mchits_idx   + foff)]
    pre_rw      = obj[(prerw_idx    + foff)]
    post_rw     = obj[(postrw_idx   + foff)]

    pre_wrs     = 0 if pre_rw < -0.01 else int((pc_accs / (pre_rw+1)))
    pre_rds     = int((pc_accs - pre_wrs))
    post_wrs    = 0 if post_rw < -0.01 else int((bw / (post_rw+1)))
    post_rds    = int((bw - post_wrs))

    pc_hit_rate = None if pc_hit_rate < -0.01 else pc_hit_rate
    mc_hit_rate = None if mc_hit_rate < -0.01 else mc_hit_rate

    pc_hits = mc_hits = 0.0
    if pc_hit_rate != None:
      pc_hits = (pc_accs * (pc_hit_rate))
    if mc_hit_rate != None:
      mc_hits = (mc_accs * (mc_hit_rate))

    otdict[AGG][0] += 1
    otdict[AGG][1] += clsize
    otdict[AGG][2] += pre_rds
    otdict[AGG][3] += pre_wrs
    otdict[AGG][4] += post_rds
    otdict[AGG][5] += post_wrs
    otdict[AGG][6] += pc_hits
    otdict[AGG][7] += mc_accs
    otdict[AGG][8] += mc_hits
    otdict[AGG][9] += lifetime

    for i,cxt in enumerate(pin_cxt_lengths):
      site = obj[i]
      if not site in otdict[SITES][cxt]:
        otdict[SITES][cxt][site] = [ 0 for _ in range(10) ] + [[]]

      otdict[SITES][cxt][site][0] += 1
      otdict[SITES][cxt][site][1] += clsize
      otdict[SITES][cxt][site][2] += pre_rds
      otdict[SITES][cxt][site][3] += pre_wrs
      otdict[SITES][cxt][site][4] += post_rds
      otdict[SITES][cxt][site][5] += post_wrs
      otdict[SITES][cxt][site][6] += pc_hits
      otdict[SITES][cxt][site][7] += mc_accs
      otdict[SITES][cxt][site][8] += mc_hits
      otdict[SITES][cxt][site][9] += lifetime
      otdict[SITES][cxt][site][10].append(clsize)

    size = obj[size_idx]
    if not size in otdict[SIZE]:
      otdict[SIZE][size] = [ 0 for _ in range(10) ] + [[]]
    otdict[SIZE][size][0] += 1
    otdict[SIZE][size][1] += clsize
    otdict[SIZE][size][2] += pre_rds
    otdict[SIZE][size][3] += pre_wrs
    otdict[SIZE][size][4] += post_rds
    otdict[SIZE][size][5] += post_wrs
    otdict[SIZE][size][6] += pc_hits
    otdict[SIZE][size][7] += mc_accs
    otdict[SIZE][size][8] += mc_hits
    otdict[SIZE][size][9] += lifetime
    otdict[SIZE][size][10].append(clsize)

    szbucket = obj[szbucket_idx]
    if not szbucket in otdict[SIZE_BUCKET]:
      otdict[SIZE_BUCKET][szbucket] = [ 0 for _ in range(10) ] + [[]]
    otdict[SIZE_BUCKET][szbucket][0] += 1
    otdict[SIZE_BUCKET][szbucket][1] += clsize
    otdict[SIZE_BUCKET][szbucket][2] += pre_rds
    otdict[SIZE_BUCKET][szbucket][3] += pre_wrs
    otdict[SIZE_BUCKET][szbucket][4] += post_rds
    otdict[SIZE_BUCKET][szbucket][5] += post_wrs
    otdict[SIZE_BUCKET][szbucket][6] += pc_hits
    otdict[SIZE_BUCKET][szbucket][7] += mc_accs
    otdict[SIZE_BUCKET][szbucket][8] += mc_hits
    otdict[SIZE_BUCKET][szbucket][9] += lifetime
    otdict[SIZE_BUCKET][szbucket][10].append(clsize)

    tsig = obj[tsig_idx]
    if not tsig in otdict[TYPE_SIG]:
      otdict[TYPE_SIG][tsig] = [ 0 for _ in range(10) ] + [[]]
    otdict[TYPE_SIG][tsig][0] += 1
    otdict[TYPE_SIG][tsig][1] += clsize
    otdict[TYPE_SIG][tsig][2] += pre_rds
    otdict[TYPE_SIG][tsig][3] += pre_wrs
    otdict[TYPE_SIG][tsig][4] += post_rds
    otdict[TYPE_SIG][tsig][5] += post_wrs
    otdict[TYPE_SIG][tsig][6] += pc_hits
    otdict[TYPE_SIG][tsig][7] += mc_accs
    otdict[TYPE_SIG][tsig][8] += mc_hits
    otdict[TYPE_SIG][tsig][9] += lifetime
    otdict[TYPE_SIG][tsig][10].append(clsize)

    asig = obj[asig_idx]
    if not asig in otdict[ACC_SIG]:
      otdict[ACC_SIG][asig] = [ 0 for _ in range(10) ] + [[]]
    otdict[ACC_SIG][asig][0] += 1
    otdict[ACC_SIG][asig][1] += clsize
    otdict[ACC_SIG][asig][2] += pre_rds
    otdict[ACC_SIG][asig][3] += pre_wrs
    otdict[ACC_SIG][asig][4] += post_rds
    otdict[ACC_SIG][asig][5] += post_wrs
    otdict[ACC_SIG][asig][6] += pc_hits
    otdict[ACC_SIG][asig][7] += mc_accs
    otdict[ACC_SIG][asig][8] += mc_hits
    otdict[ACC_SIG][asig][9] += lifetime
    otdict[ACC_SIG][asig][10].append(clsize)

    if phase_cfg:

      aph = obj[aph_idx]
      if not aph in otdict[ALLOC_PHASE]:
        otdict[ALLOC_PHASE][aph] = [ 0 for _ in range(10) ] + [[]]
      otdict[ALLOC_PHASE][aph][0] += 1
      otdict[ALLOC_PHASE][aph][1] += clsize
      otdict[ALLOC_PHASE][aph][2] += pre_rds
      otdict[ALLOC_PHASE][aph][3] += pre_wrs
      otdict[ALLOC_PHASE][aph][4] += post_rds
      otdict[ALLOC_PHASE][aph][5] += post_wrs
      otdict[ALLOC_PHASE][aph][6] += pc_hits
      otdict[ALLOC_PHASE][aph][7] += mc_accs
      otdict[ALLOC_PHASE][aph][8] += mc_hits
      otdict[ALLOC_PHASE][aph][9] += lifetime
      otdict[ALLOC_PHASE][aph][10].append(clsize)

      for i,cxt in enumerate(pin_cxt_lengths):
        phsite = obj[(phsite_idx+i)]
        if not phsite in otdict[PHASE_SITES][cxt]:
          otdict[PHASE_SITES][cxt][phsite] = [ 0 for _ in range(10) ] + [[]]

        otdict[PHASE_SITES][cxt][phsite][0] += 1
        otdict[PHASE_SITES][cxt][phsite][1] += clsize
        otdict[PHASE_SITES][cxt][phsite][2] += pre_rds
        otdict[PHASE_SITES][cxt][phsite][3] += pre_wrs
        otdict[PHASE_SITES][cxt][phsite][4] += post_rds
        otdict[PHASE_SITES][cxt][phsite][5] += post_wrs
        otdict[PHASE_SITES][cxt][phsite][6] += pc_hits
        otdict[PHASE_SITES][cxt][phsite][7] += mc_accs
        otdict[PHASE_SITES][cxt][phsite][8] += mc_hits
        otdict[PHASE_SITES][cxt][phsite][9] += lifetime
        otdict[PHASE_SITES][cxt][phsite][10].append(clsize)

      for i,cxt in enumerate(pin_cxt_lengths):
        phszst = obj[(phszst_idx+i)]
        if not phszst in otdict[PHSZ_SITES][cxt]:
          otdict[PHSZ_SITES][cxt][phszst] = [ 0 for _ in range(10) ] + [[]]

        otdict[PHSZ_SITES][cxt][phszst][0] += 1
        otdict[PHSZ_SITES][cxt][phszst][1] += clsize
        otdict[PHSZ_SITES][cxt][phszst][2] += pre_rds
        otdict[PHSZ_SITES][cxt][phszst][3] += pre_wrs
        otdict[PHSZ_SITES][cxt][phszst][4] += post_rds
        otdict[PHSZ_SITES][cxt][phszst][5] += post_wrs
        otdict[PHSZ_SITES][cxt][phszst][6] += pc_hits
        otdict[PHSZ_SITES][cxt][phszst][7] += mc_accs
        otdict[PHSZ_SITES][cxt][phszst][8] += mc_hits
        otdict[PHSZ_SITES][cxt][phszst][9] += lifetime
        otdict[PHSZ_SITES][cxt][phszst][10].append(clsize)

      phsig = obj[phsig_idx]
      if not phsig in otdict[PHASE_SIG]:
        otdict[PHASE_SIG][phsig] = [ 0 for _ in range(10) ] + [[]]
      otdict[PHASE_SIG][phsig][0] += 1
      otdict[PHASE_SIG][phsig][1] += clsize
      otdict[PHASE_SIG][phsig][2] += pre_rds
      otdict[PHASE_SIG][phsig][3] += pre_wrs
      otdict[PHASE_SIG][phsig][4] += post_rds
      otdict[PHASE_SIG][phsig][5] += post_wrs
      otdict[PHASE_SIG][phsig][6] += pc_hits
      otdict[PHASE_SIG][phsig][7] += mc_accs
      otdict[PHASE_SIG][phsig][8] += mc_hits
      otdict[PHASE_SIG][phsig][9] += lifetime
      otdict[PHASE_SIG][phsig][10].append(clsize)

      typesz = obj[typesz_idx]
      if not typesz in otdict[TYPE_SIZE]:
        otdict[TYPE_SIZE][typesz] = [ 0 for _ in range(10) ] + [[]]
      otdict[TYPE_SIZE][typesz][0] += 1
      otdict[TYPE_SIZE][typesz][1] += clsize
      otdict[TYPE_SIZE][typesz][2] += pre_rds
      otdict[TYPE_SIZE][typesz][3] += pre_wrs
      otdict[TYPE_SIZE][typesz][4] += post_rds
      otdict[TYPE_SIZE][typesz][5] += post_wrs
      otdict[TYPE_SIZE][typesz][6] += pc_hits
      otdict[TYPE_SIZE][typesz][7] += mc_accs
      otdict[TYPE_SIZE][typesz][8] += mc_hits
      otdict[TYPE_SIZE][typesz][9] += lifetime
      otdict[TYPE_SIZE][typesz][10].append(clsize)

      phsz = obj[phsz_idx]
      if not phsz in otdict[PHASE_SIZE]:
        otdict[PHASE_SIZE][phsz] = [ 0 for _ in range(10) ] + [[]]
      otdict[PHASE_SIZE][phsz][0] += 1
      otdict[PHASE_SIZE][phsz][1] += clsize
      otdict[PHASE_SIZE][phsz][2] += pre_rds
      otdict[PHASE_SIZE][phsz][3] += pre_wrs
      otdict[PHASE_SIZE][phsz][4] += post_rds
      otdict[PHASE_SIZE][phsz][5] += post_wrs
      otdict[PHASE_SIZE][phsz][6] += pc_hits
      otdict[PHASE_SIZE][phsz][7] += mc_accs
      otdict[PHASE_SIZE][phsz][8] += mc_hits
      otdict[PHASE_SIZE][phsz][9] += lifetime
      otdict[PHASE_SIZE][phsz][10].append(clsize)

  if lowmem:
    print("pickling ... ", end='')
    results_dir = get_results_dir(bench, cfg_name)
    cut_pkl_fd = open(('%s.object_%s-%s.pkl' % (results_dir, cutstyle.lower(), tzstr)), 'wb')
    pickle.dump(otdict, cut_pkl_fd)
    cut_pkl_fd.close()
    otdict = None
    print("done.")

  return otdict

def create_cut_infos(benches, configs, tsizes=def_tsizes, \
  object_cut=THERMOS, feature_cut=THERMOS, train_cfg=None, \
  do_objects=True, do_features=True, bwfilter=True, lowmem=False, \
  train_miss_style=OPT_OUT):

  for bench in benches:
    for cfg_name in configs:
      if do_objects:
        create_object_cut_info(bench, cfg_name, tsizes=tsizes, \
                               cutstyle=object_cut, bwfilter=bwfilter,\
                               lowmem=lowmem)
      if do_features:
        create_feature_cut_info(bench, cfg_name, train_cfg=train_cfg, \
                                tsizes=tsizes, cutstyle=feature_cut, \
                                train_miss_style=train_miss_style)

def create_object_cut_info(bench, cfg_name, tsizes=def_tsizes,
  cutstyle=THERMOS, bwfilter=True, lowmem=False):

  otdict = {}
  object_thermi = []

  otzstrs = [cut_str(cutstyle, tz) for tz in tsizes]

  phase_cfg = False
  if cfg_name == 'pin_lpc_9cxt':
    foff  = (asig_idx + 1)
  elif cfg_name in phase_cfgs:
    foff  = (phsz_idx + 1)
    phase_cfg = True

  print("%s-%s" % (bench, cfg_name))
  objlist = get_object_list(bench, cfg_name)
  print("  got objects.")

  # remove objects with no bandwidth
  #
  # some objects have size, but no bandwidth -- which can easily happen
  # because of the way we profile (the objects in the cache by the time we
  # start recording their accesses)
  #
  if bwfilter:
    if 'mpc' in cfg_name:
      objlist = [ x for x in objlist if x[(mcaccs_idx+foff)] != 0 ]
    else:
      objlist = [ x for x in objlist if x[(bw_idx+foff)] != 0 ]

  cutfn = None
  if cutstyle == THERMOS:
    cutfn = object_thermos
  elif cutstyle == BANDWIDTH_PER_BYTE:
    cutfn = object_bwpb_cut
  elif cutstyle == WRITES_PER_BYTE:
    cutfn = object_wrpb_cut
  elif cutstyle == RDWR_RATIO:
    cutfn = object_rdwr_cut
  elif cutstyle == LIFETIME:
    cutfn = object_lifetime_cut
  elif cutstyle == LIFETIME_PCT:
    cutfn = object_lifetime_pct_cut
 
  print("%s-%s" % (bench, cfg_name))
  print("  doing object %s cut:" % cutstyle.lower())
  for tz,tzstr in zip(tsizes,otzstrs):
    print(("    %8.4f ... " % tz), end='')
    object_thermi.append( (tzstr, cutfn(objlist, tz, foff, \
                           get_ins_count(bench, cfg_name) )) )
    print("done.")

  print ("  object dict ... ", end='')
  otdict[ALL] = get_otdict(bench, cfg_name, objlist, phase_cfg, foff,
                           cutstyle=cutstyle, tzstr=ALL, lowmem=lowmem)
  print ("done.")

  for tzstr,thermos in object_thermi:
    print (("  %24s ... " % tzstr), end='')
    otdict[tzstr] = get_otdict(bench, cfg_name, objlist, phase_cfg, foff,
                               cutstyle=cutstyle, objidxs=thermos, tzstr=tzstr,
                               lowmem=lowmem)
    print ("done.")

  objlist = None
  if lowmem:
    for tzstr in ( [ ALL ] + otzstrs ):
      otdict[tzstr] = get_object_tzstr_info(bench, cfg_name, cutstyle, tzstr)

      results_dir = get_results_dir(bench, cfg_name)
      cmd = ("rm -f %s" % ('%s.object_%s-%s.pkl' % (results_dir, cutstyle.lower(), tzstr)))
      print(cmd)

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sobject_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(otdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

def get_ftobj_info(otdict, feature, cxt=None):
  ftobj_info = {}
  if feature == BENCH:
    ftobj_info[AGG] = {}
    ftobj_info[AGG][TOUCHED]      = otdict[ALL][AGG][0]
    ftobj_info[AGG][LINE_RSS]     = otdict[ALL][AGG][1]
    ftobj_info[AGG][PRE_READS]    = otdict[ALL][AGG][2]
    ftobj_info[AGG][PRE_WRITES]   = otdict[ALL][AGG][3]
    ftobj_info[AGG][POST_READS]   = otdict[ALL][AGG][4]
    ftobj_info[AGG][POST_WRITES]  = otdict[ALL][AGG][5]
    ftobj_info[AGG][PC_HITS]      = otdict[ALL][AGG][6]
    ftobj_info[AGG][MC_ACCS]      = otdict[ALL][AGG][7]
    ftobj_info[AGG][MC_HITS]      = otdict[ALL][AGG][8]
    ftobj_info[AGG][LIFETIME]     = otdict[ALL][AGG][9]
  elif feature in scalar_features:
    for ftkey in otdict[ALL][feature]:
      ftobj_info[ftkey] = {}
      ftobj_info[ftkey][TOUCHED]      = otdict[ALL][feature][ftkey][0]
      ftobj_info[ftkey][LINE_RSS]     = otdict[ALL][feature][ftkey][1]
      ftobj_info[ftkey][PRE_READS]    = otdict[ALL][feature][ftkey][2]
      ftobj_info[ftkey][PRE_WRITES]   = otdict[ALL][feature][ftkey][3]
      ftobj_info[ftkey][POST_READS]   = otdict[ALL][feature][ftkey][4]
      ftobj_info[ftkey][POST_WRITES]  = otdict[ALL][feature][ftkey][5]
      ftobj_info[ftkey][PC_HITS]      = otdict[ALL][feature][ftkey][6]
      ftobj_info[ftkey][MC_ACCS]      = otdict[ALL][feature][ftkey][7]
      ftobj_info[ftkey][MC_HITS]      = otdict[ALL][feature][ftkey][8]
      ftobj_info[ftkey][LIFETIME]     = otdict[ALL][feature][ftkey][9]
  elif feature in site_features:
    for ftkey in otdict[ALL][feature][cxt]:
      ftobj_info[ftkey] = {}
      ftobj_info[ftkey][TOUCHED]      = otdict[ALL][feature][cxt][ftkey][0]
      ftobj_info[ftkey][LINE_RSS]     = otdict[ALL][feature][cxt][ftkey][1]
      ftobj_info[ftkey][PRE_READS]    = otdict[ALL][feature][cxt][ftkey][2]
      ftobj_info[ftkey][PRE_WRITES]   = otdict[ALL][feature][cxt][ftkey][3]
      ftobj_info[ftkey][POST_READS]   = otdict[ALL][feature][cxt][ftkey][4]
      ftobj_info[ftkey][POST_WRITES]  = otdict[ALL][feature][cxt][ftkey][5]
      ftobj_info[ftkey][PC_HITS]      = otdict[ALL][feature][cxt][ftkey][6]
      ftobj_info[ftkey][MC_ACCS]      = otdict[ALL][feature][cxt][ftkey][7]
      ftobj_info[ftkey][MC_HITS]      = otdict[ALL][feature][cxt][ftkey][8]
      ftobj_info[ftkey][LIFETIME]     = otdict[ALL][feature][cxt][ftkey][9]
  return ftobj_info

def update_ftt_info(fttinfo, ftobj_info, key):
  fttinfo[0] += ftobj_info[key][TOUCHED]
  fttinfo[1] += ftobj_info[key][LINE_RSS]
  fttinfo[2] += ftobj_info[key][PRE_READS]
  fttinfo[3] += ftobj_info[key][PRE_WRITES]
  fttinfo[4] += ftobj_info[key][POST_READS]
  fttinfo[5] += ftobj_info[key][POST_WRITES]
  fttinfo[6] += ftobj_info[key][PC_HITS]
  fttinfo[7] += ftobj_info[key][MC_ACCS]
  fttinfo[8] += ftobj_info[key][MC_HITS]
  fttinfo[9] += ftobj_info[key][LIFETIME]
  if key == AGG:
    fttinfo[10].add(True)
  else:
    fttinfo[10].add(key)

def get_ftt_info(ftobj_info, feature, tz, cutstyle, \
  cutfn, bnlife=None, cxt=None, train_ftobj_info=None, \
  train2ref=None, train_miss_style=OPT_OUT):

  if train_ftobj_info:
    thermos = cutfn(train_ftobj_info, tz, bnlife)
  else:
    thermos = cutfn(ftobj_info, tz, bnlife)

  fttinfo = [0 for _ in range(10)] + [set([])]
  if feature == BENCH:
    if len(thermos) > 0:
      update_ftt_info(fttinfo, ftobj_info, AGG)

  elif feature in scalar_features:
    for val in thermos:
      if train2ref:
        if val in train2ref[feature]:
          if feature in phase_features:
            rkeys = train2ref[feature][val]
            #if feature == ALLOC_PHASE:
            #  print("%d --> " % val, end='')
            #  print(rkeys)
            
            for rkey in rkeys:
              if rkey in ftobj_info:
                update_ftt_info( fttinfo, ftobj_info, rkey )
          else:
            rkey = train2ref[feature][val]
            if rkey in ftobj_info:
              update_ftt_info( fttinfo, ftobj_info, rkey )

      else:
        update_ftt_info (fttinfo, ftobj_info, val)

    if train2ref and train_miss_style == OPT_IN:
      if feature in phase_features:
        t2rvals = set(itertools.chain.from_iterable(train2ref[feature].values()))
      else:
        t2rvals = set(train2ref[feature].values())
      for rkey in ftobj_info.keys():
        if not rkey in t2rvals:
          update_ftt_info( fttinfo, ftobj_info, rkey )

  elif feature in site_features:
    for val in thermos:
      if train2ref:
        if val in train2ref[feature][cxt]:
          if feature in phase_features:
            rkeys = train2ref[feature][cxt][val]
            for rkey in rkeys:
              if rkey in ftobj_info:
                update_ftt_info( fttinfo, ftobj_info, rkey )
          else:
            rkey = train2ref[feature][cxt][val]
            if rkey in ftobj_info:
              update_ftt_info( fttinfo, ftobj_info, rkey )
      else:
        update_ftt_info (fttinfo, ftobj_info, val)

    if train2ref and train_miss_style == OPT_IN:
      if feature in phase_features:
        t2rvals = set(itertools.chain.from_iterable(train2ref[feature].values()))
      else:
        t2rvals = set(train2ref[feature][cxt].values())
      for rkey in ftobj_info.keys():
        if not rkey in t2rvals:
          update_ftt_info( fttinfo, ftobj_info, rkey )

  return fttinfo

def create_feature_cut_info(bench, cfg_name, train_cfg=None, \
  tsizes=def_tsizes, train_miss_style=OPT_OUT, \
  cutstyle=THERMOS):

  ftdict  = {}
  ftzstrs = [cut_str(cutstyle, tz) for tz in tsizes]

  phase_cfg = False
  if cfg_name in phase_cfgs:
    phase_cfg = True

  cutfn = None
  if cutstyle == THERMOS:
    cutfn = feature_thermos
  elif cutstyle == BANDWIDTH_PER_BYTE:
    cutfn = feature_bwpb_cut
  elif cutstyle == WRITES_PER_BYTE:
    cutfn = feature_wrpb_cut
  elif cutstyle == RDWR_RATIO:
    cutfn = feature_rdwr_cut
  elif cutstyle == LIFETIME:
    cutfn = feature_lifetime_cut
  elif cutstyle == LIFETIME_PCT:
    cutfn = feature_lifetime_pct_cut

  print("%s-%s" % (bench, cfg_name))

  ftdict[ALL]      = {}
  ftdict[ALL][AGG] = [0 for _ in range(10)]
  otdict           = get_object_cut_info(bench, cfg_name, cutstyle)
  bnlife           = otdict[ALL][LIFETIME]

  print("  doing feature %s cut:" % cutstyle.lower())
  for i in range(len(ftdict[ALL][AGG])):
    ftdict[ALL][AGG][i] = otdict[ALL][AGG][i]

  for tz,tzstr in zip(tsizes, ftzstrs):
    ftdict[tzstr] = {}
    for ft in site_features:
      ftdict[tzstr][ft] = {}

  for ft in scalar_features:
    print("ref: %s" % ft)
    for tz,tzstr in zip(tsizes, ftzstrs):
      ftdict[tzstr][ft] = get_ftt_info( get_ftobj_info(otdict, ft), \
                                        ft, tz, cutstyle, cutfn, bnlife )

  for ft in site_features:
    for cxt in pin_cxt_lengths:
      print("ref: %s-%d" % (ft,cxt))
      for tz,tzstr in zip(tsizes, ftzstrs):
        ftdict[tzstr][ft][cxt] = get_ftt_info( get_ftobj_info(otdict, ft, cxt), \
                                               ft, tz, cutstyle, cutfn, \
                                               bnlife, cxt )

  print("pickling ref ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sfeature_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(ftdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

  if train_cfg:

    train_ftdict  = {}
    train_otdict  = get_object_cut_info(bench, train_cfg, cutstyle)
    train2ref_map = get_train2ref_map(bench, cfg_name, train_cfg)

    train_ftdict[ALL]      = {}
    train_ftdict[ALL][AGG] = [0 for _ in range(10)]
    for i in range(len(train_ftdict[ALL][AGG])):
      train_ftdict[ALL][AGG][i] = otdict[ALL][AGG][i]

    for tz,tzstr in zip(tsizes, ftzstrs):
      train_ftdict[tzstr] = {}
      for ft in site_features:
        train_ftdict[tzstr][ft] = {}

    if train_miss_style == OPT_BENCH:
      thrm = cutfn(get_ftobj_info(train_otdict, BENCH), tz, bnlife)
      train_miss_style = OPT_IN if len(thrm) > 0 else OPT_OUT


    for ft in scalar_features:
      print("train: %s" % ft)
      for tz,tzstr in zip(tsizes, ftzstrs):
        train_ftdict[tzstr][ft] = get_ftt_info(
                                    get_ftobj_info(otdict, ft), \
                                    ft, tz, cutstyle, cutfn, bnlife, \
                                    train_ftobj_info=get_ftobj_info(train_otdict, ft), \
                                    train2ref=train2ref_map,
                                    train_miss_style=train_miss_style
                                  )

    for ft in site_features:
      for cxt in pin_cxt_lengths:
        print("train: %s-%d" % (ft,cxt))
        for tz,tzstr in zip(tsizes, ftzstrs):
          train_ftdict[tzstr][ft][cxt] = get_ftt_info(
                                           get_ftobj_info(otdict, ft, cxt), \
                                           ft, tz, cutstyle, cutfn, \
                                           bnlife, cxt, \
                                           train_ftobj_info=get_ftobj_info(train_otdict, ft, cxt), \
                                           train2ref=train2ref_map,
                                           train_miss_style=train_miss_style
                                         )

    print("pickling train ... ", end='')
    results_dir = get_results_dir(bench, cfg_name)
    cut_pkl_fd = open( ('%sfeature_train_%s.pkl' % \
                       (results_dir, cutstyle.lower())), 'wb')
    pickle.dump(train_ftdict, cut_pkl_fd)
    cut_pkl_fd.close()
    print("done.")

def inc_feature_object(ft_record, rec):
#  for i,r in enumerate(rec):
#    if r != None:
#      ft_record[i] += r

  for i,r in enumerate(rec):
    if isinstance(r,tuple):
      if ft_record[i] == 0:
        ft_record[i] = (0, 0)
      ft_record[i] = (ft_record[i][0] + r[0], ft_record[i][1] + r[1])
    else:
      ft_record[i] += r

def get_feature_object_sums(bench, cfg_name, objlist, foff=(phsz_idx+1),
  obj_tick=0, train_cfg=None, clean=False):

  if not clean:
    results_dir = get_results_dir(bench, cfg_name)
    pkl_file    = ('%s.feature_sums.pkl' % (results_dir)) if train_cfg == None else \
                  ('%s.train_feature_sums.pkl' % (results_dir))
    if os.path.exists(pkl_file):
      pkl_fd = open(pkl_file, 'rb')
      info = pickle.load(pkl_fd)
      return info
    else:
      print ("could not find pkl_file: %s" % pkl_file)

  if train_cfg == None:
    nr_ft_stats = len(raw_feature_stats)
  else:
    nr_ft_stats = len(feature_stats)

    ref_icnt    = get_ins_count(bench, cfg_name)
    train_icnt  = get_ins_count(bench, train_cfg)
    ins_scale   = safediv(ref_icnt, train_icnt)

    ref_lines   = get_object_cut_info(bench, cfg_name,\
                  BANDWIDTH_PER_BYTE)[ALL][AGG][1]
    train_lines = get_object_cut_info(bench, train_cfg,\
                  BANDWIDTH_PER_BYTE)[ALL][AGG][1]
    mem_scale   = safediv(ref_lines, train_lines)


  otdict = {}
  otdict[AGG]         = [0 for _ in range(nr_ft_stats)]
  otdict[SIZE]        = {}
  otdict[SIZE_BUCKET] = {}
  otdict[TYPE_SIG]    = {}
  otdict[ACC_SIG]     = {}
  otdict[SITES]       = {}
  for cxt in pin_cxt_lengths:
    otdict[SITES][cxt] = {}

  otdict[ALLOC_PHASE] = {}
  otdict[TYPE_SIZE]   = {}
  otdict[PHASE_SIZE]  = {}
  otdict[PHASE_SIG]   = {}
  otdict[PHASE_SITES] = {}
  otdict[PHSZ_SITES]  = {}
  for cxt in pin_cxt_lengths:
    otdict[PHASE_SITES][cxt] = {}
    otdict[PHSZ_SITES][cxt]  = {}

  for k,obj in enumerate(get_otdict_object(objlist,None)):
    if (obj_tick != 0) and ((k % obj_tick) == 0):
      print("tick: %-24d" % k)

    clsize      = obj[(lines_idx    + foff)]
    pc_accs     = obj[(pcaccs_idx   + foff)]
    mc_accs     = obj[(mcaccs_idx   + foff)]
    bw          = obj[(bw_idx       + foff)]
    lifetime    = obj[(lifetime_idx + foff)]
    pc_hit_rate = obj[(pchits_idx   + foff)]
    mc_hit_rate = obj[(mchits_idx   + foff)]
    pre_rw      = obj[(prerw_idx    + foff)]
    post_rw     = obj[(postrw_idx   + foff)]

    pre_wrs  = 0 if pre_rw < -0.01 else int((pc_accs / (pre_rw+1)))
    pre_rds  = int((pc_accs - pre_wrs))
    post_wrs = 0 if post_rw < -0.01 else int((bw / (post_rw+1)))
    post_rds = int((bw - post_wrs))

    bwpb     = safediv(bw, (clsize*CACHE_LINE_SIZE))
    wrpb     = safediv(post_wrs, (clsize*CACHE_LINE_SIZE))

    pc_hit_rate = None if pc_hit_rate < -0.01 else pc_hit_rate
    mc_hit_rate = None if mc_hit_rate < -0.01 else mc_hit_rate

    pc_hits = mc_hits = 0.0
    if pc_hit_rate != None:
      pc_hits = (pc_accs * (pc_hit_rate))
    if mc_hit_rate != None:
      mc_hits = (mc_accs * (mc_hit_rate))

#    rec = [1, clsize, pc_accs, mc_accs, bw, post_wrs, bwpb, wrpb, \
#           pc_hit_rate, mc_hit_rate, lifetime]

    rec = [1, clsize, pc_accs, mc_accs, bw, post_wrs,
           (bw, (clsize*CACHE_LINE_SIZE)),
           (post_wrs, (clsize*CACHE_LINE_SIZE)),
           (pc_hits, pc_accs), (mc_hits, mc_accs), lifetime]

    if train_cfg != None:
      #bwpb_scale = safediv( (bw       * ins_scale), (clsize*mem_scale*CACHE_LINE_SIZE) )
      #wrpb_scale = safediv( (post_wrs * ins_scale), (clsize*mem_scale*CACHE_LINE_SIZE) )
      bwpb_scale    = ( (bw       * ins_scale), (clsize*mem_scale*CACHE_LINE_SIZE) )
      wrpb_scale    = ( (post_wrs * ins_scale), (clsize*mem_scale*CACHE_LINE_SIZE) )
      pc_hits_scale = ( (pc_hits  * ins_scale), (pc_accs  * ins_scale) )
      mc_hits_scale = ( (mc_hits  * ins_scale), (mc_accs  * ins_scale) )

      rec.append((clsize   * mem_scale))
      rec.append((pc_accs  * ins_scale))
      rec.append((mc_accs  * ins_scale))
      rec.append((bw       * ins_scale))
      rec.append((post_wrs * ins_scale))
      rec.append(bwpb_scale)
      rec.append(wrpb_scale)
      rec.append(pc_hits_scale)
      rec.append(mc_hits_scale)
      rec.append((lifetime * ins_scale))

    rec = tuple(rec)

    inc_feature_object(otdict[AGG], rec)
    for i,cxt in enumerate(pin_cxt_lengths):
      site = obj[i]
      if not site in otdict[SITES][cxt]:
        otdict[SITES][cxt][site] = [0 for _ in range(nr_ft_stats)]
      inc_feature_object(otdict[SITES][cxt][site], rec)

    size = obj[size_idx]
    if not size in otdict[SIZE]:
      otdict[SIZE][size] = [0 for _ in range(nr_ft_stats)]
    inc_feature_object(otdict[SIZE][size], rec)

    szbucket = obj[szbucket_idx]
    if not szbucket in otdict[SIZE_BUCKET]:
      otdict[SIZE_BUCKET][szbucket] = [0 for _ in range(nr_ft_stats)]
    inc_feature_object(otdict[SIZE_BUCKET][szbucket], rec)

    tsig = obj[tsig_idx]
    if not tsig in otdict[TYPE_SIG]:
      otdict[TYPE_SIG][tsig] = [0 for _ in range(nr_ft_stats)]
    inc_feature_object(otdict[TYPE_SIG][tsig], rec)

    asig = obj[asig_idx]
    if not asig in otdict[ACC_SIG]:
      otdict[ACC_SIG][asig] = [0 for _ in range(nr_ft_stats)]
    inc_feature_object(otdict[ACC_SIG][asig], rec)

    aph = obj[aph_idx]
    if not aph in otdict[ALLOC_PHASE]:
      otdict[ALLOC_PHASE][aph] = [0 for _ in range(nr_ft_stats)]
    inc_feature_object(otdict[ALLOC_PHASE][aph], rec)

    for i,cxt in enumerate(pin_cxt_lengths):
      phsite = obj[(phsite_idx+i)]
      if not phsite in otdict[PHASE_SITES][cxt]:
        otdict[PHASE_SITES][cxt][phsite] = [0 for _ in range(nr_ft_stats)]
      inc_feature_object(otdict[PHASE_SITES][cxt][phsite], rec)

    for i,cxt in enumerate(pin_cxt_lengths):
      phszst = obj[(phszst_idx+i)]
      if not phszst in otdict[PHSZ_SITES][cxt]:
        otdict[PHSZ_SITES][cxt][phszst] = [0 for _ in range(nr_ft_stats)]
      inc_feature_object(otdict[PHSZ_SITES][cxt][phszst], rec)

    phsig = obj[phsig_idx]
    if not phsig in otdict[PHASE_SIG]:
      otdict[PHASE_SIG][phsig] = [0 for _ in range(nr_ft_stats)]
    inc_feature_object(otdict[PHASE_SIG][phsig], rec)

    typesz = obj[typesz_idx]
    if not typesz in otdict[TYPE_SIZE]:
      otdict[TYPE_SIZE][typesz] = [0 for _ in range(nr_ft_stats)]
    inc_feature_object(otdict[TYPE_SIZE][typesz], rec)

    phsz = obj[phsz_idx]
    if not phsz in otdict[PHASE_SIZE]:
      otdict[PHASE_SIZE][phsz] = [0 for _ in range(nr_ft_stats)]
    inc_feature_object(otdict[PHASE_SIZE][phsz], rec)

#  for feature in otdict:
#    if feature == AGG:
#      for i,rec in enumerate(otdict[feature]):
#        if isinstance(rec,tuple):
#          num, den = rec
#          otdict[feature][i] = safediv(num, den)
#
#    elif feature in scalar_features:
#      for ftkey in otdict[feature]:
#        for i,rec in enumerate(otdict[feature][ftkey]):
#          if isinstance(rec,tuple):
#            num, den = rec
#            otdict[feature][ftkey][i] = safediv(num, den)
#
#    elif feature in site_features:
#      for cxt in otdict[feature]:
#        for ftkey in otdict[feature][cxt]:
#          for i,rec in enumerate(otdict[feature][cxt][ftkey]):
#            if isinstance(rec,tuple):
#              num, den = rec
#              otdict[feature][cxt][ftkey][i] = safediv(num, den)

#  print("agg")
#  print(otdict[AGG])
#  for key in otdict[SITES][2]:
#    print("site: %d" % key)
#    print(otdict[SITES][2][key])

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  pkl_file    = ('%s.feature_sums.pkl' % (results_dir)) if train_cfg == None else \
                ('%s.train_feature_sums.pkl' % (results_dir))
  cut_pkl_fd = open(pkl_file, 'wb')
  pickle.dump(otdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

  return otdict

def update_mdd_avgs(ft_rec, gd_rec, bn_rec, obj_rec, cmp_idxs,
  match=True, pp=False):

  # The error is normalized over the sum of all object values for the feature.
  # Since we're summing these values, this method is similar to normalizing
  # each object by the object average for each benchmark.
  #
  ft_objs = gd_rec[0]

  if match:
    ft_rec[0][1] += 1

  for gi,bi,oi in cmp_idxs:

    ft_sum  = gd_rec[gi]
    bn_sum  = bn_rec[bi]
    obj_val = obj_rec[oi]

    if obj_val == None:
      continue

    if isinstance(obj_val, tuple):

      ft_sum_num, ft_sum_den = ft_sum
      bn_sum_num, bn_sum_den = bn_sum
      obj_val_num, obj_val_den = obj_val

      loi_mean = safediv( ft_sum_num, ft_sum_den, -1.0 )
      loo_mean = safediv( (ft_sum_num-obj_val_num), (ft_sum_den-obj_val_den), -1.0 )

      if loi_mean > -0.01:
        obj_num_guess = (loi_mean * obj_val_den)
        loi_err = abs(obj_num_guess-obj_val_num)

        ft_rec[gi][2] += 1
        ft_rec[gi][3] += loi_err
        ft_rec[gi][4] += safediv( loi_err, bn_sum_den )
        if match:
          ft_rec[gi][8] += safediv( loi_err, bn_sum_den )

#        if pp and gi == 9:
#          obj_rate = safediv( obj_val_num, obj_val_den, -1.0 )
#          print ("%8.4f %8.4f %12d %12d %12d %12d %12d %8.4f %8.4f" % \
#                 (loi_mean, obj_rate, obj_val_num, obj_val_den,
#                  obj_num_guess, loi_err, bn_sum_den,
#                  safediv( loi_err, bn_sum_den ), ft_rec[gi][4]
#                 )
#                )

      if loo_mean > -0.01:
        obj_num_guess = (loo_mean * obj_val_den)
        loo_err = abs(obj_num_guess-obj_val_num)

        ft_rec[gi][5] += 1
        ft_rec[gi][6] += loo_err
        ft_rec[gi][7] += safediv( loo_err, bn_sum_den )
        if match:
          ft_rec[gi][9] += safediv( loo_err, bn_sum_den )

#        if pp and gi == 9:
#          obj_rate = safediv( obj_val_num, obj_val_den, -1.0 )
#          print ("%8.4f %8.4f %12d %12d %12d %12d %12d %8.4f %8.4f" % \
#                 (loo_mean, obj_rate, obj_val_num, obj_val_den,
#                  obj_num_guess, loo_err, bn_sum_den,
#                  safediv( loo_err, bn_sum_den ), ft_rec[gi][7]
#                 )
#                )

    else:

      loi_mean = safediv( ft_sum, ft_objs, -1.0 )
      loo_mean = safediv( (ft_sum-obj_val), (ft_objs-1), -1.0 )

      if loi_mean > -0.01:
        loi_err = min ([obj_val, abs(loi_mean-obj_val)])

        ft_rec[gi][2] += 1
        ft_rec[gi][3] += loi_err
        ft_rec[gi][4] += safediv( loi_err, bn_sum )
        if match:
          ft_rec[gi][8] += safediv( loi_err, bn_sum )
      if loo_mean > -0.01:
        loo_err = min ([obj_val, abs(loo_mean-obj_val)])

        ft_rec[gi][5] += 1
        ft_rec[gi][6] += loo_err
        ft_rec[gi][7] += safediv( loo_err, bn_sum )
        if match:
          ft_rec[gi][9] += safediv( loo_err, bn_sum )


#    if pp and gi == 11:
#      print ("%16.4f %16.4f %12.4f %12.4f %16.4f %12.6f %12.4f" % \
#             (ft_sum, bn_rec[0], obj_val, ft_objs, loi_mean, \
#              safediv( (abs( loi_mean - obj_val )), bn_sum ),
#              ft_rec[gi][4]
#             ))


#    if isinstance(obj_rec[oi], tuple):
#      ft_num, ft_den  = gd_rec[gi]
#      bn_num, bn_den  = bn_rec[bi]
#      obj_num, obj_den = obj_rec[oi]
#
#      loi_mean = safediv( ft_num, ft_den, -1.0 )
#      loo_mean = safediv( (ft_num-obj_num), (ft_den-obj_den), -1.0 )
#      
#      obj_val = safediv(obj_num, obj_den)
#      bn_sum  = safediv(bn_num, bn_den)
#      ft_sum  = safediv( ft_num, ft_den, -1.0 )
#
#      if loi_mean > -0.01:
#        loi_err = min ([obj_val, abs(loi_mean-obj_val)])
#
#        ft_rec[gi][2] += 1
#        ft_rec[gi][3] += loi_err
#        ft_rec[gi][4] += safediv( loi_err, bn_rec[0] )
#        if match:
#          ft_rec[gi][8] += safediv( loi_err, bn_rec[0] )
#      if loo_mean > -0.01:
#        loo_err = min ([obj_val, abs(loo_mean-obj_val)])
#
#        ft_rec[gi][5] += 1
#        ft_rec[gi][6] += loo_err
#        ft_rec[gi][7] += safediv( loo_err, bn_rec[0] )
#        if match:
#          ft_rec[gi][9] += safediv( loo_err, bn_rec[0] )
#
#      if pp and gi == 4:
#        print ("%16.4f %16.4f %12.4f %12.4f %16.4f %12.6f %12.4f" % \
#               (ft_sum, bn_rec[0], obj_val, ft_objs, loi_mean, \
#                safediv( (abs( loi_mean - obj_val )), bn_sum ),
#                ft_rec[gi][4]
#               ))
#
#    else:
#
#      ft_sum  = gd_rec[gi]
#      bn_sum  = bn_rec[bi]
#      obj_val = obj_rec[oi]
#
#      loi_mean = safediv( ft_sum, ft_objs, -1.0 )
#      loo_mean = safediv( (ft_sum-obj_val), (ft_objs-1), -1.0 )
#
#      if loi_mean > -0.01:
#        loi_err = min ([obj_val, abs(loi_mean-obj_val)])
#
#        ft_rec[gi][2] += 1
#        ft_rec[gi][3] += loi_err
#        ft_rec[gi][4] += safediv( loi_err, bn_sum )
#        if match:
#          ft_rec[gi][8] += safediv( loi_err, bn_sum )
#      if loo_mean > -0.01:
#        loo_err = min ([obj_val, abs(loo_mean-obj_val)])
#
#        ft_rec[gi][5] += 1
#        ft_rec[gi][6] += loo_err
#        ft_rec[gi][7] += safediv( loo_err, bn_sum )
#        if match:
#          ft_rec[gi][9] += safediv( loo_err, bn_sum )


def get_guide_rec(ftavgs, feature, refkey, ref2train, cxt=None, pp=False):
  if feature == AGG:
    return (True, ftavgs[AGG])

  elif feature in scalar_features:
    if refkey in ref2train[feature]:
      tkey = ref2train[feature][refkey]
      if tkey in ftavgs[feature]:
        return (True, ftavgs[feature][tkey])

  elif feature in site_features:
    if refkey in ref2train[feature][cxt]:
      tkey = ref2train[feature][cxt][refkey]
      if tkey in ftavgs[feature][cxt]:
        return (True, ftavgs[feature][cxt][tkey])

  return (False, ftavgs[AGG])

def init_distd(ftavgs, cmp_idxs):

  distd = {}
  for feature in ftavgs:

    if feature == AGG:
      distd[AGG] = []
      distd[AGG].append([ftavgs[AGG][0],0])
      for i,j,k in cmp_idxs:
        distd[AGG].append( [ftavgs[AGG][k]] + [0 for _ in range(9)] )

    elif feature in scalar_features:
      distd[feature] = {}
      for ftkey in ftavgs[feature]:
        distd[feature][ftkey] = []
        distd[feature][ftkey].append([ftavgs[feature][ftkey][0],0])
        for i,j,k in cmp_idxs:
          distd[feature][ftkey].append( [ftavgs[feature][ftkey][k]] + [0 for _ in range(9)] )

    elif feature in site_features:
      distd[feature] = {}
      for cxt in ftavgs[feature]:
        distd[feature][cxt] = {}
        for ftkey in ftavgs[feature][cxt]:
          distd[feature][cxt][ftkey] = []
          distd[feature][cxt][ftkey].append([ftavgs[feature][cxt][ftkey][0],0])
          for i,j,k in cmp_idxs:
            distd[feature][cxt][ftkey].append( [ftavgs[feature][cxt][ftkey][k]] + [0 for _ in range(9)] )

  return distd

def get_mean_distance_dict(bench, cfg_name, train_cfg, foff=(phsz_idx+1),\
  clean=False, ftclean=False, obj_tick=0, bwfilter=True):

  if not clean:
    results_dir = get_results_dir(bench, cfg_name)
    pkl_file    = ('%s.mean_distance_dict.pkl' % (results_dir))
    if os.path.exists(pkl_file):
      pkl_fd = open(pkl_file, 'rb')
      info   = pickle.load(pkl_fd)
      return info
    else:
      print ("could not find pkl_file: %s" % pkl_file)


  train_objlist = get_object_list(bench, train_cfg)
  print("got train objects.")

  if bwfilter:
    if 'mpc' in cfg_name:
      train_objlist = [ x for x in train_objlist if x[(mcaccs_idx+foff)] != 0 ]
    else:
      train_objlist = [ x for x in train_objlist if x[(bw_idx+foff)] != 0 ]
  print("ready")

  train_guide   = get_feature_object_sums(bench, cfg_name, train_objlist, \
                  obj_tick=obj_tick, train_cfg=train_cfg, clean=ftclean)
  train_objlist = None
  print("got train feature averages.")


  objlist = get_object_list(bench, cfg_name)
  print("got objects.")

  if bwfilter:
    if 'mpc' in cfg_name:
      objlist = [ x for x in objlist if x[(mcaccs_idx+foff)] != 0 ]
    else:
      objlist = [ x for x in objlist if x[(bw_idx+foff)] != 0 ]
  print("ready")

  ref_guide = get_feature_object_sums(bench, cfg_name, objlist, \
              obj_tick=obj_tick, clean=ftclean)
  print("got feature averages.")


  ref2train = get_ref2train_map(bench, cfg_name, train_cfg)
  print("got ref2train map.")


  nr_raw         = len(raw_feature_stats)
  nr_scale       = len(scale_feature_stats)
  ref_cmp_idxs   = [ (i,i,i) for i in range(1,nr_raw) ]
  train_cmp_idxs = [ (i,i,i) for i in range(1,nr_raw) ]                + \
                   [ (((nr_raw-1)+j),j,j) for j in range(1,nr_scale+1) ]

  print("getting mean distances ...", end='')

  distd        = {}
  distd[REF]   = init_distd(ref_guide, ref_cmp_idxs)
  distd[TRAIN] = init_distd(ref_guide, train_cmp_idxs)
  for k,obj in enumerate(get_otdict_object(objlist,None)):
    if (obj_tick != 0) and ((k % obj_tick) == 0):
      print("tick: %-24d" % k)

    clsize      = obj[(lines_idx    + foff)]
    pc_accs     = obj[(pcaccs_idx   + foff)]
    mc_accs     = obj[(mcaccs_idx   + foff)]
    bw          = obj[(bw_idx       + foff)]
    lifetime    = obj[(lifetime_idx + foff)]
    pc_hit_rate = obj[(pchits_idx   + foff)]
    mc_hit_rate = obj[(mchits_idx   + foff)]
    pre_rw      = obj[(prerw_idx    + foff)]
    post_rw     = obj[(postrw_idx   + foff)]

    pre_wrs     = 0 if pre_rw < -0.01 else int((pc_accs / (pre_rw+1)))
    pre_rds     = int((pc_accs - pre_wrs))
    post_wrs    = 0 if post_rw < -0.01 else int((bw / (post_rw+1)))
    post_rds    = int((bw - post_wrs))

    bwpb        = safediv(bw, (clsize*CACHE_LINE_SIZE))
    wrpb        = safediv(post_wrs, (clsize*CACHE_LINE_SIZE))

    pc_hit_rate = None if pc_hit_rate < -0.01 else pc_hit_rate
    mc_hit_rate = None if mc_hit_rate < -0.01 else mc_hit_rate

    pc_hits = mc_hits = 0.0
    if pc_hit_rate != None:
      pc_hits = (pc_accs * (pc_hit_rate))
    if mc_hit_rate != None:
      mc_hits = (mc_accs * (mc_hit_rate))

    obj_rec = ( 1, clsize, pc_accs, mc_accs, bw, post_wrs,
                (bw, (clsize*CACHE_LINE_SIZE)),
                (post_wrs, (clsize*CACHE_LINE_SIZE)),
                (pc_hits, pc_accs), (mc_hits, mc_accs), lifetime )

    #if (k%100 == 0):
    #  print ("%4d: %4d %16d " % (k, obj[2], lifetime), end='')
    update_mdd_avgs(distd[REF][AGG],
                    ref_guide[AGG],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs, pp=True)

    update_mdd_avgs(distd[REF][SIZE][obj[size_idx]],
                    ref_guide[SIZE][obj[size_idx]],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs)

    update_mdd_avgs(distd[REF][SIZE_BUCKET][obj[szbucket_idx]],
                    ref_guide[SIZE_BUCKET][obj[szbucket_idx]],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs)
    
    update_mdd_avgs(distd[REF][TYPE_SIG][obj[tsig_idx]],
                    ref_guide[TYPE_SIG][obj[tsig_idx]],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs)

    update_mdd_avgs(distd[REF][ACC_SIG][obj[asig_idx]],
                    ref_guide[ACC_SIG][obj[asig_idx]],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs)

    update_mdd_avgs(distd[REF][ALLOC_PHASE][obj[aph_idx]],
                    ref_guide[ALLOC_PHASE][obj[aph_idx]],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs)

    update_mdd_avgs(distd[REF][PHASE_SIG][obj[phsig_idx]],
                    ref_guide[PHASE_SIG][obj[phsig_idx]],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs)

    update_mdd_avgs(distd[REF][TYPE_SIZE][obj[typesz_idx]],
                    ref_guide[TYPE_SIZE][obj[typesz_idx]],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs)

    update_mdd_avgs(distd[REF][PHASE_SIZE][obj[phsz_idx]],
                    ref_guide[PHASE_SIZE][obj[phsz_idx]],
                    ref_guide[AGG],
                    obj_rec, ref_cmp_idxs)

#    if (k%100 == 0):
#      print ("%4d: %4d %12d %12d " % (k, obj[2], clsize, bw), end='')
    for i,cxt in enumerate(pin_cxt_lengths):
      update_mdd_avgs(distd[REF][SITES][cxt][obj[i]],
                      ref_guide[SITES][cxt][obj[i]],
                      ref_guide[AGG],
                      obj_rec, ref_cmp_idxs)
#                      pp=(True if ((cxt == 2) and ((k%100)==0)) else False))

      update_mdd_avgs(distd[REF][PHASE_SITES][cxt][obj[(phsite_idx+i)]],
                      ref_guide[PHASE_SITES][cxt][obj[(phsite_idx+i)]],
                      ref_guide[AGG], obj_rec, ref_cmp_idxs)

      update_mdd_avgs(distd[REF][PHSZ_SITES][cxt][obj[(phszst_idx+i)]],
                      ref_guide[PHSZ_SITES][cxt][obj[(phszst_idx+i)]],
                      ref_guide[AGG], obj_rec, ref_cmp_idxs)


#    print ("%4d: %16d %16d " % (k, obj[size_idx], clsize), end='')
    match,gd_rec = get_guide_rec(train_guide, AGG, None, None)
    update_mdd_avgs(distd[TRAIN][AGG], gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)

    match,gd_rec = get_guide_rec(train_guide, SIZE, obj[size_idx], ref2train)
    update_mdd_avgs(distd[TRAIN][SIZE][obj[size_idx]],
                    gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)
#    print("%16d " % obj[size_idx], end='')
#    print(distd[TRAIN][SIZE][obj[size_idx]][11])

    match,gd_rec = get_guide_rec(train_guide, SIZE_BUCKET, obj[szbucket_idx], ref2train)
    update_mdd_avgs(distd[TRAIN][SIZE_BUCKET][obj[szbucket_idx]],
                    gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)
    
    match,gd_rec = get_guide_rec(train_guide, TYPE_SIG, obj[tsig_idx], ref2train)
    update_mdd_avgs(distd[TRAIN][TYPE_SIG][obj[tsig_idx]],
                    gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)

    match,gd_rec = get_guide_rec(train_guide, ACC_SIG, obj[asig_idx], ref2train)
    update_mdd_avgs(distd[TRAIN][ACC_SIG][obj[asig_idx]],
                    gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)

    match,gd_rec = get_guide_rec(train_guide, ALLOC_PHASE, obj[aph_idx], ref2train)
    update_mdd_avgs(distd[TRAIN][ALLOC_PHASE][obj[aph_idx]],
                    gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)

    match,gd_rec = get_guide_rec(train_guide, PHASE_SIG, obj[phsig_idx], ref2train)
    update_mdd_avgs(distd[TRAIN][PHASE_SIG][obj[phsig_idx]],
                    gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)

    match,gd_rec = get_guide_rec(train_guide, TYPE_SIZE, obj[typesz_idx], ref2train)
    update_mdd_avgs(distd[TRAIN][TYPE_SIZE][obj[typesz_idx]],
                    gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)

    match,gd_rec = get_guide_rec(train_guide, PHASE_SIZE, obj[phsz_idx], ref2train)
    update_mdd_avgs(distd[TRAIN][PHASE_SIZE][obj[phsz_idx]],
                    gd_rec, ref_guide[AGG],
                    obj_rec, train_cmp_idxs, match=match)

    for i,cxt in enumerate(pin_cxt_lengths):
      match,gd_rec = get_guide_rec(train_guide, SITES, obj[i],
                                   ref2train, cxt=cxt)
      update_mdd_avgs(distd[TRAIN][SITES][cxt][obj[i]],
                      gd_rec, ref_guide[AGG], obj_rec,
                      train_cmp_idxs, match=match)

      match,gd_rec = get_guide_rec(train_guide, PHASE_SITES, obj[(phsite_idx+i)],
                                   ref2train, cxt=cxt)
      update_mdd_avgs(distd[TRAIN][PHASE_SITES][cxt][obj[(phsite_idx+i)]],
                      gd_rec, ref_guide[AGG], obj_rec,
                      train_cmp_idxs, match=match)

      match,gd_rec = get_guide_rec(train_guide, PHSZ_SITES, obj[(phszst_idx+i)],
                                   ref2train, cxt=cxt)
      update_mdd_avgs(distd[TRAIN][PHSZ_SITES][cxt][obj[(phszst_idx+i)]],
                      gd_rec, ref_guide[AGG], obj_rec,
                      train_cmp_idxs, match=match)
  print("done.")

  print("making mean distance dict ... ", end='')
  mdd        = {}
  nr_objects = distd[REF][AGG][0][0]
  total_data = CLMB( distd[REF][AGG][1][0] )
  total_bw   = distd[REF][AGG][4][0]

  for guide in [REF, TRAIN]:
    nr_ft_stats = len(raw_feature_stats) if guide == REF else \
                  len(feature_stats)

    mdd[guide] = {}

    for feature in distd[guide]:

      if feature == AGG:
        mdd[guide][BENCH] = {}
        for i in range(1,nr_ft_stats):
          loi_objs    = distd[guide][AGG][i][2]
          loi_sum     = distd[guide][AGG][i][3]
          loi_rel_sum = distd[guide][AGG][i][4]
          loo_objs    = distd[guide][AGG][i][5]
          loo_sum     = distd[guide][AGG][i][6]
          loo_rel_sum = distd[guide][AGG][i][7]
          mdd[guide][BENCH][feature_stats[i]] = (
            1,
            nr_objects,
            total_data,
            1,
            nr_objects,
            total_data,
            total_bw,
            safediv(loi_sum, loi_objs, -1.0),
            safediv(loo_sum, loo_objs, -1.0),
            loi_rel_sum,
            loo_rel_sum,
            loi_rel_sum,
            loo_rel_sum,
            1.0,
            1.0,
          )

      if feature in scalar_features:
        mdd[guide][feature] = {}

        num_sets   = len(distd[guide][feature].keys())
        match_sets = len([x for x in distd[guide][feature].values() \
                          if x[0][1] > 0])
        match_objs = sum([x[0][1] for x in distd[guide][feature].values()])
        match_data = CLMB(sum([x[1][0] for x in distd[guide][feature].values() \
                          if x[0][1] > 0]))
        match_bw   = sum([x[4][0] for x in distd[guide][feature].values() \
                          if x[0][1] > 0])
        for i in range(1,nr_ft_stats):

          loi_objs      = loi_sum     = loo_objs      = loo_sum = \
          loi_rel_sum   = loo_rel_sum = match_loi_sum = \
          match_loo_sum = 0
          for ftkey in distd[guide][feature]:
            loi_objs      += distd[guide][feature][ftkey][i][2]
            loi_sum       += distd[guide][feature][ftkey][i][3]
            loi_rel_sum   += distd[guide][feature][ftkey][i][4]
            loo_objs      += distd[guide][feature][ftkey][i][5]
            loo_sum       += distd[guide][feature][ftkey][i][6]
            loo_rel_sum   += distd[guide][feature][ftkey][i][7]
            match_loi_sum += distd[guide][feature][ftkey][i][8]
            match_loo_sum += distd[guide][feature][ftkey][i][9]

          mdd[guide][feature][feature_stats[i]] = (
            num_sets,
            safediv(nr_objects, num_sets, -1.0),
            safediv(total_data, num_sets, -1.0),
            match_sets,
            safediv(match_objs, num_sets, -1.0),
            safediv(match_data, num_sets, -1.0),
            safediv(match_bw,   num_sets, -1.0),
            safediv(loi_sum,    loi_objs, -1.0),
            safediv(loo_sum,    loo_objs, -1.0),
            loi_rel_sum,
            loo_rel_sum,
            match_loi_sum,
            match_loo_sum,
            safediv(match_loi_sum, loi_rel_sum),
            safediv(match_loo_sum, loo_rel_sum)
          )

      elif feature in site_features:
        mdd[guide][feature] = {}
        for cxt in distd[guide][feature]:

          num_sets   = len(distd[guide][feature][cxt].keys())
          match_sets = len([x for x in distd[guide][feature][cxt].values() \
                            if x[0][1] > 0])
          match_objs = sum([x[0][1] for x in distd[guide][feature][cxt].values()])
          match_data = CLMB(sum([x[1][0] for x in distd[guide][feature][cxt].values() \
                            if x[0][1] > 0]))
          match_bw   = sum([x[4][0] for x in distd[guide][feature][cxt].values() \
                            if x[0][1] > 0])

          mdd[guide][feature][cxt] = {}
          for i in range(1,nr_ft_stats):

            loi_objs      = loi_sum     = loo_objs      = loo_sum = \
            loi_rel_sum   = loo_rel_sum = match_loi_sum = \
            match_loo_sum = 0
            for ftkey in distd[guide][feature][cxt]:
              loi_objs      += distd[guide][feature][cxt][ftkey][i][2]
              loi_sum       += distd[guide][feature][cxt][ftkey][i][3]
              loi_rel_sum   += distd[guide][feature][cxt][ftkey][i][4]
              loo_objs      += distd[guide][feature][cxt][ftkey][i][5]
              loo_sum       += distd[guide][feature][cxt][ftkey][i][6]
              loo_rel_sum   += distd[guide][feature][cxt][ftkey][i][7]
              match_loi_sum += distd[guide][feature][cxt][ftkey][i][8]
              match_loo_sum += distd[guide][feature][cxt][ftkey][i][9]
              #if feature == SITES and cxt == 2 and i == 2:
              #  print("%4d : %12.2f" % (ftkey, \
              #    distd[guide][feature][cxt][ftkey][i][6]))

            mdd[guide][feature][cxt][feature_stats[i]] = (
              num_sets,
              safediv(nr_objects, num_sets, -1.0),
              safediv(total_data, num_sets, -1.0),
              match_sets,
              safediv(match_objs, num_sets, -1.0),
              safediv(match_data, num_sets, -1.0),
              safediv(match_bw,   num_sets, -1.0),
              safediv(loi_sum,    loi_objs, -1.0),
              safediv(loo_sum,    loo_objs, -1.0),
              loi_rel_sum,
              loo_rel_sum,
              match_loi_sum,
              match_loo_sum,
              safediv(match_loi_sum, loi_rel_sum),
              safediv(match_loo_sum, loo_rel_sum)
            )

  print("done.")

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%s.mean_distance_dict.pkl' % (results_dir)), 'wb')
  pickle.dump(mdd, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

  return mdd

def mean_distance_report(benches, cfg_name, train_cfg, guide=REF,
  stats=[LINE_RSS,PC_ACCS,BANDWIDTH], features=all_features,
  print_benches=True, style=LOI_ERR, per_set=False,
  bwfilter=True, obj_tick=0, clean=False, ftclean=False):

  print("\n%s --> ref mean distance report\n\n" % (guide.lower()))

  header_format_str   = "%-12s %9s %12s %12s %9s %12s %12s %9s %9s %9s"
  header_strs         = ["feature", "sets", "obj/set", "data/set",
                         "match", "mo/set", "md/set", "mo-rat",
                         "md-rat", "mbw-rat"]
  for s in stats:
    header_format_str  += " %9s"
    header_strs        += [short_feature_stats[s]]
    if per_set:
      header_format_str += " %9s"
      header_strs       += ["per_set"]

  header_strs         = tuple(header_strs)

  ft_format_str       = "%9d %12.2f %12.2f %9d %12.2f %12.2f %9.4f %9.4f %9.4f "
  if per_set:
    ft_format_str      += " ".join(["%9.4f %.4E" for _ in range(len(stats))])
  else:
    ft_format_str      += " ".join(["%9.4f" for _ in range(len(stats))])

  if 'mpc' in cfg_name:
    style -= 1

  avgs = {}
  for bench in benches:
    mdd = get_mean_distance_dict(bench, cfg_name, train_cfg,
          bwfilter=bwfilter, obj_tick=obj_tick, clean=clean,
          ftclean=ftclean)

    if print_benches:
      print("%s-%s" % (bench, cfg_name))
      print(header_format_str % header_strs)

    for feature in features:

      if feature in scalar_features:
        if print_benches:
          print("%-12s " % feature, end='')

        if feature == BENCH:
          match_data_per_set = mdd[guide][feature][LINE_RSS][5]
          #print (mdd[guide][feature][BANDWIDTH])
          match_bw_per_set   = ( (mdd[guide][feature][BANDWIDTH][5] \
                                 / CACHE_LINE_SIZE) * (1024*1024) )
        else:
          match_data_per_set = CLMB(mdd[guide][feature][LINE_RSS][5])
          match_bw_per_set   = mdd[guide][feature][BANDWIDTH][5]

        vals = []
        vals.append(mdd[guide][feature][LINE_RSS][0])
        vals.append(mdd[guide][feature][LINE_RSS][1])
        vals.append(mdd[guide][feature][LINE_RSS][2])
        vals.append(mdd[guide][feature][LINE_RSS][3])
        vals.append(mdd[guide][feature][LINE_RSS][4])
        vals.append(mdd[guide][feature][LINE_RSS][5])

        total_objs = mdd[REF][BENCH][LINE_RSS][1]
        match_objs = ( mdd[guide][feature][LINE_RSS][0] * \
                       mdd[guide][feature][LINE_RSS][4] )
        mo_rat = safediv( match_objs, total_objs, -1.0 )
        vals.append(mo_rat)

        total_data = mdd[REF][BENCH][LINE_RSS][2]
        match_data = ( mdd[guide][feature][LINE_RSS][0] * \
                       mdd[guide][feature][LINE_RSS][5] )
        md_rat = safediv( match_data, total_data, -1.0 )
        vals.append(md_rat)
        #print ("dat: %9.2f %9.2f" % (total_data, match_data))

        if not 'mpc' in cfg_name:
          total_bw = mdd[REF][BENCH][LINE_RSS][6]
          match_bw = ( mdd[guide][feature][LINE_RSS][0] * \
                       mdd[guide][feature][LINE_RSS][6] )
          #print ("bw:  %9.2f %9.2f" % (total_bw, match_bw))
          mbw_rat  = safediv( match_bw, total_bw, -1.0 )
          vals.append(mbw_rat)
        else:
          vals.append(0.0)

        for stat in stats:
          vals.append(mdd[guide][feature][stat][style])
          if per_set:
            bnerr  = mdd[guide][BENCH][stat][style]
            less   = max([0,(bnerr - mdd[guide][feature][stat][style])])
            ftvals = mdd[guide][feature][LINE_RSS][0]
            vals.append(safediv(less,ftvals))

        if not feature in avgs:
          avgs[feature] = []
        avgs[feature].append(vals)

        if print_benches:
          print(ft_format_str % tuple(vals))

      elif feature in site_features:
        for clen in pin_cxt_lengths:

          if print_benches:
            print(("%-12s " % ("%s-%d" % (feature,clen))), end='')

          vals = []
          vals.append(mdd[guide][feature][clen][LINE_RSS][0])
          vals.append(mdd[guide][feature][clen][LINE_RSS][1])
          vals.append(mdd[guide][feature][clen][LINE_RSS][2])
          vals.append(mdd[guide][feature][clen][LINE_RSS][3])
          vals.append(mdd[guide][feature][clen][LINE_RSS][4])
          vals.append(CLMB(mdd[guide][feature][clen][LINE_RSS][5]))

          total_objs = mdd[REF][BENCH][LINE_RSS][1]
          match_objs = ( mdd[guide][feature][clen][LINE_RSS][0] * \
                         mdd[guide][feature][clen][LINE_RSS][4] )
          mo_rat = safediv( match_objs, total_objs, -1.0 )
          vals.append(mo_rat)

          total_data = mdd[REF][BENCH][LINE_RSS][2]
          match_data = ( mdd[guide][feature][clen][LINE_RSS][0] * \
                         mdd[guide][feature][clen][LINE_RSS][5] )
          md_rat = safediv( match_data, total_data, -1.0 )
          vals.append(md_rat)

          if not 'mpc' in cfg_name:
            total_bw = mdd[REF][BENCH][LINE_RSS][6]
            match_bw = ( mdd[guide][feature][clen][LINE_RSS][0] * \
                         mdd[guide][feature][clen][LINE_RSS][6] )
            mbw_rat  = safediv( match_bw, total_bw, -1.0 )
            vals.append(mbw_rat)
          else:
            vals.append(0)

          for stat in stats:
            vals.append(mdd[guide][feature][clen][stat][style])
            if per_set:
              bnerr  = mdd[guide][BENCH][stat][style]
              less   = max([0,(bnerr - mdd[guide][feature][clen][stat][style])])
              ftvals = mdd[guide][feature][clen][LINE_RSS][0]
              vals.append(safediv(less,ftvals))

          key = ("%s-%d" % (feature,clen))
          if not key in avgs:
            avgs[key] = []
          avgs[key].append(vals)

          if print_benches:
            print(ft_format_str % tuple(vals))

    if print_benches:
      print ("")

  print ("average")
  print(header_format_str % header_strs)
  for feature in features:
    if feature in scalar_features:

      vals = []
      for i in range(9):
        ivals = [ x[i] for x in avgs[feature] ]
        vals.append ( mean ([ v for v in ivals if v > -0.1 ]) )
      if per_set:
        for i in range(9,(9+(2*len(stats)))):
          ivals = [ x[i] for x in avgs[feature] ]
          if ((i%2) == 1):
            vals.append ( safe_mean ([ v for v in ivals if v > -0.1 ]) )
          else:
            vals.append ( safe_geo_mean ([ v for v in ivals if v > -0.1 ]) )
      else:
        for i in range(9,(9+len(stats))):
          ivals = [ x[i] for x in avgs[feature] ]
          vals.append ( safe_geo_mean ([ v for v in ivals if v > -0.1 ]) )

      print("%-12s " % feature, end='')
      print(ft_format_str % tuple(vals))

    elif feature in site_features:
      for clen in pin_cxt_lengths:
        key  = ("%s-%d" % (feature,clen))

        vals = []
        for i in range(9):
          ivals = [ x[i] for x in avgs[key] ]
          vals.append ( mean ([ v for v in ivals if v > -0.1 ]) )
        if per_set:
          for i in range(9,(9+(2*len(stats)))):
            ivals = [ x[i] for x in avgs[key] ]
            if ((i%2) == 1):
              vals.append ( safe_mean ([ v for v in ivals if v > -0.1 ]) )
            else:
              vals.append ( safe_geo_mean ([ v for v in ivals if v > -0.1 ]) )
        else:
          for i in range(9,(9+len(stats))):
            ivals = [ x[i] for x in avgs[key] ]
            vals.append ( safe_geo_mean ([ v for v in ivals if v > -0.1 ]) )

        print("%-12s " % key, end='')
        print(ft_format_str % tuple(vals))
  print("")

def fix_mean_distance(benches, cfg_name, bwfilter=True):

  for bench in benches:
    mdd = get_mean_distance_dict(bench, cfg_name, bwfilter=bwfilter, \
                                 clean=False)

    nr_recs = len(mdd[BENCH])
    bench_lois = [x[3] for x in mdd[BENCH]]
    bench_loos = [x[4] for x in mdd[BENCH]]

    for feature in mdd:
      if feature in scalar_features:
        fixed = []
        for i,rec in enumerate(mdd[feature]):
          fixed.append( ( rec[0], rec[1], rec[2], rec[3], rec[4],
            safediv(rec[3], bench_lois[i], -1.0),
            safediv(rec[4], bench_loos[i], -1.0)))
        mdd[feature] = fixed
      elif feature in site_features:
        for cxt in mdd[feature]:
          fixed = []
          for i,rec in enumerate(mdd[feature][cxt]):
            fixed.append( ( rec[0], rec[1], rec[2], rec[3], rec[4],
              safediv(rec[3], bench_lois[i], -1.0),
              safediv(rec[4], bench_loos[i], -1.0)))
          mdd[feature][cxt] = fixed

    print("pickling ... ", end='')
    results_dir = get_results_dir(bench, cfg_name)
    cut_pkl_fd = open(('%s.mean_distance_dict.pkl' % (results_dir)), 'wb')
    pickle.dump(mdd, cut_pkl_fd)
    cut_pkl_fd.close()
    print("done.")


def site_map(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_sites.out' % results_dir), 'r')

  smap = {}
  for line in fd:
    if line.isspace():
      break

    pts  = line.split()
    site = int(pts[0])

    smap[site] = {}
    smap[site][PAGE_RSS]     = ( int(pts[1]) * PAGE_SIZE )
    smap[site][LINE_RSS]     = ( int(pts[2]) * CACHE_LINE_SIZE )
    smap[site][TOTAL_HITS]   = int(pts[3])
    smap[site][TOTAL_MISSES] = int(pts[4])
    smap[site][READ_HITS]    = int(pts[5])
    smap[site][READ_MISSES]  = int(pts[6])
    smap[site][WRITE_HITS]   = int(pts[7])
    smap[site][WRITE_MISSES] = int(pts[8])
    smap[site][WRITEBACK]    = int(pts[9])

  fd.close()
  return smap

def site_strs_map(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_site_strs.out' % results_dir), 'r')

  smap = {}
  for line in fd:
    if line.isspace():
      break

    pts  = line.split()
    site = int(pts[0])

    smap[site][PAGE_RSS]     = ( int(pts[1]) * PAGE_SIZE )
    smap[site][LINE_RSS]     = ( int(pts[2]) * CACHE_LINE_SIZE )
    smap[site][TOTAL_HITS]   = int(pts[3])
    smap[site][TOTAL_MISSES] = int(pts[4])
    smap[site][READ_HITS]    = int(pts[5])
    smap[site][READ_MISSES]  = int(pts[6])
    smap[site][WRITE_HITS]   = int(pts[7])
    smap[site][WRITE_MISSES] = int(pts[8])
    smap[site][WRITEBACK]    = int(pts[9])

    smap[site][SYMBOLS] = []
    for sline in fd:
      if sline.isspace():
        break
      addr,sym = sline.split()
      smap[site][SYMBOLS].append((addr, sym))

  fd.close()
  return smap

def obj_report_format_str(stat):
  name, style = stat
  if name in [TOUCHED, ALLOCATED]:
    return "%12d"
  elif name in int_stats:
    if style in [MEAN, STDEV]:
      return "%4.3f"
    else:
      return "%12d"
  else:
    return "%4.3f"

def get_obj_report_stat(record, stat):
  name, style = stat
  stat_val = record[name]
  if name in [TOUCHED, ALLOCATED]:
    return stat_val
  else:
    return stat_val[style_idx[style]]

def format_str(stat):
  if "ratio" in stat:
    return "%4.3f"
  else:
    return "%12d"

def get_object_list(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_obj_info.out' % results_dir), 'r')

  objlist = []
  for line in fd:
    pts = line.split()
    objstats = []
    if cfg_name in phase_cfgs:
      for idx,stat in enumerate(object_phase_info_stats):
        objstats.append(stat[1](pts[idx]))
    else:
      for idx,stat in enumerate(object_info_stats):
        objstats.append(stat[1](pts[idx]))
    objlist.append(tuple(objstats))
  return objlist

def get_object_stats(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_stats.out' % results_dir), 'r')

  obj_stats = {}
  for line in fd:
    if line.startswith("ID:"):

      pts  = line.split()
      site = int(pts[1])
      obj_stats[site] = {}

      obj_stats[site][TOUCHED]     = int(pts[2])
      obj_stats[site][ALLOCATED]   = int(pts[4])
      obj_stats[site][PAGE_RSS]    = get_int_info(next(fd))
      obj_stats[site][LINE_RSS]    = get_int_info(next(fd))
      obj_stats[site][ACCESSES]    = get_int_info(next(fd))
      obj_stats[site][MISSES]      = get_int_info(next(fd))
      obj_stats[site][BANDWIDTH]   = get_float_info(next(fd))
      obj_stats[site][BW_PER_LINE] = get_float_info(next(fd))
      obj_stats[site][PC_HIT_RATE] = get_float_info(next(fd))
      obj_stats[site][RW_RATIO]    = get_float_info(next(fd))

  fd.close()
  return obj_stats

def object_report(bench, cfg_name, report_stats=object_report_stats,
  sortkey=TOUCHED, sortstyle=RAWVAL, revsort=True, cut=50):

  obj_stats = get_object_stats(bench, cfg_name)
  if sortkey in [TOUCHED, ALLOCATED]:
    sorted_stats = sorted(obj_stats.items(), \
                          key=lambda x: x[1][sortkey], \
                          reverse=revsort)
  else:
    sorted_stats = sorted(obj_stats.items(), \
                          key=lambda x: x[1][sortkey][style_idx[sortstyle]], \
                          reverse=revsort)

  print("%-4s %-6s" % ("rank", "id"), end='')
  for stat,style in report_stats:
    x = ("%s-%s" % (stat.lower(), short_style[style].lower()))
    print(" %12s" % x, end='')
  print("")

  for i,ostats in enumerate(sorted_stats[:cut]):
    outstr = ("%-4d %-6d" % (i, ostats[0]))
    for stat in report_stats:
      outstr += (" %12s" % (obj_report_format_str(stat) % \
                (get_obj_report_stat(ostats[1], stat))))
    print(outstr)


def site_info(bench, cfg_name):

  smap = site_map(bench, cfg_name)

  total_rss    = \
  total_reads  = \
  total_writes = \
  total_hits   = \
  total_misses = \
  total_accs   = 0

  for site in smap:
    site_reads  = smap[site][READ_HITS]   + smap[site][READ_MISSES]
    site_writes = smap[site][WRITE_HITS]  + smap[site][WRITE_MISSES]
    site_hits   = smap[site][READ_HITS]   + smap[site][WRITE_HITS]
    site_misses = smap[site][READ_MISSES] + smap[site][WRITE_MISSES]
    site_accs   = site_hits               + site_misses

    total_rss    += smap[site][PAGE_RSS]
    total_reads  += site_reads
    total_writes += site_writes
    total_hits   += site_hits
    total_misses += site_misses
    total_accs   += site_accs

    smap[site][BANDWIDTH] = ( smap[site][READ_MISSES] + \
                              smap[site][WRITEBACK] ) * \
                            CACHE_LINE_SIZE

    smap[site][SITE_HITS_RATIO]      = 0.0 if site_accs == 0 else \
                                     float(site_hits)   / site_accs
    smap[site][SITE_MISSES_RATIO]    = 0.0 if site_accs == 0 else \
                                     float(site_misses) / site_accs
    smap[site][SITE_RDWR_RATIO]      = 0.0 if site_writes == 0 else \
                                     float(site_reads)  / site_writes
    smap[site][SITE_RDWR_MISS_RATIO] = 0.0 if smap[site][WRITE_MISSES] == 0 else \
                                     float(smap[site][READ_MISSES]) / smap[site][WRITE_MISSES]

  for site in smap:
    site_reads  = smap[site][READ_HITS]   + smap[site][READ_MISSES]
    site_writes = smap[site][WRITE_HITS]  + smap[site][WRITE_MISSES]
    site_hits   = smap[site][READ_HITS]   + smap[site][WRITE_HITS]
    site_misses = smap[site][READ_MISSES] + smap[site][WRITE_MISSES]
    site_accs   = site_hits               + site_misses

    smap[site][TOTAL_HITS_RATIO]     = 0.0 if total_hits == 0 else \
                                     float(site_hits)   / total_hits
    smap[site][TOTAL_MISSES_RATIO]   = 0.0 if total_misses == 0 else \
                                     float(site_misses) / total_misses
    smap[site][TOTAL_ACCS_RATIO]     = 0.0 if total_accs == 0 else \
                                     float(site_accs)   / total_accs
    smap[site][PAGE_RSS_RATIO]       = 0.0 if total_rss == 0 else \
                                     float(smap[site][PAGE_RSS]) / total_rss
 
  return smap

def site_report(bench, cfg_name, stats=site_report_stats,
  sortkey=TOTAL_MISSES, revsort=True, cut=50):

  sinfo = site_info(bench, cfg_name)
  sorted_sinfo = sorted(sinfo.items(), \
                        key=lambda x: x[1][sortkey], \
                        reverse=revsort)

  print("%-4s %6s" % ("rank", "id"), end='')
  for stat in site_report_stats:
    x = stat
    if "site_" in stat:
      x = x.replace("site","s")
    if "total_" in stat:
      x = x.replace("total","t")
    if "miss_ratio" in stat:
      x = x.replace("miss_ratio","mr")
    elif "ratio" in stat:
      x = x.replace("ratio","r")

    print(" %12s" % x, end='')
  print("")

  for i,si in enumerate(sorted_sinfo[:cut]):
    outstr = ("%-4d %-6d" % (i, si[0]))
    for stat in stats:
      outstr += (" %12s" % (format_str(stat) % (si[1][stat])))
    print(outstr)

def get_cut_sites(sinfo, rev, cutkey, cut):

  sites = []
  if rev:
    for k in sinfo:
      if sinfo[k][cutkey] < cut:
        sites.append(k)
  else:
    for k in sinfo:
      if sinfo[k][cutkey] >= cut:
        sites.append(k)

  return sites

def get_sort_sites(sinfo, sortkey, revsort, cutkey, cut):

  agg_info = {}
  agg_info[cutkey]       = \
  agg_info[NUM_SITES]    = \
  agg_info[TOTAL_READS]  = \
  agg_info[TOTAL_WRITES] = \
  agg_info[READ_MISSES]  = \
  agg_info[WRITE_MISSES] = \
  agg_info[TOTAL_HITS]   = \
  agg_info[TOTAL_MISSES] = \
  agg_info[TOTAL_ACCS]   = 0

  sorted_sinfo = sorted(sinfo.items(), \
                        key=lambda x: x[1][sortkey], \
                        reverse=revsort)

  for i,si in enumerate(sorted_sinfo):
    if cut != None:
      if cutkey == NUM_SITES:
        tmp = agg_info[NUM_SITES] + 1
      else:
        tmp = (agg_info[cutkey] + si[1][cutkey])
      if tmp > cut:
        break

    site_hits   = si[1][READ_HITS]   + si[1][WRITE_HITS]
    site_misses = si[1][READ_MISSES] + si[1][WRITE_MISSES]
    site_reads  = si[1][READ_HITS]   + si[1][READ_MISSES]
    site_writes = si[1][WRITE_HITS]  + si[1][WRITE_MISSES]
    site_read_misses  = si[1][READ_MISSES]
    site_write_misses = si[1][WRITE_MISSES]

    agg_info[TOTAL_HITS]   += site_hits
    agg_info[TOTAL_MISSES] += site_misses
    agg_info[TOTAL_ACCS]   += (site_hits + site_misses)
    agg_info[TOTAL_READS]  += site_reads
    agg_info[TOTAL_WRITES] += site_writes
    agg_info[READ_MISSES]  += site_read_misses
    agg_info[WRITE_MISSES] += site_write_misses

    agg_info[NUM_SITES] += 1
    agg_info[cutkey] += si[1][cutkey]

  return ([x[0] for x in sorted_sinfo[:i]])

def prob_info(bench, cfg_name, cutstyle=BANDWIDTH_PER_BYTE, \
  feature=SITES, cxt=0, cutrat=1.28, zcutoff=0.1, pre="X", post="Y", \
  bad_fts=set([]), style=OBJECTS):

  otdict  = get_object_cut_info(bench, cfg_name, cutstyle)
  pr_info = {}

  cond = "%s|%s" % (pre, post)
  tz   = cut_str(cutstyle, cutrat)

  total_objects = otdict[ALL][AGG][0]
  total_data    = CLMB( otdict[ALL][AGG][1] )
  if total_objects == 0:
    print("no objects in otdict!")
    return None

  if feature != SITES:
    ot_all_dict = otdict[ALL][feature]
    ot_tz_dict  = otdict[tz][feature]
  else:
    ot_all_dict = otdict[ALL][feature][cxt]
    ot_tz_dict  = otdict[tz][feature][cxt]

  thermos_objects = 0
  thermos_data    = 0
  for ftkey in ot_tz_dict:
    thermos_objects += float(ot_tz_dict[ftkey][0])
    thermos_data    += float(CLMB(ot_tz_dict[ftkey][1]))

  pr_x = (thermos_objects / total_objects)
  dp_x = (thermos_data    / total_data)

  if style == OBJECTS:
    pr_info[pre] = (pr_x, thermos_objects, total_objects)
    #print("  PR(X)   : %6.4f %10d / %10d" % \
    #      (pr_x, thermos_objects, total_objects))
  elif style == DATA:
    pr_info[pre] = (dp_x, thermos_data, total_data)
    #print("  PR(X)   : %6.4f %10d / %10d" % \
    #      (dp_x, thermos_data, total_data))

  if post == "Y":

    pr_y         = 0.0
    pr_x_and_y   = 0.0
    dp_y         = 0.0
    dp_x_and_y   = 0.0

    y_objs       = 0
    x_and_y_objs = 0
    y_data       = 0
    x_and_y_data = 0
    for ftkey in ot_all_dict:
      all_ft_objects = ot_all_dict[ftkey][0]
      all_ft_data    = CLMB(ot_all_dict[ftkey][1])
      
      thm_ft_objects = 0
      thm_ft_data    = 0
      if ftkey in ot_tz_dict:
        thm_ft_objects = ot_tz_dict[ftkey][0]
        thm_ft_data    = CLMB(ot_tz_dict[ftkey][1])

      if thm_ft_objects > 1:
        x_and_y_objs += thm_ft_objects
        pr_x_and_y   += ( float(thm_ft_objects) / total_objects )

        y_objs       += all_ft_objects
        pr_y         += ( float(all_ft_objects) / total_objects )

        x_and_y_data += thm_ft_data
        dp_x_and_y   += ( float(thm_ft_data) / total_data )

        y_data       += all_ft_data
        dp_y         += ( float(all_ft_data) / total_data )

      elif thm_ft_objects == 1:
        y_objs       += ( all_ft_objects-1 )
        pr_y         += ( float(all_ft_objects-1) / total_objects )

        y_data       += ( all_ft_data-thm_ft_data )
        dp_y         += ( float(all_ft_data-thm_ft_data) / total_data )

    if y_objs == 0:
      if x_and_y_objs != 0:
        print ("pr_y should not be 0")
      else:
        pr_x_given_y = 0.0
        dp_x_given_y = 0.0
    else:
      pr_x_given_y = ((pr_x_and_y) / pr_y)
      dp_x_given_y = ((dp_x_and_y) / dp_y)

    if style == OBJECTS:
      pr_info[cond] = (pr_x_given_y, x_and_y_objs, y_objs)
      #print("  PR(X|Y) : %6.4f %10d / %10d" % \
      #      (pr_x_given_y, x_and_y_objs, y_objs))
    elif style == DATA:
      pr_info[cond] = (dp_x_given_y, x_and_y_data, y_data)
      #print("  PR(X|Y) : %6.4f %10d / %10d" % \
      #      (dp_x_given_y, x_and_y_data, y_data))

  if post == "Z":
    pr_z         = 0.0
    pr_x_and_z   = 0.0
    dp_z         = 0.0
    dp_x_and_z   = 0.0

    z_objs       = 0
    x_and_z_objs = 0
    z_data       = 0
    x_and_z_data = 0
    for ftkey in ot_all_dict:
      all_ft_objects = ot_all_dict[ftkey][0]
      all_ft_data    = CLMB(ot_all_dict[ftkey][1])
      all_ft_sizes   = ot_all_dict[ftkey][7]

      thm_ft_objects = 0
      thm_ft_data    = 0
      thm_ft_sizes   = []
      if ftkey in ot_tz_dict:
        thm_ft_objects = ot_tz_dict[ftkey][0]
        thm_ft_data    = CLMB(ot_tz_dict[ftkey][1])
        thm_ft_sizes   = ot_tz_dict[ftkey][7]

      if all_ft_data == 0:
        bad_fts.add((bench, feature, ftkey, all_ft_objects, all_ft_data))
        #print("no ft data! ft: %d %d %d" % \
        #      (ft, all_ft_objects, all_ft_data))
        continue

      #print ("ft: %d all: %16.8f" % (ftkey,all_ft_data))
      for size in thm_ft_sizes:
        dat = CLMB(size)

        if (float(thm_ft_data - dat) / all_ft_data) > zcutoff:
          x_and_z_objs += 1
          pr_x_and_z   += ( 1.0 / total_objects )

          x_and_z_data += dat
          dp_x_and_z   += ( dat / total_data )

      for size in all_ft_sizes:
        dat = CLMB(size)

        if (float(thm_ft_data - dat) / all_ft_data) > zcutoff:
          z_objs += 1
          pr_z   += ( 1.0 / total_objects )

          z_data += dat
          dp_z   += ( dat / total_data )

    if z_objs == 0:
      if x_and_z_objs != 0:
        print ("pr_z should not be 0")
      else:
        pr_x_given_z = 0.0
        dp_x_given_z = 0.0
    else:
      pr_x_given_z = ((pr_x_and_z) / pr_z)
      dp_x_given_z = ((dp_x_and_z) / dp_z)

    if style == OBJECTS:
      pr_info[cond] = (pr_x_given_z, x_and_z_objs, z_objs)
      #print("  PR(X|Z) : %6.4f %10d / %10d" % \
      #      (pr_x_given_z, x_and_z_objs, z_objs))
    elif style == DATA:
      pr_info[cond] = (dp_x_given_z, x_and_z_data, z_data)
      #print("  PR(X|Z) : %6.4f %10d / %10d" % \
      #      (dp_x_given_z, x_and_z_data, z_data))

  return pr_info

def prob_info_report(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrat=1.28, zcutoff=0.1, feature=SITES, cxt=0, pre="X", post="Y", \
  style=DATA):

  header_strs = [
    "T: total objects in the benchmark",
    "X: object is in the set",
    "Y: object from the same site is in the set",
    ("Z: at least %1.2f%% of the data from the same site is in the set" % \
     (zcutoff*100.0))
  ]

  print(header_strs[0])
  for s in header_strs[1:]:
    if pre == s[0] or post == s[0]:
      print(s)

  print("")
  print("%-20s %8s %10s %10s %8s %10s %10s" % \
        ( ("bench", ("P(%s)" % pre), ("%s" % pre), \
            "T", ("P(%s|%s)" % (pre, post)), \
            ("%s&%s" % (pre, post)), ("%s"%post)) )
       )

  cond = ("%s|%s" % (pre, post))
  avgs = []
  bad_fts = set([])
  for bench in benches:
    for cfg_name in cfgs:
      prob = prob_info(bench, cfg_name, cutstyle=cutstyle, cutrat=cutrat, \
                       zcutoff=zcutoff, feature=feature, cxt=cxt, pre=pre, \
                       post=post, bad_fts=bad_fts, style=style)
      avgs.append(prob)
      print ("%-20s " % bench, end='')
      print ("%8.4f " % prob[pre][0], end='')
      print ("%10d "  % prob[pre][1], end='')
      print ("%10d "  % prob[pre][2], end='')
      print ("%8.4f " % prob[cond][0], end='')
      print ("%10d "  % prob[cond][1], end='')
      print ("%10d "  % prob[cond][2], end='')
      print ("")
  
  print (("%-20s " % "average"), end='') 
  print ("%8.4f " % safe_geo_mean([x[pre][0] for x in avgs]), end='')
  print ("%10d "  % mean([x[pre][1] for x in avgs]), end='')
  print ("%10d "  % mean([x[pre][2] for x in avgs]), end='')
  print ("%8.4f " % safe_geo_mean([x[cond][0] for x in avgs]), end='')
  print ("%10d "  % mean([x[cond][1] for x in avgs]), end='')
  print ("%10d "  % mean([x[cond][2] for x in avgs]), end='')
  print("")

  print (("%-20s " % "median"), end='') 
  print ("%8.4f " % median([x[pre][0] for x in avgs]), end='')
  print ("%10d "  % median([x[pre][1] for x in avgs]), end='')
  print ("%10d "  % median([x[pre][2] for x in avgs]), end='')
  print ("%8.4f " % median([x[cond][0] for x in avgs]), end='')
  print ("%10d "  % median([x[cond][1] for x in avgs]), end='')
  print ("%10d "  % median([x[cond][2] for x in avgs]), end='')
  print("")

  if bad_fts:
    print("")
    print("bad features:")
    for bft in bad_fts:
      print("%-20s %12s %8d %12d %12d" % bft)

def get_feature_bucket_len(bucket, style):
  if style == "ot":
    return len(bucket.keys())
  elif style == "ft":
    return len(bucket[10])
  raise SystemExit(1)

def get_class_feature_stats(info, otdict, tz, pref, phase_cfg):

  info[(pref+"-sizes")]   = get_feature_bucket_len ( otdict[tz][SIZE], pref )
  info[(pref+"-szbckts")] = get_feature_bucket_len ( otdict[tz][SIZE_BUCKET], pref )
  info[(pref+"-tsigs")]   = get_feature_bucket_len ( otdict[tz][TYPE_SIG], pref )
  info[(pref+"-asigs")]   = get_feature_bucket_len ( otdict[tz][ACC_SIG], pref )
  for cxt in pin_cxt_lengths:
    info[(pref+("-scxt%d"%cxt))] = get_feature_bucket_len ( otdict[tz][SITES][cxt], pref )

  if phase_cfg:
    info[(pref+"-aphs")]   = get_feature_bucket_len ( otdict[tz][ALLOC_PHASE], pref )
    info[(pref+"-typesz")] = get_feature_bucket_len ( otdict[tz][TYPE_SIZE], pref )
    info[(pref+"-phsigs")] = get_feature_bucket_len ( otdict[tz][PHASE_SIG], pref )
    info[(pref+"-phsz")]   = get_feature_bucket_len ( otdict[tz][PHASE_SIZE], pref )
    for cxt in pin_cxt_lengths:
      info[(pref+("-phsite%d"%cxt))] = get_feature_bucket_len ( otdict[tz][PHASE_SITES][cxt], pref )
      info[(pref+("-phszst%d"%cxt))] = get_feature_bucket_len ( otdict[tz][PHSZ_SITES][cxt], pref )


  info[(pref+"-szs-r")]  = safediv ( info[(pref+"-sizes")]   , info["sizes"]   )
  info[(pref+"-zbs-r")]  = safediv ( info[(pref+"-szbckts")] , info["szbckts"] )
  info[(pref+"-tsig-r")] = safediv ( info[(pref+"-tsigs")]   , info["tsigs"]   )
  info[(pref+"-asig-r")] = safediv ( info[(pref+"-asigs")]   , info["asigs"]   )
  for cxt in pin_cxt_lengths:
    info[(pref+("-scxt%d-r"%cxt))] = safediv ( info[(pref+("-scxt%d"%cxt))] , info[("scxt%d"%cxt)] )

  if phase_cfg:
    info[(pref+"-aphs-r")]   = safediv ( info[(pref+"-aphs")]    , info["aphs"] )
    info[(pref+"-phsigs-r")] = safediv ( info[(pref+"-phsigs")]  , info["phsigs"] )
    info[(pref+"-tzs-r")]    = safediv ( info[(pref+"-typesz")]  , info["typesz"] )
    info[(pref+"-pzs-r")]    = safediv ( info[(pref+"-phsz")]    , info["phsz"]   )
    for cxt in pin_cxt_lengths:
      info[(pref+("-p%d-r"%cxt))] = safediv ( info[(pref+("-phsite%d"%cxt))] , info[("phsite%d"%cxt)] )
      info[(pref+("-k%d-r"%cxt))] = safediv ( info[(pref+("-phszst%d"%cxt))] , info[("phszst%d"%cxt)] )


def get_object_feature_stats(info, otdict, ftdict, tz, feature, pref, \
  cxt=None, cmptz=None):

  if feature in site_features:

    info[(pref+"-objs")]    = ftdict[tz][feature][cxt][0]
    info[(pref+"-data")]    = CLMB ( ftdict[tz][feature][cxt][1] )
    info[(pref+"-pre-RD")]  = ftdict[tz][feature][cxt][2]
    info[(pref+"-pre-WR")]  = ftdict[tz][feature][cxt][3]
    info[(pref+"-post-RD")] = (MB(ftdict[tz][feature][cxt][4]))
    info[(pref+"-post-WR")] = (MB(ftdict[tz][feature][cxt][5]))
    info[(pref+"-bw")]      = (info[(pref+"-post-RD")] + info[(pref+"-post-WR")])

  else:

    info[(pref+"-objs")]    = ftdict[tz][feature][0]
    info[(pref+"-data")]    = CLMB ( ftdict[tz][feature][1] )
    info[(pref+"-pre-RD")]  = ftdict[tz][feature][2]
    info[(pref+"-pre-WR")]  = ftdict[tz][feature][3]
    info[(pref+"-post-RD")] = (MB(ftdict[tz][feature][4]))
    info[(pref+"-post-WR")] = (MB(ftdict[tz][feature][5]))
    info[(pref+"-bw")]      = (info[(pref+"-post-RD")] + info[(pref+"-post-WR")])

  info[(pref+"-rdwr")]    = safediv ( info[(pref+"-pre-RD")]  , info[(pref+"-pre-WR")], default=-1.0 )
  info[(pref+"-bwpb")]    = safediv ( info[(pref+"-bw")]      , info[(pref+"-data")],   default=-1.0 )
  info[(pref+"-wrpb")]    = safediv ( info[(pref+"-post-WR")] , info[(pref+"-data")],   default=-1.0 )

  info[(pref+"-obj-r")]   = safediv ( info[(pref+"-objs")]    , info["objects"] )
  info[(pref+"-data-r")]  = safediv ( info[(pref+"-data")]    , info["data"]    )
  info[(pref+"-bw-r")]    = safediv ( info[(pref+"-bw")]      , info["bw"]      )
  info[(pref+"-wr-r")]    = safediv ( info[(pref+"-post-WR")] , info["post-WR"]  )
 
  if feature != AGG:
    get_feature_accuracy(info, otdict, ftdict, tz, feature, pref,
                         cmptz=cmptz, cxt=cxt) 


def get_feature_accuracy(info, otdict, ftdict, tz, feature, pref, cxt=None,
  cmptz=None):

  ot_all_objs     = info["objects"]
  ot_all_data     = info["data"]
  ot_all_bw       = info["bw"]
  ot_all_reads    = info["pre-RD"]
  ot_all_writes   = info["pre-WR"]
  ot_all_life     = info["dyninst"]

  total_pos_objs  = total_neg_objs = 0
  total_pos_data  = total_neg_data = 0
  total_pos_bw    = total_neg_bw = 0
  ft_tp_objs = ft_fp_objs = ft_tn_objs = ft_fn_objs = 0
  ft_tp_data = ft_fp_data = ft_tn_data = ft_fn_data = 0
  ft_tp_bw   = ft_fp_bw   = ft_tn_bw   = ft_fn_bw   = 0

  if cmptz == None:
    cmptz = tz

  if feature == BENCH:
    pos_objs   = otdict[cmptz][AGG][0]
    pos_data   = CLMB ( otdict[cmptz][AGG][1] )
    pos_reads  = otdict[cmptz][AGG][2]
    pos_writes = otdict[cmptz][AGG][3]
    pos_bw     = ( MB ( otdict[cmptz][AGG][4] + \
                        otdict[cmptz][AGG][5] ) )
    pos_life   = otdict[cmptz][AGG][9]

    neg_objs   = ( ot_all_objs   - pos_objs   )
    neg_data   = ( ot_all_data   - pos_data   )
    neg_reads  = ( ot_all_reads  - pos_reads  )
    neg_writes = ( ot_all_writes - pos_writes )
    neg_bw     = ( ot_all_bw     - pos_bw     )
    neg_life   = ( ot_all_life   - pos_life   )

    total_pos_objs += pos_objs
    total_neg_objs += neg_objs
    total_pos_data += pos_data
    total_neg_data += neg_data
    total_pos_bw   += pos_bw
    total_neg_bw   += neg_bw

    if len(ftdict[tz][BENCH][10]) > 0:
      ft_tp_objs += pos_objs
      ft_fp_objs += neg_objs

      ft_tp_data += pos_data
      ft_fp_data += neg_data

      ft_tp_bw   += pos_bw
      ft_fp_bw   += neg_bw

    else:
      ft_tn_objs += neg_objs
      ft_fn_objs += pos_objs

      ft_tn_data += neg_data
      ft_fn_data += pos_data

      ft_tn_bw   += neg_bw
      ft_fn_bw   += pos_bw

  else:
    if not feature in site_features:
      ot_all_dict = otdict[ALL][feature]
      ot_tz_dict  = otdict[cmptz][feature]
      ft_tz_dict  = ftdict[tz][feature]
    else:
      ot_all_dict = otdict[ALL][feature][cxt]
      ot_tz_dict  = otdict[cmptz][feature][cxt]
      ft_tz_dict  = ftdict[tz][feature][cxt]
      #print("cxt: %d keys: %s" % (cxt, str(ft_tz_dict)))

    ftset = set(ft_tz_dict[10])
    for ftkey in ot_all_dict:
      pos_objs    = \
      pos_data    = \
      pos_bw      = \
      pos_reads   = \
      pos_writes  = \
      pos_life    = 0

      if ftkey in ot_tz_dict:
        pos_objs   = ot_tz_dict[ftkey][0]
        pos_data   = CLMB ( ot_tz_dict[ftkey][1] )
        pos_reads  = ot_tz_dict[ftkey][2]
        pos_writes = ot_tz_dict[ftkey][3]
        pos_bw     = ( MB ( ot_tz_dict[ftkey][4] + \
                            ot_tz_dict[ftkey][5] ) )
        pos_life   = ot_tz_dict[ftkey][9]

      neg_objs   = ( ot_all_dict[ftkey][0]            - pos_objs   )
      neg_data   = ( (CLMB ( ot_all_dict[ftkey][1] )) - pos_data   )
      neg_reads  = ( ot_all_dict[ftkey][2]            - pos_reads  )
      neg_writes = ( ot_all_dict[ftkey][3]            - pos_writes )
      neg_bw     = ( (MB ( ot_all_dict[ftkey][4] + ot_all_dict[ftkey][5] ) ) - pos_bw )
      neg_life   = ( ot_all_dict[ftkey][9]            - pos_life   )


      total_pos_objs += pos_objs
      total_neg_objs += neg_objs
      total_pos_data += pos_data
      total_neg_data += neg_data
      total_pos_bw   += pos_bw
      total_neg_bw   += neg_bw
      #if feature == SITES:
      #  print ("  site: %8d pos: %12d neg: %12d in: %s" % \
      #         (ftkey, pos_objs, neg_objs, (ftkey in ft_tz_dict)))
      if ftkey in ftset:
        ft_tp_objs += pos_objs
        ft_fp_objs += neg_objs

        ft_tp_data += pos_data
        ft_fp_data += neg_data

        ft_tp_bw   += pos_bw
        ft_fp_bw   += neg_bw

      else:
        ft_tn_objs += neg_objs
        ft_fn_objs += pos_objs

        ft_tn_data += neg_data
        ft_fn_data += pos_data

        ft_tn_bw   += neg_bw
        ft_fn_bw   += pos_bw

#  print (tz + " " + str(str2tz(tz)))
#  if feature == SITES and str2tz(tz) == 0.64:
#    print ("rar: %8d %12d %12d %12d" % (cxt, ft_tp_objs, ft_tn_objs, ot_all_objs))
#    print ("")

  info[(pref+"-otps")]  = ft_tp_objs
  info[(pref+"-otns")]  = ft_tn_objs
  info[(pref+"-ofps")]  = ft_fp_objs
  info[(pref+"-ofns")]  = ft_fn_objs
  info[(pref+"-oaps")]  = total_pos_objs
  info[(pref+"-oans")]  = total_neg_objs

  info[(pref+"-otpr")]  = safediv ( ft_tp_objs, total_pos_objs )
  info[(pref+"-otnr")]  = safediv ( ft_tn_objs, total_neg_objs )
  info[(pref+"-ofpr")]  = safediv ( ft_fp_objs, total_neg_objs )
  info[(pref+"-ofnr")]  = safediv ( ft_fn_objs, total_pos_objs )
  info[(pref+"-oacc")]  = safediv ( (ft_tp_objs + ft_tn_objs), ot_all_objs )
  info[(pref+"-onacc")] = safediv ( (ft_fp_objs + ft_fn_objs), ot_all_objs )
  info[(pref+"-oppv")]  = safediv ( ft_tp_objs, (ft_tp_objs + ft_fp_objs) )
  info[(pref+"-onpv")]  = safediv ( ft_tn_objs, (ft_tn_objs + ft_fn_objs) )

  info[(pref+"-dtps")]  = ft_tp_data
  info[(pref+"-dtns")]  = ft_tn_data
  info[(pref+"-dfps")]  = ft_fp_data
  info[(pref+"-dfns")]  = ft_fn_data
  info[(pref+"-daps")]  = total_pos_data
  info[(pref+"-dans")]  = total_neg_data

  info[(pref+"-dtpr")]  = safediv ( ft_tp_data, total_pos_data )
  info[(pref+"-dtnr")]  = safediv ( ft_tn_data, total_neg_data )
  info[(pref+"-dfpr")]  = safediv ( ft_fp_data, total_neg_data )
  info[(pref+"-dfnr")]  = safediv ( ft_fn_data, total_pos_data )
  info[(pref+"-dacc")]  = safediv ( (ft_tp_data + ft_tn_data), ot_all_data )
  info[(pref+"-dnacc")] = safediv ( (ft_fp_data + ft_fn_data), ot_all_data )
  info[(pref+"-dppv")]  = safediv ( ft_tp_data, (ft_tp_data + ft_fp_data) )
  info[(pref+"-dnpv")]  = safediv ( ft_tn_data, (ft_tn_data + ft_fn_data) )

  info[(pref+"-bwtps")]  = ft_tp_bw
  info[(pref+"-bwtns")]  = ft_tn_bw
  info[(pref+"-bwfps")]  = ft_fp_bw
  info[(pref+"-bwfns")]  = ft_fn_bw
  info[(pref+"-bwaps")]  = total_pos_bw
  info[(pref+"-bwans")]  = total_neg_bw

  info[(pref+"-bwtpr")]  = safediv ( ft_tp_bw, total_pos_bw )
  info[(pref+"-bwtnr")]  = safediv ( ft_tn_bw, total_neg_bw )
  info[(pref+"-bwfpr")]  = safediv ( ft_fp_bw, total_neg_bw )
  info[(pref+"-bwfnr")]  = safediv ( ft_fn_bw, total_pos_bw )
  info[(pref+"-bwacc")]  = safediv ( (ft_tp_bw + ft_tn_bw), ot_all_bw )
  info[(pref+"-bwnacc")] = safediv ( (ft_fp_bw + ft_fn_bw), ot_all_bw )
  info[(pref+"-bwppv")]  = safediv ( ft_tp_bw, (ft_tp_bw + ft_fp_bw) )
  info[(pref+"-bwnpv")]  = safediv ( ft_tn_bw, (ft_tn_bw + ft_fn_bw) )


def feature_object_cut_info(bench, cfg_name, cutstyle, \
  cutpct, otdict=None, ftdict=None, train=False,
  cmppct=None, clean=False, fail_on_clean=False):

  if not clean:
    if cmppct == None:
      trainstr    = "train" if train else "ref"
      results_dir = get_results_dir(bench, cfg_name)
      pkl_file    = ('%s.foc_info_%s_%4.4f_%s.pkl' % \
                    (results_dir, cutstyle.lower(), cutpct, trainstr))
      if os.path.exists(pkl_file):
        pkl_fd = open(pkl_file, 'rb')
        info   = pickle.load(pkl_fd)
        return info
      else:
        if fail_on_clean:
          return None
        else:
          print ("could not find pkl_file: %s" % pkl_file)
    else:
      trainstr    = "train" if train else "ref"
      results_dir = get_results_dir(bench, cfg_name)
      pkl_file    = ('%s.foc_info_%s_%4.4f_%4.4f_%s.pkl' % \
                    (results_dir, cutstyle.lower(), cutpct, cmppct, trainstr))
      if os.path.exists(pkl_file):
        pkl_fd = open(pkl_file, 'rb')
        info   = pickle.load(pkl_fd)
        return info
      else:
        if fail_on_clean:
          return None
        else:
          print ("could not find pkl_file: %s" % pkl_file)

  if otdict == None:
    otdict = get_object_cut_info(bench, cfg_name, cutstyle)
  if ftdict == None:
    ftdict = get_feature_cut_info(bench, cfg_name, cutstyle, train=train)

  tz    = cut_str(cutstyle, cutpct)
  cmptz = cut_str(cutstyle, cmppct) if cmppct != None else None

  phase_cfg = False
  if cfg_name in phase_cfgs:
    phase_cfg = True

  info = {}
  info["objects"]    = otdict[ALL][AGG][0]
  info["data"]       = CLMB ( otdict[ALL][AGG][1] )
  info["pre-RD"]     = otdict[ALL][AGG][2]
  info["pre-WR"]     = otdict[ALL][AGG][3]
  info["post-RD"]    = MB(otdict[ALL][AGG][4])
  info["post-WR"]    = MB(otdict[ALL][AGG][5])
  info["bw"]         = (info["post-RD"] + info["post-WR"])
  info["pc-accs"]    = (otdict[ALL][AGG][2] + otdict[ALL][AGG][3])
  info["pc-hits"]    = (otdict[ALL][AGG][6])
  info["pc-rate"]    = safediv ( info["pc-hits"] , info["pc-accs"], default=-1.0)
  info["mc-accs"]    = otdict[ALL][AGG][7]
  info["mc-hits"]    = otdict[ALL][AGG][8]
  info["mc-rate"]    = safediv ( info["mc-hits"] , info["mc-accs"], default=-1.0)
  info["rdwr"]       = safediv ( info["pre-RD"]  , info["pre-WR"], default=-1.0 )
  info["bwpb"]       = safediv ( info["bw"]      , info["data"],   default=-1.0 )
  info["wrpb"]       = safediv ( info["post-WR"] , info["data"],   default=-1.0 )
  #print (otdict.keys())
  #print (otdict[ALL].keys())
  info["dyninst"]    = (otdict[ALL][LIFETIME]/1000)
  info["BPKI"]       = safediv ( (info["bw"]*1024*1024) , info["dyninst"], default=-1.0 )
  info["MPKI"]       = safediv ( (info["pre-RD"] + info["pre-WR"]) , info["dyninst"], default=-1.0 )

  info["sizes"]      = len ( otdict[ALL][SIZE].keys() )
  info["szbckts"]    = len ( otdict[ALL][SIZE_BUCKET].keys() )
  info["tsigs"]      = len ( otdict[ALL][TYPE_SIG].keys() )
  info["asigs"]      = len ( otdict[ALL][ACC_SIG].keys() )
  for cxt in pin_cxt_lengths:
    info[("scxt%d"%cxt)] = len ( otdict[ALL][SITES][cxt].keys() )

  if phase_cfg:
    info["aphs"]     = len ( otdict[ALL][ALLOC_PHASE].keys() )
    info["phsigs"]   = len ( otdict[ALL][PHASE_SIG].keys() )
    info["typesz"]   = len ( otdict[ALL][TYPE_SIZE].keys() )
    info["phsz"]     = len ( otdict[ALL][PHASE_SIZE].keys() )
    for cxt in pin_cxt_lengths:
      info[("phsite%d"%cxt)] = len ( otdict[ALL][PHASE_SITES][cxt].keys() )
      info[("phszst%d"%cxt)] = len ( otdict[ALL][PHSZ_SITES][cxt].keys() )

  get_class_feature_stats(info, otdict, tz, "ot", phase_cfg)
  get_class_feature_stats(info, ftdict, tz, "ft", phase_cfg)

  get_object_feature_stats(info, otdict, otdict, tz,  AGG,         "ot", cmptz=cmptz)
  get_object_feature_stats(info, otdict, ftdict, ALL, AGG,         "ft", cmptz=cmptz)
  get_object_feature_stats(info, otdict, ftdict, tz,  BENCH,       "bt", cmptz=cmptz)
  get_object_feature_stats(info, otdict, ftdict, tz,  SIZE,        "zt", cmptz=cmptz)
  get_object_feature_stats(info, otdict, ftdict, tz,  SIZE_BUCKET, "yt", cmptz=cmptz)
  get_object_feature_stats(info, otdict, ftdict, tz,  TYPE_SIG,    "lt", cmptz=cmptz)
  get_object_feature_stats(info, otdict, ftdict, tz,  ACC_SIG,     "at", cmptz=cmptz)
  for cxt in pin_cxt_lengths:
    get_object_feature_stats(info, otdict, ftdict, tz, SITES, ("c%d"%cxt), cxt, cmptz=cmptz)

  if phase_cfg:
    get_object_feature_stats(info, otdict, ftdict, tz, ALLOC_PHASE, "pt", cmptz=cmptz)
    get_object_feature_stats(info, otdict, ftdict, tz, PHASE_SIG,   "nt", cmptz=cmptz)
    get_object_feature_stats(info, otdict, ftdict, tz, TYPE_SIZE,   "tt", cmptz=cmptz)
    get_object_feature_stats(info, otdict, ftdict, tz, PHASE_SIZE,  "it", cmptz=cmptz)
    for cxt in pin_cxt_lengths:
      get_object_feature_stats(info, otdict, ftdict, tz, PHASE_SITES, \
                               ("p%d"%cxt), cxt, cmptz=cmptz)
      get_object_feature_stats(info, otdict, ftdict, tz, PHSZ_SITES, \
                               ("k%d"%cxt), cxt, cmptz=cmptz)


  if cmppct == None:
    trainstr    = "train" if train else "ref"
    results_dir = get_results_dir(bench, cfg_name)
    pkl_file    = ('%s.foc_info_%s_%4.4f_%s.pkl' % \
                  (results_dir, cutstyle.lower(), cutpct, trainstr))
  else:
    trainstr    = "train" if train else "ref"
    results_dir = get_results_dir(bench, cfg_name)
    pkl_file    = ('%s.foc_info_%s_%4.4f_%4.4f_%s.pkl' % \
                  (results_dir, cutstyle.lower(), cutpct, cmppct, trainstr))

  pkl_fd = open(pkl_file, 'wb')
  pickle.dump(info, pkl_fd)
  pkl_fd.close()

  return info

def print_mcstats(benches, mc_cfg):

  print("%20s %12s %12s %9s %12s %12s %12s %9s" % \
        ("bench", "mc-accs", "mc-hits",
         "mc-rate", "mc-rdm", "mc-wrb",
         "mc-bw", "mc-wrbr"))

  for bench in benches:
    mc_stats = get_mc_stats_agg(bench, mc_cfg)

    mc_accs  = (mc_stats[3] + mc_stats[4])
    mc_hits  = (mc_stats[3])
    mc_rate  = ( safediv (mc_hits, mc_accs, -1.0) )
    mc_rdm   = (mc_stats[6])
    mc_wrb   = (mc_stats[9])
    mc_bw    = (mc_rdm + mc_wrb)
    mc_wrbr  = ( safediv ( mc_wrb, mc_bw ) )

    print ("%20s %12d %12d %9.4f %12d %12d %12d %9.4f" % \
           (bench, mc_accs, mc_hits, mc_rate,
            mc_rdm, mc_wrb, mc_bw, mc_wrbr))

def get_size_ratio(bench, cfg_name, it=0, copy=0):
  cfg.read_cfg(cfg_name)

  ins = 0
  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  #total_heap = total_unk = total_all = 0
  total_heap = total_unk = total_all = []
  for run in runs:
    ins_count_fd = open( ("%sins_count.out" % \
                         (get_run_results_dir(bench,cfg_name, it, copy, run))
                       ) )

    for line in ins_count_fd:
      if line.startswith("HEAP"):
        total_heap += [( int(line.split()[6]) )]
        #total_heap += ( int(line.split()[5]) )
        #total_heap += ( int(line.split()[9]) + int(line.split()[12]) )

        line = next(ins_count_fd)
        total_unk  += [( int(line.split()[6]) )]
        #total_unk  += ( int(line.split()[5]) )
        #total_unk  += ( int(line.split()[9]) + int(line.split()[12]) )

        line = next(ins_count_fd)
        total_all  += [( int(line.split()[6]) )]
        #total_all  += ( int(line.split()[5]) )
        #total_all  += ( int(line.split()[9]) + int(line.split()[12]) )

    ins_count_fd.close()

  return (float(MB(max(total_heap)*4*1024)))
  #return (float(CLMB(total_heap)))
  #return (float(total_heap) / (total_all))


def feature_object_cut_compare(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutpct=7.68, train=False, stats=class_stats, clean=False):

  print("")
  header_pts = ["bench"]
  format_pts = ["%-16s"]
  for hname,hfmt,dfmt in stats:
    header_pts.append(hname) 
    format_pts.append(hfmt)

  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  avgs = {}
  for stat in stats:
    avgs[stat] = []

  for bench in benches:
    for cfg_name in cfgs:
      socinfo = feature_object_cut_info(bench, cfg_name, cutstyle, cutpct,\
                                        train=train, clean=clean)

      format_str  = " ".join( ["%-16s"] + [ x[2] for x in stats ] )
      report_info = tuple ( [bench] + [ socinfo[x[0]] for x in stats ] )
      print( format_str % report_info )

      for stat in stats:
        if socinfo[stat[0]] > -0.01:
          avgs[stat].append(socinfo[stat[0]])


  for stat in avgs:
    if stat[0] in geomean_agg_stats:
      avgs[stat] = (stat[2] % safe_geo_mean(avgs[stat]))
    elif stat[0] in no_agg_stats:
      avgs[stat] = (stat[1] % "-")
    else:
      avgs[stat] = (stat[2] % mean(avgs[stat]))

  avgs_str = "%-16s"%"average"
  for stat in stats:
    avgs_str += (" " + avgs[stat])
  print( avgs_str )
  print("")

def multi_cut_compare(benches, cfgs, train=False, \
  cutstyle=BANDWIDTH_PER_BYTE, cmppct=None, \
  cutrats=def_bwpb_rats, stats=acc_stats, clean=False):

  avgs = {}
  for rat in cutrats:
    avgs[rat] = {}
    for stat in stats:
      avgs[rat][stat] = []

  focinfo = {}
  if not clean:
    for bench in benches:
      for cfg_name in cfgs:
        for rat in cutrats:
          key = ("%s-%s-%s"%(bench,cfg_name,str(rat)))
          foc = feature_object_cut_info(bench, cfg_name, cutstyle, rat,
                                        cmppct=cmppct, train=train, clean=clean,
                                        fail_on_clean=True)
          if foc != None:
            focinfo[key] = foc

  for bench in benches:
    for cfg_name in cfgs:
      otdict = ftdict = None
      for rat in cutrats:
        key = ("%s-%s-%s"%(bench,cfg_name,str(rat)))
        if not key in focinfo:
          if otdict == None:
            otdict = get_object_cut_info(bench, cfg_name, cutstyle)
          if ftdict == None:
            ftdict = get_feature_cut_info(bench, cfg_name, cutstyle, train=train)

          focinfo[key] = feature_object_cut_info(bench, cfg_name, cutstyle, rat, \
                                                 otdict=otdict, ftdict=ftdict, \
                                                 cmppct=cmppct, train=train, \
                                                 clean=True)
      print ("%s-%s" % (bench, cfg_name))

  print("")
  header_pts = ["ratio"]
  format_pts = ["%-12s"]
  for hname,hfmt,dfmt in stats:
    header_pts.append(hname) 
    format_pts.append(hfmt)

  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  for rat in cutrats:
    for bench in benches:
      for cfg_name in cfgs:
        key = ("%s-%s-%s"%(bench,cfg_name,str(rat)))
        for stat in stats:
          if focinfo[key][stat[0]] > -0.01:
            avgs[rat][stat].append(focinfo[key][stat[0]])

    for stat in avgs[rat]:
      if stat[0] in geomean_agg_stats:
        avgs[rat][stat] = (stat[2] % safe_geo_mean(avgs[rat][stat]))
      elif stat[0] in no_agg_stats:
        avgs[rat][stat] = (stat[1] % "-")
      else:
        avgs[rat][stat] = (stat[2] % mean(avgs[rat][stat]))

    if cutstyle == LIFETIME:
      ratstr = ("%-12d"%int(rat))
    else:
      ratstr = ("%-12s"%rat)
    for stat in stats:
      ratstr += (" " + avgs[rat][stat])
    print( ratstr )
  print("")


def get_val(vdict, style):
  if style == OBJECTS:
    return vdict[0]
  elif style == DATA:
    return (CLMB(vdict[1]))
  elif style == BANDWIDTH:
    return ((vdict[4]+vdict[5]) / (CLMB(vdict[1])))
  return None


def trcmp_info_helper(trcmp_ref, trcmp_train, trcmp_diff, \
  ref_dict, train_dict, train2ref, feature, pp=False):

  trcmp_ref["vals"]       = len (ref_dict.keys())
  trcmp_ref["objs"]       = 0
  trcmp_ref["bw"]         = 0
  trcmp_ref["data"]       = 0
  trcmp_ref["mat-vals"]   = 0
  trcmp_ref["unm-vals"]   = 0
  trcmp_ref["mat-objs"]   = 0
  trcmp_ref["unm-objs"]   = 0
  trcmp_ref["mat-bw"]     = 0
  trcmp_ref["unm-bw"]     = 0
  trcmp_ref["mat-data"]   = 0
  trcmp_ref["unm-data"]   = 0

  trcmp_train["vals"]     = len (train_dict.keys())
  trcmp_train["objs"]     = 0
  trcmp_train["bw"]       = 0
  trcmp_train["data"]     = 0
  trcmp_train["mat-vals"] = 0
  trcmp_train["unm-vals"] = 0
  trcmp_train["mat-objs"] = 0
  trcmp_train["unm-objs"] = 0
  trcmp_train["mat-bw"]   = 0
  trcmp_train["unm-bw"]   = 0
  trcmp_train["mat-data"] = 0
  trcmp_train["unm-data"] = 0

  trcmp_diff["bwpb-avg"]  = []
  trcmp_diff["bwpb-abs"]  = []

  for tkey in train_dict:

    train_objs = train_dict[tkey][0]
    train_data = CLMB ( train_dict[tkey][1] )
    train_bw   = MB ( (train_dict[tkey][4] + train_dict[tkey][5]) )
    train_bwpb = safediv ( train_bw, train_data )

    trcmp_train["objs"] += train_objs
    trcmp_train["bw"]   += train_bw
    trcmp_train["data"] += train_data

    train_match = False
    if tkey in train2ref:
      if feature in phase_features:
        if any ( x in ref_dict for x in train2ref[tkey] ):
          train_match = True
          ref_objs = ref_data = ref_bw = 0
          for rkey in train2ref[tkey]:
            if rkey in ref_dict:
              ref_objs += ref_dict[rkey][0]
              ref_data += CLMB ( ref_dict[rkey][1] )
              ref_bw   += MB ( (ref_dict[rkey][4] + ref_dict[rkey][5]) )
          ref_bwpb = safediv ( ref_bw, ref_data )
      else:
        if train2ref[tkey] in ref_dict:
          train_match = True
          rkey = train2ref[tkey]
          ref_objs = ref_dict[rkey][0]
          ref_data = CLMB ( ref_dict[rkey][1] )
          ref_bw   = MB ( (ref_dict[rkey][4] + ref_dict[rkey][5]) )
          ref_bwpb = safediv ( ref_bw, ref_data )

    if train_match:
      trcmp_train["mat-vals"] += 1
      trcmp_train["mat-objs"] += train_objs
      trcmp_train["mat-bw"]   += train_bw
      trcmp_train["mat-data"] += train_data

      trcmp_diff["bwpb-avg"].append( (ref_bwpb - train_bwpb) )
      trcmp_diff["bwpb-abs"].append( abs(ref_bwpb - train_bwpb) )
    else:
      trcmp_train["unm-vals"] += 1
      trcmp_train["unm-objs"] += train_objs
      trcmp_train["unm-bw"]   += train_bw
      trcmp_train["unm-data"] += train_data


  if feature in phase_features:
    matched_rkeys = set(itertools.chain.from_iterable(train2ref.values()))
  else:
    matched_rkeys = set(train2ref.values())

  for rkey in ref_dict:
    ref_objs = ref_dict[rkey][0]
    ref_data = CLMB ( ref_dict[rkey][1] )
    ref_bw   = MB ( (ref_dict[rkey][4] + ref_dict[rkey][5]) )
    ref_bwpb = safediv(ref_bw, ref_data)

    trcmp_ref["objs"] += ref_objs
    trcmp_ref["bw"]   += ref_bw
    trcmp_ref["data"] += ref_data

    if rkey in matched_rkeys:
      trcmp_ref["mat-vals"] += 1
      trcmp_ref["mat-objs"] += ref_objs
      trcmp_ref["mat-bw"]   += ref_bw
      trcmp_ref["mat-data"] += ref_data
    else:
      trcmp_ref["unm-vals"] += 1
      trcmp_ref["unm-objs"] += ref_objs
      trcmp_ref["unm-bw"]   += ref_bw
      trcmp_ref["unm-data"] += ref_data

  trcmp_train["bwpb"]     = safediv(trcmp_train["bw"],       trcmp_train["data"]     )
  trcmp_train["mat-bwpb"] = safediv(trcmp_train["mat-bw"],   trcmp_train["mat-data"] )
  trcmp_train["unm-bwpb"] = safediv(trcmp_train["unm-bw"],   trcmp_train["unm-data"] )
  trcmp_train["mr-vals"]  = safediv(trcmp_train["mat-vals"], trcmp_train["vals"]     )
  trcmp_train["mr-objs"]  = safediv(trcmp_train["mat-objs"], trcmp_train["objs"]     )
  trcmp_train["mr-data"]  = safediv(trcmp_train["mat-data"], trcmp_train["data"]     )
  trcmp_train["mr-bw"]    = safediv(trcmp_train["mat-bw"],   trcmp_train["bw"]       )
  trcmp_ref["bwpb"]       = safediv(trcmp_ref["bw"],         trcmp_ref["data"]       )
  trcmp_ref["mat-bwpb"]   = safediv(trcmp_ref["mat-bw"],     trcmp_ref["mat-data"]   )
  trcmp_ref["unm-bwpb"]   = safediv(trcmp_ref["unm-bw"],     trcmp_ref["unm-data"]   )
  trcmp_ref["mr-vals"]    = safediv(trcmp_ref["mat-vals"],   trcmp_ref["vals"]       )
  trcmp_ref["mr-objs"]    = safediv(trcmp_ref["mat-objs"],   trcmp_ref["objs"]       )
  trcmp_ref["mr-data"]    = safediv(trcmp_ref["mat-data"],   trcmp_ref["data"]       )
  trcmp_ref["mr-bw"]      = safediv(trcmp_ref["mat-bw"],     trcmp_ref["bw"]         )

  trcmp_diff["bwpb-avg"]  = safe_mean(trcmp_diff["bwpb-avg"])
  trcmp_diff["bwpb-abs"]  = safe_mean(trcmp_diff["bwpb-abs"])

def get_trcmp_info(trcmp, feature, ref_dict, train_dict, train2ref):

  if feature == BENCH:

    trcmp[REF][BENCH]["vals"]   = 1
    trcmp[TRAIN][BENCH]["vals"] = 1
    trcmp[REF][BENCH]["objs"]   = ref_dict[ALL][AGG][0]
    trcmp[TRAIN][BENCH]["objs"] = train_dict[ALL][AGG][0]

    ref_data   = CLMB ( ref_dict[ALL][AGG][1]   )
    train_data = CLMB ( train_dict[ALL][AGG][1] )
    ref_bw     = MB ( (ref_dict[ALL][AGG][4]   + ref_dict[ALL][AGG][5])   )
    train_bw   = MB ( (train_dict[ALL][AGG][4] + train_dict[ALL][AGG][5]) )

    trcmp[REF][BENCH]["bwpb"]      = safediv ( ref_bw, ref_data )
    trcmp[TRAIN][BENCH]["bwpb"]    = safediv ( train_bw, train_data )
    trcmp[DIFF][BENCH]["bwpb-avg"] = ( trcmp[REF][BENCH]["bwpb"] - trcmp[TRAIN][BENCH]["bwpb"] )
    trcmp[DIFF][BENCH]["bwpb-abs"] = abs( ( trcmp[REF][BENCH]["bwpb"] - trcmp[TRAIN][BENCH]["bwpb"] ) )

  elif feature in scalar_features:
    trcmp_info_helper(trcmp[REF][feature], trcmp[TRAIN][feature], \
      trcmp[DIFF][feature], ref_dict[ALL][feature], \
      train_dict[ALL][feature], train2ref[feature], feature)
#    if feature == ALLOC_PHASE:
#      trcmp_info_helper(trcmp[REF][feature], trcmp[TRAIN][feature], \
#        trcmp[DIFF][feature], ref_dict[ALL][feature], \
#        train_dict[ALL][feature], train2ref[feature], feature, pp=True)
#    else:
#      trcmp_info_helper(trcmp[REF][feature], trcmp[TRAIN][feature], \
#        trcmp[DIFF][feature], ref_dict[ALL][feature], \
#        train_dict[ALL][feature], train2ref[feature], feature, pp=False)

  elif feature in site_features:
    for cxt in pin_cxt_lengths:
      trcmp_info_helper(trcmp[REF][feature][cxt], trcmp[TRAIN][feature][cxt], \
        trcmp[DIFF][feature][cxt], ref_dict[ALL][feature][cxt], \
        train_dict[ALL][feature][cxt], train2ref[feature][cxt], feature)

def get_train_ref_compare(ref_dict, train_dict, train2ref, features=all_features):

  trcmp        = {}
  trcmp[REF]   = {}
  trcmp[TRAIN] = {}
  trcmp[DIFF]  = {}

  for feature in features:
    trcmp[REF][feature]   = {}
    trcmp[TRAIN][feature] = {}
    trcmp[DIFF][feature]  = {}
    if feature in site_features:
      for cxt in pin_cxt_lengths:
        trcmp[REF][feature][cxt]   = {}
        trcmp[TRAIN][feature][cxt] = {}
        trcmp[DIFF][feature][cxt]  = {}

  for feature in features:
    get_trcmp_info(trcmp, feature, ref_dict, train_dict, train2ref)

  return trcmp

def train_ref_report(benches, ref_cfg, train_cfg, cutstyle=BANDWIDTH_PER_BYTE,
  features=all_features, print_benches=True, stats=train_ref_stats):

  print("\ntrain --> ref report\n\n")

  avgs = {}
  for bench in benches:

    ref_dict   = get_object_cut_info(bench, ref_cfg, cutstyle)
    train_dict = get_object_cut_info(bench, train_cfg, cutstyle)
    t2r_map    = get_train2ref_map(bench, ref_cfg, train_cfg)
    trcmp      = get_train_ref_compare(ref_dict, train_dict, t2r_map, features)

    if print_benches:
      print("%s : %s --> %s" % (bench, ref_cfg, train_cfg))

      header_pts = ["feature"]
      format_pts = ["%-12s"]
      for hname,stype,hfmt,dfmt in stats:
        header_pts.append(("%s-%s" % (stype[:1].lower(), hname))) 
        format_pts.append(hfmt)

      print( ((" ".join(format_pts)) % tuple(header_pts)) )

    for feature in features:

      if feature in scalar_features:
        if print_benches:
          print("%-12s " % feature, end='')

        for stat,stype,hfmt,sfmt in stats:
          val = None
          if stat in trcmp[stype][feature]:
            val = trcmp[stype][feature][stat]

          if val != None:
            key = ("%s-%s-%s" % (stype, feature, stat))
            if not key in avgs:
              avgs[key] = []
            avgs[key].append(val)

          if print_benches:
            if val != None:
              print ((hfmt % (sfmt % val)) + " ", end='') 
            else:
              print ((hfmt % "-") + " ", end='')

        if print_benches:
          print("")

      elif feature in site_features:
        for clen in pin_cxt_lengths:

          if print_benches:
            print(("%-12s " % ("%s-%d" % (feature,clen))), end='')

          for stat,stype,hfmt,sfmt in stats:
            val = None
            if stat in trcmp[stype][feature][clen]:
              val = trcmp[stype][feature][clen][stat]

            if val != None:
              key = ("%s-%s-%s%d" % (stype, feature, stat, clen))
              if not key in avgs:
                avgs[key] = []
              avgs[key].append(val)

            if print_benches:
              if val != None:
                print ((hfmt % (sfmt % val)) + " ", end='')
              else:
                print ((hfmt % "-") + " ", end='') 

          if print_benches:
            print("")

    if print_benches:
      print("")

  print("%s : %s --> %s" % ("average", ref_cfg, train_cfg))

  header_pts = ["feature"]
  format_pts = ["%-12s"]
  for hname,stype,hfmt,dfmt in stats:
    header_pts.append(("%s-%s" % (stype[:1].lower(), hname))) 
    format_pts.append(hfmt)

  print( ((" ".join(format_pts)) % tuple(header_pts)) )
  for feature in features:

    if feature in scalar_features:
      print("%-12s " % feature, end='')
      for stat,stype,hfmt,sfmt in stats:
        key = ("%s-%s-%s" % (stype, feature, stat))
        if key in avgs:
          val = mean(avgs[key])
          print ((hfmt % (sfmt % val)) + " ", end='')
        else:
          print ((hfmt % "-") + " ", end='') 
      print("")

    elif feature in site_features:
      for clen in pin_cxt_lengths:
        print(("%-12s " % ("%s-%d" % (feature,clen))), end='')
        for stat,stype,_,sfmt in stats:
          key = ("%s-%s-%s%d" % (stype, feature, stat, clen))
          if key in avgs:
            val = mean(avgs[key])
            print ((hfmt % (sfmt % val)) + " ", end='')
          else:
            print ((hfmt % "-") + " ", end='') 
        print("")
  print("")

def feature_accuracy_report(bench, cfg_name, cutstyle=BANDWIDTH_PER_BYTE,
  cutrat=1.28, feature=SIZE, style=OBJECTS, cxt=None, cutoff=50):

  otdict = get_object_cut_info(bench, cfg_name, cutstyle)
  ftdict = get_feature_cut_info(bench, cfg_name, cutstyle)
  tz     = cut_str(cutstyle, cutrat)

  if not feature in site_features:
    ot_all_dict = otdict[ALL][feature]
    ot_tz_dict  = otdict[tz][feature]
    ft_tz_dict  = ftdict[tz][feature]
  else:
    ot_all_dict = otdict[ALL][feature][cxt]
    ot_tz_dict  = otdict[tz][feature][cxt]
    ft_tz_dict  = ftdict[tz][feature][cxt]

  print("%-6s %-10s %20s %20s %20s" % \
        ("rank", "key", "ft-pos", "obj-pos", "obj-neg"))

  ft_vals = None
  if cutstyle == BANDWIDTH_PER_BYTE:
    ft_vals = sorted ( [ ( x, ( ((y[4]+y[5]) / (CLMB(y[1]))) \
                                if y[1] != 0 else (x, 0.0)), \
                              get_val(y, style) ) \
                         for x,y in ot_all_dict.items() ],
                       key=itemgetter(1), reverse=True )

  total_data = sum([x[2] for x in ft_vals])
  if style == DATA:
    total_data *= 1024

  sum_ft_data = sum_pos_data = sum_neg_data = 0
  for rank,val in enumerate(ft_vals[:cutoff]):
    ftkey = val[0]

    all_data = get_val(ot_all_dict[val[0]], style)
    pos_data = 0
    if ftkey in ot_tz_dict:
      pos_data = get_val(ot_tz_dict[val[0]], style)
    neg_data = all_data - pos_data

    ft_data = 0
    if ftkey in ft_tz_dict[7]:
      ft_data = all_data

    if style == DATA:
      ft_data  *= 1024
      pos_data *= 1024
      neg_data *= 1024

    sum_ft_data  += ft_data
    sum_pos_data += pos_data
    sum_neg_data += neg_data

    ft_rel  = float(sum_ft_data)  / total_data
    pos_rel = float(sum_pos_data) / total_data
    neg_rel = float(sum_neg_data) / total_data

    print("%-6d %-10d %12d  %1.4f %12d  %1.4f %12d  %1.4f" % \
          (rank, ftkey, ft_data, ft_rel, pos_data, pos_rel, neg_data, neg_rel))

def get_packed_bw(bench, cfg_name, cuts, cutrats, utsize, style):
  bc = ("%s-%s" % (bench, cfg_name))

  if style == OBJECTS:
    pref = "ot"
  elif style == SITES:
    pref = "st"
  elif style == BENCH:
    pref = "bt"
  else:
    print ("invalid style: %s" % style)
    raise (SystemExit(1))

  datakey = ("%s-data" % pref)
  bwkey   = ("%s-bw"   % pref)

  total_data = cuts[bc][cutrats[0]]["data"]
  total_bw   = cuts[bc][cutrats[0]]["bw"]
  target = (total_data * (utsize/100.0))
  packed_bw = (total_bw * target)
  for rat in sorted(cutrats):
    if cuts[bc][rat][datakey] <= target:
      packed_bw  = cuts[bc][rat][bwkey]
      datapt     = ( (target - cuts[bc][rat][datakey]) / \
                     (cuts[bc][rat]["data"] - cuts[bc][rat][datakey] ) )
      bwpt       = ( cuts[bc][rat]["bw"] - cuts[bc][rat][bwkey] )
      packed_bw += ( datapt * bwpt )
      break
  return (safediv(packed_bw,total_bw))

def sim_2lm_report(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrats=def_bwpb_rats, styles=[BENCH, SITES, OBJECTS], \
  utsizes=def_upper_tier_sizes):

  print("")
  print ("getting cut infos")
  cuts = {}
  for bench in benches:
    for cfg_name in cfgs:
      bc = ("%s-%s" % (bench, cfg_name))
      print (("  %s ... " % bc),end='')
      cuts[bc] = {}
      for rat in cutrats:
        socinfo = site_object_cut_info(bench, cfg_name, cutstyle, rat)
        cuts[bc][rat] = socinfo
      print ("done.")

  for utsize in utsizes:
    print ("UL bandwidth with %4.2f%% UL capacity" % utsize)

    header_pts = ["benchmark"]
    format_pts = ["%-16s"]
    for style in styles:
      header_pts.append(style.lower()) 
      format_pts.append("%9s")

    print("")
    print( ((" ".join(format_pts)) % tuple(header_pts)) )

    avgs = []
    for bench in benches:
      for cfg_name in cfgs:
        bt_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, BENCH)
        st_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, SITES)
        ot_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, OBJECTS)

        print ("%-16s %9.3f %9.3f %9.3f" % (bench, bt_packed_bw, \
               st_packed_bw, ot_packed_bw))

        avgs.append((ot_packed_bw, st_packed_bw, bt_packed_bw))

    means = [ geo_mean ( [ x[i] for x in avgs ] ) for i in range(3) ]
    print ("%-16s %9.3f %9.3f %9.3f" % tuple ( ["average"] + means ))
    print("")

def sim_2lm_bw_summary(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrats=def_bwpb_rats, styles=[BENCH, SITES, OBJECTS], \
  utsizes=def_upper_tier_sizes):

  print("")
  print ("getting cut infos")
  cuts = {}
  for bench in benches:
    for cfg_name in cfgs:
      bc = ("%s-%s" % (bench, cfg_name))
      print (("  %s ... " % bc),end='')
      cuts[bc] = {}
      for rat in cutrats:
        socinfo = site_object_cut_info(bench, cfg_name, cutstyle, rat)
        cuts[bc][rat] = socinfo
      print ("done.")

  header_pts = ["capacity"]
  format_pts = ["%-12s"]
  for style in styles:
    header_pts.append(style.lower()) 
    format_pts.append("%9s")

  print("")
  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  for utsize in utsizes:
    utsize_vals = []
    for style in styles:
      style_vals = []
      for bench in benches:
        for cfg_name in cfgs:
          packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                      utsize, style)
          style_vals.append(packed_bw)
      utsize_vals.append( geo_mean(style_vals) )
    print ("%-12s %9.3f %9.3f %9.3f" % tuple ( [utsize] + utsize_vals ))
  print("")

def object_cut_report(bench, cfg_name, objcut=THERMOS, cutpct=10.0,
  sortkey=DATA, scutnum=50, scutrat=1.0, style=DATA):

  tzkey  = thermos_str(cutpct)
  stdict = get_thermos_dict(bench, cfg_name)

  all_dict = stdict[ALL]
  cut_dict = stdict[tzkey]

  print("%s %s %3.2f\n" % (bench, cfg_name, cutpct))

  if style == OBJECTS:
    print("%-5s %8s %10s %10s %8s %8s" % \
          ("rank", "obj-rat", "obj-cut", "obj-all",
            "cut-cr", "all-cr")
         )
  elif style == DATA:
    print("%-5s %8s %10s %10s %8s %8s" % \
          ("rank", "data-rat", "data-cut", "data-all",
            "cut-cr", "all-cr")
         )
  else:
    raise SystemExit(1)

  dpos        = 0 if style == OBJECTS else 1
  total_data  = sum ( [ float(all_dict[s][dpos]) for s in all_dict ])
  cutset_data = sum ( [ float(cut_dict[s][dpos]) for s in cut_dict ] )
  if total_data == 0:
    print("no data in all_dict!")
    return None

  bad_sites = set([])
  sorted_sites = [ k for k,v in \
                   sorted( cut_dict.items(), key=lambda x: x[1][1], \
                           reverse=True)
                 ]


  cum_cut_ratio = cum_all_ratio = 0.0
  for rank,site in enumerate(sorted_sites):
    all_data = float(all_dict[site][dpos])
    cut_data = float(cut_dict[site][dpos])

    if all_data == 0:
      bad_sites.append(site)
      continue

    cut_ratio = cut_data / all_data

    cum_cut_ratio += (cut_data / cutset_data)
    cum_all_ratio += (all_data / total_data)

    print ("%-5d %8.4f %10d %10d %8.4f %8.4f" % \
           ((rank+1), cut_ratio, cut_data, all_data, \
            cum_cut_ratio, cum_all_ratio))

    if (rank+1) >= scutnum or cum_all_ratio >= scutrat:
      break

def object_bwpb_cut(objlist, cutrat, foff, bnlife=None):
  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[(lines_idx+foff)] != 0:
      bwpb = ( (obj[(bw_idx+foff)] / (obj[(lines_idx+foff)]*CACHE_LINE_SIZE) ) )
      if bwpb > cutrat:
        cutset.append(idx)
#        if cutrat == 10.24:
#          print("i: %16d cl: %8d bw: %12d pb: %8.4f" %\
#                (idx, obj[(lines_idx+foff)], obj[(bw_idx+foff)], bwpb))
#
#  if cutrat == 10.24:
#    #print (cutset)
#    total_size = sum ( [ obj[lines_idx] for obj in objlist ] )
#    total_bw   = sum ( [ obj[bw_idx]    for obj in objlist ] )
#
#    size = sum ( [ objlist[idx][lines_idx] for idx in cutset ] )
#    bw   = sum ( [ objlist[idx][bw_idx]    for idx in cutset ] )
#
#    print("size: %12d bw: %12d zr: %8.4f br: %8.4f" %\
#          (CLMB(size), MB(bw), safediv(size,total_size), safediv(bw,total_bw)))
  return cutset

def feature_bwpb_cut(finfo, cutrat, bnlife=None):

  cutset = []
  for feature,rec in finfo.items():
    if rec[LINE_RSS]:
      bw   = ( rec[POST_READS] + rec[POST_WRITES] )
      bwpb = ( (bw / (rec[LINE_RSS]*CACHE_LINE_SIZE) ) )
      if bwpb > cutrat:
        cutset.append(feature)
#      if cutrat == 10.24:
#        print("i: %16s cl: %8d bw: %12d pb: %8.4f" %\
#              (str(feature), rec[LINE_RSS], bw, bwpb))
#
#  if cutrat == 10.24:
##    print (cutset)
##
#    size = sum ( [ finfo[ft][LINE_RSS] for ft in cutset ] )
#    bw   = sum ( [ (finfo[ft][POST_READS]+finfo[ft][POST_WRITES]) for ft in cutset ] )
##
#    print("size: %12d bw: %12d" %\
#          (CLMB(size), MB(bw)))
  return cutset 


def object_thermos(objlist, cutpct, bnlife=None):

  sizekey = 1
  bwkey   = 3

  vals = []
  for idx,obj in enumerate(objlist):
    if obj[sizekey] != 0:
      bwpl = (obj[bwkey] / obj[sizekey])
      vals.append((bwpl, obj[bwkey], obj[sizekey], idx))

  target = ( (sum( [ v[2] for v in vals ] ) * (cutpct / 100)))
  sorted_nzv  = sorted ( [ x for x in vals if x[0] != 0 ], \
                         key=lambda tup: tup[0], reverse=True )
  sorted_zv   = sorted ( [ x for x in vals if x[0] == 0 ], \
                         key=lambda tup: tup[2] )

  # becuase of the way we profile -- some objects and sites are never touched
  # zero size objects should not be in the thermos
  #
  #sorted_vals = sorted_nzv + sorted_zv
  sorted_vals = sorted_nzv

#  thermos = []
#  thermos_bw   = 0
#  thermos_size = 0
#  for bwpl,obj_bw,obj_size,idx in sorted_vals:
#    thermos += [idx]
#    thermos_size += obj_size
#    thermos_bw   += obj_bw
#    if (thermos_size > target):
#      break

#  if cutpct == 10.0:
#    print("")
#    print(target)

  thermos = []
  thermos_bw   = 0
  thermos_size = 0
  for bwpl,obj_bw,obj_size,idx in sorted_vals:

    over = max([((thermos_size + obj_size) - target),0])
    if over > 0:
      tmp_set  = []
      tmp_size = 0
      tmp_bw   = 0
      for obj in thermos:
        tmp_set  += [obj]
        tmp_bw   += objlist[obj][bwkey]
        tmp_size += objlist[obj][sizekey]
        if tmp_size > over:
          break

      # if the new object displaces all data in the thermos -- what would be
      # the bandwidth left in the thermos
      #
      my_bw = obj_bw
      if obj_size > target:
        my_bw = (obj_bw * (target / obj_size))

#      if cutpct==10.0:
#        print ("idx: %-4d tbw: %-12d thz: %-12d obw: %-12d osz: %-12d tmp: %-12d my_: %-12d dif: %-12d" % \
#               (idx, thermos_bw, thermos_size, obj_bw, obj_size, \
#               tmp_bw, my_bw, (thermos_bw - tmp_bw + my_bw)))

      if ((thermos_bw - tmp_bw + my_bw) > thermos_bw):
        thermos      += [idx]
        thermos_size += obj_size
        thermos_bw   += obj_bw

    else:
#      if cutpct == 10.0:
#        print ("aaa: %-4d tbw: %-12d tsz: %-12d obw: %-12d osz: %-12d" % \
#               (idx, thermos_bw, thermos_size, obj_bw, obj_size))
      thermos += [idx]
      thermos_size += obj_size
      thermos_bw   += obj_bw

  return thermos


def feature_thermos(sinfo, cutpct, bnlife=None):

  vals = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS] != 0:
      bwpl = (rec[BANDWIDTH] / rec[LINE_RSS])
      vals.append((bwpl, rec[BANDWIDTH], rec[LINE_RSS], site))

  target = ( (sum( [ v[2] for v in vals ] ) * (cutpct / 100)))

  sorted_nzv  = sorted ( [ x for x in vals if x[1] != 0 ], \
                         key=lambda tup: tup[0], reverse=True )
  sorted_zv   = sorted ( [ x for x in vals if x[1] == 0 ], \
                         key=lambda tup: tup[2] )

  # becuase of the way we profile -- many objects and sites are never touched
  # zero size objects should not be in the thermos
  #
  #sorted_vals = sorted_nzv + sorted_zv
  sorted_vals = sorted_nzv

#  thermos = []
#  thermos_bw   = 0
#  thermos_size = 0
#  for bwpl,site_bw,site_size,sid in sorted_vals:
#    thermos      += [sid]
#    thermos_size += site_size
#    thermos_bw   += site_bw
#    if (thermos_size > target):
#      break

#  if cutpct == 10.0:
#    print("")
#    print(target)

  thermos = []
  thermos_bw = 0
  thermos_size = 0
  for bwpl,site_bw,site_size,sid in sorted_vals:

    over = max([((thermos_size + site_size) - target),0])
    if over > 0:
      tmp_set  = []
      tmp_size = 0
      tmp_bw   = 0
      for site in thermos:
        tmp_set  += [site]
        tmp_bw   += sinfo[site][BANDWIDTH]
        tmp_size += sinfo[site][LINE_RSS]
        if tmp_size > over:
          break

      # if the new site displaces all data in the thermos -- what would be the
      # bandwidth left in the thermos
      #
      my_bw = site_bw
      if site_size > target:
        my_bw = (site_bw * (target / site_size))

#      if cutpct==10.0:
#        print ("idx: %-4d thm: %-12d thz: %-12d sbw: %-12d ssz: %-12d tmp: %-12d my_: %-12d dif: %-12d" % \
#               (sid, thermos_bw, thermos_size, site_bw, site_size, \
#               tmp_bw, my_bw, (thermos_bw - tmp_bw + my_bw)))

      if ((thermos_bw - tmp_bw + my_bw) > thermos_bw):
        thermos      += [sid]
        thermos_size += site_size
        thermos_bw   += site_bw

    else:
#      if cutpct == 10.0:
#        print ("aaa: %-4d tbw: %-12d tsz: %-12d sbw: %-12d ssz: %-12d" % \
#               (sid, thermos_bw, thermos_size, site_bw, site_size))

      thermos      += [sid]
      thermos_size += site_size
      thermos_bw   += site_bw

  return thermos


def object_wrpb_cut(objlist, cutrat, foff, bnlife=None):
  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[(lines_idx+foff)] != 0:
      obj_writes = 0 if obj[(postrw_idx+foff)] == -1.0 else \
                   (obj[(bw_idx+foff)] / (obj[(postrw_idx)]+1))
      wrpb = ( ( obj_writes / (obj[(lines_idx+foff)]*CACHE_LINE_SIZE) ) )
      if wrpb < cutrat:
        cutset.append(idx)

  return cutset

def feature_wrpb_cut(sinfo, cutrat, bnlife=None):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS] != 0:
      wrpb = ( (rec[POST_WRITES] / (rec[LINE_RSS]*CACHE_LINE_SIZE) ) )
      if wrpb < cutrat:
        cutset.append(site)
  return cutset 


def object_lifetime_pct_cut(objlist, cutpct, foff, bnlife):

  cut = int((bnlife/1000)*cutpct)

  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[(lifetime_idx+foff)] > cut:
      cutset.append(idx)

  return cutset

def feature_lifetime_pct_cut(sinfo, cutpct, bnlife):

  cut = int((bnlife/1000)*cutpct)

  cutset = []
  for site,rec in sinfo.items():
    if rec[LIFETIME] > cut:
      cutset.append(site)
  return cutset 


def object_lifetime_cut(objlist, cut, foff, bnlife=None):
  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[(lifetime_idx+foff)] < cut:
      cutset.append(idx)

  return cutset

def feature_lifetime_cut(sinfo, cut, bnlife=None):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LIFETIME] < cut:
      cutset.append(site)
  return cutset 


def object_rdwr_cut(objlist, cutrat, bnlife=None):
  sizekey = 1
  rdwrkey = 5

  return [ idx for idx,obj in enumerate(objlist) \
           if (obj[sizekey] != 0) and \
              ((obj[rdwrkey] < -0.01) or (obj[rdwrkey] > cutrat)) ]

def feature_rdwr_cut(sinfo, cutrat, bnlife=None):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      rdwr = -1.0 if rec[TOTAL_WRITES] == 0 else \
             float(rec[TOTAL_READS]) / rec[TOTAL_WRITES]
      if (rdwr < -0.01) or (rdwr > cutrat):
        cutset.append(site)
  return cutset 

def object_rdwr_pack(objlist, cutrat):

  sizekey = 1
  accskey = 2
  rdwrkey = 5

  vals = []
  for idx,obj in enumerate(objlist):
    if obj[sizekey]:
      obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
      obj_reads   = (obj[accskey] - obj_writes)
      vals.append((obj_reads, obj_writes, obj[rdwrkey], obj[sizekey], idx))

  rdmostly     = sorted ( [ x for x in vals if ((x[2] < -0.01) or  (x[2] >= cutrat)) ], \
                          key=lambda tup: tup[3], reverse=True )
#  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
#                          key=lambda tup: tup[2], reverse=True )
  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
                          key=lambda tup: tup[3], reverse=True )

  cutset = [ x[4] for x in rdmostly ]
  reads  = sum ( [ x[0] for x in rdmostly ] )
  writes = sum ( [ x[1] for x in rdmostly ] )
  for obj in sorted_cands:
    next_reads  = (reads  + obj[0])
    next_writes = (writes + obj[1])
    rdwr_rat    = ( float(next_reads) / next_writes )

#    if obj[0] > 0 and obj[2] > 0:
#      print ("%12d %12d %12d %12d %3.4f %d" % \
#             (obj_reads, obj_writes, next_reads, next_writes, rdwr_rat, obj[3]))

    if rdwr_rat > cutrat:
      cutset += [obj[4]]
      reads   = next_reads
      writes  = next_writes

  return cutset

def feature_rdwr_pack(sinfo, cutrat):

  vals = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      rdwr = -1.0 if rec[TOTAL_WRITES] == 0 else \
             float(rec[TOTAL_READS]) / rec[TOTAL_WRITES]
      vals.append((rec[TOTAL_READS], rec[TOTAL_WRITES], rdwr, rec[LINE_RSS], site))

  rdmostly     = sorted ( [ x for x in vals if ((x[2] < -0.01) or  (x[2] >= cutrat)) ], \
                          key=lambda tup: tup[3], reverse=True )
#  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
#                          key=lambda tup: tup[2], reverse=True )
  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
                          key=lambda tup: tup[3], reverse=True )

  cutset = [ x[4] for x in rdmostly ]
  reads  = sum ( [ x[0] for x in rdmostly ] )
  writes = sum ( [ x[1] for x in rdmostly ] )
  for site in sorted_cands:
    next_reads  = (reads  + site[0])
    next_writes = (writes + site[1])
    rdwr_rat    = ( float(next_reads) / next_writes )
    if rdwr_rat > cutrat:
      cutset += [site[4]]
      reads   = next_reads
      writes  = next_writes

  return cutset


def get_ins_count(bench, cfg_name, it=0, copy=0):
  cfg.read_cfg(cfg_name)

  ins = 0
  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for run in runs:
    ins_count_fd = open( ("%sins_count.out" % \
                         (get_run_results_dir(bench,cfg_name, it, copy, run))
                       ) )

    next(ins_count_fd)
    for _ in range(8):
      ins += int(next(ins_count_fd).split()[1])
    ins_count_fd.close()

  return ins


def candidate_sites(bench, cfg_name, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  if aggtype == ALL_SITES:
    sinfo = site_info(bench, cfg_name)
    cands = sinfo.keys()
  elif aggtype == SORT:
    sinfo = site_info(bench, cfg_name)
    cands = get_sort_sites(sinfo, sortkey, revsort, cutkey, cut)
  elif aggtype == CUT:
    sinfo = site_info(bench, cfg_name)
    cands = get_cut_sites(sinfo, revsort, cutkey, cut)
  elif aggtype == THERMOS:
    sinfo = get_object_stats(bench, cfg_name)
    cands = site_thermos(sinfo, cut)
  else:
    print ("unknown aggtype: %s" % aggtype)
    return None

  return cands

def agg_site_info(bench, cfg_name, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  cands = candidate_sites(bench, cfg_name, aggtype=aggtype, \
           sortkey=sortkey, revsort=revsort, cutkey=cutkey, cut=cut)

  always_stats = [ NUM_SITES,    \
                   TOTAL_READS,  \
                   TOTAL_WRITES, \
                   READ_MISSES,  \
                   WRITE_MISSES, \
                   TOTAL_HITS,   \
                   TOTAL_MISSES, \
                   TOTAL_ACCS    \
                 ]

  agg_info = {}

  for stat in set(always_stats + stats):
    agg_info[stat] = 0

  for cand in cands:
    site_hits   = sinfo[cand][READ_HITS]   + sinfo[cand][WRITE_HITS]
    site_misses = sinfo[cand][READ_MISSES] + sinfo[cand][WRITE_MISSES]
    site_reads  = sinfo[cand][READ_HITS]   + sinfo[cand][READ_MISSES]
    site_writes = sinfo[cand][WRITE_HITS]  + sinfo[cand][WRITE_MISSES]
    site_read_misses  = sinfo[cand][READ_MISSES]
    site_write_misses = sinfo[cand][WRITE_MISSES]

    agg_info[TOTAL_HITS]   += site_hits
    agg_info[TOTAL_MISSES] += site_misses
    agg_info[TOTAL_ACCS]   += (site_hits + site_misses)
    agg_info[TOTAL_READS]  += site_reads
    agg_info[TOTAL_WRITES] += site_writes
    agg_info[READ_MISSES]  += site_read_misses
    agg_info[WRITE_MISSES] += site_write_misses

    agg_info[NUM_SITES] += 1
    for stat in [x for x in stats if not x in always_stats]:
      agg_info[stat] += sinfo[cand][stat]

  for stat in stats:
    if stat == SITE_HITS_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_ACCS]   == 0 else \
                       float(agg_info[TOTAL_HITS]) / agg_info[TOTAL_ACCS]
    elif stat == SITE_MISSES_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_ACCS]   == 0 else \
                       float(agg_info[TOTAL_MISSES]) / agg_info[TOTAL_ACCS]
    elif stat == SITE_RDWR_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_WRITES] == 0 else \
                       float(agg_info[TOTAL_READS]) / agg_info[TOTAL_WRITES]
    elif stat == SITE_RDWR_MISS_RATIO:
      agg_info[stat] = 0.0 if agg_info[WRITE_MISSES] == 0 else \
                       float(agg_info[READ_MISSES]) / agg_info[WRITE_MISSES]

  return agg_info

def agg_site_report(benches, configs, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  print_cfg_name = True if len(configs) > 1 else False
  if print_cfg_name:
    print("%-20s %-20s %6s" % ("bench", "config", "sites"), end='')
  else:
    print("%-20s %6s" % ("bench", "sites"), end='')

  for stat in site_report_stats:
    x = stat
    if "site_" in stat:
      x = x.replace("site","s")
    if "total_" in stat:
      x = x.replace("total","t")
    if "miss_ratio" in stat:
      x = x.replace("miss_ratio","mr")
    elif "ratio" in stat:
      x = x.replace("ratio","r")

    print(" %12s" % x, end='')
  print("")

  avg_info = {}
  avg_info[NUM_SITES] = []
  for stat in stats:
    avg_info[stat] = []

  for bench in benches:
    for cfg_name in configs:
      sinfo = agg_site_info(bench, cfg_name, stats=stats, \
              aggtype=aggtype, sortkey=sortkey, revsort=revsort,
              cutkey=cutkey, cut=cut)

      outstr = ("%-20s" % (bench))
      if print_cfg_name:
        outstr += (" %-20s" % (cfg_name))
      outstr += (" %6d" % sinfo[NUM_SITES])
      for stat in stats:
        outstr += (" %12s" % (format_str(stat) % (sinfo[stat])))
      print(outstr)

      avg_info[NUM_SITES].append(sinfo[NUM_SITES])
      for stat in stats:
        avg_info[stat].append(sinfo[stat])

  outstr = ("%-20s" % ("average"))
  if print_cfg_name:
    outstr += (" %-20s" % ("-"))

  outstr += (" %6d" % median(avg_info[NUM_SITES]))
  for stat in stats:
    if 'ratio' in stat and not stat.endswith("miss_ratio"):
      avg_stat = mean(avg_info[stat])
    else:
      avg_stat = median(avg_info[stat])
    outstr += (" %12s" % (format_str(stat) % (avg_stat)))
  print(outstr)

def parse_site_info(bench, cfg_name, it, rdict):
  cfg.read_cfg(cfg_name)

  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for copy in range(cfg.current_cfg['runcfg']['copies']):

    rdict[TOTAL_HEAP_PAGES]  = 0
    rdict[TOTAL_HEAP_LINES]  = 0
    rdict[TOTAL_HEAP_HITS]   = 0
    rdict[TOTAL_HEAP_MISSES] = 0
    rdict[TOTAL_UNKN_PAGES]  = 0
    rdict[TOTAL_UNKN_LINES]  = 0
    rdict[TOTAL_UNKN_HITS]   = 0
    rdict[TOTAL_UNKN_MISSES] = 0
    rdict[TOTAL_PAGES]       = 0
    rdict[TOTAL_LINES]       = 0
    rdict[TOTAL_HITS]        = 0
    rdict[TOTAL_MISSES]      = 0
    rdict[TOTAL_ACCS]        = 0
    rdict[HEAP_ACCS]         = 0
    rdict[UNKN_ACCS]         = 0

    err = False
    for run in runs:
      try:
        results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
        fd = open(('%sorig_ap_info.out' % results_dir), 'r')
      except:
        err = True
        continue

      for line in fd:
        pts = line.split()
        if line.startswith('HEAP'):
          rdict[TOTAL_HEAP_PAGES]  += int(pts[1])
          rdict[TOTAL_HEAP_LINES]  += int(pts[2])
          rdict[TOTAL_HEAP_HITS]   += int(pts[3])
          rdict[TOTAL_HEAP_MISSES] += int(pts[4])
        elif line.startswith('UNKN'):
          rdict[TOTAL_UNKN_PAGES]  += int(pts[1])
          rdict[TOTAL_UNKN_LINES]  += int(pts[2])
          rdict[TOTAL_UNKN_HITS]   += int(pts[3])
          rdict[TOTAL_UNKN_MISSES] += int(pts[4])
      fd.close()

    rdict[TOTAL_PAGES]        = (rdict[TOTAL_HEAP_PAGES] + rdict[TOTAL_UNKN_PAGES])
    rdict[KNOWN_PAGES_RATIO]  = 0.0 if rdict[TOTAL_PAGES] == 0 else \
                                (rdict[TOTAL_HEAP_PAGES] / rdict[TOTAL_PAGES])

    rdict[TOTAL_LINES]        = (rdict[TOTAL_HEAP_LINES] + rdict[TOTAL_UNKN_LINES])
    rdict[KNOWN_LINES_RATIO]  = 0.0 if rdict[TOTAL_LINES] == 0 else \
                                (rdict[TOTAL_HEAP_LINES] / rdict[TOTAL_LINES])

    rdict[TOTAL_HITS]         = (rdict[TOTAL_HEAP_HITS] + rdict[TOTAL_UNKN_HITS])
    rdict[KNOWN_HITS_RATIO]   = 0.0 if rdict[TOTAL_HITS] == 0 else \
                                (rdict[TOTAL_HEAP_HITS] / rdict[TOTAL_HITS])

    rdict[TOTAL_MISSES]       = (rdict[TOTAL_HEAP_MISSES] + rdict[TOTAL_UNKN_MISSES])
    rdict[KNOWN_MISSES_RATIO] = 0.0 if rdict[TOTAL_MISSES] == 0 else \
                                (rdict[TOTAL_HEAP_MISSES] / rdict[TOTAL_MISSES])

    rdict[TOTAL_ACCS]         = rdict[TOTAL_HITS] + rdict[TOTAL_MISSES]
    rdict[HEAP_ACCS]          = (rdict[TOTAL_HEAP_HITS] + rdict[TOTAL_HEAP_MISSES])
    rdict[KNOWN_ACCS_RATIO]   = 0.0 if rdict[TOTAL_ACCS] == 0 else \
                                (rdict[HEAP_ACCS] / rdict[TOTAL_ACCS])

    rdict[TOTAL_HITS_RATIO]   = 0.0 if rdict[TOTAL_ACCS] == 0 else \
                                (rdict[TOTAL_HITS] / rdict[TOTAL_ACCS])
    rdict[HEAP_HITS_RATIO]    = 0.0 if rdict[HEAP_ACCS] == 0 else \
                                (rdict[TOTAL_HEAP_HITS] / rdict[HEAP_ACCS])

    rdict[UNKN_ACCS]          = (rdict[TOTAL_UNKN_HITS] + rdict[TOTAL_UNKN_MISSES])
    rdict[UNKN_HITS_RATIO]    = 0.0 if rdict[UNKN_ACCS] == 0 else \
                                (rdict[TOTAL_UNKN_HITS] / rdict[UNKN_ACCS])

    if err:
      for key in rdict:
        rdict[key] = None
    #print(rdict)

def aggregate_results(bench, cfg_name, iters):
  avgs = dict()
  for i in range(iters):
    for key,val in results[bench][cfg_name][i].items():
      if not isinstance(val, list) and not isinstance(val, dict):
        if key in avgs:
          avgs[key] += val
        else:
          avgs[key] = val
  return avgs

def get_results_dict(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  full_parse=False):

  results = dict()
  for bench, cfg_name, i in expiter(benches, cfgs, iters):
    if not bench in results:
      results[bench] = dict()

    if not cfg_name in results[bench]:
      results[bench][cfg_name] = dict()

    results[bench][cfg_name][i] = dict()

    rdict = results[bench][cfg_name][i]
    results_dir = get_iter_results_dir(bench, cfg_name, i)
    if stat in [EXE_TIME,PAGE_RSS,RUNTIME] or full_parse:
      parse_time(bench, cfg_name, results_dir, rdict)
    elif stat in marena_stats or full_parse:
      parse_marena(bench, cfg_name, results_dir, rdict)
    elif stat in numastat_stats or full_parse:
      parse_numastat(results_dir, rdict)
    elif stat in memreserve_stats or full_parse:
      parse_memreserve(results_dir, rdict)
    elif stat in pcm_stats or full_parse:
      parse_pcm(results_dir, rdict)
    elif stat in site_info_stats or full_parse:
      parse_site_info(bench, cfg_name, i, rdict)

  return results

def get_report_strs(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  basecfg='default_noir', style=MEAN, cis=True, absolute=False):

  if cis and iters < 2:
    cis = False

  results = get_results_dict(benches, cfgs, iters, stat, False)
  report_strs = dict()

  averages = dict()
  for expcfg in cfgs:
    averages[expcfg] = []

  for bench in results.keys():
    report_strs[bench] = dict()

    if not absolute:
      basevals   = [ results[bench][basecfg][i][stat] for i in \
                     results[bench][basecfg].keys() ]
      if None in basevals:
        basemean = basemedian = basestd = None
      else:
        basemean   = mean(basevals) 
        basemedian = median(basevals) 
        basestd    = stdev(basevals) if len(basevals) > 1 else 0.0

    for expcfg in results[bench].keys():
      expvals   = [ results[bench][expcfg][i][stat] for i in \
                     results[bench][expcfg].keys() ]

      if None in expvals:
        expmean = expmedian = expstd = None
      else:
        #print (expvals)
        expmean   = mean(expvals)
        expmedian = median(expvals)
        expstd    = stdev(expvals) if len(expvals) > 1 else 0.0

      if cis:
        if absolute:
          ci = get95CI(expmean, expmean, expstd, expstd, iters)
        else:
          ci = get95CI(basemean, expmean, basestd, expstd, iters)
          ci /= expmean

      val = None
      if expmean != None:
        if not absolute:
          if style == MEAN:
            val = (float(expmean) / basemean) if basemean > 0.0 else None
          else:
            val = (float(expmedian) / basemedian) if basemedian > 0.0 else None
        else:
          val = (float(expmean)) if style == MEAN else (float(expmedian))

      if val == None:
        report_strs[bench][expcfg] = "N/A" if not cis else ("N/A", "N/A")
      else:
        if absolute:
          if "ratio" in stat:
            report_strs[bench][expcfg] = ("%4.3f" % val) if not cis else \
                                         (("%4.3f" % val), (("%4.3f" % ci)).rjust(6))
          else:
            report_strs[bench][expcfg] = ("%5.1f" % val) if not cis else \
                                         (("%5.1f" % val), (("%5.1f" % ci)).rjust(6))
        else:
          report_strs[bench][expcfg] = ("%4.3f" % val) if not cis else \
                                       (("%4.3f" % val), (("%3.2f" % ci)).rjust(6))

      if val != None:
        averages[expcfg].append(val)

  report_strs["average"] = dict()
  for expcfg in cfgs:
    val = None
    if (len(averages[expcfg]) > 0):
      if not absolute:
        val = safe_geo_mean(averages[expcfg])
      else:
        val = mean(averages[expcfg])

    if val == None:
      report_strs["average"][expcfg] = "N/A" if not cis else ("N/A", "-".rjust(6))
    else:
      if absolute:
        if "ratio" in stat:
          report_strs["average"][expcfg] = ("%4.3f" % val) if not cis else \
                                           (("%4.3f" % val), ("-".rjust(6)))
        else:
          report_strs["average"][expcfg] = ("%5.1f" % val) if not cis else \
                                           (("%5.1f" % val), ("-".rjust(6)))
      else:
        report_strs["average"][expcfg] = ("%4.3f" % val) if not cis else \
                                         (("%4.3f" % val), ("-".rjust(6)))
                                      
  return report_strs

# Gets a dict of results for the ddr_bandwidth configs
def get_ddr_bandwidth_results(benches, cfgs):
  profcfg_results = dict()
  bandwidth_profile_results = dict()
  for bench in benches:
    profcfg_results[bench] = dict()
    bandwidth_profile_results[bench] = dict()
    for cfg_name in cfgs:
      cfg.read_cfg(cfg_name)
      # Get the results for the profcfg
      profcfg_results[bench][cfg_name] = dict()
      bandwidth_profile_results[bench][cfg_name] = dict()

      profcfg_results_dir = get_results_dir(bench, cfg.current_cfg['runcfg']['profcfg'], 0)
      parse_bench(bench, cfg.current_cfg['runcfg']['profcfg'], profcfg_results_dir, profcfg_results[bench][cfg_name])
      bandwidth_profile_dir = results_dir + bench + '/' + cfg_name + '/'

      # Get the bandwidth of the baseline run
      refres = None
      idirs    = [ x+'/' for x in glob(bandwidth_profile_dir+"*") ]
      for x in idirs:
        if x.endswith('i0/'):
          refres = dict()
          parse_pcm(x, refres)
          #print("refres: avg: %5.2f max: %5.2f" % \
          #      (refres[AVG_DDR4_BANDWIDTH], refres[MAX_DDR4_BANDWIDTH]))
      for idir in idirs:
        site = tuple([int(idir.split('/')[-2].strip('i'))])
        bandwidth_profile_results[bench][cfg_name][site[0]] = dict()
        parse_pcm(idir, bandwidth_profile_results[bench][cfg_name][site[0]], refres=refres)

  return (profcfg_results, bandwidth_profile_results)


# MRJ -- this code is not useful but I'm keeping it around in case we need a
# knapsack implementation for something
#
def object_writes_knapsack(objlist, cutrat, sigfigs=5):

  sizekey = 1
  accskey = 2
  rdwrkey = 5

  vals = []
  total_accs = total_writes = 0
  for idx,obj in enumerate(objlist):
    if obj[sizekey]:
      obj_writes = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
      total_accs   += obj[accskey]
      total_writes += obj_writes
      vals.append((obj_writes, obj[sizekey], idx))

  if (len(vals)>1000000):
    sigfigs -= 1
  print (sigfigs)

  WRITES = 0
  SIZE   = 1
  IDX    = 2

  target = ( total_accs * (cutrat / 100))

  wincs = (10**sigfigs)
  cutoff = (target / total_writes)
  print( "target: %8.2f total: %d cutoff: %4.4f" % (target, total_writes, cutoff) )

  wgt_vals = [ (v[SIZE], int((float(v[WRITES]) / total_writes) * wincs), idx) \
                for v in vals ]

  wgt_vals.sort(key=lambda tup: tup[WRITES], reverse=True)

  sig_wgts = [ x for x in wgt_vals if x[WRITES] > 0 ]
  insig_wgts = wgt_vals[len(sig_wgts):]

  wgt_incs = range(int(cutoff*wincs))

  objs = {}
  for v in vals:
    objs[v[IDX]] = (v[WRITES], v[SIZE])

  m = np.zeros((len(sig_wgts)+1, len(wgt_incs)))

  for i,wgt in enumerate(sig_wgts,start=1):
    for j,inc in enumerate(wgt_incs):
      if wgt[WRITES] <= inc:
        m[i][j] = max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + objs[wgt[IDX]][SIZE])
      else:
        m[i][j] = m[i-1][j]
    if i % 1000 == 0:
      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))

  hots = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      hots.append(sig_wgts[i-1][IDX])
      j -= sig_wgts[i-1][WRITES]
    i -= 1

  for wgt in insig_wgts:
    hots.append(wgt[IDX])

  hotset_writes = 0
  for idx in hots:
    obj = objlist[idx]
    obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
    hotset_writes += obj_writes

  print ("target_writes: %3.2f" % (target        / (1024*1024)))
  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

#  m = []
#  m.append([])
#  for j in wgt_incs:
#    m[0].append(0)
#
#  for i,wgt in enumerate(sig_wgts,start=1):
#    m.append([])
#    for j,inc in enumerate(wgt_incs):
#      if wgt[WRITES] <= inc:
#        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + objs[wgt[IDX]][SIZE]))
#      else:
#        m[i].append(m[i-1][j])
#    if i % 1000 == 0:
#      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))
#
#  hots = []
#  i = len(sig_wgts)
#  j = (len(wgt_incs)-1)
#  while i > 0:
#    if m[i][j] != m[i-1][j]:
#      hots.append(sig_wgts[i-1][IDX])
#      j -= sig_wgts[i-1][WRITES]
#    i -= 1
#
#  for wgt in insig_wgts:
#    hots.append(wgt[IDX])
#
#  hotset_writes = 0
#  for idx in hots:
#    obj = objlist[idx]
#    obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
#    hotset_writes += obj_writes
#
#  print ("target_writes: %3.2f" % (target        / (1024*1024)))
#  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

  return hots

def site_writes_knapsack(sinfo, cutrat, sigfigs=5):

  vals = []
  total_accs = total_writes = 0
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      total_accs   += (rec[TOTAL_READS] + rec[TOTAL_WRITES])
      total_writes += rec[TOTAL_WRITES]
      vals.append((rec[TOTAL_WRITES], rec[LINE_RSS], site))

  if (len(vals)>1000000):
    sigfigs -= 1
  print (sigfigs)

  WRITES = 0
  SIZE   = 1
  SITE   = 2

  target = ( total_accs * (cutrat / 100))

  wincs = (10**sigfigs)
  cutoff = (target / total_writes)
  print( "target: %8.2f total: %d cutoff: %4.4f" % (target, total_writes, cutoff) )

  wgt_vals = [ (v[SIZE], int((float(v[WRITES]) / total_writes) * wincs), idx) \
                for v in vals ]

  wgt_vals.sort(key=lambda tup: tup[WRITES], reverse=True)

  sig_wgts = [ x for x in wgt_vals if x[WRITES] > 0 ]
  insig_wgts = wgt_vals[len(sig_wgts):]

  wgt_incs = range(int(cutoff*wincs))

  sites = {}
  for v in vals:
    sites[v[SITE]] = (v[WRITES], v[SIZE])

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[WRITES] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + sites[wgt[SITE]][SIZE]))
      else:
        m[i].append(m[i-1][j])
    if i % 1000 == 0:
      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))

  hots = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      hots.append(sig_wgts[i-1][SITE])
      j -= sig_wgts[i-1][WRITES]
    i -= 1

  for wgt in insig_wgts:
    hots.append(wgt[SITE])

  hotset_writes = 0
  for site in hots:
    hotset_writes += (sinfo[site][TOTAL_WRITES])

  print ("target_writes: %3.2f" % (target        / (1024*1024)))
  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

  return hots

def object_apb_cut_old(objlist, cutpct):

  accskey    = 0
  sizekey    = 1
  apbkey     = 2
  bad_objs   = 0
  total_size = 0

  vals = []
  for idx,obj in enumerate(objlist):
    if isnan(obj[apbkey]) or isinf(obj[apbkey]):
      bad_objs += 1
    else:
      vals.append((obj[apbkey], obj[sizekey], idx))

  accesses_per_byte = []
  zero_vals     = [ x for x in vals if x[0] == 0 ]
  non_zero_vals = [ x for x in vals if x[0] != 0 ]

  for apb,size,idx in non_zero_vals:
    accesses_per_byte.append((apb, idx))

  if (len(accesses_per_byte) > 0):
    accesses_per_byte.sort(key=lambda tup: tup[0], reverse=True)

  zero_vals.sort(key=lambda tup: tup[1])
  for apb,size,idx in zero_vals:
    accesses_per_byte.append((apb, idx))

  cut    = int((len(accesses_per_byte) * (cutpct / 100)))
  cutset = [ x[1] for x in accesses_per_byte[:cut] ]

  if bad_objs:
    print("bad objects: %d" % bad_objs)

  return cutset

def object_hotset(objlist, cutpct):

  accskey    = 0
  sizekey    = 1
  apbkey     = 2
  bad_objs   = 0
  total_size = 0

  vals = []
  for idx,obj in enumerate(objlist):
    if isnan(obj[apbkey]) or isinf(obj[apbkey]):
      bad_objs += 1
    else:
      vals.append((obj[apbkey], obj[sizekey], idx))

  target = ( (sum( [ v[0] for v in vals ] ) * (cutpct / 100)))

  accesses_per_byte = []
  zero_vals     = [ x for x in vals if x[0] == 0 ]
  non_zero_vals = [ x for x in vals if x[0] != 0 ]

  for apb,size,idx in non_zero_vals:
    accesses_per_byte.append((apb, idx))

  if (len(accesses_per_byte) > 0):
    accesses_per_byte.sort(key=lambda tup: tup[0], reverse=True)

  zero_vals.sort(key=lambda tup: tup[1])
  for apb,size,idx in zero_vals:
    accesses_per_byte.append((apb, idx))

  hotset = []
  for apb,idx in accesses_per_byte:
    total_size += objlist[idx][sizekey]
    hotset.append(idx)
    if total_size > target:
      break

  print("%10d %10d" % (total_size, target))
  if bad_objs:
    print("bad objects: %d" % bad_objs)

  return hotset

