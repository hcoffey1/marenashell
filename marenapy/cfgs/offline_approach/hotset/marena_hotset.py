from marenapy.cfgs.buildcfgs.marena_buildcfg import *

current_cfg['name'] = 'marena_hotset'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['hotset.txt'],
  'outputfiles': ['profiling.txt'],
  'profcfg': 'marena_profiling',
  'hotset_percentage': 100,
  'env_variables': {
    'XPS_NodeMask': '1',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_hotset_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v'
}
