#!/usr/bin/env python3

import glob
import os

xs = [12.5, 25, 50]
#ys = [ 128, 256, 512, 1024, 4096, 8192, 16384 ]
ys = [ 2048 ]
#yd = {
#  12.5 : [ 12.5 ],
#  25   : [ 25   ],
#  50   : [ 50   ],
#}

for x in xs:
  src  = ("%s/marena_hotset_three_cxt_avg_pebs_4096_%s_%s.py" % (os.getcwd(), str(x), str(x)))

  for y in ys:
    dst = ("%s/marena_hotset_three_cxt_avg_pebs_%s_%s_%s.py" % (os.getcwd(), str(y), str(x), str(x)))
    
    cmd = ("cp %s %s" % (src, dst))
    print (cmd)
    os.system(cmd)
  
    cmd = ("sed -i 's/4096/%s/g' %s" % (str(y), dst))
    print (cmd)
    os.system(cmd)
