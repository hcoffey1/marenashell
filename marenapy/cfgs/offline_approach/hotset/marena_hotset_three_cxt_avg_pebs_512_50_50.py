from marenapy.cfgs.buildcfgs.marena_buildcfg_threelayer import *

current_cfg['name'] = 'marena_hotset_three_cxt_avg_pebs_512_50_50'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['hotset.txt'],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'profcfg': 'marena_shared_site_profiling_three_cxt_512',
  'hotset_percentage': 50,
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '4',
    'XPS_NodeMask': '2',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_hotset_three_cxt_avg_pebs_512_50_50_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m', 'memreserve-50'],
  'wrapper': 'time -v numactl --preferred=1',
}

