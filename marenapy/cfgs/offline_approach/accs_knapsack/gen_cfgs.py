#!/usr/bin/env python3

import os

xs = [2.083,4.166,8.333,16.667]

for x in xs:
  a = ("%s/marena_avg_bandwidth_accs_knapsack_%s.py" % (os.getcwd(), str(x)))
  b = ("%s/marena_avg_ddr_bandwidth_accs_knapsack_%s.py" % (os.getcwd(), str(x)))
  cmd = ("cp %s %s" % (a, b))
  print (cmd)
  os.system(cmd)

for x in xs:
  a = ("%s/marena_avg_ddr_bandwidth_accs_knapsack_%s.py" % (os.getcwd(), str(x)))
  cmd = ("sed -i 's/marena_bandwidth_profile/marena_ddr_bandwidth_profile/g' %s" % (a))
  print (cmd)
  os.system(cmd)

  cmd = ("sed -i 's/avg_bandwidth_accs/avg_ddr_bandwidth_accs/g' %s" % (a))
  print (cmd)
  os.system(cmd)

