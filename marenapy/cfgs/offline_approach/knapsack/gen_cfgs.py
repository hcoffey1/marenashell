#!/usr/bin/env python3

import glob
import os

xs = [3.125, 6.25, 12.5, 25]
yd = {
  3.125 : [ 1.5625, 3.125, 6.25 ],
  6.25  : [ 3.125,   6.25, 12.5 ],
  12.5  : [ 6.25,    12.5, 25   ],
  25.0  : [ 12.5,    25,   50   ],
}

for x in xs:
  a = ("%s/marena_knapsack_three_cxt_%s.py" % (os.getcwd(), str(x)))
  for y in yd[x]:
    b = ("  %s/marena_knapsack_three_cxt_%s_%s.py" % (os.getcwd(), str(y), str(x)))
    cmd = ("cp %s %s" % (a, b))
    print (cmd)
    os.system(cmd)
  cmd = ("rm -f %s" % (a))
  print (cmd)
  os.system(cmd)

for x in xs:
  a = ("%s/marena_knapsack_three_cxt_%s.py" % (os.getcwd(), str(x)))
  for y in yd[x]:
    b = ("  %s/marena_knapsack_three_cxt_%s_%s.py" % (os.getcwd(), str(y), str(x)))

    pct_a = b.split('/')[-1].split('_')[4]
    pct_b = b.split('/')[-1].split('_')[5][:-3]

    cmd = ("sed -i 's/%s,/%s,/g' %s" % (pct_b, pct_a, b))
    print (cmd)
    os.system(cmd)

    cmd = ("sed -i 's/marena_knapsack_three_cxt_%s/marena_knapsack_three_cxt_%s_%s/g' %s" % (pct_b, pct_a, pct_b, b))
    print (cmd)
    os.system(cmd)

