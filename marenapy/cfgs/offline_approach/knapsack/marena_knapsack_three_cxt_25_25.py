from marenapy.cfgs.buildcfgs.marena_buildcfg_threelayer import *

current_cfg['name'] = 'marena_knapsack_three_cxt_25_25'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['hotset.txt'],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'profcfg': 'marena_shared_site_profiling_three_cxt',
  'knapsack_percentage': 25,
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '4',
    'XPS_NodeMask': '1',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_knapsack_three_cxt_25_25_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'memreserve-25'],
  'wrapper': 'time -v numactl --preferred=0',
}
