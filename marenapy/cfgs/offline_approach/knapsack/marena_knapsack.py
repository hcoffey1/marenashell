from marenapy.cfgs.buildcfgs.marena_buildcfg import *

current_cfg['name'] = 'marena_knapsack'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['hotset.txt'],
  'outputfiles': ['profiling.txt'],
  'profcfg': 'marena_profiling',
  'knapsack_percentage': 100,
  'env_variables': {
    'XPS_NodeMask': '1',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_knapsack_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v'
}
