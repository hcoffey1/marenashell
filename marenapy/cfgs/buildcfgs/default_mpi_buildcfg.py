from marenapy.paths import *
current_cfg = {}
current_cfg['buildcfg'] = {

  # Name of the buildcfg
  'name': 'default_mpi_buildcfg',

  # Compilers
  'backend_compiler':  tool_dir + 'openmpi-3.0.0/bin/mpicc',
  'c_compiler':        tool_dir + 'openmpi-3.0.0/bin/mpicc',
  'cxx_compiler':      tool_dir + 'openmpi-3.0.0/bin/mpic++',
  'fortran_compiler':  tool_dir + 'openmpi-3.0.0/bin/mpifort',

  # Linkers
  'c_linker':          tool_dir + 'openmpi-3.0.0/bin/mpicc',
  'cxx_linker':        tool_dir + 'openmpi-3.0.0/bin/mpic++',
  'fortran_linker':    tool_dir + 'openmpi-3.0.0/bin/mpifort',
  'ir_linker':         tool_dir + 'llvm-4.0.1/bin/llvm-link',

  # Flags
  'compiler_flags':    '-g -c -fopenmp -I' + tool_dir + 'flang/include',
  'ir_compiler_flags': '-g -S -fopenmp -I' + tool_dir + 'flang/include '
                       '-emit-llvm -Wno-everything',
  'ir_linker_flags':   '-S',
  'linker_flags':      '-g -fopenmp -L' + tool_dir + 'flang/lib64 -lflang '
                       '-lflangrti -L' + marena_dir + 'benmarena/build '
                       '-lmarena -L' + marena_dir + 'libjemalloc/lib -ljemalloc '
                       '-Wl,-rpath,' + tool_dir + 'flang/lib64 '
                       '-Wl,-rpath,' + tool_dir + 'flang/lib '
                       '-Wl,-rpath,' + marena_dir + 'benmarena/build '
                       '-Wl,-rpath,' + marena_dir + 'libjemalloc/lib',

  # Bools
  'transform': False,
  'toir': False,

  # Tong's tool
  'inputfiles': [],
  'pass_path': marena_dir + 'llparser/passes',
  'sopt': marena_dir + 'llparser/bin/sopt',

  # MPI wrappers
  'ompi_cc': tool_dir + 'flang/bin/clang',
  'ompi_cxx': tool_dir + 'flang/bin/clang++',
  'ompi_fc': tool_dir + 'flang/bin/flang -mp'
}
