from marenapy.paths import *
current_cfg = {}
current_cfg['buildcfg'] = {

  # Name of the buildcfg
  'name': 'default_buildcfg',

  # Compilers
  'backend_compiler':  tool_dir + 'flang/bin/clang',
  'c_compiler':        tool_dir + 'flang/bin/clang',
  'cxx_compiler':      tool_dir + 'flang/bin/clang++',
  'fortran_compiler':  tool_dir + 'flang/bin/flang -mp',

  # Linkers
  'c_linker':          tool_dir + 'flang/bin/clang',
  'cxx_linker':        tool_dir + 'flang/bin/clang++',
  'fortran_linker':    tool_dir + 'flang/bin/flang -mp',
  'ir_linker':         tool_dir + 'flang/bin/llvm-link',

  # Flags
  'compiler_flags':    '-c -fopenmp -I' + tool_dir + 'flang/include',
  'ir_compiler_flags': '-S -fopenmp -I' + tool_dir + 'flang/include'
                       '-emit-llvm -Wno-everything',
  'ir_linker_flags':   '-S',
  'linker_flags':      '-L'          + '/home/libs/jemalloc-5.0.1/lib' + ' -ljemalloc '
                       '-fopenmp -L' + tool_dir   + 'flang/lib -lflang -lflangrti ' # Link flang libraries
                       '-Wl,-rpath,' + tool_dir   + 'flang/lib ' # Add all to the rpath
                       '-Wl,-rpath,' + '/home/libs/jemalloc-5.0.1/lib',

  'specperllib':       benches_dir + 'cpu2017/bin/modules.specpp',

  # Bools
  'transform': False,
  'toir': False,

  # Tong's tool
  'inputfiles': [],
  'pass_path': marena_dir + 'llparser/passes',
  'sopt': marena_dir + 'llparser/bin/sopt',
}
