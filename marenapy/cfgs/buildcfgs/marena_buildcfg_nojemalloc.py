from marenapy.paths import *
current_cfg = {}
current_cfg['buildcfg'] = {

  # Name of the buildcfg
  'name': 'marena_buildcfg_nojemalloc',

  # Compilers
  'backend_compiler':  tool_dir + 'flang/bin/clang',
  'c_compiler':        tool_dir + 'flang/bin/clang',
  'cxx_compiler':      tool_dir + 'flang/bin/clang++',
  'fortran_compiler':  tool_dir + 'flang/bin/flang',

  # Linkers
  'c_linker':          tool_dir + 'flang/bin/clang',
  'cxx_linker':        tool_dir + 'flang/bin/clang++',
  'fortran_linker':    tool_dir + 'flang/bin/flang',
  'ir_linker':         tool_dir + 'llvm-4.0.1/bin/llvm-link',

  # Flags
  'compiler_flags':    '-g -c -fopenmp -I' + tool_dir + 'flang/include',
  'ir_compiler_flags': '-g -S -fopenmp -I' + tool_dir + 'flang/include '
                       '-emit-llvm -Wno-everything',
  'ir_linker_flags':   '-S',
  'linker_flags':      '-g -fopenmp -L' + tool_dir + 'flang/lib64 -lflang '
                       '-lflangrti -L' + marena_dir + 'benmarena/build '
                       '-lmarena '
                       '-Wl,-rpath,' + tool_dir + 'flang/lib64 '
                       '-Wl,-rpath,' + tool_dir + 'flang/lib '
                       '-Wl,-rpath,' + marena_dir + 'benmarena/build ',

  # Bools
  'transform': True,
  'toir': True,

  # Tong's tool
  'inputfiles': ['post-contexts.txt'],
  'pass_path': marena_dir + 'llparser/passes',
  'sopt': marena_dir + 'llparser/bin/sopt',
}
