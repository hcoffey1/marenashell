from marenapy.paths import *
current_cfg = {}
current_cfg['buildcfg'] = {

  # Name of the buildcfg
  'name': 'marena_threelayer_test_buildcfg',

  # Compilers
  'backend_compiler':  tool_dir + 'flang-new/bin/clang',
  'c_compiler':        tool_dir + 'flang-new/bin/clang',
  'cxx_compiler':      tool_dir + 'flang-new/bin/clang++',
  'fortran_compiler':  tool_dir + 'flang-new/bin/flang',

  # Linkers
  'c_linker':          tool_dir + 'flang-new/bin/clang',
  'cxx_linker':        tool_dir + 'flang-new/bin/clang++',
  'fortran_linker':    tool_dir + 'flang-new/bin/flang',
  'ir_linker':         tool_dir + 'llvm-4.0.1/bin/llvm-link',

  # Flags
  'compiler_flags':    '-g -march=knl -c -fopenmp -I' + tool_dir + 'flang-new/include ',
  'ir_compiler_flags': '-g -march=knl -S -fopenmp -I' + tool_dir + 'flang-new/include '
                       '-emit-llvm -Wno-everything',
  'ir_linker_flags':   '-S',
  'linker_flags':      '-L' + marena_dir + 'benmarena/build -lmarena ' # Link Marena
                       '-L' + marena_dir + 'orig-libjemalloc/lib -ljemalloc ' #Link jemalloc
                       '-fopenmp -L' + tool_dir + 'flang-new/lib64 -lflang -lflangrti ' # Link flang libraries
                       '-Wl,-rpath,' + tool_dir + 'flang-new/lib64 ' # Add all to the rpath
                       '-Wl,-rpath,' + tool_dir + 'flang-new/lib '
                       '-Wl,-rpath,' + marena_dir + 'benmarena/build '
                       '-Wl,-rpath,' + marena_dir + 'orig-libjemalloc/lib',

  # To find SPEC's Perl modules
  'specperllib': benches_dir + 'cpu2017/bin/modules.specpp',

  # Bools
  'transform': True,
  'toir': True,

  # Tong's tool
  'inputfiles': ['contexts.txt','post-contexts.txt'],
  'pass_path': marena_dir + 'llparser/passes',
  'sopt': marena_dir + 'llparser/bin/sopt',
  'context_layers': '4'
}
