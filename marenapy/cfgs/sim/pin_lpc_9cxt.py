from marenapy.cfgs.pincfgs.default_large_cache import *

current_cfg['name'] = 'pin_lpc_9cxt'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [ 'mc_stats.out',          'site_cxt_strs.out',
                   'cache_line_info.out',   'page_info.out',
                   'site_cxt0_info.out',    'site_cxt1_info.out',
                   'site_cxt2_info.out',    'site_cxt3_info.out',
                   'site_cxt4_info.out',    'site_cxt5_info.out',
                   'site_cxt6_info.out',    'site_cxt7_info.out',
                   'site_cxt8_info.out',    'site_cxt0_stats.out',
                   'site_cxt1_stats.out',   'site_cxt2_stats.out',
                   'site_cxt3_stats.out',   'site_cxt4_stats.out',
                   'site_cxt5_stats.out',   'site_cxt6_stats.out',
                   'site_cxt7_stats.out',   'site_cxt8_stats.out',
                   'site_cxt0_cxts.out',    'site_cxt1_cxts.out',
                   'site_cxt2_cxts.out',    'site_cxt3_cxts.out',
                   'site_cxt4_cxts.out',    'site_cxt5_cxts.out',
                   'site_cxt6_cxts.out',    'site_cxt7_cxts.out',
                   'site_cxt8_cxts.out',    'size_info.out',
                   'size_bucket_info.out',  'type_sig_info.out',
                   'acc_sig_info.out',      'size_stats.out',
                   'size_bucket_stats.out', 'type_sig_stats.out',
                   'acc_sig_stats.out',     'type_strs.out',
                   'acc_strs.out',          'type_sigs.out',
                   'acc_sigs.out',          'obj_info.out',
                   'ins_count.out'
                 ],
  'env_variables': {
    'XPS_CollectStrings' : 1,
    'XPS_CxtSize'        : 9,
  },
  'name': 'pin_large_cache',
  'size': 'ref',
  'threads': 1,
  'tools': [],
  'wrapper': 'time -v'
}

