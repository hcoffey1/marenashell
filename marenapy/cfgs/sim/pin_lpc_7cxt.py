from marenapy.cfgs.pincfgs.default_large_cache import *

current_cfg['name'] = 'pin_lpc_7cxt'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [ 'memtracer.out', 'mc_stats.out', 'thread_ap_info.out',
                   'malloc.log', 'profiling.txt', 'run_ap_map.out',
                   'run_ap_info.out', 'rss_api.out', 'orig_ap_info.out',
                   'bt_sites.txt', 'cache_line_info.out', 'page_info.out',
                   'live_info.out', 'stats_info.out','obj_info.out'],
  'env_variables': {
    'XPS_CollectStrings' : 1,
    'XPS_CxtSize'        : 7,
  },
  'name': 'pin_large_cache',
  'size': 'ref',
  'threads': 1,
  'tools': [],
  'wrapper': 'time -v'
}

