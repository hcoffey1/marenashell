from marenapy.cfgs.pincfgs.default_large_cache import *

current_cfg['name'] = 'pin_phase_lpc_9cxt_test'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [ 'mc_stats.out',          'site_cxt_strs.out',
                   'cache_line_info.out',   'page_info.out',
                   'site_cxt0_info.out',    'site_cxt1_info.out',
                   'site_cxt2_info.out',    'site_cxt4_info.out',
                   'site_cxt8_info.out',    'site_cxt0_stats.out',
                   'site_cxt1_stats.out',   'site_cxt2_stats.out',
                   'site_cxt4_stats.out',   'site_cxt8_stats.out',
                   'site_cxt0_cxts.out',    'site_cxt1_cxts.out',
                   'site_cxt2_cxts.out',    'site_cxt4_cxts.out',
                   'site_cxt8_cxts.out',    'size_info.out',
                   'size_bucket_info.out',  'type_sig_info.out',
                   'acc_sig_info.out',      'size_stats.out',
                   'size_bucket_stats.out', 'type_sig_stats.out',
                   'acc_sig_stats.out',     'type_strs.out',
                   'acc_strs.out',          'obj_info.out',
                   'type_sigs.out',         'acc_sigs.out',
                   'phase_sigs.out',        'phase_sig_info.out',
                   'size_typesigs.out',     'size_typesig_info.out',
                   'phase_size.out',        'phase_size_info.out',
                   'phase_site0_info.out',  'phase_site1_info.out',
                   'phase_site2_info.out',  'phase_site4_info.out',
                   'phase_site8_info.out',  'phase_site0.out',
                   'phase_site1.out',       'phase_site2.out',
                   'phase_site4.out',       'phase_site8.out',
                   'phsz_site0_info.out',   'phsz_site1_info.out',
                   'phsz_site2_info.out',   'phsz_site4_info.out',
                   'phsz_site8_info.out',   'phsz_site0.out',
                   'phsz_site1.out',        'phsz_site2.out',
                   'phsz_site4.out',        'phsz_site8.out',
                   'phase_info.out',        'ins_count.out'
                 ],
  'env_variables': {
    'XPS_CollectStrings' : 1,
    'XPS_CxtSize'        : 9,
  },
  'name': 'pin_large_cache',
  'size': 'test',
  'threads': 1,
  'tools': [],
  'wrapper': 'time -v'
}

