from marenapy.cfgs.pincfgs.default_proccount import *

current_cfg['name'] = 'pin_proccount'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {
    'XPS_CollectStrings' : 1,
    'XPS_CxtSize'        : 8,
  },
  'name': 'pin_proccount',
  'size': 'test',
  'threads': 1,
  'tools': [],
  'wrapper': 'time -v'
}

