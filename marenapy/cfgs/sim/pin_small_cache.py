from marenapy.cfgs.pincfgs.default_small_cache import *

current_cfg['name'] = 'small_cache_pin'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [ 'memtracer.out', 'mc_stats.out', 'thread_ap_info.out',
                   'malloc.log', 'profiling.txt', 'run_ap_map.out',
                   'run_ap_info.out', 'rss_api.out', 'orig_ap_info.out',
                   'bt_sites.txt', 'cache_line_info.out', 'page_info.out',
                   'live_info.out'],
  'env_variables': {
    'XPS_CollectStrings' : 1,
  },
  'name': 'small_cache_pin',
  'size': 'ref',
  'threads': 1,
  'tools': [],
  'wrapper': 'time -v'
}

