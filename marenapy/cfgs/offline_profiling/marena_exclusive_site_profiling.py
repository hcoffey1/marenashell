from marenapy.cfgs.buildcfgs.marena_buildcfg import *

current_cfg['name'] = 'marena_exclusive_site_profiling'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '6',
    'XPS_NodeMask': '1',
    'XPS_PageMapSampling': '1',
    'XPS_PebsSampling': '1'},
  'name': 'marena_shared_site_profiling_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v'
}
