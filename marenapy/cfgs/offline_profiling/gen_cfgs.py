#!/usr/bin/env python3

import glob
import os

xs = [ 128, 256, 512, 1024, 4096, 8192, 16384 ]
#yd = {
#  12.5 : [ 12.5 ],
#  25   : [ 25   ],
#  50   : [ 50   ],
#}

for x in xs:
  src = ("%s/marena_shared_site_profiling_three_cxt_2048.py" % (os.getcwd()))
  dst = ("%s/marena_shared_site_profiling_three_cxt_%s.py" % (os.getcwd(), str(x)))
  
  cmd = ("cp %s %s" % (src, dst))
  print (cmd)
  os.system(cmd)

  cmd = ("sed -i 's/2048/%s/g' %s" % (str(x), dst))
  print (cmd)
  os.system(cmd)
