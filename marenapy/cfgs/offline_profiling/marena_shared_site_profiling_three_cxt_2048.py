from marenapy.cfgs.buildcfgs.marena_buildcfg_threelayer import *

current_cfg['name'] = 'marena_shared_site_profiling_three_cxt_2048'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '5',
    'XPS_NodeMask': '2',
    'XPS_PebsSampling': '1',
    'XPS_PageMapSampling': '1'},
  'name': 'marena_shared_site_profiling_three_cxt_2048_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m', 'pebs-2048-4096-32768'],
  'wrapper': 'time -v numactl --preferred=1'
}
