from marenapy.cfgs.buildcfgs.default_buildcfg import *

current_cfg['name'] = 'default_noir'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {
  },
  'name': 'default_noir_runcfg',
  'size': 'ref',
  'threads': 1,
  'tools': ['memory'],
  'wrapper': 'time -v'
}
