from marenapy.cfgs.buildcfgs.default_ir_buildcfg import *

current_cfg['name'] = 'cache_100'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {
  },
  'name': 'cache_100_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v numactl --preferred=0'
}
