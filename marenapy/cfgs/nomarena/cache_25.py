from marenapy.cfgs.buildcfgs.default_ir_buildcfg import *

current_cfg['name'] = 'cache_25'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {
  },
  'name': 'cache_25_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m'],
  'wrapper': 'time -v numactl --preferred=0'
}
