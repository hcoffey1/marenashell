from marenapy.cfgs.buildcfgs.optimized_default_buildcfg import *

current_cfg['name'] = 'optimized_default'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {
  },
  'name': 'optimized_default_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m'],
  'wrapper': 'time -v'
}
