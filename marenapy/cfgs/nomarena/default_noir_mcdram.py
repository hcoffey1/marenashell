from marenapy.cfgs.buildcfgs.default_buildcfg import *

current_cfg['name'] = 'default_noir_mcdram'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {
  },
  'name': 'default_noir_mcdram_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v numactl --preferred=1'
}
