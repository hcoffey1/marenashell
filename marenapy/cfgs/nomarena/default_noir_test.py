from marenapy.cfgs.buildcfgs.default_buildcfg import *

current_cfg['name'] = 'default_noir_test'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'env_variables': {
  },
  'name': 'default_noir_test_runcfg',
  'size': 'test',
  'threads': 1,
  'tools': ['memory'],
  'wrapper': 'time -v',
  'runseq': True,
}

