from marenapy.cfgs.buildcfgs.sim_buildcfg import *

current_cfg['pincfg'] = {
  'name'         : 'default_small_cache',
  'pintool'      : 'source/tools/memtracer/obj-intel64/memtracer.so',
  'cache_args'   : '-S',
  'window_shift' : 19,
  'slice_shift'  : 13,
}

