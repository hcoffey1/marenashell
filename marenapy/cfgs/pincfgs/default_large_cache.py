from marenapy.cfgs.buildcfgs.sim_buildcfg import *

  #'pintool'      : 'source/tools/memtracer/obj-intel64/memtracer.so',
current_cfg['pincfg'] = {
  'name'         : 'default_large_cache',
  'pintool'      : 'source/tools/ManualExamples/obj-intel64/inscount0.so',
  'cache_args'   : '',
  'window_shift' : 19,
  'slice_shift'  : 13,
}
