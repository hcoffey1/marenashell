from marenapy.cfgs.buildcfgs.sim_notypes_buildcfg import *

current_cfg['pincfg'] = {
  'name'         : 'notypes_large_cache',
  'pintool'      : 'source/tools/memtracer/obj-intel64/memtracer.so',
  'cache_args'   : '',
  'window_shift' : 19,
  'slice_shift'  : 13,
}

