from marenapy.cfgs.buildcfgs.sim_buildcfg import *

current_cfg['pincfg'] = {
  'name'         : 'default_memory_cache_12_5',
  'pintool'      : 'source/tools/memtracer/obj-intel64/memtracer.so',
  'cache_args'   : '',
  'window_shift' : 19,
  'slice_shift'  : 13,
  'mc_sizepct'   : 12.5,
  'mc_sizecfg'   : 'pin_phase_lpc_9cxt'
}

