from marenapy.cfgs.buildcfgs.marena_buildcfg_onelayer import *

current_cfg['name'] = 'marena_firsttouch_3.125_onelayer'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'profcfg': 'marena_profiling_onelayer',
  'env_variables': {
    'XPS_NodeMask': '2'},
  'name': 'marena_firsttouch_3.125_onelayer_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'memreserve-3.125'],
  'wrapper': 'time -v'
}
