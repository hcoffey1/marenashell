from marenapy.cfgs.buildcfgs.default_ir_buildcfg import *

current_cfg['name'] = 'static_firsttouch_4GB'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'peak_rss_cfg': 'default_ir',
  'env_variables': {
  },
  'name': 'static_firsttouch_4GB_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m', 'memreserve-12.5'],
  'wrapper': 'time -v numactl --preferred=1'
}

