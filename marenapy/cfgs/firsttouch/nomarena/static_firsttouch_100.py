from marenapy.cfgs.buildcfgs.default_ir_buildcfg import *

current_cfg['name'] = 'static_firsttouch_100'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': [],
  'peak_rss_cfg': 'default_ir',
  'env_variables': {
  },
  'name': 'static_firsttouch_100_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v numactl --preferred=1'
}

