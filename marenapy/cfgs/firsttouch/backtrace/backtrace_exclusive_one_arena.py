from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'backtrace_exclusive_one_arena'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt', 'bt_sites.txt'],
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '2',
    'XPS_ContextMode': '3',
    'XPS_CollectStrings': '1',
    'XPS_NodeMask': '1'},
  'name': 'backtrace_exclusive_one_arena_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v'
}
