from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'backtrace_lock'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '2',
    'XPS_ContextMode': '1',
    'XPS_NodeMask': '2'},
  'name': 'backtrace_lock_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m'],
  'wrapper': 'time -v numactl --preferred=1'
}
