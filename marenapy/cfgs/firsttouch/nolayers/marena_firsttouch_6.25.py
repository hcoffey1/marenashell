from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'marena_firsttouch_6.25'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'profcfg': 'marena_shared_site_profiling',
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '5',
    'XPS_NodeMask': '2'},
  'name': 'marena_firsttouch_6.25_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'memreserve-6.25'],
  'wrapper': 'time -v'
}

