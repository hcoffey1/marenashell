from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'marena_firsttouch_25'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '0',
    'XPS_NodeMask': '2'},
  'name': 'marena_firsttouch_25_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m', 'memreserve-25'],
  'wrapper': 'time -v numactl --preferred=1'
}

