from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'marena_shared_site_firsttouch_12.5'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '5',
    'XPS_NodeMask': '2'},
  'name': 'marena_shared_site_firsttouch_12.5_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'memreserve-12.5'],
  'wrapper': 'time -v'
}
