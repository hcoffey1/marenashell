from marenapy.cfgs.buildcfgs.marena_buildcfg_threelayer import *

current_cfg['name'] = 'marena_bandwidth_profile_three_cxt'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'profcfg': 'marena_shared_site_profiling_three_cxt',
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '5',
    'XPS_NodeMask': '1',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_bandwidth_profile_three_cxt_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v numactl --preferred=0',
  'bandwidth_profile': 2
}
