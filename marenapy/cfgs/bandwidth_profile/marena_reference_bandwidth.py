from marenapy.cfgs.buildcfgs.marena_buildcfg_onelayer import *

current_cfg['name'] = 'marena_reference_bandwidth'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt', 'sites.txt'],
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '4',
    'XPS_NodeMask': '2'},
  'name': 'marena_reference_bandwidth_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m'],
  'wrapper': 'time -v numactl --preferred=1'
}

