from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'marena_bandwidth_profile'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': ['post-contexts.txt'],
  'outputfiles': ['profiling.txt'],
  'profcfg': 'marena_shared_site_profiling',
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '5',
    'XPS_NodeMask': '1',
    'XPS_HotAPsFile': 'hotset.txt'},
  'name': 'marena_bandwidth_profile_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory'],
  'wrapper': 'time -v numactl --preferred=0',
  'bandwidth_profile': 2
}
