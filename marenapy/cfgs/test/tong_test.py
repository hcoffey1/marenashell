from marenapy.cfgs.buildcfgs.marena_optimized_default_buildcfg import *

current_cfg['name'] = 'tong_test'
current_cfg['runcfg'] = {
  'copies': 1,
  'inputfiles': [],
  'outputfiles': ['profiling.txt'],
  'peak_rss_cfg': 'default_ir',
  'env_variables': {
    'XPS_NumThreads': '256',
    'XPS_ArenaMode': '4',
    'XPS_NodeMask': '2'},
  'name': 'tong_test_runcfg',
  'size': 'ref',
  'threads': 256,
  'tools': ['memory', 'numastat -m'],
  'wrapper': 'time -v '
}
