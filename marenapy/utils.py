import os
import errno
from distutils.dir_util import copy_tree, remove_tree
from functools import reduce
import operator
from copy import copy
from glob import glob
from marenapy.benches.bench import *

student_t_95 = {
  1  : 12.71,
  2  : 4.303,
  3  : 3.182,
  4  : 2.776,
  5  : 2.571,
  6  : 2.447,
  7  : 2.998,
  8  : 2.306,
  9  : 2.262,
  10 : 2.228,
  11 : 2.201,
  12 : 2.179,
  13 : 2.160,
  14 : 2.145,
  15 : 2.131,
  16 : 2.120,
  17 : 2.110,
  18 : 2.101,
  19 : 2.093,
  20 : 2.086
}

MEAN = "mean"
MEDIAN = "median"

def KB(num):
  return (num / 1024.0)

def MB(num):
  return (KB(num) / (1024.0))

def GB(num):
  return (MB(num) / (1024.0))

def CLMB(num):
  return (MB(num*CACHE_LINE_SIZE))

def mkdir_p(path):
  try:
    os.makedirs(path)
  except OSError as exc:
    if exc.errno == errno.EEXIST:
      pass
    else: raise

def rm_f(path):
  try:
    os.remove(path)
  except OSError as exc:
    if exc.errno == errno.EEXIST:
      pass
    else: raise

def rm_rf(path, root=False):
  if root:
    os.system(("sudo rm -rf %s" % path))
  else:
    try:
      remove_tree(path)
    except OSError as exc:
      if exc.errno == errno.ENOENT:
        pass
      else: raise

def copy_any(src, dst):
  try:
    copy_tree(src, dst)
  except OSError as exc:
    if exc.errno == errno.ENOTDIR:
      copy(src, dst) 
    else: raise

def copy_to_dir(f, dir):
  copy_any(f, (dir+f.split('/')[-1]))

def getdir(dir, create=False, clean=False):
  if clean:
    os.system("rm -rf %s" % dir)
  if create:
    mkdir_p(dir)
  return dir

def source(script):
  proc = subprocess.Popen(
      ['bash', '-c', 'set -a && source {} && env -0'.format(script)], 
      stdout=subprocess.PIPE, shell=False)
  output, err = proc.communicate()
  output = output.decode('utf8')
  env = dict((line.split("=", 1) for line in output.split('\x00') if line))
  return env

def geo_mean(iterable):
  return (reduce(operator.mul, iterable)) ** (1.0/len(iterable))

def safe_mean(iterable, default=None):
  if len(iterable) == 0:
    return default 
  return mean(iterable)

def safe_geo_mean(iterable):
  if (0.0 in iterable):
    xxx = [(x+1) for x in iterable]
    val = (reduce(operator.mul, xxx)) ** (1.0/len(xxx))
    return (val-1)
  return (reduce(operator.mul, iterable)) ** (1.0/len(iterable))

def safediv(num, den, default=0.0):
  return ( (float(num) / den) if den != 0 else default )

# MRJ: this breaks MBI
#def get_results_dir(bench, cfg, it=None):
#  if it == None:
#    basedir = results_dir + bench + '/' + cfg + '/'
#    choices = glob(basedir+"*")
#    first_it = min([ int(x.split('/')[-1].strip('i')) for x in choices ])
#    return (basedir + 'i' + str(first_it) + '/') 
#  else:
#    return (results_dir + bench + '/' + cfg + '/i' + str(it) + '/')
#
#def create_results_dir(bench, cfg, it):
#  path = get_results_dir(bench, cfg, it)
#  rmtree(path, ignore_errors=True)
#  if not os.path.exists(path):
#    os.makedirs(path)
#  return path

def get_results_dir(bench, cfg):
  return (results_dir + bench + "/" + cfg + "/")

def get_iter_results_dir(bench, cfg, it):
  return (get_results_dir(bench, cfg) + ("i%d/" % it))

def get_copy_results_dir(bench, cfg, it, copy):
  return (get_iter_results_dir(bench, cfg, it) + ("copy-%d/" % copy))

def get_run_results_dir(bench, cfg, it, copy, run):
  return (get_copy_results_dir(bench, cfg, it, copy) + ("run-%d/" % run))

def create_run_results_dir(bench, cfg, it, copy, run):
  path = get_run_results_dir(bench, cfg, it, copy, run)
  rmtree(path, ignore_errors=True)
  if not os.path.exists(path):
    os.makedirs(path)
  return path

def create_iter_dir(bench, cfg_name, it):
  path = get_iter_dir(bench, cfg_name, it)
  rmtree(path, ignore_errors=True)
  if not os.path.exists(path):
    os.makedirs(path)
  return path

def get_iter_dir(bench, cfg_name, it):
  bench_dir = get_bench_dir(bench)
  return (bench_dir + ('run/%s/i%d/'%(cfg_name,it)))

def get_copy_dir(bench, cfg_name, it, c):
  return (get_iter_dir(bench, cfg_name, it) + ('copy-%d/'%c))

def get_run_dir(bench, cfg_name, it, copy, run):
  return (get_copy_dir(bench, cfg_name, it, copy) + ("run-%d/" % run))

def get_run_ref_dir(bench, size):
  return (get_bench_dir(bench) + ('run-%s/'%size))

def get_runs(bench, size):
  return range(len(glob((get_run_ref_dir(bench, size) + "run-*.sh"))))

def get_exe_dir(bench, cfg_name):
  return (get_bench_dir(bench) + ('exe/%s/'%cfg_name))

def get_cfg_exe_file(bench, cfg_name):
  return (get_exe_dir(bench, cfg_name) + exe_name(bench))

def exe_name(bench):
  if bench_suite[bench] == 'cpu2017':
    return cpu2017_exe_filename[bench]
  else:
    return ("%s.exe" % bench)

def cut_str(cut, tz):
  pts = ("%8.4f" % tz).split(".")
  pre = pts[0].lstrip().rstrip()
  pst = pts[1].lstrip().rstrip()
  return ("%s_%s_%s" % ( cut.lower(), pre, pst ))

def str2tz(thermos_str):
  pts = thermos_str.split("_")[-2:]
  return ( (float(pts[0]) + (float(pts[1])/10000.0)) )

def backup_objinfo(benches, configs, \
  backup_dir="/data/memcache_backup/", rmsrc=False, \
  verbose=True):

  for bench in benches:
    for cfg_name in configs:

      cfg.read_cfg(cfg_name)
      runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])

      for run in runs:

        dst_dir     = ("%s%s/%s/run-%d/" % (backup_dir, bench, cfg_name, run))
        results_dir = get_run_results_dir(bench, cfg_name, 0, 0, run)

        mkdir_p (dst_dir)
        cmd = ("cp %sobj_info.out %s" % (results_dir, dst_dir))
        if verbose:
          print(cmd)
        os.system(cmd)

        if rmsrc:
          cmd = ("rm -f %sobj_info.out" % (results_dir))
          if verbose:
            print(cmd)
          os.system(cmd)

def backup_results(benches, configs, \
  backup_dir="/data/memcache_backup/", rmsrc=False, \
  verbose=True):

  for bench in benches:
    for cfg_name in configs:

      dst_dir     = ("%s%s/" % (backup_dir, bench))
      results_dir = get_results_dir(bench, cfg_name)

      if not os.path.exists(results_dir):
        print("%s does not exist"%results_dir)
        continue

      mkdir_p (dst_dir)
      cmd = ("cp -r %s %s" % (results_dir, dst_dir))
      if verbose:
        print(cmd)
      os.system(cmd)

      if rmsrc:
        cmd = ("rm -rf %s" % (results_dir))
        if verbose:
          print(cmd)
        os.system(cmd)


#def run_apinfo_file(bench, cfg_name, it, copy, run):
#  return (get_run_dir(bench, cfg_name, it, copy, run)) + run_apinfo_out)
#
#def run_apmap_file(bench, cfg_name, it, copy, run):
#  return (get_run_dir(bench, cfg_name, it, copy, run)) + run_apmap_out)
#
#def memtracer_file(bench, cfg_name, it, copy, run):
#  return (get_run_dir(bench, cfg_name, it, copy, run)) + memtracer_out
#
#def malloc_trace_file(bench, cfg_name, it, copy, run):
#  return (get_run_dir(bench, cfg_name, it, copy, run)) + malloc_trace_out

extra_exes = {
  '511.povray_r'  : [ 'imagevalidate_511' ],
  '521.wrf_r'     : [ 'diffwrf_521' ],
  '525.x264_r'    : [ 'imagevalidate_525', 'ldecod_r' ],
  '526.blender_r' : [ 'imagevalidate_526' ],
  '527.cam4_r'    : [ 'cam4_validate_527' ],
  '538.imagick_r' : [ 'imagevalidate_538' ],
}

# get95CI is used to calculate a confidence interval for the difference between
# two means (basemean and expmean). The value returned can be used to express
# an interval [-ci, +ci], which defines the range that the difference between
# the means will fall between with 95% likelihood.
#
def get95CI(basemean, expmean, basestd, expstd, nruns):

  foo   = ( (basestd**2) / nruns) + ( (expstd**2) / nruns)
  s_x   = sqrt( foo )

  if nruns < 30:

    faz  = (( (basestd**2) / nruns) **2) / (nruns-1)
    fez  = (( (expstd**2)  / nruns) **2) / (nruns-1)
    n_df = int(round( ((foo**2) / (faz + fez)) ))

    ci = (student_t_95[n_df] * s_x)

  else:

    # assume normal distribution
    ci = 1.96 * s_x

  return ci

def expiter(benches, cfgs, iters):

  for bench in benches:
    for cfg in cfgs:
      for i in range(iters):
        yield (bench, cfg, i)

