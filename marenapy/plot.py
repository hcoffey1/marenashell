import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib as mpl
mpl.use('pgf')
import matplotlib.pyplot as plt
import pprint
from marenapy.results import *

def prepare_ddr_bandwidth(results_dict, profcfg_results, accumulate=False):
  array = list()
  sites = list()
  rss = list()
  bandwidth = list()

  bandwidth.append(0)
  rss.append(0)

  # Get the totals of each stat
  total_bandwidth = 0
  total_rss = 0
  for site in results_dict:
    if site == 0:
      continue
    total_bandwidth += results_dict[site][AVG_DDR4_BANDWIDTH]
    total_rss += profcfg_results['sites'][(site,)][PEAK_RSS]

  # Get the bandwidth/byte and sort by it
  for site in results_dict:
    if site == 0:
      continue
    tmp_rss = (profcfg_results['sites'][(site,)][PEAK_RSS])
    tmp_bandwidth = (results_dict[site][AVG_DDR4_BANDWIDTH])
    tmp_bw_per_byte = (tmp_bandwidth * 1024) / tmp_rss
    array.append((tmp_bw_per_byte, site))
  array.sort(reverse=True)

  # Construct the individual arrays with cumulative stats
  tmp_rss = 0
  tmp_bandwidth = 0
  for val in array:
    site = val[1]
    if accumulate:
      tmp_rss += profcfg_results['sites'][(site,)][PEAK_RSS]
      tmp_bandwidth += results_dict[site][AVG_DDR4_BANDWIDTH]
    else:
      tmp_rss = profcfg_results['sites'][(site,)][PEAK_RSS]
      tmp_bandwidth = results_dict[site][AVG_DDR4_BANDWIDTH]
    rss_percentage = (tmp_rss / total_rss) * 100
    bandwidth_percentage = (tmp_bandwidth / total_bandwidth) * 100
    if rss_percentage > 100:
      rss_percentage = 100
    if bandwidth_percentage > 100:
      bandwidth_percentage = 100
    rss.append(rss_percentage)
    bandwidth.append(bandwidth_percentage)

  # Return the bandwidth and rss arrays, sorted by bandwidth
  return (bandwidth, rss)

# Plots a single line with the RSS on the x axis and bandwidth on the y
def bandwidth_profile_line_plot(benches, results_dict, profcfg_results, cfgs, stats):
  mpl.rc('pgf', rcfonts=False)
  mpl.rc('text', usetex=True)
  pgf_with_rc_fonts = {
      "font.family": "serif",
      "font.serif": [],                    # use latex default serif font
      "font.sans-serif": [],  # use a specific sans-serif font
  }
  mpl.rcParams.update(pgf_with_rc_fonts)
  mpl.rc('figure', figsize=(len(benches) * 2, len(cfgs) * 2))
  colors = ['#a8d2f0',
            '#258fda',
            '#0f3957']
  fig, axs = plt.subplots(len(cfgs), len(benches))
  col = 0
  for bench in benches:
    print("Doing " + bench)
    row = 0
    for cfg_name in cfgs:
      print("Doing " + cfg_name)
      (bandwidth, rss) = prepare_ddr_bandwidth(results_dict[bench][cfg_name], profcfg_results[bench][cfg_name], accumulate=True)
      axs[row,col].grid(True)
      axs[row,col].set_xticks([0, 20, 40, 60, 80, 100])
      axs[row,col].set_yticks([0, 20, 40, 60, 80, 100])
      axs[row,col].set_xbound(0, 100)
      axs[row,col].set_ybound(0, 100)
      axs[row,col].set_xlim(left=0, right=100, auto=False)
      axs[row,col].set_ylim(bottom=0, top=100, auto=False)
      print(rss, bandwidth)
      axs[row,col].plot(rss, bandwidth, '.-', color=colors[0], markerfacecolor=colors[1], markeredgecolor=colors[1])
      # Set axis labels
      if row is (len(cfgs) - 1):
        axs[row,col].set_xlabel('% of total RSS')
      if col is 0:
        axs[row,col].set_ylabel('% of total bandwidth')

      # Do config/bench labels
      if col == 0:
        title = ""
        if "three_cxt" in cfg_name:
          title = "With call path context"
        else:
          title = "No context"
        axs[row,col].annotate(title, xy=(0, 0.5), xytext=(-axs[row,col].yaxis.labelpad - 5, 0),
                    xycoords=axs[row,col].yaxis.label, textcoords='offset points', rotation=90,
                    size='large', ha='right', va='center')
      if row == 0:
        axs[row,col].set_title(bench)
      row += 1
    col += 1
  fig.tight_layout()
  fig.subplots_adjust(left=0.1)
  plt.savefig('bandwidth_profile.pgf')
  plt.savefig('bandwidth_profile.pdf')

# Plots a single bar graph with two y axes
def cache_plot(benches, rstrs1, rstrs2, cfgs, stats):

  # Use pgf output
  mpl.use('pgf')
  mpl.rc('pgf', rcfonts=False)
  mpl.rc('text', usetex=True)
  pgf_with_rc_fonts = {
      "font.family": "serif",
      "font.serif": [],                    # use latex default serif font
      "font.sans-serif": [],  # use a specific sans-serif font
  }
  mpl.rcParams.update(pgf_with_rc_fonts)

  num_benches = len(rstrs1)
  num_cfgs = len(cfgs)
  cfgs_per_plot = 3
  bar_width = (1.0 - 0.1) / cfgs_per_plot
  
  mpl.rc('figure', figsize=(6, 3))
  fig, ax1 = plt.subplots()
  ax2 = ax1.twinx()
  # Shades of blue
  colors = ['#a8d2f0',
            '#7cbce9',
            '#51a5e1',
            '#258fda',
            '#1e72ae',
            '#165683',
            '#0f3957',
            '#071d2c']
  # Oranges
  colors2 = ['#f1b9a7',
            '#ea977b',
            '#e3744f',
            '#dd5122',
            '#b0411c',
            '#843115',
            '#58200e']

  index = list()
  for i in range(0, num_benches):
    index.append(i)
  xticks = list()
  for x in index:
    xticks.append(x + ((bar_width / 2) * (cfgs_per_plot - 1)))
  benches = list()
  for bench in benches:
    if bench is 'average':
      benches.append('geomean')
    else:
      benches.append(bench)

  # Plot two configs per plot, 3 x 2
  ctr = 0
  for cfg_name in cfgs:
    vals = []
    vals2 = []
    err = []
    err2 = []
    tmp_index = []
    if cfg_name is 'default_ir_mcdram':
      continue
    for bench in benches:
      vals.append(float(rstrs1[bench][cfg_name][0]))
      vals2.append(float(rstrs2[bench][cfg_name][0]))
    for bench in benches:
      if bench is 'average':
        err.append(0)
        err2.append(0)
      else:
        err.append(float(rstrs1[bench][cfg_name][1]))
        err2.append(float(rstrs2[bench][cfg_name][1]))
    for i, val in enumerate(index):
      tmp_index.append(val + bar_width * ctr)
  
    labelstr = ''
    marker=''
    if 'cache_25' in cfg_name:
      labelstr = 'cache-mode'
      marker = 'X'
    elif 'marena_avg_ddr_bandwidth_hotset_three_cxt_4GB' in cfg_name:
      labelstr = 'MBI-guided'
      marker = 's'
    elif 'marena_hotset_three_cxt_pebs_512_4GB' in cfg_name:
      labelstr = 'PEBS-guided'
      marker = 'o'
    
    # Plot the actual bars, with error bars
    ax1.bar(tmp_index, list(vals), width=bar_width, yerr=err, alpha=1.0, color=colors[ctr], label=labelstr, edgecolor='black', zorder=4,
            error_kw={'capsize':3, 'barsabove':True, 'elinewidth':0.5, 'capthick':0.5, 'zorder':5})
    ax2.scatter(tmp_index, list(vals2), alpha=1.0, color=colors2[ctr], label='          ', zorder=6,
                marker=marker, edgecolors='black')
    ctr = ctr + 1

  ax1.set_yticks([0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0, 2.1])
  ax2.set_yticks([])
  ax2.set_ylim(bottom=0.9, top=2.1, auto=False)
  ax1.set_ylim(bottom=0.9, top=2.1, auto=False)
  ax1.set_axisbelow(True)
  ax1.grid(True)
  ax1.set_xticks(xticks)
  ax1.set_xticklabels(benches)
  #plt.setp( ax1.xaxis.get_majorticklabels(), rotation=-45, ha="left", rotation_mode="anchor") 
  ax1.set_ylabel('Execution time/Bandwidth \n (relative to MCDRAM-only)', fontsize=12)
  ax1.axhline(y=1, color='black', linestyle='-', zorder=1)

  # Legend
  handles, labels = ax1.get_legend_handles_labels()
  ax1.legend(handles, labels, labelspacing=0.3, handletextpad=0.4, framealpha=1.0, loc=1)
  handles, labels = ax2.get_legend_handles_labels()
  ax2.legend(handles, labels, labelspacing=0.25, handletextpad=5.8, framealpha=0.0, loc=1)

  fig.tight_layout()
  #fig.subplots_adjust(left=0.1)
  plt.savefig('cache.pgf')
  plt.savefig('cache.pdf')

# Plots a single bar graph
def backtrace_plot(benches, rstrs, cfgs, stat):

  # Use pgf output
  mpl.use('pgf')
  mpl.rc('pgf', rcfonts=False)
  mpl.rc('text', usetex=True)
  pgf_with_rc_fonts = {
      "font.family": "serif",
      "font.serif": [],                    # use latex default serif font
      "font.sans-serif": [],  # use a specific sans-serif font
  }
  mpl.rcParams.update(pgf_with_rc_fonts)

  num_benches = len(rstrs)
  num_cfgs = len(cfgs)
  cfgs_per_plot = 2
  bar_width = (1.0 - 0.1) / cfgs_per_plot
  
  mpl.rc('figure', figsize=(4, 3))
  fig, ax = plt.subplots()
  # Shades of orange
  colors = ['#f1b9a7',
            '#ea977b',
            '#e3744f',
            '#dd5122',
            '#b0411c',
            '#843115',
            '#58200e']

  index = list()
  for i in range(0, num_benches):
    index.append(i)
  xticks = list()
  for x in index:
    xticks.append(x + ((bar_width / 2) * (cfgs_per_plot - 1)))
  benches = list()
  for bench in benches:
    if bench is 'average':
      benches.append('geomean')
    else:
      benches.append(bench)

  # Plot two configs per plot, 3 x 2
  ctr = 0
  for cfg_name in cfgs:
    vals = []
    err = []
    tmp_index = []
    if cfg_name is 'marena_nocxt':
      continue
    for bench in benches:
      vals.append(float(rstrs[bench][cfg_name][0]))
    for bench in benches:
      if bench is 'average':
        err.append(0)
      else:
        err.append(float(rstrs[bench][cfg_name][1]))
    for i, val in enumerate(index):
      tmp_index.append(val + bar_width * ctr)
  
    labelstr = ''
    if 'marena_with_cxt' in cfg_name:
      labelstr = 'with call path context'
    elif 'backtrace_lock' in cfg_name:
      labelstr = 'backtrace'
    
    # Plot the actual bars, with error bars
    ax.bar(tmp_index, list(vals), width=bar_width, yerr=err, alpha=1.0, color=colors[ctr], label=labelstr, edgecolor='black', zorder=2,
                                  error_kw={'capsize':3, 'barsabove':True, 'elinewidth':0.5, 'capthick':0.5, 'zorder':3})
    ctr = ctr + 1

  ax.set_axisbelow(True)
  ax.grid(True)
  ax.set_xticks(xticks)
  ax.set_xticklabels(benches)
  plt.setp( ax.xaxis.get_majorticklabels(), rotation=-45, ha="left", rotation_mode="anchor") 
  #ax.set_yticks([0.95, 1, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45])
  #ax.set_ylim(bottom=0.9, top=1.45, auto=False)
  ax.set_ylabel('Execution time \n (relative to MCDRAM-only)', fontsize=12)
  ax.axhline(y=1, color='black', linestyle='-', zorder=1)

  # Legend
  handles, labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, labelspacing=0.5, framealpha=1.0)

  fig.tight_layout()
  #fig.subplots_adjust(left=0.1)
  plt.savefig('backtrace.pgf')
  plt.savefig('backtrace.pdf')

# Plots a single bar graph
def pebs_bar_plot(benches, rstrs, cfgs, stat):

  # Use pgf output
  mpl.use('pgf')
  mpl.rc('pgf', rcfonts=False)
  mpl.rc('text', usetex=True)
  pgf_with_rc_fonts = {
      "font.family": "serif",
      "font.serif": [],                    # use latex default serif font
      "font.sans-serif": [],  # use a specific sans-serif font
  }
  mpl.rcParams.update(pgf_with_rc_fonts)

  num_benches = len(rstrs)
  num_cfgs = len(cfgs)
  cfgs_per_plot = 7
  bar_width = (1.0 - 0.1) / cfgs_per_plot
  
  mpl.rc('figure', figsize=(6, 3))
  fig, ax = plt.subplots()
  # Shades of blue
  colors = ['#a8d2f0',
            '#7cbce9',
            '#51a5e1',
            '#258fda',
            '#1e72ae',
            '#165683',
            '#0f3957',
            '#071d2c']

  index = list()
  for i in range(0, num_benches):
    index.append(i)
  xticks = list()
  for x in index:
    xticks.append(x + ((bar_width / 2) * (cfgs_per_plot - 1)))
  benches = list()
  for bench in benches:
    if bench is 'average':
      benches.append('geomean')
    else:
      benches.append(bench)

  # Plot two configs per plot, 3 x 2
  ctr = 0
  for cfg_name in cfgs:
    vals = []
    err = []
    tmp_index = []
    if cfg_name is 'default_ir_mcdram':
      continue
    for bench in benches:
      vals.append(float(rstrs[bench][cfg_name][0]))
    for bench in benches:
      if bench is 'average':
        err.append(0)
      else:
        err.append(float(rstrs[bench][cfg_name][1]))
    for i, val in enumerate(index):
      tmp_index.append(val + bar_width * ctr)
  
    labelstr = ''
    if '8192' in cfg_name:
      labelstr = '8192'
    if '4096' in cfg_name:
      labelstr = '4096'
    if '2048' in cfg_name:
      labelstr = '2048'
    if '1024' in cfg_name:
      labelstr = '1024'
    if '512' in cfg_name:
      labelstr = '512'
    if '256' in cfg_name:
      labelstr = '256'
    if '128' in cfg_name:
      labelstr = '128'
    
    # Plot the actual bars, with error bars
    ax.bar(tmp_index, list(vals), width=bar_width, yerr=err, alpha=1.0, color=colors[ctr], label=labelstr, edgecolor='black', zorder=2,
                                  error_kw={'capsize':3, 'barsabove':True, 'elinewidth':0.5, 'capthick':0.5, 'zorder':3})
    ctr = ctr + 1

  ax.set_axisbelow(True)
  ax.grid(True)
  ax.set_xticks(xticks)
  ax.set_xticklabels(benches)
  ax.set_yticks([0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8])
  ax.set_ylim(bottom=0.9, top=1.8, auto=False)
  ax.set_ylabel('Execution time \n (relative to MCDRAM-only)', fontsize=12)
  ax.axhline(y=1, color='black', linestyle='-', zorder=1)

  # Legend
  handles, labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, title='PEBS Sampling Rate', labelspacing=0.3, framealpha=1.0, ncol=2, handletextpad=0.4) #loc=(0.72, 0.53))

  fig.tight_layout()
  plt.savefig('pebs.pgf')
  plt.savefig('pebs.pdf')

# Plots a single bar graph
def overhead_plot(benches, rstrs, cfgs, stat):

  print(rstrs)

  # Use pgf output
  mpl.use('pgf')
  mpl.rc('pgf', rcfonts=False)
  mpl.rc('text', usetex=True)
  pgf_with_rc_fonts = {
      "font.family": "serif",
      "font.serif": [],                    # use latex default serif font
      "font.sans-serif": [],  # use a specific sans-serif font
  }
  mpl.rcParams.update(pgf_with_rc_fonts)

  num_benches = len(rstrs)
  num_cfgs = len(cfgs)
  cfgs_per_plot = 7
  bar_width = (1.0 - 0.1) / cfgs_per_plot
  
  mpl.rc('figure', figsize=(6, 3))
  fig, ax = plt.subplots()
  # Shades of orange
  colors = ['#f1b9a7',
            '#ea977b',
            '#e3744f',
            '#dd5122',
            '#b0411c',
            '#843115',
            '#58200e']

  index = list()
  for i in range(0, num_benches):
    index.append(i)
  xticks = list()
  for x in index:
    xticks.append(x + ((bar_width / 2) * (cfgs_per_plot - 1)))
  benches = list()
  for bench in benches:
    if bench is 'average':
      benches.append('geomean')
    else:
      benches.append(bench)

  # Plot two configs per plot, 3 x 2
  ctr = 0
  for cfg_name in cfgs:
    vals = []
    err = []
    tmp_index = []
    if cfg_name is 'default_ir_mcdram':
      continue
    for bench in benches:
      vals.append(float(rstrs[bench][cfg_name][0]))
    for bench in benches:
      if bench is 'average':
        err.append(0)
      else:
        err.append(float(rstrs[bench][cfg_name][1]))
    for i, val in enumerate(index):
      tmp_index.append(val + bar_width * ctr)
  
    labelstr = ''
    if '8192' in cfg_name:
      labelstr = '8192'
    if '4096' in cfg_name:
      labelstr = '4096'
    if '2048' in cfg_name:
      labelstr = '2048'
    if '1024' in cfg_name:
      labelstr = '1024'
    if '512' in cfg_name:
      labelstr = '512'
    if '256' in cfg_name:
      labelstr = '256'
    if '128' in cfg_name:
      labelstr = '128'
    
    # Plot the actual bars, with error bars
    print((tmp_index, err))
    ax.bar(tmp_index, list(vals), width=bar_width, yerr=err, alpha=1.0, color=colors[ctr], label=labelstr, edgecolor='black', zorder=2,
                                  error_kw={'capsize':3, 'barsabove':True, 'elinewidth':0.5, 'capthick':0.5, 'zorder':3})
    ctr = ctr + 1

  ax.set_axisbelow(True)
  ax.grid(True)
  ax.set_xticks(xticks)
  ax.set_xticklabels(benches)
  ax.set_yticks([0.95, 1, 1.05, 1.1, 1.15, 1.2, 1.25, 1.3, 1.35, 1.4, 1.45])
  ax.set_ylim(bottom=0.9, top=1.45, auto=False)
  ax.set_ylabel('Execution time \n (relative to MCDRAM-only)', fontsize=12)
  ax.axhline(y=1, color='black', linestyle='-', zorder=1)

  # Legend
  handles, labels = ax.get_legend_handles_labels()
  ax.legend(handles, labels, title='PEBS Sampling Rate', labelspacing=0.2, framealpha=1.0, ncol=3, handletextpad=0.3)

  fig.tight_layout()
  #fig.subplots_adjust(left=0.1)
  plt.savefig('overhead.pgf')
  plt.savefig('overhead.pdf')

# Plots a number of configs as bars, three by two grid
def threeway_perf_bar_plot(benches, rstrs, cfgs, stat):

  # Use pgf output
  mpl.use('pgf')
  mpl.rc('pgf', rcfonts=False)
  mpl.rc('text', usetex=True)
  pgf_with_rc_fonts = {
      "font.family": "serif",
      "font.serif": [],                    # use latex default serif font
      "font.sans-serif": [],  # use a specific sans-serif font
  }
  mpl.rcParams.update(pgf_with_rc_fonts)

  num_benches = len(benches)
  num_cfgs = len(cfgs)
  cfgs_per_plot = 3
  bar_width = (1.0 - 0.1) / cfgs_per_plot
  
  # Hardcoded as 3x2
  mpl.rc('figure', figsize=(9, 2.5))
  fig, axs = plt.subplots(1, 3, sharex=True, sharey=True)
  # Shades of blue
  colors = ['#a8d2f0',
            '#51a5e1',
            '#1e72ae',
            '#0f3957']

  index = list()
  for i in range(0, num_benches):
    index.append(i)
  xticks = list()
  for x in index:
    xticks.append(x + ((bar_width / 2) * (cfgs_per_plot - 1)))

  # Plot two configs per plot, 3 x 2
  rects = []
  errbars = []
  ctr = 0
  row = 0
  col = 0
  for cfg_name in cfgs:
    if cfg_name is 'default_ir_mcdram':
      continue
    vals = []
    err = []
    tmp_index = []
    for bench in benches:
      vals.append(float(rstrs[bench][cfg_name][0]))
    for bench in benches:
      if bench is 'average':
        err.append(0)
      else:
        err.append(float(rstrs[bench][cfg_name][1]))
    for i, val in enumerate(index):
      tmp_index.append(val + bar_width * ctr)
    axs[col].set_axisbelow(True)
    axs[col].grid(True)
    if col is 0:
      if stat is 'exe_time':
        axs[col].set_ylabel('Execution time \n (relative to MCDRAM-only)')
      else:
        axs[col].set_ylabel(stat)
    axs[col].set_xticks(xticks)
    axs[col].set_xticklabels(['geomean' if x=='average' else x for x in benches])
    axs[col].set_yticks([0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2.0])
    axs[col].set_ylim(bottom=0.9, top=2.0, auto=False)
    #axs[col].set_ybound(0.75, 2.5)
    axs[col].axhline(y=1, color='black', linestyle='-', zorder=1)

    # Rotate and anchor the xtick labels
    plt.setp( axs[col].xaxis.get_majorticklabels(), rotation=-45, ha="left", rotation_mode="anchor") 

    labelstr = ''
    if 'pebs_512' in cfg_name:
      labelstr = 'PEBS-guided'
    elif 'hotset' in cfg_name:
      labelstr = 'MBI-guided'
    elif 'firsttouch' in cfg_name:
      labelstr = 'first-touch'

    # Column labels 
    if row == 0:
      title = ""
      if '12.5' in cfg_name:
        title = '12.5%'
      elif '25' in cfg_name:
        title = '25%'
      elif '50' in cfg_name:
        title = '50%'
      axs[col].set_title(title)

    # Plot the actual bars, with error bars
    rects.append(axs[col].bar(tmp_index, list(vals), width=bar_width, yerr=err, alpha=1.0, color=colors[ctr], label=labelstr, edgecolor='black', zorder=2,
                                  error_kw={'capsize':3, 'barsabove':True, 'elinewidth':0.5, 'capthick':0.5, 'zorder':3}))

    # Legend only on the first axis
    if row is 0 and col is 2:
      axs[col].legend(labelspacing=0.2, ncol=1, handletextpad=0.2, loc=2)

    if ctr != 2:
      ctr = ctr + 1
    else:
      ctr = 0
      col += 1
      if col > 2:
        col = 0
        row += 1

  fig.tight_layout()
  fig.subplots_adjust(left=0.1)
  plt.savefig('threeway_perf.pgf')
  plt.savefig('threeway_perf.pdf')

# Plots a number of configs as bars, three by two grid
def perf_bar_plot(benches, rstrs, cfgs, stat):

  # Use pgf output
  mpl.use('pgf')
  mpl.rc('pgf', rcfonts=False)
  mpl.rc('text', usetex=True)
  pgf_with_rc_fonts = {
      "font.family": "serif",
      "font.serif": [],                    # use latex default serif font
      "font.sans-serif": [],  # use a specific sans-serif font
  }
  mpl.rcParams.update(pgf_with_rc_fonts)

  num_benches = len(benches)
  num_cfgs = len(cfgs)
  cfgs_per_plot = 4
  bar_width = (1.0 - 0.1) / cfgs_per_plot
  
  # Hardcoded as 3x2
  mpl.rc('figure', figsize=(3 * 3, 2 * 2.5))
  fig, axs = plt.subplots(2, 3, sharex=True, sharey='row')
  # Shades of blue
  colors = ['#a8d2f0',
            '#51a5e1',
            '#1e72ae',
            '#0f3957']

  index = list()
  for i in range(0, num_benches):
    index.append(i)
  xticks = list()
  for x in index:
    xticks.append(x + ((bar_width / 2) * (cfgs_per_plot - 1)))

  # Plot two configs per plot, 3 x 2
  rects = []
  errbars = []
  ctr = 0
  row = 0
  col = 0
  for cfg_name in cfgs:
    if cfg_name is 'default_ir_mcdram':
      continue
    vals = []
    err = []
    tmp_index = []
    for bench in benches:
      vals.append(float(rstrs[bench][cfg_name][0]))
    for bench in benches:
      if bench is 'average':
        err.append(0)
      else:
        err.append(float(rstrs[bench][cfg_name][1]))
    for i, val in enumerate(index):
      tmp_index.append(val + bar_width * ctr)
    axs[row,col].set_axisbelow(True)
    axs[row,col].grid(True)
    if col is 0:
      if stat is 'exe_time':
        axs[row,col].set_ylabel('Execution time \n (relative to MCDRAM-only)')
      else:
        axs[row,col].set_ylabel(stat)
    axs[row,col].set_xticks(xticks)
    axs[row,col].set_xticklabels(['geomean' if x=='average' else x for x in benches])
    if row is 0:
      axs[row,col].set_yticks([0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2, 2.1, 2.2, 2.3, 2.4, 2.5])
      axs[row,col].set_ylim(bottom=0.9, top=2.5, auto=False)
    else:
      axs[row,col].set_yticks([0.9, 1, 1.1, 1.2, 1.3, 1.4, 1.5, 1.6, 1.7, 1.8, 1.9, 2])
      axs[row,col].set_ylim(bottom=0.9, top=2, auto=False)
    axs[row,col].axhline(y=1, color='black', linestyle='-', zorder=1)

    # Rotate and anchor the xtick labels
    plt.setp( axs[row,col].xaxis.get_majorticklabels(), rotation=-45, ha="left", rotation_mode="anchor") 

    labelstr = ''
    if 'orig_hotset' in cfg_name:
      labelstr = 'hotset'
    elif 'hotset' in cfg_name:
      labelstr = 'thermos'
    elif 'knapsack' in cfg_name:
      labelstr = 'knapsack'
    elif 'firsttouch' in cfg_name:
      labelstr = 'first-touch'

    # Row labels
    if col == 0:
      title = ""
      if row == 0:
        title = "No context"
      elif row == 1:
        title = "With call path context"
      axs[row,col].annotate(title, xy=(0, 0.5), xytext=(-axs[row,col].yaxis.labelpad - 5, 0),
                  xycoords=axs[row,col].yaxis.label, textcoords='offset points', rotation=90,
                  size='large', ha='right', va='center')
    
    # Column labels 
    if row == 0:
      title = ""
      if '12.5' in cfg_name:
        title = '12.5%'
      elif '25' in cfg_name:
        title = '25%'
      elif '50' in cfg_name:
        title = '50%'
      axs[row,col].set_title(title)

    # Plot the actual bars, with error bars
    print("Indices and vals:")
    print(tmp_index)
    print(list(vals))
    rects.append(axs[row,col].bar(tmp_index, list(vals), width=bar_width, yerr=err, alpha=1.0, color=colors[ctr], label=labelstr, edgecolor='black', zorder=2,
                                  error_kw={'capsize':3, 'barsabove':True, 'elinewidth':0.5, 'capthick':0.5, 'zorder':3}))

    # Legend only on the first axis
    if row is 0 and col is 0:
      axs[row,col].legend(labelspacing=0.2, ncol=2, handletextpad=0.3, columnspacing=0.7)

    if ctr != 3:
      ctr = ctr + 1
    else:
      ctr = 0
      col += 1
      if col > 2:
        col = 0
        row += 1

  fig.tight_layout()
  fig.subplots_adjust(left=0.12)
  print("Writing result to bandwidth_perf.pdf and bandwidth_perf.pgf.")
  plt.savefig('bandwidth_perf.pgf')
  plt.savefig('bandwidth_perf.pdf')

def double_line_plot(bench, results_dict, profcfg_results, cfgs, stats):
  fig, ax = plt.subplots()

  (bandwidth, rss) = prepare_ddr_bandwidth(results_dict, profcfg_results, accumulate=True)
  ones = list(range(1, 50+1))
  
  #plt.xscale('log')
  ax.grid(True)
  ax.plot(ones, bandwidth[:50], '.-', label='% of total bandwidth')
  ax.plot(ones, rss[:50], '.-', label='% of total RSS')
  plt.show()

# stat is a list of two stats: y and z
def bar_plot_3d(bench, results_dict, profcfg_results, cfgs, stats, vert_angle=30, horiz_angle=30):
  fig = plt.figure()
  ax = fig.add_subplot(111, projection='3d')

  (bandwidth, rss) = prepare_ddr_bandwidth(results_dict, profcfg_results, accumulate=True)
  
  ones = list(range(1, len(bandwidth)+1))
  xpos = []
  for val in ones:
    xpos.append(val - 0.5)
  ypos = bandwidth
  zpos = [0] * len(xpos)
  ypos_normalized = list()
  for val in ypos:
    if val < 50:
      val = 50
    ypos_normalized.append(val / 100)
  colors = plt.cm.Reds(ypos_normalized)
  colors[:,-1] = ypos_normalized

  dx = [1] * len(xpos)
  dy = [2] * len(xpos)
  dz = rss

  # Make sure the colors are between 0 and 1
  for i,elem in enumerate(colors):
    for n,val in enumerate(elem):
      if val > 1:
        colors[i][n] = 1.0
      elif val < 0:
        colors[i][n] = 0.0

  for elem in colors:
    for val in elem:
      if val > 1 or val < 0:
        print('ERROR')

  ax.view_init(elev=vert_angle, azim=horiz_angle)
  ax.set_title(bench)
  ax.set_xlabel('Sites')
  ax.set_ylabel('% of total bandwidth')
  ax.set_zlabel('% of total RSS')
  plt.xticks(fontsize=2)
  plt.ylim(0, 100)
  ax.set_xticks(list(range(1, len(bandwidth)+1)))
  ax.set_yticks([0,20,40,60,80,100])
  ax.set_zticks([0,20,40,60,80,100])
  ax.bar3d(xpos, ypos, zpos, dx, dy, dz, color=colors)
  plt.show()
  

# Plots two configs, one on the x-axis and the other on the y-axis.
# cfgs is a list of lists, each inner list being one line
def line_plot(benches, rstrs, rstrs2, cfgs, stats):
  fig, ax = plt.subplots()
  bench = 'roms'
  vals = list()
  vals2 = list()
  # Expects cfgs to be a list of lists
  # One line per cfg array
  for cfg_arr in cfgs:
    for cfg_name in cfg_arr:
      # Two stats, one for x axis and the other for y
      vals.append(float(rstrs[bench][cfg_name]))
      vals2.append(float(rstrs2[bench][cfg_name]))
    ax.plot(vals, vals2, '.-', label=cfg_arr[0])
  plt.show()
  

# Plots a single config with a bar graph
def bar_plot(benches, rstrs, cfgs, stat):
  num_benches = len(rstrs)
  num_cfgs = len(cfgs)
  bar_width = (1.0 - 0.1) / num_cfgs
  
  fig, ax = plt.subplots()
  colors = ['#FF9292', '#F1C97C', '#F8F994', '#9FF1A5', '#B9FFF3']

  index = list()
  for i in range(0, num_benches):
    index.append(i)

  # Plot all benches for each config
  rects = []
  ctr = 0
  for cfg_name in cfgs:
    if cfg_name is 'default_ir_mcdram':
      continue
    vals = []
    tmp_index = []
    for bench in benches:
      vals.append(float(rstrs[bench][cfg_name]))
    for i, val in enumerate(index):
      tmp_index.append(val + bar_width * ctr)
    rects.append(ax.bar(tmp_index, list(vals), bar_width, alpha=1.0,  color=colors[ctr], label=cfg_name))
    ctr = ctr + 1

  ax.set_xlabel('Benchmarks')
  ax.set_ylabel(stat)
  #ax.set_title(stat)
  xticks = list()
  # Add a half-bar_width (times the number of configs minus one) to each xtick
  for x in index:
    xticks.append(x + ((bar_width / 2) * (num_cfgs - 1)))
  ax.set_xticks(xticks)
  benches = list()
  for bench in benches:
    if bench is 'average':
      benches.append('geomean')
    else:
      benches.append(bench)

  ax.set_xticklabels(benches)
  ax.legend()

  fig.tight_layout()
  plt.show()
