import os
from shutil import *
from marenapy.paths import *
from marenapy.tools import *
from marenapy.benches.bench import *
from marenapy.utils import *
from functools import reduce
from math import sqrt, isnan, isinf
from statistics import mean, median
from operator import itemgetter
import numpy as np
import operator
import pickle
import gc

from statistics import mean,stdev,median

def peekline(f):
  pos = f.tell()
  line = f.readline()
  f.seek(pos)
  return line

def update_scalar_stat(stats, x):
  mean     = stats[0]
  variance = stats[1]
  minval   = stats[2]
  maxval   = stats[3]
  rawval   = stats[4]
  n        = stats[5]

  n += 1

  if (n > 1):
    variance += ( (((float(x) - mean) ** 2) / n) - (variance / (n-1)) )

  mean += ( (x - mean) / n )
  if (n == 1):
    minval = maxval = x;
  else:
    maxval = x if x > maxval else maxval
    minval = x if x < minval else minval
  
  rawval += x

  stats[0] = mean
  stats[1] = variance
  stats[2] = minval
  stats[3] = maxval
  stats[4] = rawval
  stats[5] = n

def update_ratio_stat(stats, x, num, den):
  mean     = stats[0]
  variance = stats[1]
  minval   = stats[2]
  maxval   = stats[3]
  rawnum   = stats[4]
  rawden   = stats[5]
  n        = stats[6]

  n += 1

  if (n > 1):
    variance += ( (((float(x) - mean) ** 2) / n) - (variance / (n-1)) )

  mean += ( (x - mean) / n )
  if (n == 1):
    minval = maxval = x;
  else:
    maxval = x if x > maxval else maxval
    minval = x if x < minval else minval
  
  rawnum += num
  rawden += den

  stats[0] = mean
  stats[1] = variance
  stats[2] = minval
  stats[3] = maxval
  stats[4] = rawnum
  stats[5] = rawden
  stats[6] = n

def new_int_stat():
  return [0.0, 0.0, 0, 0, 0, 0]

def new_float_stat():
  return [0.0, 0.0, 0.0, 0.0, 0, 0, 0]

def get_int_info(line):
  pts = line.split()
  return ( [ float(pts[0]), float(pts[1]) ] + \
           [ int(x) for x in pts[2:] ]        \
         )

def get_float_info(line):
  pts = line.split()
  return ( [ float(x) for x in pts ] )

def get_agg_stats(vals, objects, maxval=False, ratval=False, pp=False):

  agg_mean  = 0.0
  agg_stdev = 0.0
  agg_min   = 0
  agg_max   = 0
  agg_val   = 0

  n = 0
  for val,m in zip(vals,objects):
    if m == 0:
      continue

    prev_usq = (agg_mean ** 2)
    prev_var = (agg_stdev ** 2)

    agg_mean = ( (agg_mean * (float(n) / (n+m))) + \
                 (val[0]   * (float(m) / (n+m))) )

    cur_usq = (val[0] ** 2)
    cur_var = (val[1] ** 2)
    t1 = (n * (prev_var + prev_usq))
    t2 = (m * (cur_var + cur_usq))
    agg_var = ( (( t1 + t2 ) / (m + n)) - (agg_mean**2) )
    if (agg_var < 0.0):
      if (agg_var < -0.1):
        print("error: bad var: " + str(agg_var))
        raise (SystemExit(1))
      else:
        agg_stdev = 0.0
    else:
      agg_stdev = (agg_var ** 0.5)

    agg_min = min([agg_min, val[2]])
    agg_max = max([agg_max, val[2]])

    if maxval:
      agg_val = max([agg_val, val[4]])
    elif ratval:
      agg_val = ( (agg_val * (float(n) / (n+m))) + \
                  (val[4]  * (float(m) / (n+m))) )
    else:
      agg_val += val[4]

    n += m

  return [ agg_mean, agg_stdev, agg_min, agg_max, agg_val ]

def get_object_tzstr_info(bench, cfg_name, cutstyle, tzstr):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%s.object_%s-%s.pkl' % (results_dir, cutstyle.lower(), tzstr)), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_object_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sobject_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_feature_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sfeature_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_alt_site_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%salt_site_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_bench_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sbench_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  btdict = pickle.load(cut_pkl_fd)
  return btdict


def get_agg_info_line(info_id, agg_list):
  objects  = sum ( [ x[0] for x in agg_list ]  )
  size     = sum ( [ x[1] for x in agg_list ]  )
  touched  = sum ( [ x[2] for x in agg_list ]  )
  page_rss = max ( [ x[3] for x in agg_list ]  ) 
  line_rss = max ( [ x[4] for x in agg_list ]  ) 
  agpg_rss = max ( [ x[5] for x in agg_list ]  ) 
  agcl_rss = max ( [ x[6] for x in agg_list ]  ) 
  szinfo   = [ sum( [ x[i] for x in agg_list ] ) \
               for i in range(7, len( agg_list[0] )) ]
  pts = [info_id, objects, size, touched, page_rss, line_rss, \
         agpg_rss, agcl_rss] + szinfo
  aggline = " ".join([("%-16d" % pts[0])] + [ ("%16d" % x) for x in pts[1:]])
  return aggline

def get_output_files(benches, configs, its=[0], copies=[0],
  files=pinfiles):
  for bench in benches:
    for cfg_name in configs:
      for it in its:
        for copy in copies:
          get_bench_cfg_files(bench, cfg_name, it, copy, files=files)

def get_bench_cfg_files(bench, cfg_name, it, copy, files=pinfiles): 
  cfg.read_cfg(cfg_name)

  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for run in runs:
    for fname in files:
      run_dir     = get_run_dir(bench, cfg_name, it, copy, run)
      results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
      cmd = ("cp %s %s" % ((run_dir+fname), (results_dir+fname)))
      os.system(cmd)

def remove_staged_files(benches, configs, its=[0], copies=[0],\
  files=['obj_info.out']):
  for bench in benches:
    for cfg_name in configs:
      for it in its:
        for copy in copies:
          remove_bench_cfg_files(bench, cfg_name, it, copy, files=files)

def remove_bench_cfg_files(bench, cfg_name, it, copy,\
  files=['obj_info.out']): 

  cfg.read_cfg(cfg_name)
  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for run in runs:
    for fname in files:
      run_dir = get_run_dir(bench, cfg_name, it, copy, run)
      cmd = ("/usr/bin/sudo rm -f %s" % ((run_dir+fname)))
      print(cmd)
      os.system(cmd)

def manhattan_distance(a, b):
  dist = 0
  for x,y in zip(a,b):
    dist += (abs(x - y))
  return dist

def get_agg_phase_id(bbv, agg_bbvs, top_id):

  min_dist = None
  agg_id = None
  for cur_id,cur_bbv in agg_bbvs.items():
    dist = manhattan_distance(bbv, cur_bbv)
    if min_dist == None or dist < min_dist:
      min_dist = dist
      agg_id = cur_id

  if (min_dist == None) or (min_dist >= UNIQUE_PHASE_THRESHOLD):
    agg_id = top_id
    top_id += 1
    agg_bbvs[agg_id] = bbv

  return (agg_id, top_id)

def create_agg_object_infos(benches, configs, do_objects=False, \
  do_cachelines=False):
  for bench in benches:
    for cfg_name in configs:
      make_agg_object_info(bench, cfg_name, do_objects=do_objects)

def make_agg_object_info(bench, cfg_name, it=0, do_objects=False):

  cfg.read_cfg(cfg_name)
  print("%-20s %-30s ... " % (bench, cfg_name), end="")

  curid   = 0
  clsum   = 0
  totobjs = 0

  imap    = {}
  clmap   = {}
  clstats = {}
  objlist = []

  fncfg = False
  if cfg_name == 'pin_fnlpc_9cxt':
    fncfg = True

  phase_cfg = False
  if cfg_name in ['pin_phase_lpc_9cxt', 'pin_phase2_lpc_9cxt']:
    phase_cfg = True


  runs    = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  err     = False

  tmpdir  = get_run_results_dir(bench, cfg_name, it, 0, 0)
  agg_obj_fd = open(('%sagg_obj_info.out' % \
                   (get_results_dir(bench, cfg_name))), 'w')

  site_strs        = {}
  type_strs        = {}
  acc_strs         = {}
  agg_size_map     = {}
  agg_szbucket_map = {}
  agg_site_map     = {}
  agg_type_map     = {}
  agg_acc_map      = {}
  top_site_id      = {}

  top_type_id      = 0
  top_type_str_id  = 0
  top_acc_id       = 0
  top_acc_str_id   = 0

  if phase_cfg:
    agg_bbvs            = {}
    run_agg_phase_id    = {}
    agg_alloc_phase_map = {}
    agg_phase_sig_map   = {}
    agg_typesz_map      = {}
    agg_phasesz_map     = {}
    agg_phase_site_map  = {}
    agg_phsz_site_map   = {}

    top_alloc_phase_id  = 0
    top_phase_sig_id    = 0
    top_typesz_id       = 0
    top_phasesz_id      = 0
    top_phase_site_id   = {}
    top_phsz_site_id    = {}


  for copy in range(cfg.current_cfg['runcfg']['copies']):

    for run in runs:
#      try:
        run_site_map = {}
        run_type_map = {}
        run_acc_map  = {}
        run_types    = {}

        if phase_cfg:
          run_agg_phase_id[run] = {}
          run_phase_map         = {}
          run_phase_sig_map     = {}
          run_typesz_map        = {}
          run_phasesz_map       = {}
          run_phase_site_map    = {}
          run_phsz_site_map     = {}


        results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)


        #print("size-%d" % run)
        size_info_fd = open(('%ssize_info.out' % (results_dir)), 'r')
        for line in size_info_fd:
          if line.isspace():
            break

          szinfo = [int(x) for x in line.split()]
          szkey  = szinfo[0]
          if not szkey in agg_size_map:
            agg_size_map[szkey] = []
          agg_size_map[szkey].append(szinfo[1:])
        size_info_fd.close()


        #print("szbucket-%d" % run)
        size_bucket_info_fd = open(('%ssize_bucket_info.out' % (results_dir)), 'r')
        for line in size_bucket_info_fd:
          if line.isspace():
            break

          szbinfo = [int(x) for x in line.split()]
          szbkey  = szbinfo[0]
          if not szbkey in agg_szbucket_map:
            agg_szbucket_map[szbkey] = []
          agg_szbucket_map[szbkey].append(szbinfo[1:])
        size_bucket_info_fd.close()



        #print("types-%d" % run)
        type_strs_fd = open(('%stype_strs.out' % (results_dir)), 'r', \
                             encoding="ISO-8859-1")
        for line in type_strs_fd:
          if line.isspace():
            break

          run_type_id = int(line.split()[0])
          type_str    = " ".join( line.split()[1:] )
          if not type_str in type_strs:
            type_strs[type_str] = top_type_str_id
            top_type_str_id += 1
          run_types[run_type_id] = type_strs[type_str]
        type_strs_fd.close()

        type_sigs_fd = open(('%stype_sigs.out' % (results_dir)), 'r')
        for line in type_sigs_fd:
          if line.isspace():
            break

          pts = line.split()
          type_id  = int(pts[0])
          type_sig = tuple([ run_types[int(x)] for x in pts[1:] ])
          run_type_map[type_id] = type_sig
        type_sigs_fd.close()

        type_info_fd = open(('%stype_sig_info.out' % (results_dir)), 'r')
        for line in type_info_fd:
          if line.isspace():
            break

          tinfo = [int(x) for x in line.split()]
          tkey  = run_type_map[tinfo[0]]
          if not tkey in agg_type_map:
            agg_type_map[tkey] = (top_type_id, [])
            top_type_id += 1
          agg_type_map[tkey][1].append(tinfo[1:])
        type_info_fd.close()



        #print("accs-%d" % run)
        acc_strs_fd = open(('%sacc_strs.out' % (results_dir)), 'r', \
                             encoding="ISO-8859-1")
        for line in acc_strs_fd:
          if line.isspace():
            break

          if len(line.split()) == 2:
            addr_part, str_part = line.split()
          else:
            #print(line)
            if len(line.split(':')) == 2:
              addr_part, str_part = line.split(':')
              str_part += ':'
            elif len(line.split('/')) == 2:
              addr_part, str_part = line.split('/')
              str_part += '/'
            else:
              raise (SystemExit(1))

          addr = int(addr_part,base=16)
          if not addr in acc_strs:
            acc_strs[addr] = str_part
        acc_strs_fd.close()

        acc_sigs_fd = open(('%sacc_sigs.out' % (results_dir)), 'r')
        for line in acc_sigs_fd:
          if line.isspace():
            break

          pts = line.split()
          acc_id  = int(pts[0])
          acc_sig = tuple([ int(x,base=16) for x in pts[1:] ])
          run_acc_map[acc_id] = acc_sig
        acc_sigs_fd.close()

        acc_info_fd = open(('%sacc_sig_info.out' % (results_dir)), 'r')
        for line in acc_info_fd:
          if line.isspace():
            break

          accinfo = [int(x) for x in line.split()]
          acckey  = run_acc_map[accinfo[0]]
          if not acckey in agg_acc_map:
            agg_acc_map[acckey] = (top_acc_id, [])
            top_acc_id += 1
          agg_acc_map[acckey][1].append(accinfo[1:])
        acc_info_fd.close()



        #print("sites-%d" % run)
        site_strs_fd = open(('%ssite_cxt_strs.out' % (results_dir)), 'r', \
                             encoding="ISO-8859-1")
        for line in site_strs_fd:
          if line.isspace():
            break

          addr_part, str_part = line.split()
          addr = int(addr_part,base=16)
          if not addr in site_strs:
            site_strs[addr] = str_part
        site_strs_fd.close()

        for clen in pin_cxt_lengths:
          #print("site_cxt-%d-%d" % (clen,run))

          if not clen in run_site_map:
            run_site_map[clen] = {}
          if not clen in agg_site_map:
            agg_site_map[clen] = {}
          if not clen in top_site_id:
            top_site_id[clen] = 0

          site_cxts_fd = open(('%ssite_cxt%d_cxts.out' % (results_dir, clen)), 'r')
          for line in site_cxts_fd:
            if line.isspace():
              break

            pts = line.split()
            site_id    = int(pts[0])
            site_addrs = tuple([ int(x,base=16) for x in pts[1:] ])
            run_site_map[clen][site_id] = site_addrs
          site_cxts_fd.close()


          site_info_fd = open(('%ssite_cxt%d_info.out' % (results_dir, clen)), 'r')
          for line in site_info_fd:
            if line.isspace():
              break

            sinfo = [int(x) for x in line.split()]
            skey  = run_site_map[clen][sinfo[0]]
            if not skey in agg_site_map[clen]:
              agg_site_map[clen][skey] = (top_site_id[clen], [])
              top_site_id[clen] += 1
            agg_site_map[clen][skey][1].append(sinfo[1:])
          site_info_fd.close()

        if phase_cfg:
          phase_bbvs_fd = open(('%sphase_bbvs.out' % (results_dir)), 'r')
          for line in phase_bbvs_fd:
            if line.isspace():
              break

            pts = line.split()
            phase_id  = int(pts[0])
            phase_bbv = tuple([ int(x) for x in pts[1:] ])
            run_phase_map[phase_id] = phase_bbv
            (agg_phase_id, top_alloc_phase_id) = get_agg_phase_id(phase_bbv, \
                                                 agg_bbvs, top_alloc_phase_id)
            run_agg_phase_id[run][phase_id] = agg_phase_id
          phase_bbvs_fd.close()

          alloc_phase_info_fd = open(('%salloc_phase_info.out' % (results_dir)), 'r')
          for line in alloc_phase_info_fd:
            if line.isspace():
              break

            aph_info = [int(x) for x in line.split()]
            aph_id   = run_agg_phase_id[run][aph_info[0]]
            if not aph_id in agg_alloc_phase_map:
              agg_alloc_phase_map[aph_id] = []
            agg_alloc_phase_map[aph_id].append(aph_info[1:])
          alloc_phase_info_fd.close()

          phase_sigs_fd = open(('%sphase_sigs.out' % (results_dir)), 'r')
          for line in phase_sigs_fd:
            if line.isspace():
              break

            pts = line.split()
            phase_sig_id = int(pts[0])
            phase_sig    = tuple([ run_agg_phase_id[run][int(x)] for x in pts[1:] ])
            run_phase_sig_map[phase_sig_id] = phase_sig
          phase_sigs_fd.close()

          phase_sig_info_fd = open(('%sphase_sig_info.out' % (results_dir)), 'r')
          for line in phase_sig_info_fd:
            if line.isspace():
              break


            phase_sig_info = [int(x) for x in line.split()]
            phase_sig_key  = run_phase_sig_map[phase_sig_info[0]]
            #print(phase_sig_info[0])
            #print(run_phase_sig_map[phase_sig_info[0]])
            if not phase_sig_key in agg_phase_sig_map:
              agg_phase_sig_map[phase_sig_key] = (top_phase_sig_id, [])
              top_phase_sig_id += 1
            agg_phase_sig_map[phase_sig_key][1].append(phase_sig_info[1:])
          phase_sig_info_fd.close()


          size_tsigs_fd = open(('%ssize_typesigs.out' % (results_dir)), 'r')
          for line in size_tsigs_fd:
            if line.isspace():
              break

            # these are in hex due to an error in the pintool
            pts = line.split()
            typesz_id  = int(pts[0])
            typesz_sig = tuple( ( [int(pts[1],base=16)] + \
                                  [ run_types[int(x,base=16)] for x in pts[2:] ] \
                              ) )
            run_typesz_map[typesz_id] = typesz_sig
          size_tsigs_fd.close()

          size_tsigs_info_fd = open(('%ssize_typesig_info.out' % (results_dir)), 'r')
          for line in size_tsigs_info_fd:
            if line.isspace():
              break

            tinfo = [int(x) for x in line.split()]
            tkey  = run_typesz_map[tinfo[0]]
            if not tkey in agg_typesz_map:
              agg_typesz_map[tkey] = (top_typesz_id, [])
              top_typesz_id += 1
            agg_typesz_map[tkey][1].append(tinfo[1:])
          size_tsigs_info_fd.close()


          phase_size_fd = open(('%sphase_size.out' % (results_dir)), 'r')
          for line in phase_size_fd:
            if line.isspace():
              break

            # the size is in hex due to an error in the pintool
            pts = line.split()
            phasesz_id  = int(pts[0])
            phasesz_sig = ( run_agg_phase_id[run][int(pts[1])], int(pts[2]) )
            run_phasesz_map[phasesz_id] = phasesz_sig
          phase_size_fd.close()

          phase_size_info_fd = open(('%sphase_size_info.out' % (results_dir)), 'r')
          for line in phase_size_info_fd:
            if line.isspace():
              break

            phsz_info = [int(x) for x in line.split()]
            phsz_key  = run_phasesz_map[phsz_info[0]]
            if not phsz_key in agg_phasesz_map:
              agg_phasesz_map[phsz_key] = (top_phasesz_id, [])
              top_phasesz_id += 1
            agg_phasesz_map[phsz_key][1].append(phsz_info[1:])
          phase_size_info_fd.close()


          for clen in pin_cxt_lengths:
            #print("site_cxt-%d-%d" % (clen,run))

            if not clen in run_phase_site_map:
              run_phase_site_map[clen] = {}
            if not clen in agg_phase_site_map:
              agg_phase_site_map[clen] = {}
            if not clen in top_phase_site_id:
              top_phase_site_id[clen] = 0

            phase_site_fd = open(('%sphase_site%d.out' % (results_dir, clen)), 'r')
            for line in phase_site_fd:
              if line.isspace():
                break

              pts = line.split()
              phase_site_id    = int(pts[0])
              phase_site_addrs = ( run_agg_phase_id[run][int(pts[1])],
                                   run_site_map[clen][int(pts[2])] )
              run_phase_site_map[clen][phase_site_id] = phase_site_addrs
            phase_site_fd.close()


            phase_site_info_fd = open(('%sphase_site%d_info.out' % (results_dir, clen)), 'r')
            for line in phase_site_info_fd:
              if line.isspace():
                break

              sinfo = [int(x) for x in line.split()]
              skey  = run_phase_site_map[clen][sinfo[0]]
              if not skey in agg_phase_site_map[clen]:
                agg_phase_site_map[clen][skey] = (top_phase_site_id[clen], [])
                top_phase_site_id[clen] += 1
              agg_phase_site_map[clen][skey][1].append(sinfo[1:])
            phase_site_info_fd.close()

            if not clen in run_phsz_site_map:
              run_phsz_site_map[clen] = {}
            if not clen in agg_phsz_site_map:
              agg_phsz_site_map[clen] = {}
            if not clen in top_phsz_site_id:
              top_phsz_site_id[clen] = 0

            phsz_site_fd = open(('%sphsz_site%d.out' % (results_dir, clen)), 'r')
            for line in phsz_site_fd:
              if line.isspace():
                break

              pts = line.split()
              phsz_site_id    = int(pts[0])
              phsz_site_addrs = ( run_agg_phase_id[run][int(pts[1])],
                                  int(pts[2]),
                                  run_site_map[clen][int(pts[3])] )
              run_phsz_site_map[clen][phsz_site_id] = phsz_site_addrs
            phsz_site_fd.close()


            phsz_site_info_fd = open(('%sphsz_site%d_info.out' % (results_dir, clen)), 'r')
            for line in phsz_site_info_fd:
              if line.isspace():
                break

              sinfo = [int(x) for x in line.split()]
              skey  = run_phsz_site_map[clen][sinfo[0]]
              if not skey in agg_phsz_site_map[clen]:
                agg_phsz_site_map[clen][skey] = (top_phsz_site_id[clen], [])
                top_phsz_site_id[clen] += 1
              agg_phsz_site_map[clen][skey][1].append(sinfo[1:])
            phsz_site_info_fd.close()


        if do_objects:
          #print("objects-%d" % run)
          cur_obj_fd = open(('%sobj_info.out' % results_dir), 'r')
          for line in cur_obj_fd:
            pts = line.split()

            idx = 0
            site_ids = []
            for i in pin_cxt_lengths:
              site_ids.append(agg_site_map[i][run_site_map[i][int(pts[idx])]][0])
              idx += 1

            size     = int(pts[idx])
            idx     += 1

            szbucket = int(pts[(idx)])
            idx     += 1

            type_id  = agg_type_map[run_type_map[int(pts[(idx)])]][0]
            idx     += 1

            acc_id   = agg_acc_map[run_acc_map[int(pts[(idx)])]][0]
            idx     += 1

            aph_id   = run_agg_phase_id[run][int(pts[(idx)])]
            idx     += 1

            if phase_cfg:
              phase_site_ids = []
              for i in pin_cxt_lengths:
                phase_site_ids.append(agg_phase_site_map[i][run_phase_site_map[i][int(pts[idx])]][0])
                idx += 1

              phsz_site_ids = []
              for i in pin_cxt_lengths:
                phsz_site_ids.append(agg_phsz_site_map[i][run_phsz_site_map[i][int(pts[idx])]][0])
                idx += 1

              phase_sig_id  = agg_phase_sig_map[run_phase_sig_map[int(pts[idx])]][0]
              idx          += 1

              typesz_id     = agg_typesz_map[run_typesz_map[int(pts[(idx)])]][0]
              idx          += 1

              phasesz_id    = agg_phasesz_map[run_phasesz_map[int(pts[(idx)])]][0]
              idx          += 1

            agg_pts = site_ids + [size, szbucket, type_id, acc_id, aph_id]
            if phase_cfg:
              agg_pts += phase_site_ids + phsz_site_ids + \
                         [phase_sig_id, typesz_id, phasesz_id]

            agg_pts   += [int(x) for x in pts[(idx):(idx+5)]]
            idx       += 5

            agg_pts   += [float(x) for x in pts[(idx):]]

            str_pts   = [ ("%16d"   % x) for x in agg_pts[:idx] ] + \
                        [ ("%16.4f" % x) for x in agg_pts[idx:] ]
            objline   = " ".join(str_pts)
            print (objline, file=agg_obj_fd)
          cur_obj_fd.close()

  agg_obj_fd.close()

#      except:
#        print (("error parsing run-%d."%run))
#        err = True
#        break


  if not err:
#    try: 
      results_dir = get_results_dir(bench, cfg_name)


      agg_type_strs_fd = open(('%sagg_type_strs.out' % (results_dir)), 'w')
      for type_str, type_id in type_strs.items():
        aggline = " ".join( ( [ ("%-10d" % type_id) ] + [ type_str ] ) )
        print (aggline, file=agg_type_strs_fd)
      print ("", file=agg_type_strs_fd)
      agg_type_strs_fd.close()

      agg_acc_strs_fd = open(('%sagg_acc_strs.out' % (results_dir)), 'w')
      for acc_id, acc_str in acc_strs.items():
        aggline = " ".join( ( [ ("%-16s" % hex(acc_id)) ] + [ acc_str ] ) )
        print (aggline, file=agg_acc_strs_fd)
      print ("", file=agg_acc_strs_fd)
      agg_acc_strs_fd.close()

      agg_site_strs_fd = open(('%sagg_site_cxt_strs.out' % (results_dir)), 'w')
      for addr in site_strs:
        aggline = " ".join( ( [ ("%-16s" % hex(addr)) ] + [ site_strs[addr] ] ) )
        print (aggline, file=agg_site_strs_fd)
      print ("", file=agg_site_strs_fd)
      agg_site_strs_fd.close()


      agg_type_sigs_fd = open(('%sagg_type_sigs.out' % (results_dir)), 'w')
      for tsig in agg_type_map:
        aggline = " ".join( ( [ ("%-10d" % agg_type_map[tsig][0]) ] + \
                              [ ("%10d" % tnum) for tnum in tsig ] ) )
        print (aggline, file=agg_type_sigs_fd)
      print ("", file=agg_type_sigs_fd)
      agg_type_sigs_fd.close()

      agg_acc_sigs_fd = open(('%sagg_acc_sigs.out' % (results_dir)), 'w')
      for asig in agg_acc_map:
        aggline = " ".join( ( [ ("%-10d" % agg_acc_map[asig][0]) ] + \
                              [ ("%16x" % addr) for addr in asig ] ) )
        print (aggline, file=agg_acc_sigs_fd)
      print ("", file=agg_acc_sigs_fd)
      agg_acc_sigs_fd.close()


      agg_size_info_fd  = open(('%sagg_size_info.out' % (results_dir)), 'w')
      for size in agg_size_map:
        print (get_agg_info_line(size, agg_size_map[size]), file=agg_size_info_fd)
      print ("", file=agg_size_info_fd)
      agg_size_info_fd.close()

      agg_szbucket_info_fd  = open(('%sagg_size_bucket_info.out' % (results_dir)), 'w')
      for szbucket in agg_szbucket_map: 
        print (get_agg_info_line(szbucket, agg_szbucket_map[szbucket]),
               file=agg_szbucket_info_fd)
      print ("", file=agg_szbucket_info_fd)
      agg_szbucket_info_fd.close()

      agg_type_info_fd  = open(('%sagg_type_sig_info.out' % (results_dir)), 'w')
      for tsig in agg_type_map:
        print (get_agg_info_line(agg_type_map[tsig][0], \
                                 agg_type_map[tsig][1]),
               file=agg_type_info_fd)
      print ("", file=agg_type_info_fd)
      agg_type_info_fd.close()

      agg_acc_info_fd  = open(('%sagg_acc_sig_info.out' % (results_dir)), 'w')
      for asig in agg_acc_map:
        print (get_agg_info_line(agg_acc_map[asig][0], \
                                 agg_acc_map[asig][1]),
               file=agg_acc_info_fd)
      print ("", file=agg_acc_info_fd)
      agg_acc_info_fd.close()


      for clen in pin_cxt_lengths:

        agg_cxts_fd  = open(('%sagg_site_cxt%d_cxts.out' % (results_dir,clen)), 'w')
        for site in agg_site_map[clen]:
          aggline = " ".join( ( [ ("%-10d" % agg_site_map[clen][site][0]) ] + \
                                [ ("%16s" % hex(addr)) for addr in site ] ) )
          print (aggline, file=agg_cxts_fd)
        print ("", file=agg_cxts_fd)
        agg_cxts_fd.close()

        agg_site_info_fd  = open(('%sagg_site_cxt%d_info.out' % (results_dir,clen)), 'w')
        for site in agg_site_map[clen]:
          print (get_agg_info_line( agg_site_map[clen][site][0],   \
                                    agg_site_map[clen][site][1] ), \
                 file=agg_site_info_fd )
        print ("", file=agg_site_info_fd)
        agg_site_info_fd.close()


      if phase_cfg:
        agg_phases_fd = open(('%sagg_phases.out' % (results_dir)), 'w')
        for phase_id in agg_alloc_phase_map:
          aggline = " ".join( ( [ ("%-10d" % phase_id) ] + \
                                [ ("%16d"  % val) for val in agg_bbvs[phase_id] ] ) )
          print (aggline, file=agg_phases_fd)
        print ("", file=agg_phases_fd)
        agg_phases_fd.close()

        agg_phase_sigs_fd = open(('%sagg_phase_sigs.out' % (results_dir)), 'w')
        for phase_sig in agg_phase_sig_map:
          aggline = " ".join( ( [ ("%-10d" % agg_phase_sig_map[phase_sig][0]) ] + \
                                [ ("%16d"  % val) for val in phase_sig ] ) )
          print (aggline, file=agg_phase_sigs_fd)
        print ("", file=agg_phase_sigs_fd)
        agg_phase_sigs_fd.close()


        agg_typesz_fd = open(('%sagg_size_typesigs.out' % (results_dir)), 'w')
        for typesz in agg_typesz_map:
          aggline = " ".join( ( [ ("%-10d" % agg_typesz_map[typesz][0]) ] + \
                                [ ("%16d" % val) for val in typesz ] ) )
          print (aggline, file=agg_typesz_fd)
        print ("", file=agg_typesz_fd)
        agg_typesz_fd.close()

        agg_phasesz_fd = open(('%sagg_phase_size_sigs.out' % (results_dir)), 'w')
        for phasesz in agg_phasesz_map:
          aggline = " ".join( ( [ ("%-10d" % agg_phasesz_map[phasesz][0]) ] + \
                                [ ("%16d"  % phasesz[0]) ] + \
                                [ ("%16d"  % phasesz[1]) ] ) )
          print (aggline, file=agg_phasesz_fd)
        print ("", file=agg_phasesz_fd)
        agg_phasesz_fd.close()


        agg_alloc_phase_info_fd  = open(('%sagg_alloc_phase_info.out' % (results_dir)), 'w')
        for phid in agg_alloc_phase_map:
          print (get_agg_info_line(phid, agg_alloc_phase_map[phid]),
                 file=agg_alloc_phase_info_fd)
        print ("", file=agg_alloc_phase_info_fd)
        agg_alloc_phase_info_fd.close()

        agg_phase_sig_info_fd  = open(('%sagg_phase_sig_info.out' % (results_dir)), 'w')
        for phase_sig in agg_phase_sig_map:
          print (get_agg_info_line(agg_phase_sig_map[phase_sig][0], \
                                   agg_phase_sig_map[phase_sig][1]),
                 file=agg_phase_sig_info_fd)
        print ("", file=agg_phase_sig_info_fd)
        agg_phase_sig_info_fd.close()

        agg_typesz_info_fd  = open(('%sagg_typesz_info.out' % (results_dir)), 'w')
        for typesz in agg_typesz_map:
          print (get_agg_info_line(agg_typesz_map[typesz][0], \
                                   agg_typesz_map[typesz][1]),
                 file=agg_typesz_info_fd)
        print ("", file=agg_typesz_info_fd)
        agg_typesz_info_fd.close()

        agg_phasesz_info_fd  = open(('%sagg_phase_size_info.out' % (results_dir)), 'w')
        for phasesz in agg_phasesz_map:
          print (get_agg_info_line(agg_phasesz_map[phasesz][0], \
                                   agg_phasesz_map[phasesz][1]),
                 file=agg_phasesz_info_fd)
        print ("", file=agg_phasesz_info_fd)
        agg_phasesz_info_fd.close()

        for clen in pin_cxt_lengths:

          agg_phase_site_fd  = open(('%sagg_phase_site%d.out' % (results_dir,clen)), 'w')
          for phsite in agg_phase_site_map[clen]:
            aggline = " ".join( ( [ ("%-10d" % agg_phase_site_map[clen][phsite][0]) ] + \
                                  [ ("%16d"  % phsite[0]) ] + \
                                  [ ("%16s"  % hex(addr)) for addr in phsite[1] ] ) )
            print (aggline, file=agg_phase_site_fd)
          print ("", file=agg_phase_site_fd)
          agg_phase_site_fd.close()

          agg_phase_site_info_fd  = open(('%sagg_phase_site%d_info.out' % (results_dir,clen)), 'w')
          for phsite in agg_phase_site_map[clen]:
            print (get_agg_info_line( agg_phase_site_map[clen][phsite][0],   \
                                      agg_phase_site_map[clen][phsite][1] ), \
                   file=agg_phase_site_info_fd )
          print ("", file=agg_phase_site_info_fd)
          agg_phase_site_info_fd.close()


          agg_phsz_site_fd  = open(('%sagg_phsz_site%d.out' % (results_dir,clen)), 'w')
          for phsite in agg_phsz_site_map[clen]:
            aggline = " ".join( ( [ ("%-10d" % agg_phsz_site_map[clen][phsite][0]) ] + \
                                  [ ("%16d"  % phsite[0]) ] + [ ("%16d" % phsite[1]) ] + \
                                  [ ("%16d"  % val) for val in phsite[2] ] ) )
            print (aggline, file=agg_phsz_site_fd)
          print ("", file=agg_phsz_site_fd)
          agg_phsz_site_fd.close()

          agg_phsz_site_info_fd  = open(('%sagg_phsz_site%d_info.out' % (results_dir,clen)), 'w')
          for phsite in agg_phsz_site_map[clen]:
            print (get_agg_info_line( agg_phsz_site_map[clen][phsite][0],   \
                                      agg_phsz_site_map[clen][phsite][1] ), \
                   file=agg_phsz_site_info_fd )
          print ("", file=agg_phsz_site_info_fd)
          agg_phsz_site_info_fd.close()

      print ("ok.")

#    except:
#      print ("error printing agg files.")

def make_otdict_pickle(bench, cfg_name, tsizes=def_tsizes, \
  cutstyle=THERMOS):

  otdict = {}

  otzstrs = [cut_str(cutstyle, tz) for tz in tsizes]
  for tzstr in ( [ ALL ] + otzstrs ):
    otdict[tzstr] = get_object_tzstr_info(bench, cfg_name, cutstyle, tzstr)

    results_dir = get_results_dir(bench, cfg_name)
    cmd = ("rm -f %s" % ('%s.object_%s-%s.pkl' % (results_dir, cutstyle.lower(), tzstr)))
    print(cmd)

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sobject_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(otdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

def get_otdict_object(objlist, objidxs):
  if objidxs == None:
    for obj in objlist:
      yield obj
  else:
    for idx in objidxs:
      yield objlist[idx]

def get_otdict(bench, cfg_name, objlist, phase_cfg, foff,
  cutstyle=None, tzstr=None, objidxs=None, lowmem=False):

  otdict = {}
  otdict[AGG]         = [0, 0, 0, 0, 0, 0, 0]
  otdict[SIZE]        = {}
  otdict[SIZE_BUCKET] = {}
  otdict[TYPE_SIG]    = {}
  otdict[ACC_SIG]     = {}
  otdict[SITES]       = {}
  for cxt in pin_cxt_lengths:
    otdict[SITES][cxt] = {}

  if phase_cfg:
    otdict[ALLOC_PHASE] = {}
    otdict[TYPE_SIZE]   = {}
    otdict[PHASE_SIZE]  = {}
    otdict[PHASE_SIG]   = {}
    otdict[PHASE_SITES] = {}
    otdict[PHSZ_SITES]  = {}
    for cxt in pin_cxt_lengths:
      otdict[PHASE_SITES][cxt] = {}
      otdict[PHSZ_SITES][cxt]  = {}

  otdict[LIFETIME] = get_ins_count(bench, cfg_name)

  for k,obj in enumerate(get_otdict_object(objlist,objidxs)):
    clsize   = obj[(lines_idx    + foff)]
    pc_accs  = obj[(pcaccs_idx   + foff)]
    mc_accs  = obj[(mcaccs_idx   + foff)]
    bw       = obj[(bw_idx       + foff)]
    lifetime = obj[(lifetime_idx + foff)]
    pc_hits  = obj[(pchits_idx   + foff)]
    mc_hits  = obj[(mchits_idx   + foff)]
    pre_rw   = obj[(prerw_idx    + foff)]
    post_rw  = obj[(postrw_idx   + foff)]

    pre_wrs  = 0 if pre_rw < -0.01 else int((pc_accs / (pre_rw+1)))
    pre_rds  = int((pc_accs - pre_wrs))
    post_wrs = 0 if post_rw < -0.01 else int((bw / (post_rw+1)))
    post_rds = int((bw - post_wrs))

    otdict[AGG][0] += 1
    otdict[AGG][1] += clsize
    otdict[AGG][2] += pre_rds
    otdict[AGG][3] += pre_wrs
    otdict[AGG][4] += post_rds
    otdict[AGG][5] += post_wrs
    otdict[AGG][6] += lifetime

    for i,cxt in enumerate(pin_cxt_lengths):
      site = obj[i]
      if not site in otdict[SITES][cxt]:
        otdict[SITES][cxt][site] = [0, 0, 0, 0, 0, 0, 0, []]

      otdict[SITES][cxt][site][0] += 1
      otdict[SITES][cxt][site][1] += clsize
      otdict[SITES][cxt][site][2] += pre_rds
      otdict[SITES][cxt][site][3] += pre_wrs
      otdict[SITES][cxt][site][4] += post_rds
      otdict[SITES][cxt][site][5] += post_wrs
      otdict[SITES][cxt][site][6] += lifetime
      otdict[SITES][cxt][site][7].append(clsize)

    size = obj[size_idx]
    if not size in otdict[SIZE]:
      otdict[SIZE][size] = [0, 0, 0, 0, 0, 0, 0, []]
    otdict[SIZE][size][0] += 1
    otdict[SIZE][size][1] += clsize
    otdict[SIZE][size][2] += pre_rds
    otdict[SIZE][size][3] += pre_wrs
    otdict[SIZE][size][4] += post_rds
    otdict[SIZE][size][5] += post_wrs
    otdict[SIZE][size][6] += lifetime
    otdict[SIZE][size][7].append(clsize)

    szbucket = obj[szbucket_idx]
    if not szbucket in otdict[SIZE_BUCKET]:
      otdict[SIZE_BUCKET][szbucket] = [0, 0, 0, 0, 0, 0, 0, []]
    otdict[SIZE_BUCKET][szbucket][0] += 1
    otdict[SIZE_BUCKET][szbucket][1] += clsize
    otdict[SIZE_BUCKET][szbucket][2] += pre_rds
    otdict[SIZE_BUCKET][szbucket][3] += pre_wrs
    otdict[SIZE_BUCKET][szbucket][4] += post_rds
    otdict[SIZE_BUCKET][szbucket][5] += post_wrs
    otdict[SIZE_BUCKET][szbucket][6] += lifetime
    otdict[SIZE_BUCKET][szbucket][7].append(clsize)

    tsig = obj[tsig_idx]
    if not tsig in otdict[TYPE_SIG]:
      otdict[TYPE_SIG][tsig] = [0, 0, 0, 0, 0, 0, 0, []]
    otdict[TYPE_SIG][tsig][0] += 1
    otdict[TYPE_SIG][tsig][1] += clsize
    otdict[TYPE_SIG][tsig][2] += pre_rds
    otdict[TYPE_SIG][tsig][3] += pre_wrs
    otdict[TYPE_SIG][tsig][4] += post_rds
    otdict[TYPE_SIG][tsig][5] += post_wrs
    otdict[TYPE_SIG][tsig][6] += lifetime
    otdict[TYPE_SIG][tsig][7].append(clsize)

    asig = obj[asig_idx]
    if not asig in otdict[ACC_SIG]:
      otdict[ACC_SIG][asig] = [0, 0, 0, 0, 0, 0, 0, []]
    otdict[ACC_SIG][asig][0] += 1
    otdict[ACC_SIG][asig][1] += clsize
    otdict[ACC_SIG][asig][2] += pre_rds
    otdict[ACC_SIG][asig][3] += pre_wrs
    otdict[ACC_SIG][asig][4] += post_rds
    otdict[ACC_SIG][asig][5] += post_wrs
    otdict[ACC_SIG][asig][6] += lifetime
    otdict[ACC_SIG][asig][7].append(clsize)

    if phase_cfg:

      aph = obj[aph_idx]
      if not aph in otdict[ALLOC_PHASE]:
        otdict[ALLOC_PHASE][aph] = [0, 0, 0, 0, 0, 0, 0, []]
      otdict[ALLOC_PHASE][aph][0] += 1
      otdict[ALLOC_PHASE][aph][1] += clsize
      otdict[ALLOC_PHASE][aph][2] += pre_rds
      otdict[ALLOC_PHASE][aph][3] += pre_wrs
      otdict[ALLOC_PHASE][aph][4] += post_rds
      otdict[ALLOC_PHASE][aph][5] += post_wrs
      otdict[ALLOC_PHASE][aph][6] += lifetime
      otdict[ALLOC_PHASE][aph][7].append(clsize)

      for i,cxt in enumerate(pin_cxt_lengths):
        phsite = obj[(phsite_idx+i)]
        if not phsite in otdict[PHASE_SITES][cxt]:
          otdict[PHASE_SITES][cxt][phsite] = [0, 0, 0, 0, 0, 0, 0, []]

        otdict[PHASE_SITES][cxt][phsite][0] += 1
        otdict[PHASE_SITES][cxt][phsite][1] += clsize
        otdict[PHASE_SITES][cxt][phsite][2] += pre_rds
        otdict[PHASE_SITES][cxt][phsite][3] += pre_wrs
        otdict[PHASE_SITES][cxt][phsite][4] += post_rds
        otdict[PHASE_SITES][cxt][phsite][5] += post_wrs
        otdict[PHASE_SITES][cxt][phsite][6] += lifetime
        otdict[PHASE_SITES][cxt][phsite][7].append(clsize)

      for i,cxt in enumerate(pin_cxt_lengths):
        phszst = obj[(phszst_idx+i)]
        if not phszst in otdict[PHSZ_SITES][cxt]:
          otdict[PHSZ_SITES][cxt][phszst] = [0, 0, 0, 0, 0, 0, 0, []]

        otdict[PHSZ_SITES][cxt][phszst][0] += 1
        otdict[PHSZ_SITES][cxt][phszst][1] += clsize
        otdict[PHSZ_SITES][cxt][phszst][2] += pre_rds
        otdict[PHSZ_SITES][cxt][phszst][3] += pre_wrs
        otdict[PHSZ_SITES][cxt][phszst][4] += post_rds
        otdict[PHSZ_SITES][cxt][phszst][5] += post_wrs
        otdict[PHSZ_SITES][cxt][phszst][6] += lifetime
        otdict[PHSZ_SITES][cxt][phszst][7].append(clsize)

      phsig = obj[phsig_idx]
      if not phsig in otdict[PHASE_SIG]:
        otdict[PHASE_SIG][phsig] = [0, 0, 0, 0, 0, 0, 0, []]
      otdict[PHASE_SIG][phsig][0] += 1
      otdict[PHASE_SIG][phsig][1] += clsize
      otdict[PHASE_SIG][phsig][2] += pre_rds
      otdict[PHASE_SIG][phsig][3] += pre_wrs
      otdict[PHASE_SIG][phsig][4] += post_rds
      otdict[PHASE_SIG][phsig][5] += post_wrs
      otdict[PHASE_SIG][phsig][6] += lifetime
      otdict[PHASE_SIG][phsig][7].append(clsize)

      typesz = obj[typesz_idx]
      if not typesz in otdict[TYPE_SIZE]:
        otdict[TYPE_SIZE][typesz] = [0, 0, 0, 0, 0, 0, 0, []]
      otdict[TYPE_SIZE][typesz][0] += 1
      otdict[TYPE_SIZE][typesz][1] += clsize
      otdict[TYPE_SIZE][typesz][2] += pre_rds
      otdict[TYPE_SIZE][typesz][3] += pre_wrs
      otdict[TYPE_SIZE][typesz][4] += post_rds
      otdict[TYPE_SIZE][typesz][5] += post_wrs
      otdict[TYPE_SIZE][typesz][6] += lifetime
      otdict[TYPE_SIZE][typesz][7].append(clsize)

      phsz = obj[phsz_idx]
      if not phsz in otdict[PHASE_SIZE]:
        otdict[PHASE_SIZE][phsz] = [0, 0, 0, 0, 0, 0, 0, []]
      otdict[PHASE_SIZE][phsz][0] += 1
      otdict[PHASE_SIZE][phsz][1] += clsize
      otdict[PHASE_SIZE][phsz][2] += pre_rds
      otdict[PHASE_SIZE][phsz][3] += pre_wrs
      otdict[PHASE_SIZE][phsz][4] += post_rds
      otdict[PHASE_SIZE][phsz][5] += post_wrs
      otdict[PHASE_SIZE][phsz][6] += lifetime
      otdict[PHASE_SIZE][phsz][7].append(clsize)

  if lowmem:
    print("pickling ... ", end='')
    results_dir = get_results_dir(bench, cfg_name)
    cut_pkl_fd = open(('%s.object_%s-%s.pkl' % (results_dir, cutstyle.lower(), tzstr)), 'wb')
    pickle.dump(otdict, cut_pkl_fd)
    cut_pkl_fd.close()
    otdict = None
    print("done.")

  return otdict

def create_cut_infos(benches, configs, tsizes=def_tsizes, \
  object_cut=THERMOS, feature_cut=THERMOS, train_cfg=None, \
  do_objects=True, do_features=True, bwfilter=True, lowmem=False):

  for bench in benches:
    for cfg_name in configs:
      if do_objects:
        create_object_cut_info(bench, cfg_name, tsizes=tsizes, \
                               cutstyle=object_cut, bwfilter=bwfilter,\
                               lowmem=lowmem)
      if do_features:
        create_feature_cut_info(bench, cfg_name, tsizes=tsizes, \
                                cutstyle=feature_cut)
      if train_cfg:
        create_train_feature_cut_info(bench, cfg_name, train_cfg, \
          tsizes=tsizes, cutstyle=feature_cut)
#        create_alt_site_thermos_info(bench, cfg_name, tsizes=tsizes, \
#                                     cutstyle=site_cut)
#      if do_bench:
#        create_bench_thermos_info(bench, cfg_name, tsizes=tsizes, \
#                                  cutstyle=site_cut)

def create_object_cut_info(bench, cfg_name, tsizes=def_tsizes,
  cutstyle=THERMOS, bwfilter=True, lowmem=False):

  otdict = {}
  object_thermi = []

  otzstrs = [cut_str(cutstyle, tz) for tz in tsizes]

  phase_cfg = False
  if cfg_name == 'pin_lpc_9cxt':
    foff  = (asig_idx + 1)
  elif cfg_name in ['pin_phase_lpc_9cxt', 'pin_phase2_lpc_9cxt']:
    foff  = (phsz_idx + 1)
    phase_cfg = True

  print("%s-%s" % (bench, cfg_name))
  objlist = get_object_list(bench, cfg_name)
  print("  got objects.")

  # remove objects with no bandwidth
  #
  # some objects have size, but no bandwidth -- which can easily happen
  # because of the way we profile (the objects in the cache by the time we
  # start recording their accesses)
  #
  if bwfilter:
    objlist = [ x for x in objlist if x[(bw_idx+foff)] != 0 ]

  cutfn = None
  if cutstyle == THERMOS:
    cutfn = object_thermos
  elif cutstyle == BANDWIDTH_PER_BYTE:
    cutfn = object_bwpb_cut
  elif cutstyle == WRITES_PER_BYTE:
    cutfn = object_wrpb_cut
  elif cutstyle == RDWR_RATIO:
    cutfn = object_rdwr_cut
  elif cutstyle == LIFETIME:
    cutfn = object_lifetime_cut
  elif cutstyle == LIFETIME_PCT:
    cutfn = object_lifetime_pct_cut
 
  print("%s-%s" % (bench, cfg_name))
  print("  doing object %s cut:" % cutstyle.lower())
  for tz,tzstr in zip(tsizes,otzstrs):
    print(("    %8.4f ... " % tz), end='')
    if cutstyle == LIFETIME_PCT:
      object_thermi.append( (tzstr, cutfn(objlist, tz, foff, \
                             get_ins_count(bench, cfg_name) )) )
    else:
      object_thermi.append(  (tzstr, cutfn(objlist, tz, foff)) )
    print("done.")

  print ("  object dict ... ", end='')
  otdict[ALL] = get_otdict(bench, cfg_name, objlist, phase_cfg, foff,
                           cutstyle=cutstyle, tzstr=ALL, lowmem=lowmem)
  print ("done.")

  for tzstr,thermos in object_thermi:
    print (("  %24s ... " % tzstr), end='')
    otdict[tzstr] = get_otdict(bench, cfg_name, objlist, phase_cfg, foff,
                               cutstyle=cutstyle, objidxs=thermos, tzstr=tzstr,
                               lowmem=lowmem)
    print ("done.")

  objlist = None
  if lowmem:
    for tzstr in ( [ ALL ] + otzstrs ):
      otdict[tzstr] = get_object_tzstr_info(bench, cfg_name, cutstyle, tzstr)

      results_dir = get_results_dir(bench, cfg_name)
      cmd = ("rm -f %s" % ('%s.object_%s-%s.pkl' % (results_dir, cutstyle.lower(), tzstr)))
      print(cmd)

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sobject_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(otdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

def get_ftobj_info(otdict, feature, cxt=None):
  ftobj_info = {}
  if feature == BENCH:
    ftobj_info[AGG] = {}
    ftobj_info[AGG][TOUCHED]      = otdict[ALL][AGG][0]
    ftobj_info[AGG][LINE_RSS]     = otdict[ALL][AGG][1]
    ftobj_info[AGG][PRE_READS]    = otdict[ALL][AGG][2]
    ftobj_info[AGG][PRE_WRITES]   = otdict[ALL][AGG][3]
    ftobj_info[AGG][POST_READS]   = otdict[ALL][AGG][4]
    ftobj_info[AGG][POST_WRITES]  = otdict[ALL][AGG][5]
    ftobj_info[AGG][LIFETIME]     = otdict[ALL][AGG][6]
  elif feature in scalar_features:
    for ftkey in otdict[ALL][feature]:
      ftobj_info[ftkey] = {}
      ftobj_info[ftkey][TOUCHED]      = otdict[ALL][feature][ftkey][0]
      ftobj_info[ftkey][LINE_RSS]     = otdict[ALL][feature][ftkey][1]
      ftobj_info[ftkey][PRE_READS]    = otdict[ALL][feature][ftkey][2]
      ftobj_info[ftkey][PRE_WRITES]   = otdict[ALL][feature][ftkey][3]
      ftobj_info[ftkey][POST_READS]   = otdict[ALL][feature][ftkey][4]
      ftobj_info[ftkey][POST_WRITES]  = otdict[ALL][feature][ftkey][5]
      ftobj_info[ftkey][LIFETIME]     = otdict[ALL][feature][ftkey][6]
  elif feature in site_features:
    for ftkey in otdict[ALL][feature][cxt]:
      ftobj_info[ftkey] = {}
      ftobj_info[ftkey][TOUCHED]      = otdict[ALL][feature][cxt][ftkey][0]
      ftobj_info[ftkey][LINE_RSS]     = otdict[ALL][feature][cxt][ftkey][1]
      ftobj_info[ftkey][PRE_READS]    = otdict[ALL][feature][cxt][ftkey][2]
      ftobj_info[ftkey][PRE_WRITES]   = otdict[ALL][feature][cxt][ftkey][3]
      ftobj_info[ftkey][POST_READS]   = otdict[ALL][feature][cxt][ftkey][4]
      ftobj_info[ftkey][POST_WRITES]  = otdict[ALL][feature][cxt][ftkey][5]
      ftobj_info[ftkey][LIFETIME]     = otdict[ALL][feature][cxt][ftkey][6]
  return ftobj_info

def get_ftt_info(ftobj_info, feature, tz, cutstyle, \
  cutfn, bnlife=None, cxt=None):

  if cutstyle == LIFETIME_PCT:
    thermos = cutfn(ftobj_info, tz, bnlife)
  else:
    thermos = cutfn(ftobj_info, tz)

  fttinfo = [0, 0, 0, 0, 0, 0, 0, set([])]
  if feature == BENCH:
    if len(thermos) > 0:
      fttinfo[0] += ftobj_info[AGG][TOUCHED]
      fttinfo[1] += ftobj_info[AGG][LINE_RSS]
      fttinfo[2] += ftobj_info[AGG][PRE_READS]
      fttinfo[3] += ftobj_info[AGG][PRE_WRITES]
      fttinfo[4] += ftobj_info[AGG][POST_READS]
      fttinfo[5] += ftobj_info[AGG][POST_WRITES]
      fttinfo[6] += ftobj_info[AGG][LIFETIME]
      fttinfo[7].add(True)
  else:
    for val in thermos:
      fttinfo[0] += ftobj_info[val][TOUCHED]
      fttinfo[1] += ftobj_info[val][LINE_RSS]
      fttinfo[2] += ftobj_info[val][PRE_READS]
      fttinfo[3] += ftobj_info[val][PRE_WRITES]
      fttinfo[4] += ftobj_info[val][POST_READS]
      fttinfo[5] += ftobj_info[val][POST_WRITES]
      fttinfo[6] += ftobj_info[val][LIFETIME]
      fttinfo[7].add(val)
  return fttinfo

def create_feature_cut_info(bench, cfg_name, train_cfg=None, \
  tsizes=def_tsizes, cutstyle=THERMOS):

  ftdict  = {}
  ftzstrs = [cut_str(cutstyle, tz) for tz in tsizes]

  phase_cfg = False
  if cfg_name in ['pin_phase_lpc_9cxt', 'pin_phase2_lpc_9cxt']:
    phase_cfg = True

  cutfn = None
  if cutstyle == THERMOS:
    cutfn = feature_thermos
  elif cutstyle == BANDWIDTH_PER_BYTE:
    cutfn = feature_bwpb_cut
  elif cutstyle == WRITES_PER_BYTE:
    cutfn = feature_wrpb_cut
  elif cutstyle == RDWR_RATIO:
    cutfn = feature_rdwr_cut
  elif cutstyle == LIFETIME:
    cutfn = feature_lifetime_cut
  elif cutstyle == LIFETIME_PCT:
    cutfn = feature_lifetime_pct_cut

  print("%s-%s" % (bench, cfg_name))

  ftdict[ALL]      = {}
  ftdict[ALL][AGG] = [0, 0, 0, 0, 0, 0, 0]
  otdict = get_object_cut_info(bench, cfg_name, cutstyle)
  bnlife = otdict[ALL][LIFETIME]

  print("  doing feature %s cut:" % cutstyle.lower())
  for i in range(len(ftdict[ALL][AGG])):
    ftdict[ALL][AGG][i] = otdict[ALL][AGG][i]

  for tz,tzstr in zip(tsizes, ftzstrs):
    ftdict[tzstr] = {}
    for ft in site_features:
      ftdict[tzstr][ft] = {}

  for ft in scalar_features:
    for tz,tzstr in zip(tsizes, ftzstrs):
      ftdict[tzstr][ft] = get_ftt_info( get_ftobj_info(otdict, ft), \
                                        ft, tz, cutstyle, cutfn, bnlife )

  for ft in site_features:
    for cxt in pin_cxt_lengths:
      for tz,tzstr in zip(tsizes, ftzstrs):
        ftdict[tzstr][ft][cxt] = get_ftt_info( get_ftobj_info(otdict, ft, cxt), \
                                               ft, tz, cutstyle, cutfn, \
                                               bnlife, cxt )

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sfeature_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(ftdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")


#  szbucket_info   = {}
#  szbucket_thermi = []
#  for szbucket in otdict[ALL][SIZE_BUCKET]:
#    szbucket_info[szbucket] = {}
#    szbucket_info[szbucket][TOUCHED]      = otdict[ALL][SIZE_BUCKET][szbucket][0]
#    szbucket_info[szbucket][LINE_RSS]     = otdict[ALL][SIZE_BUCKET][szbucket][1]
#    szbucket_info[szbucket][PRE_READS]    = otdict[ALL][SIZE_BUCKET][szbucket][2]
#    szbucket_info[szbucket][PRE_WRITES]   = otdict[ALL][SIZE_BUCKET][szbucket][3]
#    szbucket_info[szbucket][POST_READS]   = otdict[ALL][SIZE_BUCKET][szbucket][4]
#    szbucket_info[szbucket][POST_WRITES]  = otdict[ALL][SIZE_BUCKET][szbucket][5]
#    szbucket_info[szbucket][LIFETIME]     = otdict[ALL][SIZE_BUCKET][szbucket][6]
#
#  #print ("szbucket")
#  szbucket_thermi = get_feature_thermi(szbucket_info, tsizes, ftzstrs, cutstyle,\
#                                       cutfn, bnlife)
#  #print ("")
#
#  for tzstr,thermos in szbucket_thermi:
#    ftdict[tzstr][SIZE_BUCKET]  = [0, 0, 0, 0, 0, 0, 0, set([])]
#    for szbucket in thermos:
#      ftdict[tzstr][SIZE_BUCKET][0] += szbucket_info[szbucket][TOUCHED]
#      ftdict[tzstr][SIZE_BUCKET][1] += szbucket_info[szbucket][LINE_RSS]
#      ftdict[tzstr][SIZE_BUCKET][2] += szbucket_info[szbucket][PRE_READS]
#      ftdict[tzstr][SIZE_BUCKET][3] += szbucket_info[szbucket][PRE_WRITES]
#      ftdict[tzstr][SIZE_BUCKET][4] += szbucket_info[szbucket][POST_READS]
#      ftdict[tzstr][SIZE_BUCKET][5] += szbucket_info[szbucket][POST_WRITES]
#      ftdict[tzstr][SIZE_BUCKET][6] += szbucket_info[szbucket][LIFETIME]
#      ftdict[tzstr][SIZE_BUCKET][7].add(szbucket)
#
#
#  tsig_info   = {}
#  tsig_thermi = []
#  for tsig in otdict[ALL][TYPE_SIG]:
#    tsig_info[tsig] = {}
#    tsig_info[tsig][TOUCHED]      = otdict[ALL][TYPE_SIG][tsig][0]
#    tsig_info[tsig][LINE_RSS]     = otdict[ALL][TYPE_SIG][tsig][1]
#    tsig_info[tsig][PRE_READS]    = otdict[ALL][TYPE_SIG][tsig][2]
#    tsig_info[tsig][PRE_WRITES]   = otdict[ALL][TYPE_SIG][tsig][3]
#    tsig_info[tsig][POST_READS]   = otdict[ALL][TYPE_SIG][tsig][4]
#    tsig_info[tsig][POST_WRITES]  = otdict[ALL][TYPE_SIG][tsig][5]
#    tsig_info[tsig][LIFETIME]     = otdict[ALL][TYPE_SIG][tsig][6]
#
#  #print ("typesig")
#  tsig_thermi = get_feature_thermi(tsig_info, tsizes, ftzstrs, cutstyle,\
#                                   cutfn, bnlife)
#  #print ("")
#
#  for tzstr,thermos in tsig_thermi:
#    ftdict[tzstr][TYPE_SIG] = [0, 0, 0, 0, 0, 0, 0, set([])]
#    for tsig in thermos:
#      ftdict[tzstr][TYPE_SIG][0] += tsig_info[tsig][TOUCHED]
#      ftdict[tzstr][TYPE_SIG][1] += tsig_info[tsig][LINE_RSS]
#      ftdict[tzstr][TYPE_SIG][2] += tsig_info[tsig][PRE_READS]
#      ftdict[tzstr][TYPE_SIG][3] += tsig_info[tsig][PRE_WRITES]
#      ftdict[tzstr][TYPE_SIG][4] += tsig_info[tsig][POST_READS]
#      ftdict[tzstr][TYPE_SIG][5] += tsig_info[tsig][POST_WRITES]
#      ftdict[tzstr][TYPE_SIG][6] += tsig_info[tsig][LIFETIME]
#      ftdict[tzstr][TYPE_SIG][7].add(tsig)
#
#
#  asig_info   = {}
#  asig_thermi = []
#  for asig in otdict[ALL][ACC_SIG]:
#    asig_info[asig] = {}
#    asig_info[asig][TOUCHED]      = otdict[ALL][ACC_SIG][asig][0]
#    asig_info[asig][LINE_RSS]     = otdict[ALL][ACC_SIG][asig][1]
#    asig_info[asig][PRE_READS]    = otdict[ALL][ACC_SIG][asig][2]
#    asig_info[asig][PRE_WRITES]   = otdict[ALL][ACC_SIG][asig][3]
#    asig_info[asig][POST_READS]   = otdict[ALL][ACC_SIG][asig][4]
#    asig_info[asig][POST_WRITES]  = otdict[ALL][ACC_SIG][asig][5]
#    asig_info[asig][LIFETIME]     = otdict[ALL][ACC_SIG][asig][6]
#
#  #print ("accsig")
#  asig_thermi = get_feature_thermi(asig_info, tsizes, ftzstrs, cutstyle,\
#                                   cutfn, bnlife)
#  #print ("")
#
#  for tzstr,thermos in asig_thermi:
#    ftdict[tzstr][ACC_SIG]  = [0, 0, 0, 0, 0, 0, 0, set([])]
#    for asig in thermos:
#      ftdict[tzstr][ACC_SIG][0] += asig_info[asig][TOUCHED]
#      ftdict[tzstr][ACC_SIG][1] += asig_info[asig][LINE_RSS]
#      ftdict[tzstr][ACC_SIG][2] += asig_info[asig][PRE_READS]
#      ftdict[tzstr][ACC_SIG][3] += asig_info[asig][PRE_WRITES]
#      ftdict[tzstr][ACC_SIG][4] += asig_info[asig][POST_READS]
#      ftdict[tzstr][ACC_SIG][5] += asig_info[asig][POST_WRITES]
#      ftdict[tzstr][ACC_SIG][6] += asig_info[asig][LIFETIME]
#      ftdict[tzstr][ACC_SIG][7].add(asig)
#
#
#  site_info = {}
#  for cxt in pin_cxt_lengths:
#    site_info[cxt]  = {}
#    site_cxt_thermi = []
#    for site in otdict[ALL][SITES][cxt]:
#      site_info[cxt][site] = {}
#      site_info[cxt][site][TOUCHED]      = otdict[ALL][SITES][cxt][site][0]
#      site_info[cxt][site][LINE_RSS]     = otdict[ALL][SITES][cxt][site][1]
#      site_info[cxt][site][PRE_READS]    = otdict[ALL][SITES][cxt][site][2]
#      site_info[cxt][site][PRE_WRITES]   = otdict[ALL][SITES][cxt][site][3]
#      site_info[cxt][site][POST_READS]   = otdict[ALL][SITES][cxt][site][4]
#      site_info[cxt][site][POST_WRITES]  = otdict[ALL][SITES][cxt][site][5]
#      site_info[cxt][site][LIFETIME]     = otdict[ALL][SITES][cxt][site][6]
#
#    #print ("cxt = %d" % cxt)
#    site_cxt_thermi = get_feature_thermi(site_info[cxt], tsizes, ftzstrs, cutstyle,\
#                                         cutfn, bnlife)
#    #print ("")
#
#    for tzstr,thermos in site_cxt_thermi:
#      if not SITES in ftdict[tzstr]:
#        ftdict[tzstr][SITES] = {}
#
#      ftdict[tzstr][SITES][cxt] = [0, 0, 0, 0, 0, 0, 0, set([])]
#      for site in thermos:
#        ftdict[tzstr][SITES][cxt][0] += site_info[cxt][site][TOUCHED]
#        ftdict[tzstr][SITES][cxt][1] += site_info[cxt][site][LINE_RSS]
#        ftdict[tzstr][SITES][cxt][2] += site_info[cxt][site][PRE_READS]
#        ftdict[tzstr][SITES][cxt][3] += site_info[cxt][site][PRE_WRITES]
#        ftdict[tzstr][SITES][cxt][4] += site_info[cxt][site][POST_READS]
#        ftdict[tzstr][SITES][cxt][5] += site_info[cxt][site][POST_WRITES]
#        ftdict[tzstr][SITES][cxt][6] += site_info[cxt][site][LIFETIME]
#        ftdict[tzstr][SITES][cxt][7].add(site)
#
#      #if str2tz(tzstr) == 1.28:
#      #  print("okj: %12d %12d" % (CLMB(otdict[ALL][AGG][1]),\
#      #        CLMB(ftdict[tzstr][SITES][cxt][1])))
#  
#  if phase_cfg:
#    aph_info   = {}
#    aph_thermi = []
#    for aph in otdict[ALL][ALLOC_PHASE]:
#      aph_info[aph] = {}
#      aph_info[aph][TOUCHED]      = otdict[ALL][ALLOC_PHASE][aph][0]
#      aph_info[aph][LINE_RSS]     = otdict[ALL][ALLOC_PHASE][aph][1]
#      aph_info[aph][PRE_READS]    = otdict[ALL][ALLOC_PHASE][aph][2]
#      aph_info[aph][PRE_WRITES]   = otdict[ALL][ALLOC_PHASE][aph][3]
#      aph_info[aph][POST_READS]   = otdict[ALL][ALLOC_PHASE][aph][4]
#      aph_info[aph][POST_WRITES]  = otdict[ALL][ALLOC_PHASE][aph][5]
#      aph_info[aph][LIFETIME]     = otdict[ALL][ALLOC_PHASE][aph][6]
#
#    #print ("accsig")
#    aph_thermi = get_feature_thermi(aph_info, tsizes, ftzstrs, cutstyle,\
#                                    cutfn, bnlife)
#    #print ("")
#
#    for tzstr,thermos in aph_thermi:
#      ftdict[tzstr][ALLOC_PHASE]  = [0, 0, 0, 0, 0, 0, 0, set([])]
#      for aph in thermos:
#        ftdict[tzstr][ALLOC_PHASE][0] += aph_info[aph][TOUCHED]
#        ftdict[tzstr][ALLOC_PHASE][1] += aph_info[aph][LINE_RSS]
#        ftdict[tzstr][ALLOC_PHASE][2] += aph_info[aph][PRE_READS]
#        ftdict[tzstr][ALLOC_PHASE][3] += aph_info[aph][PRE_WRITES]
#        ftdict[tzstr][ALLOC_PHASE][4] += aph_info[aph][POST_READS]
#        ftdict[tzstr][ALLOC_PHASE][5] += aph_info[aph][POST_WRITES]
#        ftdict[tzstr][ALLOC_PHASE][6] += aph_info[aph][LIFETIME]
#        ftdict[tzstr][ALLOC_PHASE][7].add(aph)
#
#
#    phsig_info   = {}
#    phsig_thermi = []
#    for phsig in otdict[ALL][PHASE_SIG]:
#      phsig_info[phsig] = {}
#      phsig_info[phsig][TOUCHED]      = otdict[ALL][PHASE_SIG][phsig][0]
#      phsig_info[phsig][LINE_RSS]     = otdict[ALL][PHASE_SIG][phsig][1]
#      phsig_info[phsig][PRE_READS]    = otdict[ALL][PHASE_SIG][phsig][2]
#      phsig_info[phsig][PRE_WRITES]   = otdict[ALL][PHASE_SIG][phsig][3]
#      phsig_info[phsig][POST_READS]   = otdict[ALL][PHASE_SIG][phsig][4]
#      phsig_info[phsig][POST_WRITES]  = otdict[ALL][PHASE_SIG][phsig][5]
#      phsig_info[phsig][LIFETIME]     = otdict[ALL][PHASE_SIG][phsig][6]
#
#    #print ("accsig")
#    phsig_thermi = get_feature_thermi(phsig_info, tsizes, ftzstrs, cutstyle,\
#                                      cutfn, bnlife)
#    #print ("")
#
#    for tzstr,thermos in phsig_thermi:
#      ftdict[tzstr][PHASE_SIG]  = [0, 0, 0, 0, 0, 0, 0, set([])]
#      for phsig in thermos:
#        ftdict[tzstr][PHASE_SIG][0] += phsig_info[phsig][TOUCHED]
#        ftdict[tzstr][PHASE_SIG][1] += phsig_info[phsig][LINE_RSS]
#        ftdict[tzstr][PHASE_SIG][2] += phsig_info[phsig][PRE_READS]
#        ftdict[tzstr][PHASE_SIG][3] += phsig_info[phsig][PRE_WRITES]
#        ftdict[tzstr][PHASE_SIG][4] += phsig_info[phsig][POST_READS]
#        ftdict[tzstr][PHASE_SIG][5] += phsig_info[phsig][POST_WRITES]
#        ftdict[tzstr][PHASE_SIG][6] += phsig_info[phsig][LIFETIME]
#        ftdict[tzstr][PHASE_SIG][7].add(phsig)
#
#    typesz_info   = {}
#    typesz_thermi = []
#    for typesz in otdict[ALL][TYPE_SIZE]:
#      typesz_info[typesz] = {}
#      typesz_info[typesz][TOUCHED]      = otdict[ALL][TYPE_SIZE][typesz][0]
#      typesz_info[typesz][LINE_RSS]     = otdict[ALL][TYPE_SIZE][typesz][1]
#      typesz_info[typesz][PRE_READS]    = otdict[ALL][TYPE_SIZE][typesz][2]
#      typesz_info[typesz][PRE_WRITES]   = otdict[ALL][TYPE_SIZE][typesz][3]
#      typesz_info[typesz][POST_READS]   = otdict[ALL][TYPE_SIZE][typesz][4]
#      typesz_info[typesz][POST_WRITES]  = otdict[ALL][TYPE_SIZE][typesz][5]
#      typesz_info[typesz][LIFETIME]     = otdict[ALL][TYPE_SIZE][typesz][6]
#
#    #print ("accsig")
#    typesz_thermi = get_feature_thermi(typesz_info, tsizes, ftzstrs, cutstyle,\
#                                       cutfn, bnlife)
#    #print ("")
#
#    for tzstr,thermos in typesz_thermi:
#      ftdict[tzstr][TYPE_SIZE]  = [0, 0, 0, 0, 0, 0, 0, set([])]
#      for typesz in thermos:
#        ftdict[tzstr][TYPE_SIZE][0] += typesz_info[typesz][TOUCHED]
#        ftdict[tzstr][TYPE_SIZE][1] += typesz_info[typesz][LINE_RSS]
#        ftdict[tzstr][TYPE_SIZE][2] += typesz_info[typesz][PRE_READS]
#        ftdict[tzstr][TYPE_SIZE][3] += typesz_info[typesz][PRE_WRITES]
#        ftdict[tzstr][TYPE_SIZE][4] += typesz_info[typesz][POST_READS]
#        ftdict[tzstr][TYPE_SIZE][5] += typesz_info[typesz][POST_WRITES]
#        ftdict[tzstr][TYPE_SIZE][6] += typesz_info[typesz][LIFETIME]
#        ftdict[tzstr][TYPE_SIZE][7].add(typesz)
#
#
#    phsz_info   = {}
#    phsz_thermi = []
#    for phsz in otdict[ALL][PHASE_SIZE]:
#      phsz_info[phsz] = {}
#      phsz_info[phsz][TOUCHED]      = otdict[ALL][PHASE_SIZE][phsz][0]
#      phsz_info[phsz][LINE_RSS]     = otdict[ALL][PHASE_SIZE][phsz][1]
#      phsz_info[phsz][PRE_READS]    = otdict[ALL][PHASE_SIZE][phsz][2]
#      phsz_info[phsz][PRE_WRITES]   = otdict[ALL][PHASE_SIZE][phsz][3]
#      phsz_info[phsz][POST_READS]   = otdict[ALL][PHASE_SIZE][phsz][4]
#      phsz_info[phsz][POST_WRITES]  = otdict[ALL][PHASE_SIZE][phsz][5]
#      phsz_info[phsz][LIFETIME]     = otdict[ALL][PHASE_SIZE][phsz][6]
#
#    #print ("accsig")
#    phsz_thermi = get_feature_thermi(phsz_info, tsizes, ftzstrs, cutstyle,\
#                                     cutfn, bnlife)
#    #print ("")
#
#    for tzstr,thermos in phsz_thermi:
#      ftdict[tzstr][PHASE_SIZE]  = [0, 0, 0, 0, 0, 0, 0, set([])]
#      for phsz in thermos:
#        ftdict[tzstr][PHASE_SIZE][0] += phsz_info[phsz][TOUCHED]
#        ftdict[tzstr][PHASE_SIZE][1] += phsz_info[phsz][LINE_RSS]
#        ftdict[tzstr][PHASE_SIZE][2] += phsz_info[phsz][PRE_READS]
#        ftdict[tzstr][PHASE_SIZE][3] += phsz_info[phsz][PRE_WRITES]
#        ftdict[tzstr][PHASE_SIZE][4] += phsz_info[phsz][POST_READS]
#        ftdict[tzstr][PHASE_SIZE][5] += phsz_info[phsz][POST_WRITES]
#        ftdict[tzstr][PHASE_SIZE][6] += phsz_info[phsz][LIFETIME]
#        ftdict[tzstr][PHASE_SIZE][7].add(phsz)
#
#    phase_site_info = {}
#    for cxt in pin_cxt_lengths:
#      phase_site_info[cxt] = {}
#      phase_site_thermi    = []
#      for phsite in otdict[ALL][PHASE_SITES][cxt]:
#        phase_site_info[cxt][phsite] = {}
#        phase_site_info[cxt][phsite][TOUCHED]      = otdict[ALL][PHASE_SITES][cxt][phsite][0]
#        phase_site_info[cxt][phsite][LINE_RSS]     = otdict[ALL][PHASE_SITES][cxt][phsite][1]
#        phase_site_info[cxt][phsite][PRE_READS]    = otdict[ALL][PHASE_SITES][cxt][phsite][2]
#        phase_site_info[cxt][phsite][PRE_WRITES]   = otdict[ALL][PHASE_SITES][cxt][phsite][3]
#        phase_site_info[cxt][phsite][POST_READS]   = otdict[ALL][PHASE_SITES][cxt][phsite][4]
#        phase_site_info[cxt][phsite][POST_WRITES]  = otdict[ALL][PHASE_SITES][cxt][phsite][5]
#        phase_site_info[cxt][phsite][LIFETIME]     = otdict[ALL][PHASE_SITES][cxt][phsite][6]
#
#      #print ("fcxt = %d" % cxt)
#      phase_site_thermi = get_feature_thermi(phase_site_info[cxt], tsizes, ftzstrs, cutstyle,\
#                                             cutfn, bnlife)
#      #print ("")
#
#      for tzstr,thermos in phase_site_thermi:
#        if not PHASE_SITES in ftdict[tzstr]:
#          ftdict[tzstr][PHASE_SITES] = {}
#
#        ftdict[tzstr][PHASE_SITES][cxt] = [0, 0, 0, 0, 0, 0, 0, set([])]
#        for phsite in thermos:
#          #if not phsite in phase_site_info[cxt]:
#          #  print (phsite)
#          #  print (cxt)
#          #  print (phase_site_info[cxt].keys())
#          #  raise SystemExit(1)
#          ftdict[tzstr][PHASE_SITES][cxt][0] += phase_site_info[cxt][phsite][TOUCHED]
#          ftdict[tzstr][PHASE_SITES][cxt][1] += phase_site_info[cxt][phsite][LINE_RSS]
#          ftdict[tzstr][PHASE_SITES][cxt][2] += phase_site_info[cxt][phsite][PRE_READS]
#          ftdict[tzstr][PHASE_SITES][cxt][3] += phase_site_info[cxt][phsite][PRE_WRITES]
#          ftdict[tzstr][PHASE_SITES][cxt][4] += phase_site_info[cxt][phsite][POST_READS]
#          ftdict[tzstr][PHASE_SITES][cxt][5] += phase_site_info[cxt][phsite][POST_WRITES]
#          ftdict[tzstr][PHASE_SITES][cxt][6] += phase_site_info[cxt][phsite][LIFETIME]
#          ftdict[tzstr][PHASE_SITES][cxt][7].add(phsite)
#
#    phsz_site_info  = {}
#    for cxt in pin_cxt_lengths:
#      phsz_site_info[cxt] = {}
#      phsz_site_thermi    = []
#      for phszst in otdict[ALL][PHSZ_SITES][cxt]:
#        phsz_site_info[cxt][phszst] = {}
#        phsz_site_info[cxt][phszst][TOUCHED]      = otdict[ALL][PHSZ_SITES][cxt][phszst][0]
#        phsz_site_info[cxt][phszst][LINE_RSS]     = otdict[ALL][PHSZ_SITES][cxt][phszst][1]
#        phsz_site_info[cxt][phszst][PRE_READS]    = otdict[ALL][PHSZ_SITES][cxt][phszst][2]
#        phsz_site_info[cxt][phszst][PRE_WRITES]   = otdict[ALL][PHSZ_SITES][cxt][phszst][3]
#        phsz_site_info[cxt][phszst][POST_READS]   = otdict[ALL][PHSZ_SITES][cxt][phszst][4]
#        phsz_site_info[cxt][phszst][POST_WRITES]  = otdict[ALL][PHSZ_SITES][cxt][phszst][5]
#        phsz_site_info[cxt][phszst][LIFETIME]     = otdict[ALL][PHSZ_SITES][cxt][phszst][6]
#
#      #print ("fcxt = %d" % cxt)
#      phsz_site_thermi = get_feature_thermi(phsz_site_info[cxt], tsizes, ftzstrs, cutstyle,\
#                                            cutfn, bnlife)
#      #print ("")
#
#      for tzstr,thermos in phsz_site_thermi:
#        if not PHSZ_SITES in ftdict[tzstr]:
#          ftdict[tzstr][PHSZ_SITES] = {}
#
#        ftdict[tzstr][PHSZ_SITES][cxt] = [0, 0, 0, 0, 0, 0, 0, set([])]
#        for phszst in thermos:
#          #if not phszst in phsz_site_info[cxt]:
#          #  print (phszst)
#          #  print (cxt)
#          #  print (phsz_site_info[cxt].keys())
#          #  raise SystemExit(1)
#          ftdict[tzstr][PHSZ_SITES][cxt][0] += phsz_site_info[cxt][phszst][TOUCHED]
#          ftdict[tzstr][PHSZ_SITES][cxt][1] += phsz_site_info[cxt][phszst][LINE_RSS]
#          ftdict[tzstr][PHSZ_SITES][cxt][2] += phsz_site_info[cxt][phszst][PRE_READS]
#          ftdict[tzstr][PHSZ_SITES][cxt][3] += phsz_site_info[cxt][phszst][PRE_WRITES]
#          ftdict[tzstr][PHSZ_SITES][cxt][4] += phsz_site_info[cxt][phszst][POST_READS]
#          ftdict[tzstr][PHSZ_SITES][cxt][5] += phsz_site_info[cxt][phszst][POST_WRITES]
#          ftdict[tzstr][PHSZ_SITES][cxt][6] += phsz_site_info[cxt][phszst][LIFETIME]
#          ftdict[tzstr][PHSZ_SITES][cxt][7].add(phszst)

def site_map(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_sites.out' % results_dir), 'r')

  smap = {}
  for line in fd:
    if line.isspace():
      break

    pts  = line.split()
    site = int(pts[0])

    smap[site] = {}
    smap[site][PAGE_RSS]     = ( int(pts[1]) * PAGE_SIZE )
    smap[site][LINE_RSS]     = ( int(pts[2]) * CACHE_LINE_SIZE )
    smap[site][TOTAL_HITS]   = int(pts[3])
    smap[site][TOTAL_MISSES] = int(pts[4])
    smap[site][READ_HITS]    = int(pts[5])
    smap[site][READ_MISSES]  = int(pts[6])
    smap[site][WRITE_HITS]   = int(pts[7])
    smap[site][WRITE_MISSES] = int(pts[8])
    smap[site][WRITEBACK]    = int(pts[9])

  fd.close()
  return smap

def site_strs_map(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_site_strs.out' % results_dir), 'r')

  smap = {}
  for line in fd:
    if line.isspace():
      break

    pts  = line.split()
    site = int(pts[0])

    smap[site][PAGE_RSS]     = ( int(pts[1]) * PAGE_SIZE )
    smap[site][LINE_RSS]     = ( int(pts[2]) * CACHE_LINE_SIZE )
    smap[site][TOTAL_HITS]   = int(pts[3])
    smap[site][TOTAL_MISSES] = int(pts[4])
    smap[site][READ_HITS]    = int(pts[5])
    smap[site][READ_MISSES]  = int(pts[6])
    smap[site][WRITE_HITS]   = int(pts[7])
    smap[site][WRITE_MISSES] = int(pts[8])
    smap[site][WRITEBACK]    = int(pts[9])

    smap[site][SYMBOLS] = []
    for sline in fd:
      if sline.isspace():
        break
      addr,sym = sline.split()
      smap[site][SYMBOLS].append((addr, sym))

  fd.close()
  return smap

def obj_report_format_str(stat):
  name, style = stat
  if name in [TOUCHED, ALLOCATED]:
    return "%12d"
  elif name in int_stats:
    if style in [MEAN, STDEV]:
      return "%4.3f"
    else:
      return "%12d"
  else:
    return "%4.3f"

def get_obj_report_stat(record, stat):
  name, style = stat
  stat_val = record[name]
  if name in [TOUCHED, ALLOCATED]:
    return stat_val
  else:
    return stat_val[style_idx[style]]

def format_str(stat):
  if "ratio" in stat:
    return "%4.3f"
  else:
    return "%12d"

def get_object_list(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_obj_info.out' % results_dir), 'r')

  objlist = []
  for line in fd:
    pts = line.split()
    objstats = []
    if cfg_name in ['pin_phase_lpc_9cxt', 'pin_phase2_lpc_9cxt']:
      for idx,stat in enumerate(object_phase_info_stats):
        objstats.append(stat[1](pts[idx]))
    else:
      for idx,stat in enumerate(object_info_stats):
        objstats.append(stat[1](pts[idx]))
    objlist.append(tuple(objstats))
  return objlist

def get_object_stats(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_stats.out' % results_dir), 'r')

  obj_stats = {}
  for line in fd:
    if line.startswith("ID:"):

      pts  = line.split()
      site = int(pts[1])
      obj_stats[site] = {}

      obj_stats[site][TOUCHED]     = int(pts[2])
      obj_stats[site][ALLOCATED]   = int(pts[4])
      obj_stats[site][PAGE_RSS]    = get_int_info(next(fd))
      obj_stats[site][LINE_RSS]    = get_int_info(next(fd))
      obj_stats[site][ACCESSES]    = get_int_info(next(fd))
      obj_stats[site][MISSES]      = get_int_info(next(fd))
      obj_stats[site][BANDWIDTH]   = get_float_info(next(fd))
      obj_stats[site][BW_PER_LINE] = get_float_info(next(fd))
      obj_stats[site][PC_HIT_RATE] = get_float_info(next(fd))
      obj_stats[site][RW_RATIO]    = get_float_info(next(fd))

  fd.close()
  return obj_stats

def object_report(bench, cfg_name, report_stats=object_report_stats,
  sortkey=TOUCHED, sortstyle=RAWVAL, revsort=True, cut=50):

  obj_stats = get_object_stats(bench, cfg_name)
  if sortkey in [TOUCHED, ALLOCATED]:
    sorted_stats = sorted(obj_stats.items(), \
                          key=lambda x: x[1][sortkey], \
                          reverse=revsort)
  else:
    sorted_stats = sorted(obj_stats.items(), \
                          key=lambda x: x[1][sortkey][style_idx[sortstyle]], \
                          reverse=revsort)

  print("%-4s %-6s" % ("rank", "id"), end='')
  for stat,style in report_stats:
    x = ("%s-%s" % (stat.lower(), short_style[style].lower()))
    print(" %12s" % x, end='')
  print("")

  for i,ostats in enumerate(sorted_stats[:cut]):
    outstr = ("%-4d %-6d" % (i, ostats[0]))
    for stat in report_stats:
      outstr += (" %12s" % (obj_report_format_str(stat) % \
                (get_obj_report_stat(ostats[1], stat))))
    print(outstr)


def site_info(bench, cfg_name):

  smap = site_map(bench, cfg_name)

  total_rss    = \
  total_reads  = \
  total_writes = \
  total_hits   = \
  total_misses = \
  total_accs   = 0

  for site in smap:
    site_reads  = smap[site][READ_HITS]   + smap[site][READ_MISSES]
    site_writes = smap[site][WRITE_HITS]  + smap[site][WRITE_MISSES]
    site_hits   = smap[site][READ_HITS]   + smap[site][WRITE_HITS]
    site_misses = smap[site][READ_MISSES] + smap[site][WRITE_MISSES]
    site_accs   = site_hits               + site_misses

    total_rss    += smap[site][PAGE_RSS]
    total_reads  += site_reads
    total_writes += site_writes
    total_hits   += site_hits
    total_misses += site_misses
    total_accs   += site_accs

    smap[site][BANDWIDTH] = ( smap[site][READ_MISSES] + \
                              smap[site][WRITEBACK] ) * \
                            CACHE_LINE_SIZE

    smap[site][SITE_HITS_RATIO]      = 0.0 if site_accs == 0 else \
                                     float(site_hits)   / site_accs
    smap[site][SITE_MISSES_RATIO]    = 0.0 if site_accs == 0 else \
                                     float(site_misses) / site_accs
    smap[site][SITE_RDWR_RATIO]      = 0.0 if site_writes == 0 else \
                                     float(site_reads)  / site_writes
    smap[site][SITE_RDWR_MISS_RATIO] = 0.0 if smap[site][WRITE_MISSES] == 0 else \
                                     float(smap[site][READ_MISSES]) / smap[site][WRITE_MISSES]

  for site in smap:
    site_reads  = smap[site][READ_HITS]   + smap[site][READ_MISSES]
    site_writes = smap[site][WRITE_HITS]  + smap[site][WRITE_MISSES]
    site_hits   = smap[site][READ_HITS]   + smap[site][WRITE_HITS]
    site_misses = smap[site][READ_MISSES] + smap[site][WRITE_MISSES]
    site_accs   = site_hits               + site_misses

    smap[site][TOTAL_HITS_RATIO]     = 0.0 if total_hits == 0 else \
                                     float(site_hits)   / total_hits
    smap[site][TOTAL_MISSES_RATIO]   = 0.0 if total_misses == 0 else \
                                     float(site_misses) / total_misses
    smap[site][TOTAL_ACCS_RATIO]     = 0.0 if total_accs == 0 else \
                                     float(site_accs)   / total_accs
    smap[site][PAGE_RSS_RATIO]       = 0.0 if total_rss == 0 else \
                                     float(smap[site][PAGE_RSS]) / total_rss
 
  return smap

def site_report(bench, cfg_name, stats=site_report_stats,
  sortkey=TOTAL_MISSES, revsort=True, cut=50):

  sinfo = site_info(bench, cfg_name)
  sorted_sinfo = sorted(sinfo.items(), \
                        key=lambda x: x[1][sortkey], \
                        reverse=revsort)

  print("%-4s %6s" % ("rank", "id"), end='')
  for stat in site_report_stats:
    x = stat
    if "site_" in stat:
      x = x.replace("site","s")
    if "total_" in stat:
      x = x.replace("total","t")
    if "miss_ratio" in stat:
      x = x.replace("miss_ratio","mr")
    elif "ratio" in stat:
      x = x.replace("ratio","r")

    print(" %12s" % x, end='')
  print("")

  for i,si in enumerate(sorted_sinfo[:cut]):
    outstr = ("%-4d %-6d" % (i, si[0]))
    for stat in stats:
      outstr += (" %12s" % (format_str(stat) % (si[1][stat])))
    print(outstr)

def get_cut_sites(sinfo, rev, cutkey, cut):

  sites = []
  if rev:
    for k in sinfo:
      if sinfo[k][cutkey] < cut:
        sites.append(k)
  else:
    for k in sinfo:
      if sinfo[k][cutkey] >= cut:
        sites.append(k)

  return sites

def get_sort_sites(sinfo, sortkey, revsort, cutkey, cut):

  agg_info = {}
  agg_info[cutkey]       = \
  agg_info[NUM_SITES]    = \
  agg_info[TOTAL_READS]  = \
  agg_info[TOTAL_WRITES] = \
  agg_info[READ_MISSES]  = \
  agg_info[WRITE_MISSES] = \
  agg_info[TOTAL_HITS]   = \
  agg_info[TOTAL_MISSES] = \
  agg_info[TOTAL_ACCS]   = 0

  sorted_sinfo = sorted(sinfo.items(), \
                        key=lambda x: x[1][sortkey], \
                        reverse=revsort)

  for i,si in enumerate(sorted_sinfo):
    if cut != None:
      if cutkey == NUM_SITES:
        tmp = agg_info[NUM_SITES] + 1
      else:
        tmp = (agg_info[cutkey] + si[1][cutkey])
      if tmp > cut:
        break

    site_hits   = si[1][READ_HITS]   + si[1][WRITE_HITS]
    site_misses = si[1][READ_MISSES] + si[1][WRITE_MISSES]
    site_reads  = si[1][READ_HITS]   + si[1][READ_MISSES]
    site_writes = si[1][WRITE_HITS]  + si[1][WRITE_MISSES]
    site_read_misses  = si[1][READ_MISSES]
    site_write_misses = si[1][WRITE_MISSES]

    agg_info[TOTAL_HITS]   += site_hits
    agg_info[TOTAL_MISSES] += site_misses
    agg_info[TOTAL_ACCS]   += (site_hits + site_misses)
    agg_info[TOTAL_READS]  += site_reads
    agg_info[TOTAL_WRITES] += site_writes
    agg_info[READ_MISSES]  += site_read_misses
    agg_info[WRITE_MISSES] += site_write_misses

    agg_info[NUM_SITES] += 1
    agg_info[cutkey] += si[1][cutkey]

  return ([x[0] for x in sorted_sinfo[:i]])

def prob_info(bench, cfg_name, cutstyle=BANDWIDTH_PER_BYTE, \
  feature=SITES, cxt=0, cutrat=1.28, zcutoff=0.1, pre="X", post="Y", \
  bad_fts=set([]), style=OBJECTS):

  otdict  = get_object_cut_info(bench, cfg_name, cutstyle)
  pr_info = {}

  cond = "%s|%s" % (pre, post)
  tz   = cut_str(cutstyle, cutrat)

  total_objects = otdict[ALL][AGG][0]
  total_data    = CLMB( otdict[ALL][AGG][1] )
  if total_objects == 0:
    print("no objects in otdict!")
    return None

  if feature != SITES:
    ot_all_dict = otdict[ALL][feature]
    ot_tz_dict  = otdict[tz][feature]
  else:
    ot_all_dict = otdict[ALL][feature][cxt]
    ot_tz_dict  = otdict[tz][feature][cxt]

  thermos_objects = 0
  thermos_data    = 0
  for ftkey in ot_tz_dict:
    thermos_objects += float(ot_tz_dict[ftkey][0])
    thermos_data    += float(CLMB(ot_tz_dict[ftkey][1]))

  pr_x = (thermos_objects / total_objects)
  dp_x = (thermos_data    / total_data)

  if style == OBJECTS:
    pr_info[pre] = (pr_x, thermos_objects, total_objects)
    #print("  PR(X)   : %6.4f %10d / %10d" % \
    #      (pr_x, thermos_objects, total_objects))
  elif style == DATA:
    pr_info[pre] = (dp_x, thermos_data, total_data)
    #print("  PR(X)   : %6.4f %10d / %10d" % \
    #      (dp_x, thermos_data, total_data))

  if post == "Y":

    pr_y         = 0.0
    pr_x_and_y   = 0.0
    dp_y         = 0.0
    dp_x_and_y   = 0.0

    y_objs       = 0
    x_and_y_objs = 0
    y_data       = 0
    x_and_y_data = 0
    for ftkey in ot_all_dict:
      all_ft_objects = ot_all_dict[ftkey][0]
      all_ft_data    = CLMB(ot_all_dict[ftkey][1])
      
      thm_ft_objects = 0
      thm_ft_data    = 0
      if ftkey in ot_tz_dict:
        thm_ft_objects = ot_tz_dict[ftkey][0]
        thm_ft_data    = CLMB(ot_tz_dict[ftkey][1])

      if thm_ft_objects > 1:
        x_and_y_objs += thm_ft_objects
        pr_x_and_y   += ( float(thm_ft_objects) / total_objects )

        y_objs       += all_ft_objects
        pr_y         += ( float(all_ft_objects) / total_objects )

        x_and_y_data += thm_ft_data
        dp_x_and_y   += ( float(thm_ft_data) / total_data )

        y_data       += all_ft_data
        dp_y         += ( float(all_ft_data) / total_data )

      elif thm_ft_objects == 1:
        y_objs       += ( all_ft_objects-1 )
        pr_y         += ( float(all_ft_objects-1) / total_objects )

        y_data       += ( all_ft_data-thm_ft_data )
        dp_y         += ( float(all_ft_data-thm_ft_data) / total_data )

    if y_objs == 0:
      if x_and_y_objs != 0:
        print ("pr_y should not be 0")
      else:
        pr_x_given_y = 0.0
        dp_x_given_y = 0.0
    else:
      pr_x_given_y = ((pr_x_and_y) / pr_y)
      dp_x_given_y = ((dp_x_and_y) / dp_y)

    if style == OBJECTS:
      pr_info[cond] = (pr_x_given_y, x_and_y_objs, y_objs)
      #print("  PR(X|Y) : %6.4f %10d / %10d" % \
      #      (pr_x_given_y, x_and_y_objs, y_objs))
    elif style == DATA:
      pr_info[cond] = (dp_x_given_y, x_and_y_data, y_data)
      #print("  PR(X|Y) : %6.4f %10d / %10d" % \
      #      (dp_x_given_y, x_and_y_data, y_data))

  if post == "Z":
    pr_z         = 0.0
    pr_x_and_z   = 0.0
    dp_z         = 0.0
    dp_x_and_z   = 0.0

    z_objs       = 0
    x_and_z_objs = 0
    z_data       = 0
    x_and_z_data = 0
    for ftkey in ot_all_dict:
      all_ft_objects = ot_all_dict[ftkey][0]
      all_ft_data    = CLMB(ot_all_dict[ftkey][1])
      all_ft_sizes   = ot_all_dict[ftkey][7]

      thm_ft_objects = 0
      thm_ft_data    = 0
      thm_ft_sizes   = []
      if ftkey in ot_tz_dict:
        thm_ft_objects = ot_tz_dict[ftkey][0]
        thm_ft_data    = CLMB(ot_tz_dict[ftkey][1])
        thm_ft_sizes   = ot_tz_dict[ftkey][7]

      if all_ft_data == 0:
        bad_fts.add((bench, feature, ftkey, all_ft_objects, all_ft_data))
        #print("no ft data! ft: %d %d %d" % \
        #      (ft, all_ft_objects, all_ft_data))
        continue

      #print ("ft: %d all: %16.8f" % (ftkey,all_ft_data))
      for size in thm_ft_sizes:
        dat = CLMB(size)

        if (float(thm_ft_data - dat) / all_ft_data) > zcutoff:
          x_and_z_objs += 1
          pr_x_and_z   += ( 1.0 / total_objects )

          x_and_z_data += dat
          dp_x_and_z   += ( dat / total_data )

      for size in all_ft_sizes:
        dat = CLMB(size)

        if (float(thm_ft_data - dat) / all_ft_data) > zcutoff:
          z_objs += 1
          pr_z   += ( 1.0 / total_objects )

          z_data += dat
          dp_z   += ( dat / total_data )

    if z_objs == 0:
      if x_and_z_objs != 0:
        print ("pr_z should not be 0")
      else:
        pr_x_given_z = 0.0
        dp_x_given_z = 0.0
    else:
      pr_x_given_z = ((pr_x_and_z) / pr_z)
      dp_x_given_z = ((dp_x_and_z) / dp_z)

    if style == OBJECTS:
      pr_info[cond] = (pr_x_given_z, x_and_z_objs, z_objs)
      #print("  PR(X|Z) : %6.4f %10d / %10d" % \
      #      (pr_x_given_z, x_and_z_objs, z_objs))
    elif style == DATA:
      pr_info[cond] = (dp_x_given_z, x_and_z_data, z_data)
      #print("  PR(X|Z) : %6.4f %10d / %10d" % \
      #      (dp_x_given_z, x_and_z_data, z_data))

  return pr_info

def prob_info_report(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrat=1.28, zcutoff=0.1, feature=SITES, cxt=0, pre="X", post="Y", \
  style=DATA):

  header_strs = [
    "T: total objects in the benchmark",
    "X: object is in the set",
    "Y: object from the same site is in the set",
    ("Z: at least %1.2f%% of the data from the same site is in the set" % \
     (zcutoff*100.0))
  ]

  print(header_strs[0])
  for s in header_strs[1:]:
    if pre == s[0] or post == s[0]:
      print(s)

  print("")
  print("%-20s %8s %10s %10s %8s %10s %10s" % \
        ( ("bench", ("P(%s)" % pre), ("%s" % pre), \
            "T", ("P(%s|%s)" % (pre, post)), \
            ("%s&%s" % (pre, post)), ("%s"%post)) )
       )

  cond = ("%s|%s" % (pre, post))
  avgs = []
  bad_fts = set([])
  for bench in benches:
    for cfg_name in cfgs:
      prob = prob_info(bench, cfg_name, cutstyle=cutstyle, cutrat=cutrat, \
                       zcutoff=zcutoff, feature=feature, cxt=cxt, pre=pre, \
                       post=post, bad_fts=bad_fts, style=style)
      avgs.append(prob)
      print ("%-20s " % bench, end='')
      print ("%8.4f " % prob[pre][0], end='')
      print ("%10d "  % prob[pre][1], end='')
      print ("%10d "  % prob[pre][2], end='')
      print ("%8.4f " % prob[cond][0], end='')
      print ("%10d "  % prob[cond][1], end='')
      print ("%10d "  % prob[cond][2], end='')
      print ("")
  
  print (("%-20s " % "average"), end='') 
  print ("%8.4f " % safe_geo_mean([x[pre][0] for x in avgs]), end='')
  print ("%10d "  % mean([x[pre][1] for x in avgs]), end='')
  print ("%10d "  % mean([x[pre][2] for x in avgs]), end='')
  print ("%8.4f " % safe_geo_mean([x[cond][0] for x in avgs]), end='')
  print ("%10d "  % mean([x[cond][1] for x in avgs]), end='')
  print ("%10d "  % mean([x[cond][2] for x in avgs]), end='')
  print("")

  print (("%-20s " % "median"), end='') 
  print ("%8.4f " % median([x[pre][0] for x in avgs]), end='')
  print ("%10d "  % median([x[pre][1] for x in avgs]), end='')
  print ("%10d "  % median([x[pre][2] for x in avgs]), end='')
  print ("%8.4f " % median([x[cond][0] for x in avgs]), end='')
  print ("%10d "  % median([x[cond][1] for x in avgs]), end='')
  print ("%10d "  % median([x[cond][2] for x in avgs]), end='')
  print("")

  if bad_fts:
    print("")
    print("bad features:")
    for bft in bad_fts:
      print("%-20s %12s %8d %12d %12d" % bft)

def get_feature_bucket_len(bucket, style):
  if style == "ot":
    return len(bucket.keys())
  elif style == "ft":
    return len(bucket[7])
  raise SystemExit(1)

def get_class_feature_stats(info, otdict, tz, pref, phase_cfg):

  info[(pref+"-sizes")]   = get_feature_bucket_len ( otdict[tz][SIZE], pref )
  info[(pref+"-szbckts")] = get_feature_bucket_len ( otdict[tz][SIZE_BUCKET], pref )
  info[(pref+"-tsigs")]   = get_feature_bucket_len ( otdict[tz][TYPE_SIG], pref )
  info[(pref+"-asigs")]   = get_feature_bucket_len ( otdict[tz][ACC_SIG], pref )
  for cxt in pin_cxt_lengths:
    info[(pref+("-scxt%d"%cxt))] = get_feature_bucket_len ( otdict[tz][SITES][cxt], pref )

  if phase_cfg:
    info[(pref+"-aphs")]   = get_feature_bucket_len ( otdict[tz][ALLOC_PHASE], pref )
    info[(pref+"-typesz")] = get_feature_bucket_len ( otdict[tz][TYPE_SIZE], pref )
    info[(pref+"-phsigs")] = get_feature_bucket_len ( otdict[tz][PHASE_SIG], pref )
    info[(pref+"-phsz")]   = get_feature_bucket_len ( otdict[tz][PHASE_SIZE], pref )
    for cxt in pin_cxt_lengths:
      info[(pref+("-phsite%d"%cxt))] = get_feature_bucket_len ( otdict[tz][PHASE_SITES][cxt], pref )
      info[(pref+("-phszst%d"%cxt))] = get_feature_bucket_len ( otdict[tz][PHSZ_SITES][cxt], pref )


  info[(pref+"-szs-r")]  = safediv ( info[(pref+"-sizes")]   , info["sizes"]   )
  info[(pref+"-zbs-r")]  = safediv ( info[(pref+"-szbckts")] , info["szbckts"] )
  info[(pref+"-tsig-r")] = safediv ( info[(pref+"-tsigs")]   , info["tsigs"]   )
  info[(pref+"-asig-r")] = safediv ( info[(pref+"-asigs")]   , info["asigs"]   )
  for cxt in pin_cxt_lengths:
    info[(pref+("-scxt%d-r"%cxt))] = safediv ( info[(pref+("-scxt%d"%cxt))] , info[("scxt%d"%cxt)] )

  if phase_cfg:
    info[(pref+"-aphs-r")]   = safediv ( info[(pref+"-aphs")]    , info["aphs"] )
    info[(pref+"-phsigs-r")] = safediv ( info[(pref+"-phsigs")]  , info["phsigs"] )
    info[(pref+"-tzs-r")]    = safediv ( info[(pref+"-typesz")]  , info["typesz"] )
    info[(pref+"-pzs-r")]    = safediv ( info[(pref+"-phsz")]    , info["phsz"]   )
    for cxt in pin_cxt_lengths:
      info[(pref+("-p%d-r"%cxt))] = safediv ( info[(pref+("-phsite%d"%cxt))] , info[("phsite%d"%cxt)] )
      info[(pref+("-k%d-r"%cxt))] = safediv ( info[(pref+("-phszst%d"%cxt))] , info[("phszst%d"%cxt)] )


def get_object_feature_stats(info, otdict, ftdict, tz, feature, pref, \
  cxt=None, stats=None):

  need_acc = False
  if feature != AGG:
    for stat in stats:
      if "acc" in stat[0]:
        need_acc = True

  if feature in site_features:

    info[(pref+"-objs")]    = ftdict[tz][feature][cxt][0]
    info[(pref+"-data")]    = CLMB ( ftdict[tz][feature][cxt][1] )
    info[(pref+"-reads")]   = ftdict[tz][feature][cxt][2]
    info[(pref+"-writes")]  = ftdict[tz][feature][cxt][3]
    info[(pref+"-bw")]      = MB ( (ftdict[tz][feature][cxt][4] + ftdict[tz][feature][cxt][5]) )
    info[(pref+"-life")]    = ( ftdict[tz][feature][cxt][6] / 1000 )

  else:

    info[(pref+"-objs")]    = ftdict[tz][feature][0]
    info[(pref+"-data")]    = CLMB ( ftdict[tz][feature][1] )
    info[(pref+"-reads")]   = ftdict[tz][feature][2]
    info[(pref+"-writes")]  = ftdict[tz][feature][3]
    info[(pref+"-bw")]      = MB ( (ftdict[tz][feature][4] + ftdict[tz][feature][5]) )
    info[(pref+"-life")]    = ( ftdict[tz][feature][6] / 1000 )

  info[(pref+"-rdwr")]    = safediv_neg ( info[(pref+"-reads")]  , info[(pref+"-writes")] )
  info[(pref+"-bwpb")]    = safediv_neg ( info[(pref+"-bw")]     , info[(pref+"-data")]   )
  info[(pref+"-wrMB")]    = safediv_neg ( info[(pref+"-writes")] , info[(pref+"-data")]   )
  info[(pref+"-bwPKI")]   = safediv_neg ( info[(pref+"-bw")]     , info[(pref+"-life")]   )
  info[(pref+"-obj-r")]   = safediv     ( info[(pref+"-objs")]   , info["objects"] )
  info[(pref+"-data-r")]  = safediv     ( info[(pref+"-data")]   , info["data"]    )
  info[(pref+"-bw-r")]    = safediv     ( info[(pref+"-bw")]     , info["bw"]      )
  info[(pref+"-wr-r")]    = safediv     ( info[(pref+"-writes")] , info["writes"]  )
  info[(pref+"-bwPKI-r")] = safediv     ( info[(pref+"-bwPKI")]  , info["bwPKI"]   )

  if need_acc:
    get_feature_accuracy(info, otdict, ftdict, tz, feature, pref, cxt=cxt) 


def get_feature_accuracy(info, otdict, ftdict, tz, feature, pref, cxt=None):
  ot_all_objs     = info["objects"]
  ot_all_data     = info["data"]
  ot_all_bw       = info["bw"]
  ot_all_reads    = info["reads"]
  ot_all_writes   = info["writes"]
  ot_all_life     = info["life"]

  total_pos_objs  = total_neg_objs = 0
  total_pos_data  = total_neg_data = 0
  total_pos_bw    = total_neg_bw = 0
  ft_tp_objs = ft_fp_objs = ft_tn_objs = ft_fn_objs = 0
  ft_tp_data = ft_fp_data = ft_tn_data = ft_fn_data = 0
  ft_tp_bw   = ft_fp_bw   = ft_tn_bw   = ft_fn_bw   = 0

  if feature == BENCH:
    pos_objs   = otdict[tz][AGG][0]
    pos_data   = CLMB ( otdict[tz][AGG][1] )
    pos_reads  = otdict[tz][AGG][2]
    pos_writes = otdict[tz][AGG][3]
    pos_bw     = ( MB ( otdict[tz][AGG][4] + \
                        otdict[tz][AGG][5] ) )
    pos_life   = otdict[tz][AGG][6]

    neg_objs   = ( ot_all_objs   - pos_objs   )
    neg_data   = ( ot_all_data   - pos_data   )
    neg_reads  = ( ot_all_reads  - pos_reads  )
    neg_writes = ( ot_all_writes - pos_writes )
    neg_bw     = ( ot_all_bw     - pos_bw     )
    neg_life   = ( ot_all_life   - pos_life   )

    total_pos_objs += pos_objs
    total_neg_objs += neg_objs
    total_pos_data += pos_data
    total_neg_data += neg_data
    total_pos_bw   += pos_bw
    total_neg_bw   += neg_bw

    if len(ftdict[tz][BENCH][7]) > 0:
      ft_tp_objs += pos_objs
      ft_fp_objs += neg_objs

      ft_tp_data += pos_data
      ft_fp_data += neg_data

      ft_tp_bw   += pos_bw
      ft_fp_bw   += neg_bw

    else:
      ft_tn_objs += neg_objs
      ft_fn_objs += pos_objs

      ft_tn_data += neg_data
      ft_fn_data += pos_data

      ft_tn_bw   += neg_bw
      ft_fn_bw   += pos_bw

  else:
    if not feature in site_features:
      ot_all_dict = otdict[ALL][feature]
      ot_tz_dict  = otdict[tz][feature]
      ft_tz_dict  = ftdict[tz][feature]
    else:
      ot_all_dict = otdict[ALL][feature][cxt]
      ot_tz_dict  = otdict[tz][feature][cxt]
      ft_tz_dict  = ftdict[tz][feature][cxt]
      #print("cxt: %d keys: %s" % (cxt, str(ft_tz_dict)))

    for ftkey in ot_all_dict:
      pos_objs    = \
      pos_data    = \
      pos_bw      = \
      pos_reads   = \
      pos_writes  = \
      pos_life    = 0

      if ftkey in ot_tz_dict:
        pos_objs   = ot_tz_dict[ftkey][0]
        pos_data   = CLMB ( ot_tz_dict[ftkey][1] )
        pos_reads  = ot_tz_dict[ftkey][2]
        pos_writes = ot_tz_dict[ftkey][3]
        pos_bw     = ( MB ( ot_tz_dict[ftkey][4] + \
                            ot_tz_dict[ftkey][5] ) )
        pos_life   = ot_tz_dict[ftkey][6]

      neg_objs   = ( ot_all_dict[ftkey][0]            - pos_objs   )
      neg_data   = ( (CLMB ( ot_all_dict[ftkey][1] )) - pos_data   )
      neg_reads  = ( ot_all_dict[ftkey][2]            - pos_reads  )
      neg_writes = ( ot_all_dict[ftkey][3]            - pos_writes )
      neg_bw     = ( (MB ( ot_all_dict[ftkey][4] + ot_all_dict[ftkey][5] ) ) - pos_bw )
      neg_life   = ( ot_all_dict[ftkey][6]            - pos_life   )


      total_pos_objs += pos_objs
      total_neg_objs += neg_objs
      total_pos_data += pos_data
      total_neg_data += neg_data
      total_pos_bw   += pos_bw
      total_neg_bw   += neg_bw
      #if feature == SITES:
      #  print ("  site: %8d pos: %12d neg: %12d in: %s" % \
      #         (ftkey, pos_objs, neg_objs, (ftkey in ft_tz_dict)))
      if ftkey in ft_tz_dict[7]:
        ft_tp_objs += pos_objs
        ft_fp_objs += neg_objs

        ft_tp_data += pos_data
        ft_fp_data += neg_data

        ft_tp_bw   += pos_bw
        ft_fp_bw   += neg_bw

      else:
        ft_tn_objs += neg_objs
        ft_fn_objs += pos_objs

        ft_tn_data += neg_data
        ft_fn_data += pos_data

        ft_tn_bw   += neg_bw
        ft_fn_bw   += pos_bw

#  print (tz + " " + str(str2tz(tz)))
#  if feature == SITES and str2tz(tz) == 0.64:
#    print ("rar: %8d %12d %12d %12d" % (cxt, ft_tp_objs, ft_tn_objs, ot_all_objs))
#    print ("")

  info[(pref+"-otpr")]  = safediv ( ft_tp_objs, total_pos_objs )
  info[(pref+"-otnr")]  = safediv ( ft_tn_objs, total_neg_objs )
  info[(pref+"-ofpr")]  = safediv ( ft_fp_objs, total_neg_objs )
  info[(pref+"-ofnr")]  = safediv ( ft_fn_objs, total_pos_objs )
  info[(pref+"-oacc")]  = safediv ( (ft_tp_objs + ft_tn_objs), ot_all_objs )
  info[(pref+"-onacc")] = safediv ( (ft_fp_objs + ft_fn_objs), ot_all_objs )

  info[(pref+"-dtpr")]  = safediv ( ft_tp_data, total_pos_data )
  info[(pref+"-dtnr")]  = safediv ( ft_tn_data, total_neg_data )
  info[(pref+"-dfpr")]  = safediv ( ft_fp_data, total_neg_data )
  info[(pref+"-dfnr")]  = safediv ( ft_fn_data, total_pos_data )
  info[(pref+"-dacc")]  = safediv ( (ft_tp_data + ft_tn_data), ot_all_data )
  info[(pref+"-dnacc")] = safediv ( (ft_fp_data + ft_fn_data), ot_all_data )

  info[(pref+"-bwtpr")]  = safediv ( ft_tp_bw, total_pos_bw )
  info[(pref+"-bwtnr")]  = safediv ( ft_tn_bw, total_neg_bw )
  info[(pref+"-bwfpr")]  = safediv ( ft_fp_bw, total_neg_bw )
  info[(pref+"-bwfnr")]  = safediv ( ft_fn_bw, total_pos_bw )
  info[(pref+"-bwacc")]  = safediv ( (ft_tp_bw + ft_tn_bw), ot_all_bw )
  info[(pref+"-bwnacc")] = safediv ( (ft_fp_bw + ft_fn_bw), ot_all_bw )


def feature_object_cut_info(bench, cfg_name, cutstyle, cutpct, \
  otdict=None, ftdict=None, stats=None):

  if otdict == None:
    otdict = get_object_cut_info(bench, cfg_name, cutstyle)
  if ftdict == None:
    ftdict = get_feature_cut_info(bench, cfg_name, cutstyle)

  tz = cut_str(cutstyle, cutpct)

  phase_cfg = False
  if cfg_name in ['pin_phase_lpc_9cxt', 'pin_phase2_lpc_9cxt']:
    phase_cfg = True

  info = {}
  info["objects"]    = otdict[ALL][AGG][0]
  info["data"]       = CLMB ( otdict[ALL][AGG][1] )
  info["reads"]      = otdict[ALL][AGG][2]
  info["writes"]     = otdict[ALL][AGG][3]
  info["bw"]         = MB ( (otdict[ALL][AGG][4] + otdict[ALL][AGG][5]) )
  info["life"]       = (otdict[ALL][AGG][6] / 1000)
  info["rdwr"]       = safediv_neg ( info["reads"]  , info["writes"] )
  info["bwpb"]       = safediv_neg ( info["bw"]     , info["data"]   )
  info["wrMB"]       = safediv_neg ( info["writes"] , info["data"]   )
  info["bwPKI"]      = safediv_neg ( info["bw"]     , info["life"]   )
  info["bnlife"]     = (otdict[ALL][LIFETIME] / 1000)

  info["sizes"]      = len ( otdict[ALL][SIZE].keys() )
  info["szbckts"]    = len ( otdict[ALL][SIZE_BUCKET].keys() )
  info["tsigs"]      = len ( otdict[ALL][TYPE_SIG].keys() )
  info["asigs"]      = len ( otdict[ALL][ACC_SIG].keys() )
  for cxt in pin_cxt_lengths:
    info[("scxt%d"%cxt)] = len ( otdict[ALL][SITES][cxt].keys() )

  if phase_cfg:
    info["aphs"]     = len ( otdict[ALL][ALLOC_PHASE].keys() )
    info["phsigs"]   = len ( otdict[ALL][PHASE_SIG].keys() )
    info["typesz"]   = len ( otdict[ALL][TYPE_SIZE].keys() )
    info["phsz"]     = len ( otdict[ALL][PHASE_SIZE].keys() )
    for cxt in pin_cxt_lengths:
      info[("phsite%d"%cxt)] = len ( otdict[ALL][PHASE_SITES][cxt].keys() )
      info[("phszst%d"%cxt)] = len ( otdict[ALL][PHSZ_SITES][cxt].keys() )

  get_class_feature_stats(info, otdict, tz, "ot", phase_cfg)
  get_class_feature_stats(info, ftdict, tz, "ft", phase_cfg)

  get_object_feature_stats(info, otdict, otdict, tz,  AGG,         "ot", stats=stats)
  get_object_feature_stats(info, otdict, ftdict, ALL, AGG,         "ft", stats=stats)
  get_object_feature_stats(info, otdict, ftdict, tz,  BENCH,       "bt", stats=stats)
  get_object_feature_stats(info, otdict, ftdict, tz,  SIZE,        "zt", stats=stats)
  get_object_feature_stats(info, otdict, ftdict, tz,  SIZE_BUCKET, "yt", stats=stats)
  get_object_feature_stats(info, otdict, ftdict, tz,  TYPE_SIG,    "lt", stats=stats)
  get_object_feature_stats(info, otdict, ftdict, tz,  ACC_SIG,     "at", stats=stats)
  for cxt in pin_cxt_lengths:
    get_object_feature_stats(info, otdict, ftdict, tz, SITES, ("c%d"%cxt), cxt, stats=stats)

  if phase_cfg:
    get_object_feature_stats(info, otdict, ftdict, tz, ALLOC_PHASE, "pt", stats=stats)
    get_object_feature_stats(info, otdict, ftdict, tz, PHASE_SIG,   "nt", stats=stats)
    get_object_feature_stats(info, otdict, ftdict, tz, TYPE_SIZE,   "tt", stats=stats)
    get_object_feature_stats(info, otdict, ftdict, tz, PHASE_SIZE,  "it", stats=stats)
    for cxt in pin_cxt_lengths:
      get_object_feature_stats(info, otdict, ftdict, tz, PHASE_SITES, \
                               ("p%d"%cxt), cxt, stats=stats)
      get_object_feature_stats(info, otdict, ftdict, tz, PHSZ_SITES, \
                               ("k%d"%cxt), cxt, stats=stats)

  return info

def feature_object_cut_compare(benches, cfgs, cutstyle=THERMOS, \
  cutpct=10.0, stats=num_stats):

  print("")
  header_pts = ["bench"]
  format_pts = ["%-16s"]
  for hname,hfmt,dfmt in stats:
    header_pts.append(hname) 
    format_pts.append(hfmt)

  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  avgs = {}
  for stat in stats:
    avgs[stat] = []

  for bench in benches:
    for cfg_name in cfgs:
      socinfo = feature_object_cut_info(bench, cfg_name, cutstyle, cutpct,\
                                        stats=stats)

      format_str  = " ".join( ["%-16s"] + [ x[2] for x in stats ] )
      report_info = tuple ( [bench] + [ socinfo[x[0]] for x in stats ] )
      print( format_str % report_info )

      for stat in stats:
        if socinfo[stat[0]] > -0.01:
          avgs[stat].append(socinfo[stat[0]])


  for stat in avgs:
    if stat[0] in geomean_agg_stats:
      avgs[stat] = (stat[2] % safe_geo_mean(avgs[stat]))
    elif stat[0] in no_agg_stats:
      avgs[stat] = (stat[1] % "-")
    else:
      avgs[stat] = (stat[2] % mean(avgs[stat]))

  avgs_str = "%-16s"%"average"
  for stat in stats:
    avgs_str += (" " + avgs[stat])
  print( avgs_str )
  print("")

def multi_cut_compare(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrats=def_bwpb_rats, stats=bw_dtr_stats):

  avgs = {}
  for rat in cutrats:
    avgs[rat] = {}
    for stat in stats:
      avgs[rat][stat] = []

  focinfo = {}
  for bench in benches:
    for cfg_name in cfgs:
      otdict = get_object_cut_info(bench, cfg_name, cutstyle)
      ftdict = get_feature_cut_info(bench, cfg_name, cutstyle)
      for rat in cutrats:
        key = ("%s-%s-%s"%(bench,cfg_name,str(rat)))
        focinfo[key] = feature_object_cut_info(bench, cfg_name, cutstyle, rat, \
                                               otdict, ftdict, stats=stats)
      print ("%s-%s" % (bench, cfg_name))

  print("")
  header_pts = ["ratio"]
  format_pts = ["%-12s"]
  for hname,hfmt,dfmt in stats:
    header_pts.append(hname) 
    format_pts.append(hfmt)

  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  for rat in cutrats:
    for bench in benches:
      for cfg_name in cfgs:
        key = ("%s-%s-%s"%(bench,cfg_name,str(rat)))
        for stat in stats:
          if focinfo[key][stat[0]] > -0.01:
            avgs[rat][stat].append(focinfo[key][stat[0]])

    for stat in avgs[rat]:
      if stat[0] in geomean_agg_stats:
        avgs[rat][stat] = (stat[2] % safe_geo_mean(avgs[rat][stat]))
      elif stat[0] in no_agg_stats:
        avgs[rat][stat] = (stat[1] % "-")
      else:
        avgs[rat][stat] = (stat[2] % mean(avgs[rat][stat]))

    if cutstyle == LIFETIME:
      ratstr = ("%-12d"%int(rat))
    else:
      ratstr = ("%-12s"%rat)
    for stat in stats:
      ratstr += (" " + avgs[rat][stat])
    print( ratstr )
  print("")

def get_val(vdict, style):
  if style == OBJECTS:
    return vdict[0]
  elif style == DATA:
    return (CLMB(vdict[1]))
  elif style == BANDWIDTH:
    return ((vdict[4]+vdict[5]) / (CLMB(vdict[1])))
  return None

def feature_accuracy_report(bench, cfg_name, cutstyle=BANDWIDTH_PER_BYTE,
  cutrat=1.28, feature=SIZE, style=OBJECTS, cxt=None, cutoff=50):

  otdict = get_object_cut_info(bench, cfg_name, cutstyle)
  ftdict = get_feature_cut_info(bench, cfg_name, cutstyle)
  tz     = cut_str(cutstyle, cutrat)

  if not feature in site_features:
    ot_all_dict = otdict[ALL][feature]
    ot_tz_dict  = otdict[tz][feature]
    ft_tz_dict  = ftdict[tz][feature]
  else:
    ot_all_dict = otdict[ALL][feature][cxt]
    ot_tz_dict  = otdict[tz][feature][cxt]
    ft_tz_dict  = ftdict[tz][feature][cxt]

  print("%-6s %-10s %20s %20s %20s" % \
        ("rank", "key", "ft-pos", "obj-pos", "obj-neg"))

  ft_vals = None
  if cutstyle == BANDWIDTH_PER_BYTE:
    ft_vals = sorted ( [ ( x, ( ((y[4]+y[5]) / (CLMB(y[1]))) \
                                if y[1] != 0 else (x, 0.0)), \
                              get_val(y, style) ) \
                         for x,y in ot_all_dict.items() ],
                       key=itemgetter(1), reverse=True )

  total_data = sum([x[2] for x in ft_vals])
  if style == DATA:
    total_data *= 1024

  sum_ft_data = sum_pos_data = sum_neg_data = 0
  for rank,val in enumerate(ft_vals[:cutoff]):
    ftkey = val[0]

    all_data = get_val(ot_all_dict[val[0]], style)
    pos_data = 0
    if ftkey in ot_tz_dict:
      pos_data = get_val(ot_tz_dict[val[0]], style)
    neg_data = all_data - pos_data

    ft_data = 0
    if ftkey in ft_tz_dict[7]:
      ft_data = all_data

    if style == DATA:
      ft_data  *= 1024
      pos_data *= 1024
      neg_data *= 1024

    sum_ft_data  += ft_data
    sum_pos_data += pos_data
    sum_neg_data += neg_data

    ft_rel  = float(sum_ft_data)  / total_data
    pos_rel = float(sum_pos_data) / total_data
    neg_rel = float(sum_neg_data) / total_data

    print("%-6d %-10d %12d  %1.4f %12d  %1.4f %12d  %1.4f" % \
          (rank, ftkey, ft_data, ft_rel, pos_data, pos_rel, neg_data, neg_rel))

def get_packed_bw(bench, cfg_name, cuts, cutrats, utsize, style):
  bc = ("%s-%s" % (bench, cfg_name))

  if style == OBJECTS:
    pref = "ot"
  elif style == SITES:
    pref = "st"
  elif style == BENCH:
    pref = "bt"
  else:
    print ("invalid style: %s" % style)
    raise (SystemExit(1))

  datakey = ("%s-data" % pref)
  bwkey   = ("%s-bw"   % pref)

  total_data = cuts[bc][cutrats[0]]["data"]
  total_bw   = cuts[bc][cutrats[0]]["bw"]
  target = (total_data * (utsize/100.0))
  packed_bw = (total_bw * target)
  for rat in sorted(cutrats):
    if cuts[bc][rat][datakey] <= target:
      packed_bw  = cuts[bc][rat][bwkey]
      datapt     = ( (target - cuts[bc][rat][datakey]) / \
                     (cuts[bc][rat]["data"] - cuts[bc][rat][datakey] ) )
      bwpt       = ( cuts[bc][rat]["bw"] - cuts[bc][rat][bwkey] )
      packed_bw += ( datapt * bwpt )
      break
  return (safediv(packed_bw,total_bw))

def sim_2lm_report(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrats=def_bwpb_rats, styles=[BENCH, SITES, OBJECTS], \
  utsizes=def_upper_tier_sizes):

  print("")
  print ("getting cut infos")
  cuts = {}
  for bench in benches:
    for cfg_name in cfgs:
      bc = ("%s-%s" % (bench, cfg_name))
      print (("  %s ... " % bc),end='')
      cuts[bc] = {}
      for rat in cutrats:
        socinfo = site_object_cut_info(bench, cfg_name, cutstyle, rat)
        cuts[bc][rat] = socinfo
      print ("done.")

  for utsize in utsizes:
    print ("UL bandwidth with %4.2f%% UL capacity" % utsize)

    header_pts = ["benchmark"]
    format_pts = ["%-16s"]
    for style in styles:
      header_pts.append(style.lower()) 
      format_pts.append("%9s")

    print("")
    print( ((" ".join(format_pts)) % tuple(header_pts)) )

    avgs = []
    for bench in benches:
      for cfg_name in cfgs:
        bt_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, BENCH)
        st_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, SITES)
        ot_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, OBJECTS)

        print ("%-16s %9.3f %9.3f %9.3f" % (bench, bt_packed_bw, \
               st_packed_bw, ot_packed_bw))

        avgs.append((ot_packed_bw, st_packed_bw, bt_packed_bw))

    means = [ geo_mean ( [ x[i] for x in avgs ] ) for i in range(3) ]
    print ("%-16s %9.3f %9.3f %9.3f" % tuple ( ["average"] + means ))
    print("")

def sim_2lm_bw_summary(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrats=def_bwpb_rats, styles=[BENCH, SITES, OBJECTS], \
  utsizes=def_upper_tier_sizes):

  print("")
  print ("getting cut infos")
  cuts = {}
  for bench in benches:
    for cfg_name in cfgs:
      bc = ("%s-%s" % (bench, cfg_name))
      print (("  %s ... " % bc),end='')
      cuts[bc] = {}
      for rat in cutrats:
        socinfo = site_object_cut_info(bench, cfg_name, cutstyle, rat)
        cuts[bc][rat] = socinfo
      print ("done.")

  header_pts = ["capacity"]
  format_pts = ["%-12s"]
  for style in styles:
    header_pts.append(style.lower()) 
    format_pts.append("%9s")

  print("")
  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  for utsize in utsizes:
    utsize_vals = []
    for style in styles:
      style_vals = []
      for bench in benches:
        for cfg_name in cfgs:
          packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                      utsize, style)
          style_vals.append(packed_bw)
      utsize_vals.append( geo_mean(style_vals) )
    print ("%-12s %9.3f %9.3f %9.3f" % tuple ( [utsize] + utsize_vals ))
  print("")

def object_cut_report(bench, cfg_name, objcut=THERMOS, cutpct=10.0,
  sortkey=DATA, scutnum=50, scutrat=1.0, style=DATA):

  tzkey  = thermos_str(cutpct)
  stdict = get_thermos_dict(bench, cfg_name)

  all_dict = stdict[ALL]
  cut_dict = stdict[tzkey]

  print("%s %s %3.2f\n" % (bench, cfg_name, cutpct))

  if style == OBJECTS:
    print("%-5s %8s %10s %10s %8s %8s" % \
          ("rank", "obj-rat", "obj-cut", "obj-all",
            "cut-cr", "all-cr")
         )
  elif style == DATA:
    print("%-5s %8s %10s %10s %8s %8s" % \
          ("rank", "data-rat", "data-cut", "data-all",
            "cut-cr", "all-cr")
         )
  else:
    raise SystemExit(1)

  dpos        = 0 if style == OBJECTS else 1
  total_data  = sum ( [ float(all_dict[s][dpos]) for s in all_dict ])
  cutset_data = sum ( [ float(cut_dict[s][dpos]) for s in cut_dict ] )
  if total_data == 0:
    print("no data in all_dict!")
    return None

  bad_sites = set([])
  sorted_sites = [ k for k,v in \
                   sorted( cut_dict.items(), key=lambda x: x[1][1], \
                           reverse=True)
                 ]


  cum_cut_ratio = cum_all_ratio = 0.0
  for rank,site in enumerate(sorted_sites):
    all_data = float(all_dict[site][dpos])
    cut_data = float(cut_dict[site][dpos])

    if all_data == 0:
      bad_sites.append(site)
      continue

    cut_ratio = cut_data / all_data

    cum_cut_ratio += (cut_data / cutset_data)
    cum_all_ratio += (all_data / total_data)

    print ("%-5d %8.4f %10d %10d %8.4f %8.4f" % \
           ((rank+1), cut_ratio, cut_data, all_data, \
            cum_cut_ratio, cum_all_ratio))

    if (rank+1) >= scutnum or cum_all_ratio >= scutrat:
      break

def object_bwpb_cut(objlist, cutrat, foff):
  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[(lines_idx+foff)] != 0:
      bwpb = ( (obj[(bw_idx+foff)] / (obj[(lines_idx+foff)]*CACHE_LINE_SIZE) ) )
      if bwpb > cutrat:
        cutset.append(idx)
#        if cutrat == 10.24:
#          print("i: %16d cl: %8d bw: %12d pb: %8.4f" %\
#                (idx, obj[(lines_idx+foff)], obj[(bw_idx+foff)], bwpb))
#
#  if cutrat == 10.24:
#    #print (cutset)
#    total_size = sum ( [ obj[lines_idx] for obj in objlist ] )
#    total_bw   = sum ( [ obj[bw_idx]    for obj in objlist ] )
#
#    size = sum ( [ objlist[idx][lines_idx] for idx in cutset ] )
#    bw   = sum ( [ objlist[idx][bw_idx]    for idx in cutset ] )
#
#    print("size: %12d bw: %12d zr: %8.4f br: %8.4f" %\
#          (CLMB(size), MB(bw), safediv(size,total_size), safediv(bw,total_bw)))
  return cutset

def feature_bwpb_cut(finfo, cutrat):

  cutset = []
  for feature,rec in finfo.items():
    if rec[LINE_RSS]:
      bw   = ( rec[POST_READS] + rec[POST_WRITES] )
      bwpb = ( (bw / (rec[LINE_RSS]*CACHE_LINE_SIZE) ) )
      if bwpb > cutrat:
        cutset.append(feature)
#      if cutrat == 10.24:
#        print("i: %16s cl: %8d bw: %12d pb: %8.4f" %\
#              (str(feature), rec[LINE_RSS], bw, bwpb))
#
#  if cutrat == 10.24:
##    print (cutset)
##
#    size = sum ( [ finfo[ft][LINE_RSS] for ft in cutset ] )
#    bw   = sum ( [ (finfo[ft][POST_READS]+finfo[ft][POST_WRITES]) for ft in cutset ] )
##
#    print("size: %12d bw: %12d" %\
#          (CLMB(size), MB(bw)))
  return cutset 


def object_thermos(objlist, cutpct):

  sizekey = 1
  bwkey   = 3

  vals = []
  for idx,obj in enumerate(objlist):
    if obj[sizekey] != 0:
      bwpl = (obj[bwkey] / obj[sizekey])
      vals.append((bwpl, obj[bwkey], obj[sizekey], idx))

  target = ( (sum( [ v[2] for v in vals ] ) * (cutpct / 100)))
  sorted_nzv  = sorted ( [ x for x in vals if x[0] != 0 ], \
                         key=lambda tup: tup[0], reverse=True )
  sorted_zv   = sorted ( [ x for x in vals if x[0] == 0 ], \
                         key=lambda tup: tup[2] )

  # becuase of the way we profile -- some objects and sites are never touched
  # zero size objects should not be in the thermos
  #
  #sorted_vals = sorted_nzv + sorted_zv
  sorted_vals = sorted_nzv

#  thermos = []
#  thermos_bw   = 0
#  thermos_size = 0
#  for bwpl,obj_bw,obj_size,idx in sorted_vals:
#    thermos += [idx]
#    thermos_size += obj_size
#    thermos_bw   += obj_bw
#    if (thermos_size > target):
#      break

#  if cutpct == 10.0:
#    print("")
#    print(target)

  thermos = []
  thermos_bw   = 0
  thermos_size = 0
  for bwpl,obj_bw,obj_size,idx in sorted_vals:

    over = max([((thermos_size + obj_size) - target),0])
    if over > 0:
      tmp_set  = []
      tmp_size = 0
      tmp_bw   = 0
      for obj in thermos:
        tmp_set  += [obj]
        tmp_bw   += objlist[obj][bwkey]
        tmp_size += objlist[obj][sizekey]
        if tmp_size > over:
          break

      # if the new object displaces all data in the thermos -- what would be
      # the bandwidth left in the thermos
      #
      my_bw = obj_bw
      if obj_size > target:
        my_bw = (obj_bw * (target / obj_size))

#      if cutpct==10.0:
#        print ("idx: %-4d tbw: %-12d thz: %-12d obw: %-12d osz: %-12d tmp: %-12d my_: %-12d dif: %-12d" % \
#               (idx, thermos_bw, thermos_size, obj_bw, obj_size, \
#               tmp_bw, my_bw, (thermos_bw - tmp_bw + my_bw)))

      if ((thermos_bw - tmp_bw + my_bw) > thermos_bw):
        thermos      += [idx]
        thermos_size += obj_size
        thermos_bw   += obj_bw

    else:
#      if cutpct == 10.0:
#        print ("aaa: %-4d tbw: %-12d tsz: %-12d obw: %-12d osz: %-12d" % \
#               (idx, thermos_bw, thermos_size, obj_bw, obj_size))
      thermos += [idx]
      thermos_size += obj_size
      thermos_bw   += obj_bw

  return thermos


def feature_thermos(sinfo, cutpct):

  vals = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS] != 0:
      bwpl = (rec[BANDWIDTH] / rec[LINE_RSS])
      vals.append((bwpl, rec[BANDWIDTH], rec[LINE_RSS], site))

  target = ( (sum( [ v[2] for v in vals ] ) * (cutpct / 100)))

  sorted_nzv  = sorted ( [ x for x in vals if x[1] != 0 ], \
                         key=lambda tup: tup[0], reverse=True )
  sorted_zv   = sorted ( [ x for x in vals if x[1] == 0 ], \
                         key=lambda tup: tup[2] )

  # becuase of the way we profile -- many objects and sites are never touched
  # zero size objects should not be in the thermos
  #
  #sorted_vals = sorted_nzv + sorted_zv
  sorted_vals = sorted_nzv

#  thermos = []
#  thermos_bw   = 0
#  thermos_size = 0
#  for bwpl,site_bw,site_size,sid in sorted_vals:
#    thermos      += [sid]
#    thermos_size += site_size
#    thermos_bw   += site_bw
#    if (thermos_size > target):
#      break

#  if cutpct == 10.0:
#    print("")
#    print(target)

  thermos = []
  thermos_bw = 0
  thermos_size = 0
  for bwpl,site_bw,site_size,sid in sorted_vals:

    over = max([((thermos_size + site_size) - target),0])
    if over > 0:
      tmp_set  = []
      tmp_size = 0
      tmp_bw   = 0
      for site in thermos:
        tmp_set  += [site]
        tmp_bw   += sinfo[site][BANDWIDTH]
        tmp_size += sinfo[site][LINE_RSS]
        if tmp_size > over:
          break

      # if the new site displaces all data in the thermos -- what would be the
      # bandwidth left in the thermos
      #
      my_bw = site_bw
      if site_size > target:
        my_bw = (site_bw * (target / site_size))

#      if cutpct==10.0:
#        print ("idx: %-4d thm: %-12d thz: %-12d sbw: %-12d ssz: %-12d tmp: %-12d my_: %-12d dif: %-12d" % \
#               (sid, thermos_bw, thermos_size, site_bw, site_size, \
#               tmp_bw, my_bw, (thermos_bw - tmp_bw + my_bw)))

      if ((thermos_bw - tmp_bw + my_bw) > thermos_bw):
        thermos      += [sid]
        thermos_size += site_size
        thermos_bw   += site_bw

    else:
#      if cutpct == 10.0:
#        print ("aaa: %-4d tbw: %-12d tsz: %-12d sbw: %-12d ssz: %-12d" % \
#               (sid, thermos_bw, thermos_size, site_bw, site_size))

      thermos      += [sid]
      thermos_size += site_size
      thermos_bw   += site_bw

  return thermos


def object_wrpb_cut(objlist, cutrat):
  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[lines_idx] != 0:
      obj_writes = 0 if obj[prerw_idx] == -1.0 else \
                   (obj[pcaccs_idx] / (obj[prerw_idx]+1))
      wrpb = ( (obj_writes / (obj[lines_idx]*CACHE_LINE_SIZE) ) )
      if wrpb < cutrat:
        cutset.append(idx)

  return cutset

def feature_wrpb_cut(sinfo, cutrat):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS] != 0:
      wrpb = ( (rec[PRE_WRITES] / (rec[LINE_RSS]*CACHE_LINE_SIZE) ) )
      if wrpb < cutrat:
        cutset.append(site)
  return cutset 


def object_lifetime_pct_cut(objlist, cutpct, foff, bnlife):

  cut = int((bnlife/1000)*cutpct)

  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[(lifetime_idx+foff)] > cut:
      cutset.append(idx)

  return cutset

def feature_lifetime_pct_cut(sinfo, cutpct, bnlife):

  cut = int((bnlife/1000)*cutpct)

  cutset = []
  for site,rec in sinfo.items():
    if rec[LIFETIME] > cut:
      cutset.append(site)
  return cutset 


def object_lifetime_cut(objlist, cut, foff):
  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[(lifetime_idx+foff)] < cut:
      cutset.append(idx)

  return cutset

def feature_lifetime_cut(sinfo, cut):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LIFETIME] < cut:
      cutset.append(site)
  return cutset 


def object_rdwr_cut(objlist, cutrat):
  sizekey = 1
  rdwrkey = 5

  return [ idx for idx,obj in enumerate(objlist) \
           if (obj[sizekey] != 0) and \
              ((obj[rdwrkey] < -0.01) or (obj[rdwrkey] > cutrat)) ]

def feature_rdwr_cut(sinfo, cutrat):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      rdwr = -1.0 if rec[TOTAL_WRITES] == 0 else \
             float(rec[TOTAL_READS]) / rec[TOTAL_WRITES]
      if (rdwr < -0.01) or (rdwr > cutrat):
        cutset.append(site)
  return cutset 

def object_rdwr_pack(objlist, cutrat):

  sizekey = 1
  accskey = 2
  rdwrkey = 5

  vals = []
  for idx,obj in enumerate(objlist):
    if obj[sizekey]:
      obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
      obj_reads   = (obj[accskey] - obj_writes)
      vals.append((obj_reads, obj_writes, obj[rdwrkey], obj[sizekey], idx))

  rdmostly     = sorted ( [ x for x in vals if ((x[2] < -0.01) or  (x[2] >= cutrat)) ], \
                          key=lambda tup: tup[3], reverse=True )
#  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
#                          key=lambda tup: tup[2], reverse=True )
  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
                          key=lambda tup: tup[3], reverse=True )

  cutset = [ x[4] for x in rdmostly ]
  reads  = sum ( [ x[0] for x in rdmostly ] )
  writes = sum ( [ x[1] for x in rdmostly ] )
  for obj in sorted_cands:
    next_reads  = (reads  + obj[0])
    next_writes = (writes + obj[1])
    rdwr_rat    = ( float(next_reads) / next_writes )

#    if obj[0] > 0 and obj[2] > 0:
#      print ("%12d %12d %12d %12d %3.4f %d" % \
#             (obj_reads, obj_writes, next_reads, next_writes, rdwr_rat, obj[3]))

    if rdwr_rat > cutrat:
      cutset += [obj[4]]
      reads   = next_reads
      writes  = next_writes

  return cutset

def feature_rdwr_pack(sinfo, cutrat):

  vals = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      rdwr = -1.0 if rec[TOTAL_WRITES] == 0 else \
             float(rec[TOTAL_READS]) / rec[TOTAL_WRITES]
      vals.append((rec[TOTAL_READS], rec[TOTAL_WRITES], rdwr, rec[LINE_RSS], site))

  rdmostly     = sorted ( [ x for x in vals if ((x[2] < -0.01) or  (x[2] >= cutrat)) ], \
                          key=lambda tup: tup[3], reverse=True )
#  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
#                          key=lambda tup: tup[2], reverse=True )
  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
                          key=lambda tup: tup[3], reverse=True )

  cutset = [ x[4] for x in rdmostly ]
  reads  = sum ( [ x[0] for x in rdmostly ] )
  writes = sum ( [ x[1] for x in rdmostly ] )
  for site in sorted_cands:
    next_reads  = (reads  + site[0])
    next_writes = (writes + site[1])
    rdwr_rat    = ( float(next_reads) / next_writes )
    if rdwr_rat > cutrat:
      cutset += [site[4]]
      reads   = next_reads
      writes  = next_writes

  return cutset


def get_ins_count(bench, cfg_name, it=0, copy=0):
  cfg.read_cfg(cfg_name)

  ins = 0
  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for run in runs:
    ins_count_fd = open( ("%sins_count.out" % \
                         (get_run_results_dir(bench,cfg_name, it, copy, run))
                       ) )

    next(ins_count_fd)
    for _ in range(8):
      ins += int(next(ins_count_fd).split()[1])
    ins_count_fd.close()
  return ins

def candidate_sites(bench, cfg_name, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  if aggtype == ALL_SITES:
    sinfo = site_info(bench, cfg_name)
    cands = sinfo.keys()
  elif aggtype == SORT:
    sinfo = site_info(bench, cfg_name)
    cands = get_sort_sites(sinfo, sortkey, revsort, cutkey, cut)
  elif aggtype == CUT:
    sinfo = site_info(bench, cfg_name)
    cands = get_cut_sites(sinfo, revsort, cutkey, cut)
  elif aggtype == THERMOS:
    sinfo = get_object_stats(bench, cfg_name)
    cands = site_thermos(sinfo, cut)
  else:
    print ("unknown aggtype: %s" % aggtype)
    return None

  return cands

def agg_site_info(bench, cfg_name, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  cands = candidate_sites(bench, cfg_name, aggtype=aggtype, \
           sortkey=sortkey, revsort=revsort, cutkey=cutkey, cut=cut)

  always_stats = [ NUM_SITES,    \
                   TOTAL_READS,  \
                   TOTAL_WRITES, \
                   READ_MISSES,  \
                   WRITE_MISSES, \
                   TOTAL_HITS,   \
                   TOTAL_MISSES, \
                   TOTAL_ACCS    \
                 ]

  agg_info = {}

  for stat in set(always_stats + stats):
    agg_info[stat] = 0

  for cand in cands:
    site_hits   = sinfo[cand][READ_HITS]   + sinfo[cand][WRITE_HITS]
    site_misses = sinfo[cand][READ_MISSES] + sinfo[cand][WRITE_MISSES]
    site_reads  = sinfo[cand][READ_HITS]   + sinfo[cand][READ_MISSES]
    site_writes = sinfo[cand][WRITE_HITS]  + sinfo[cand][WRITE_MISSES]
    site_read_misses  = sinfo[cand][READ_MISSES]
    site_write_misses = sinfo[cand][WRITE_MISSES]

    agg_info[TOTAL_HITS]   += site_hits
    agg_info[TOTAL_MISSES] += site_misses
    agg_info[TOTAL_ACCS]   += (site_hits + site_misses)
    agg_info[TOTAL_READS]  += site_reads
    agg_info[TOTAL_WRITES] += site_writes
    agg_info[READ_MISSES]  += site_read_misses
    agg_info[WRITE_MISSES] += site_write_misses

    agg_info[NUM_SITES] += 1
    for stat in [x for x in stats if not x in always_stats]:
      agg_info[stat] += sinfo[cand][stat]

  for stat in stats:
    if stat == SITE_HITS_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_ACCS]   == 0 else \
                       float(agg_info[TOTAL_HITS]) / agg_info[TOTAL_ACCS]
    elif stat == SITE_MISSES_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_ACCS]   == 0 else \
                       float(agg_info[TOTAL_MISSES]) / agg_info[TOTAL_ACCS]
    elif stat == SITE_RDWR_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_WRITES] == 0 else \
                       float(agg_info[TOTAL_READS]) / agg_info[TOTAL_WRITES]
    elif stat == SITE_RDWR_MISS_RATIO:
      agg_info[stat] = 0.0 if agg_info[WRITE_MISSES] == 0 else \
                       float(agg_info[READ_MISSES]) / agg_info[WRITE_MISSES]

  return agg_info

def agg_site_report(benches, configs, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  print_cfg_name = True if len(configs) > 1 else False
  if print_cfg_name:
    print("%-20s %-20s %6s" % ("bench", "config", "sites"), end='')
  else:
    print("%-20s %6s" % ("bench", "sites"), end='')

  for stat in site_report_stats:
    x = stat
    if "site_" in stat:
      x = x.replace("site","s")
    if "total_" in stat:
      x = x.replace("total","t")
    if "miss_ratio" in stat:
      x = x.replace("miss_ratio","mr")
    elif "ratio" in stat:
      x = x.replace("ratio","r")

    print(" %12s" % x, end='')
  print("")

  avg_info = {}
  avg_info[NUM_SITES] = []
  for stat in stats:
    avg_info[stat] = []

  for bench in benches:
    for cfg_name in configs:
      sinfo = agg_site_info(bench, cfg_name, stats=stats, \
              aggtype=aggtype, sortkey=sortkey, revsort=revsort,
              cutkey=cutkey, cut=cut)

      outstr = ("%-20s" % (bench))
      if print_cfg_name:
        outstr += (" %-20s" % (cfg_name))
      outstr += (" %6d" % sinfo[NUM_SITES])
      for stat in stats:
        outstr += (" %12s" % (format_str(stat) % (sinfo[stat])))
      print(outstr)

      avg_info[NUM_SITES].append(sinfo[NUM_SITES])
      for stat in stats:
        avg_info[stat].append(sinfo[stat])

  outstr = ("%-20s" % ("average"))
  if print_cfg_name:
    outstr += (" %-20s" % ("-"))

  outstr += (" %6d" % median(avg_info[NUM_SITES]))
  for stat in stats:
    if 'ratio' in stat and not stat.endswith("miss_ratio"):
      avg_stat = mean(avg_info[stat])
    else:
      avg_stat = median(avg_info[stat])
    outstr += (" %12s" % (format_str(stat) % (avg_stat)))
  print(outstr)

def parse_site_info(bench, cfg_name, it, rdict):
  cfg.read_cfg(cfg_name)

  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for copy in range(cfg.current_cfg['runcfg']['copies']):

    rdict[TOTAL_HEAP_PAGES]  = 0
    rdict[TOTAL_HEAP_LINES]  = 0
    rdict[TOTAL_HEAP_HITS]   = 0
    rdict[TOTAL_HEAP_MISSES] = 0
    rdict[TOTAL_UNKN_PAGES]  = 0
    rdict[TOTAL_UNKN_LINES]  = 0
    rdict[TOTAL_UNKN_HITS]   = 0
    rdict[TOTAL_UNKN_MISSES] = 0
    rdict[TOTAL_PAGES]       = 0
    rdict[TOTAL_LINES]       = 0
    rdict[TOTAL_HITS]        = 0
    rdict[TOTAL_MISSES]      = 0
    rdict[TOTAL_ACCS]        = 0
    rdict[HEAP_ACCS]         = 0
    rdict[UNKN_ACCS]         = 0

    err = False
    for run in runs:
      try:
        results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
        fd = open(('%sorig_ap_info.out' % results_dir), 'r')
      except:
        err = True
        continue

      for line in fd:
        pts = line.split()
        if line.startswith('HEAP'):
          rdict[TOTAL_HEAP_PAGES]  += int(pts[1])
          rdict[TOTAL_HEAP_LINES]  += int(pts[2])
          rdict[TOTAL_HEAP_HITS]   += int(pts[3])
          rdict[TOTAL_HEAP_MISSES] += int(pts[4])
        elif line.startswith('UNKN'):
          rdict[TOTAL_UNKN_PAGES]  += int(pts[1])
          rdict[TOTAL_UNKN_LINES]  += int(pts[2])
          rdict[TOTAL_UNKN_HITS]   += int(pts[3])
          rdict[TOTAL_UNKN_MISSES] += int(pts[4])
      fd.close()

    rdict[TOTAL_PAGES]        = (rdict[TOTAL_HEAP_PAGES] + rdict[TOTAL_UNKN_PAGES])
    rdict[KNOWN_PAGES_RATIO]  = 0.0 if rdict[TOTAL_PAGES] == 0 else \
                                (rdict[TOTAL_HEAP_PAGES] / rdict[TOTAL_PAGES])

    rdict[TOTAL_LINES]        = (rdict[TOTAL_HEAP_LINES] + rdict[TOTAL_UNKN_LINES])
    rdict[KNOWN_LINES_RATIO]  = 0.0 if rdict[TOTAL_LINES] == 0 else \
                                (rdict[TOTAL_HEAP_LINES] / rdict[TOTAL_LINES])

    rdict[TOTAL_HITS]         = (rdict[TOTAL_HEAP_HITS] + rdict[TOTAL_UNKN_HITS])
    rdict[KNOWN_HITS_RATIO]   = 0.0 if rdict[TOTAL_HITS] == 0 else \
                                (rdict[TOTAL_HEAP_HITS] / rdict[TOTAL_HITS])

    rdict[TOTAL_MISSES]       = (rdict[TOTAL_HEAP_MISSES] + rdict[TOTAL_UNKN_MISSES])
    rdict[KNOWN_MISSES_RATIO] = 0.0 if rdict[TOTAL_MISSES] == 0 else \
                                (rdict[TOTAL_HEAP_MISSES] / rdict[TOTAL_MISSES])

    rdict[TOTAL_ACCS]         = rdict[TOTAL_HITS] + rdict[TOTAL_MISSES]
    rdict[HEAP_ACCS]          = (rdict[TOTAL_HEAP_HITS] + rdict[TOTAL_HEAP_MISSES])
    rdict[KNOWN_ACCS_RATIO]   = 0.0 if rdict[TOTAL_ACCS] == 0 else \
                                (rdict[HEAP_ACCS] / rdict[TOTAL_ACCS])

    rdict[TOTAL_HITS_RATIO]   = 0.0 if rdict[TOTAL_ACCS] == 0 else \
                                (rdict[TOTAL_HITS] / rdict[TOTAL_ACCS])
    rdict[HEAP_HITS_RATIO]    = 0.0 if rdict[HEAP_ACCS] == 0 else \
                                (rdict[TOTAL_HEAP_HITS] / rdict[HEAP_ACCS])

    rdict[UNKN_ACCS]          = (rdict[TOTAL_UNKN_HITS] + rdict[TOTAL_UNKN_MISSES])
    rdict[UNKN_HITS_RATIO]    = 0.0 if rdict[UNKN_ACCS] == 0 else \
                                (rdict[TOTAL_UNKN_HITS] / rdict[UNKN_ACCS])

    if err:
      for key in rdict:
        rdict[key] = None
    #print(rdict)

def aggregate_results(bench, cfg_name, iters):
  avgs = dict()
  for i in range(iters):
    for key,val in results[bench][cfg_name][i].items():
      if not isinstance(val, list) and not isinstance(val, dict):
        if key in avgs:
          avgs[key] += val
        else:
          avgs[key] = val
  return avgs

def get_results_dict(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  full_parse=False):

  results = dict()
  for bench, cfg_name, i in expiter(benches, cfgs, iters):
    if not bench in results:
      results[bench] = dict()

    if not cfg_name in results[bench]:
      results[bench][cfg_name] = dict()

    results[bench][cfg_name][i] = dict()

    rdict = results[bench][cfg_name][i]
    results_dir = get_iter_results_dir(bench, cfg_name, i)
    if stat in [EXE_TIME,PAGE_RSS,RUNTIME] or full_parse:
      parse_time(bench, cfg_name, results_dir, rdict)
    elif stat in marena_stats or full_parse:
      parse_marena(bench, cfg_name, results_dir, rdict)
    elif stat in numastat_stats or full_parse:
      parse_numastat(results_dir, rdict)
    elif stat in memreserve_stats or full_parse:
      parse_memreserve(results_dir, rdict)
    elif stat in pcm_stats or full_parse:
      parse_pcm(results_dir, rdict)
    elif stat in site_info_stats or full_parse:
      parse_site_info(bench, cfg_name, i, rdict)

  return results

def get_report_strs(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  basecfg='default_noir', style=MEAN, cis=True, absolute=False):

  if cis and iters < 2:
    cis = False

  results = get_results_dict(benches, cfgs, iters, stat, False)
  report_strs = dict()

  averages = dict()
  for expcfg in cfgs:
    averages[expcfg] = []

  for bench in results.keys():
    report_strs[bench] = dict()

    if not absolute:
      basevals   = [ results[bench][basecfg][i][stat] for i in \
                     results[bench][basecfg].keys() ]
      if None in basevals:
        basemean = basemedian = basestd = None
      else:
        basemean   = mean(basevals) 
        basemedian = median(basevals) 
        basestd    = stdev(basevals) if len(basevals) > 1 else 0.0

    for expcfg in results[bench].keys():
      expvals   = [ results[bench][expcfg][i][stat] for i in \
                     results[bench][expcfg].keys() ]

      if None in expvals:
        expmean = expmedian = expstd = None
      else:
        #print (expvals)
        expmean   = mean(expvals)
        expmedian = median(expvals)
        expstd    = stdev(expvals) if len(expvals) > 1 else 0.0

      if cis:
        if absolute:
          ci = get95CI(expmean, expmean, expstd, expstd, iters)
        else:
          ci = get95CI(basemean, expmean, basestd, expstd, iters)
          ci /= expmean

      val = None
      if expmean != None:
        if not absolute:
          if style == MEAN:
            val = (float(expmean) / basemean) if basemean > 0.0 else None
          else:
            val = (float(expmedian) / basemedian) if basemedian > 0.0 else None
        else:
          val = (float(expmean)) if style == MEAN else (float(expmedian))

      if val == None:
        report_strs[bench][expcfg] = "N/A" if not cis else ("N/A", "N/A")
      else:
        if absolute:
          if "ratio" in stat:
            report_strs[bench][expcfg] = ("%4.3f" % val) if not cis else \
                                         (("%4.3f" % val), (("%4.3f" % ci)).rjust(6))
          else:
            report_strs[bench][expcfg] = ("%5.1f" % val) if not cis else \
                                         (("%5.1f" % val), (("%5.1f" % ci)).rjust(6))
        else:
          report_strs[bench][expcfg] = ("%4.3f" % val) if not cis else \
                                       (("%4.3f" % val), (("%3.2f" % ci)).rjust(6))

      if val != None:
        averages[expcfg].append(val)

  report_strs["average"] = dict()
  for expcfg in cfgs:
    val = None
    if (len(averages[expcfg]) > 0):
      if not absolute:
        val = safe_geo_mean(averages[expcfg])
      else:
        val = mean(averages[expcfg])

    if val == None:
      report_strs["average"][expcfg] = "N/A" if not cis else ("N/A", "-".rjust(6))
    else:
      if absolute:
        if "ratio" in stat:
          report_strs["average"][expcfg] = ("%4.3f" % val) if not cis else \
                                           (("%4.3f" % val), ("-".rjust(6)))
        else:
          report_strs["average"][expcfg] = ("%5.1f" % val) if not cis else \
                                           (("%5.1f" % val), ("-".rjust(6)))
      else:
        report_strs["average"][expcfg] = ("%4.3f" % val) if not cis else \
                                         (("%4.3f" % val), ("-".rjust(6)))
                                      
  return report_strs

# Gets a dict of results for the ddr_bandwidth configs
def get_ddr_bandwidth_results(benches, cfgs):
  profcfg_results = dict()
  bandwidth_profile_results = dict()
  for bench in benches:
    profcfg_results[bench] = dict()
    bandwidth_profile_results[bench] = dict()
    for cfg_name in cfgs:
      cfg.read_cfg(cfg_name)
      # Get the results for the profcfg
      profcfg_results[bench][cfg_name] = dict()
      bandwidth_profile_results[bench][cfg_name] = dict()

      profcfg_results_dir = get_results_dir(bench, cfg.current_cfg['runcfg']['profcfg'], 0)
      parse_bench(bench, cfg.current_cfg['runcfg']['profcfg'], profcfg_results_dir, profcfg_results[bench][cfg_name])
      bandwidth_profile_dir = results_dir + bench + '/' + cfg_name + '/'

      # Get the bandwidth of the baseline run
      refres = None
      idirs    = [ x+'/' for x in glob(bandwidth_profile_dir+"*") ]
      for x in idirs:
        if x.endswith('i0/'):
          refres = dict()
          parse_pcm(x, refres)
          #print("refres: avg: %5.2f max: %5.2f" % \
          #      (refres[AVG_DDR4_BANDWIDTH], refres[MAX_DDR4_BANDWIDTH]))
      for idir in idirs:
        site = tuple([int(idir.split('/')[-2].strip('i'))])
        bandwidth_profile_results[bench][cfg_name][site[0]] = dict()
        parse_pcm(idir, bandwidth_profile_results[bench][cfg_name][site[0]], refres=refres)

  return (profcfg_results, bandwidth_profile_results)


# MRJ -- this code is not useful but I'm keeping it around in case we need a
# knapsack implementation for something
#
def object_writes_knapsack(objlist, cutrat, sigfigs=5):

  sizekey = 1
  accskey = 2
  rdwrkey = 5

  vals = []
  total_accs = total_writes = 0
  for idx,obj in enumerate(objlist):
    if obj[sizekey]:
      obj_writes = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
      total_accs   += obj[accskey]
      total_writes += obj_writes
      vals.append((obj_writes, obj[sizekey], idx))

  if (len(vals)>1000000):
    sigfigs -= 1
  print (sigfigs)

  WRITES = 0
  SIZE   = 1
  IDX    = 2

  target = ( total_accs * (cutrat / 100))

  wincs = (10**sigfigs)
  cutoff = (target / total_writes)
  print( "target: %8.2f total: %d cutoff: %4.4f" % (target, total_writes, cutoff) )

  wgt_vals = [ (v[SIZE], int((float(v[WRITES]) / total_writes) * wincs), idx) \
                for v in vals ]

  wgt_vals.sort(key=lambda tup: tup[WRITES], reverse=True)

  sig_wgts = [ x for x in wgt_vals if x[WRITES] > 0 ]
  insig_wgts = wgt_vals[len(sig_wgts):]

  wgt_incs = range(int(cutoff*wincs))

  objs = {}
  for v in vals:
    objs[v[IDX]] = (v[WRITES], v[SIZE])

  m = np.zeros((len(sig_wgts)+1, len(wgt_incs)))

  for i,wgt in enumerate(sig_wgts,start=1):
    for j,inc in enumerate(wgt_incs):
      if wgt[WRITES] <= inc:
        m[i][j] = max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + objs[wgt[IDX]][SIZE])
      else:
        m[i][j] = m[i-1][j]
    if i % 1000 == 0:
      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))

  hots = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      hots.append(sig_wgts[i-1][IDX])
      j -= sig_wgts[i-1][WRITES]
    i -= 1

  for wgt in insig_wgts:
    hots.append(wgt[IDX])

  hotset_writes = 0
  for idx in hots:
    obj = objlist[idx]
    obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
    hotset_writes += obj_writes

  print ("target_writes: %3.2f" % (target        / (1024*1024)))
  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

#  m = []
#  m.append([])
#  for j in wgt_incs:
#    m[0].append(0)
#
#  for i,wgt in enumerate(sig_wgts,start=1):
#    m.append([])
#    for j,inc in enumerate(wgt_incs):
#      if wgt[WRITES] <= inc:
#        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + objs[wgt[IDX]][SIZE]))
#      else:
#        m[i].append(m[i-1][j])
#    if i % 1000 == 0:
#      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))
#
#  hots = []
#  i = len(sig_wgts)
#  j = (len(wgt_incs)-1)
#  while i > 0:
#    if m[i][j] != m[i-1][j]:
#      hots.append(sig_wgts[i-1][IDX])
#      j -= sig_wgts[i-1][WRITES]
#    i -= 1
#
#  for wgt in insig_wgts:
#    hots.append(wgt[IDX])
#
#  hotset_writes = 0
#  for idx in hots:
#    obj = objlist[idx]
#    obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
#    hotset_writes += obj_writes
#
#  print ("target_writes: %3.2f" % (target        / (1024*1024)))
#  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

  return hots

def site_writes_knapsack(sinfo, cutrat, sigfigs=5):

  vals = []
  total_accs = total_writes = 0
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      total_accs   += (rec[TOTAL_READS] + rec[TOTAL_WRITES])
      total_writes += rec[TOTAL_WRITES]
      vals.append((rec[TOTAL_WRITES], rec[LINE_RSS], site))

  if (len(vals)>1000000):
    sigfigs -= 1
  print (sigfigs)

  WRITES = 0
  SIZE   = 1
  SITE   = 2

  target = ( total_accs * (cutrat / 100))

  wincs = (10**sigfigs)
  cutoff = (target / total_writes)
  print( "target: %8.2f total: %d cutoff: %4.4f" % (target, total_writes, cutoff) )

  wgt_vals = [ (v[SIZE], int((float(v[WRITES]) / total_writes) * wincs), idx) \
                for v in vals ]

  wgt_vals.sort(key=lambda tup: tup[WRITES], reverse=True)

  sig_wgts = [ x for x in wgt_vals if x[WRITES] > 0 ]
  insig_wgts = wgt_vals[len(sig_wgts):]

  wgt_incs = range(int(cutoff*wincs))

  sites = {}
  for v in vals:
    sites[v[SITE]] = (v[WRITES], v[SIZE])

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[WRITES] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + sites[wgt[SITE]][SIZE]))
      else:
        m[i].append(m[i-1][j])
    if i % 1000 == 0:
      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))

  hots = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      hots.append(sig_wgts[i-1][SITE])
      j -= sig_wgts[i-1][WRITES]
    i -= 1

  for wgt in insig_wgts:
    hots.append(wgt[SITE])

  hotset_writes = 0
  for site in hots:
    hotset_writes += (sinfo[site][TOTAL_WRITES])

  print ("target_writes: %3.2f" % (target        / (1024*1024)))
  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

  return hots

def object_apb_cut_old(objlist, cutpct):

  accskey    = 0
  sizekey    = 1
  apbkey     = 2
  bad_objs   = 0
  total_size = 0

  vals = []
  for idx,obj in enumerate(objlist):
    if isnan(obj[apbkey]) or isinf(obj[apbkey]):
      bad_objs += 1
    else:
      vals.append((obj[apbkey], obj[sizekey], idx))

  accesses_per_byte = []
  zero_vals     = [ x for x in vals if x[0] == 0 ]
  non_zero_vals = [ x for x in vals if x[0] != 0 ]

  for apb,size,idx in non_zero_vals:
    accesses_per_byte.append((apb, idx))

  if (len(accesses_per_byte) > 0):
    accesses_per_byte.sort(key=lambda tup: tup[0], reverse=True)

  zero_vals.sort(key=lambda tup: tup[1])
  for apb,size,idx in zero_vals:
    accesses_per_byte.append((apb, idx))

  cut    = int((len(accesses_per_byte) * (cutpct / 100)))
  cutset = [ x[1] for x in accesses_per_byte[:cut] ]

  if bad_objs:
    print("bad objects: %d" % bad_objs)

  return cutset

def object_hotset(objlist, cutpct):

  accskey    = 0
  sizekey    = 1
  apbkey     = 2
  bad_objs   = 0
  total_size = 0

  vals = []
  for idx,obj in enumerate(objlist):
    if isnan(obj[apbkey]) or isinf(obj[apbkey]):
      bad_objs += 1
    else:
      vals.append((obj[apbkey], obj[sizekey], idx))

  target = ( (sum( [ v[0] for v in vals ] ) * (cutpct / 100)))

  accesses_per_byte = []
  zero_vals     = [ x for x in vals if x[0] == 0 ]
  non_zero_vals = [ x for x in vals if x[0] != 0 ]

  for apb,size,idx in non_zero_vals:
    accesses_per_byte.append((apb, idx))

  if (len(accesses_per_byte) > 0):
    accesses_per_byte.sort(key=lambda tup: tup[0], reverse=True)

  zero_vals.sort(key=lambda tup: tup[1])
  for apb,size,idx in zero_vals:
    accesses_per_byte.append((apb, idx))

  hotset = []
  for apb,idx in accesses_per_byte:
    total_size += objlist[idx][sizekey]
    hotset.append(idx)
    if total_size > target:
      break

  print("%10d %10d" % (total_size, target))
  if bad_objs:
    print("bad objects: %d" % bad_objs)

  return hotset

