import os
from shutil import *
from marenapy.paths import *
from marenapy.tools import *
from marenapy.benches.bench import *
from marenapy.utils import *
from functools import reduce
from math import sqrt, isnan, isinf
from statistics import mean, median
import numpy as np
import operator
import pickle

from statistics import mean,stdev,median

def peekline(f):
  pos = f.tell()
  line = f.readline()
  f.seek(pos)
  return line

def update_scalar_stat(stats, x):
  mean     = stats[0]
  variance = stats[1]
  minval   = stats[2]
  maxval   = stats[3]
  rawval   = stats[4]
  n        = stats[5]

  n += 1

  if (n > 1):
    variance += ( (((float(x) - mean) ** 2) / n) - (variance / (n-1)) )

  mean += ( (x - mean) / n )
  if (n == 1):
    minval = maxval = x;
  else:
    maxval = x if x > maxval else maxval
    minval = x if x < minval else minval
  
  rawval += x

  stats[0] = mean
  stats[1] = variance
  stats[2] = minval
  stats[3] = maxval
  stats[4] = rawval
  stats[5] = n

def update_ratio_stat(stats, x, num, den):
  mean     = stats[0]
  variance = stats[1]
  minval   = stats[2]
  maxval   = stats[3]
  rawnum   = stats[4]
  rawden   = stats[5]
  n        = stats[6]

  n += 1

  if (n > 1):
    variance += ( (((float(x) - mean) ** 2) / n) - (variance / (n-1)) )

  mean += ( (x - mean) / n )
  if (n == 1):
    minval = maxval = x;
  else:
    maxval = x if x > maxval else maxval
    minval = x if x < minval else minval
  
  rawnum += num
  rawden += den

  stats[0] = mean
  stats[1] = variance
  stats[2] = minval
  stats[3] = maxval
  stats[4] = rawnum
  stats[5] = rawden
  stats[6] = n

def new_int_stat():
  return [0.0, 0.0, 0, 0, 0, 0]

def new_float_stat():
  return [0.0, 0.0, 0.0, 0.0, 0, 0, 0]

def get_int_info(line):
  pts = line.split()
  return ( [ float(pts[0]), float(pts[1]) ] + \
           [ int(x) for x in pts[2:] ]        \
         )

def get_float_info(line):
  pts = line.split()
  return ( [ float(x) for x in pts ] )

def get_agg_stats(vals, objects, maxval=False, ratval=False, pp=False):

  agg_mean  = 0.0
  agg_stdev = 0.0
  agg_min   = 0
  agg_max   = 0
  agg_val   = 0

  n = 0
  for val,m in zip(vals,objects):
    if m == 0:
      continue

    prev_usq = (agg_mean ** 2)
    prev_var = (agg_stdev ** 2)

    agg_mean = ( (agg_mean * (float(n) / (n+m))) + \
                 (val[0]   * (float(m) / (n+m))) )

    cur_usq = (val[0] ** 2)
    cur_var = (val[1] ** 2)
    t1 = (n * (prev_var + prev_usq))
    t2 = (m * (cur_var + cur_usq))
    agg_var = ( (( t1 + t2 ) / (m + n)) - (agg_mean**2) )
    if (agg_var < 0.0):
      if (agg_var < -0.1):
        print("error: bad var: " + str(agg_var))
        raise (SystemExit(1))
      else:
        agg_stdev = 0.0
    else:
      agg_stdev = (agg_var ** 0.5)

    agg_min = min([agg_min, val[2]])
    agg_max = max([agg_max, val[2]])

    if maxval:
      agg_val = max([agg_val, val[4]])
    elif ratval:
      agg_val = ( (agg_val * (float(n) / (n+m))) + \
                  (val[4]  * (float(m) / (n+m))) )
    else:
      agg_val += val[4]

    n += m

  return [ agg_mean, agg_stdev, agg_min, agg_max, agg_val ]

def get_object_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sobject_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_site_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%ssite_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_alt_site_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%salt_site_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  stdict = pickle.load(cut_pkl_fd)
  return stdict

def get_bench_cut_info(bench, cfg_name, cutstyle):
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sbench_%s.pkl' % (results_dir, cutstyle.lower())), 'rb')
  btdict = pickle.load(cut_pkl_fd)
  return btdict

def create_agg_site_infos(benches, configs, do_objects=False, \
  do_cachelines=False):
  for bench in benches:
    for cfg_name in configs:
      make_agg_site_info(bench, cfg_name, do_objects=do_objects, \
                         do_cachelines=do_cachelines)

def make_agg_site_info(bench, cfg_name, it=0, do_objects=False, \
  do_cachelines=False):

  cfg.read_cfg(cfg_name)
  print("%-20s %-30s ... " % (bench, cfg_name), end="")

  curid   = 0
  clsum   = 0
  totobjs = 0

  smap    = {}
  imap    = {}
  clmap   = {}
  clstats = {}
  objlist = []

  runs    = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  err     = False
  for copy in range(cfg.current_cfg['runcfg']['copies']):

    curmap = {}
    for run in runs:
#      try:
        results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
        fd = open(('%sbt_sites.txt' % results_dir), 'r', encoding="ISO-8859-1")

        for line in fd:
          if line.startswith("site:"):
            site_num = int(line.split()[1])
            site_addrs = []
            site_symbols = []
            if site_num == 6180:
              print (line)
            for sline in fd:
              if site_num == 6180:
                print (sline)
              if sline.isspace():
                break
              if len(sline.split()) < 2:
                print("warning: bad addr: %s" % sline.strip())
                continue
              site_addrs.append(int(sline.split()[0],base=16))
              site_symbols.append(sline.split()[1])
            curmap[site_num] = (tuple(site_addrs), tuple(site_symbols))
        fd.close()

        fd = open(('%srss_api.out' % results_dir), 'r')
        next(fd)
        for line in fd:
          if line.isspace():
            break

          sinfo = [int(x) for x in line.split()]
          skey  = curmap[sinfo[0]]
          if not skey in smap:
            smap[skey] = (curid, [])
            curid += 1
          smap[skey][1].append(sinfo[1:])

        fd = open(('%sstats_info.out' % results_dir), 'r')
        for line in fd:
          if line.startswith("ID:") and line.split()[1].isdigit():
            linfo = [int(x) for x in line.split()[1:]]
            ikey  = smap[curmap[linfo[0]]][0]
            if not ikey in imap:
              imap[ikey] = []

            curinfo = []
            objects_info = [ int(x) for x in linfo[1:] ]
            curinfo.append(objects_info)

            curinfo.append(get_int_info(next(fd)))
            curinfo.append(get_int_info(next(fd)))
            curinfo.append(get_int_info(next(fd)))
            curinfo.append(get_int_info(next(fd)))
            curinfo.append(get_int_info(next(fd)))
            curinfo.append(get_float_info(next(fd)))
            curinfo.append(get_float_info(next(fd)))
            curinfo.append(get_float_info(next(fd)))
            imap[ikey].append(curinfo)

        if do_objects:
          fd = open(('%sobj_info.out' % results_dir), 'r')
          for line in fd:
            pts      = line.split()
            sid      = smap[curmap[int(pts[0])]][0]
            clss     = int(pts[1])
            objinfo  = [sid] + [clss] + [float(x) for x in pts[2:]]
            objlist.append(tuple(objinfo))

        if do_cachelines:
          fd = open(('%scache_line_info.out' % results_dir), 'r')
          fd.readline()
          if int((peekline(fd).split()[0])) == 0:
            fd.readline()

          nr_lines = {}
          for line in fd:
            pts = line.split()
            linfo = [int(x) for x in pts[1:6]]
            for sid in pts[6:]:
              if not sid.isdigit():
                continue

              clkey = smap[curmap[int(sid)]][0]
              if not clkey in clmap:
                clmap[clkey] = [0 for _ in range(len(linfo)+1)]

              for i,val in enumerate(linfo):
                clmap[clkey][i] += val

              if not clkey in nr_lines:
                nr_lines[clkey] = 0
              nr_lines[clkey] += 1

              if not clkey in clstats:
                clstats[clkey] = []
                clstats[clkey].append(new_int_stat())
                clstats[clkey].append(new_float_stat())
                clstats[clkey].append(new_float_stat())

              bandwidth = (linfo[1] + linfo[4])
              update_scalar_stat(clstats[clkey][0], bandwidth)

              hits   = (linfo[0] + linfo[2])
              misses = (linfo[1] + linfo[3])
              total  = hits+misses
              if total > 0:
                pc_hit_rate = float(hits) / total
                update_ratio_stat(clstats[clkey][1], pc_hit_rate, hits, total)

              reads  = (linfo[0] + linfo[1])
              writes = (linfo[2] + linfo[3])
              if writes > 0:
                rw_ratio = float(reads) / (writes)
                update_ratio_stat(clstats[clkey][2], rw_ratio, reads, writes)

          for clkey in nr_lines:
            if clmap[clkey][-1] < nr_lines[clkey]:
              clmap[clkey][-1] = nr_lines[clkey]

#      except:
#        print (("error parsing run-%d."%run))
#        err = True
#        break

  if not err:
#    try:
      results_dir  = get_results_dir(bench, cfg_name)
      agg_strs_fd  = open(('%sagg_site_strs.out' % results_dir), 'w')
      agg_fd       = open(('%sagg_sites.out' % results_dir), 'w')
      for site in smap:
        sid = smap[site][0]
        page_rss = max ( [ x[0] for x in smap[site][1] ] ) 
        line_rss = max ( [ x[1] for x in smap[site][1] ] ) 
        sinfo    = [ sum( [ x[i] for x in smap[site][1] ] ) \
                     for i in range(2, len(smap[site][1][0])) ]

        aggline = ("%6d %11d %11d" % (sid, page_rss, line_rss)) + \
                  " ".join([("%12d" % x) for x in sinfo])

        print (aggline, file=agg_fd)
        print (aggline, file=agg_strs_fd)
        for addr,sym in zip(site[0], site[1]):
          print (("  %-16s %s" % (hex(addr), sym)), file=agg_strs_fd)
        print ("", file=agg_strs_fd)

      print ("", file=agg_fd)
      agg_fd.close()
      agg_strs_fd.close()

      agg_stats_fd = open(('%sagg_stats.out' % results_dir), 'w')
      for site in smap:
        sid   = smap[site][0]
        stats = imap[sid]

        objects = [ x[0][0] for x in stats ]
        agg_objects = [ sum ( [ x[0][i] for x in stats ] ) for i in range(4) ]

        vals = []
        for i in range(1,9):
          vals.append( [ x[i] for x in stats ] )

        agg_pgrss       = get_agg_stats( vals[0], objects, maxval=True )
        agg_clrss       = get_agg_stats( vals[1], objects, maxval=True )
        agg_accs        = get_agg_stats( vals[2], objects )
        agg_misses      = get_agg_stats( vals[3], objects )
        agg_bandwidth   = get_agg_stats( vals[4], objects )
        agg_bw_per_line = get_agg_stats( vals[5], objects, ratval=True )
        agg_pc_hit_rate = get_agg_stats( vals[6], objects, ratval=True )
        agg_rw_ratio    = get_agg_stats( vals[7], objects, ratval=True )

        print ( ("ID: %7d %11d %11d %11d %11d" % \
                 tuple([sid] + agg_objects)), file=agg_stats_fd)
        print ( ("%11.4f %11.4f %11d %11d %11d" % \
                 tuple(agg_pgrss)), file=agg_stats_fd)
        print ( ("%11.4f %11.4f %11d %11d %11d" % \
                 tuple(agg_clrss)), file=agg_stats_fd)
        print ( ("%11.4f %11.4f %11d %11d %11d" % \
                 tuple(agg_accs)), file=agg_stats_fd)
        print ( ("%11.4f %11.4f %11d %11d %11d" % \
                 tuple(agg_misses)), file=agg_stats_fd)
        print ( ("%11.4f %11.4f %11d %11d %11d" % \
                 tuple(agg_bandwidth)), file=agg_stats_fd)
        print ( ("%11.4f %11.4f %11.4f %11.4f %11.4f" % \
                 tuple(agg_bw_per_line)), file=agg_stats_fd)
        print ( ("%11.4f %11.4f %11.4f %11.4f %11.4f" % \
                 tuple(agg_pc_hit_rate)), file=agg_stats_fd)
        print ( ("%11.4f %11.4f %11.4f %11.4f %11.4f" % \
                 tuple(agg_rw_ratio)), file=agg_stats_fd)
        print ("", file=agg_stats_fd)

      if do_objects:

        agg_obj_fd = open(('%sagg_obj_info.out' % results_dir), 'w')
        for obj in objlist:
          print ( "%7d %16d %16d %16d %9.4f %9.2f" % obj, file=agg_obj_fd)

      if do_cachelines:
        agg_lines_fd = open(('%sraw_cache_lines.out' % results_dir), 'w')
        for site in clmap:
          clinfo = clmap[site]
          print ( ("%8d %11d %11d %11d %11d %11d %11d" % \
                   tuple([site] + [clinfo[-1]] + clinfo[:-1])), \
                   file=agg_lines_fd )

        agg_clstats_fd = open(('%scache_line_stats.out' % results_dir), 'w')
        for site in clstats:
          clinfo = clstats[site]

          # convert variance to stdev
          for v in clinfo:
            v[1] = (v[1] ** 0.5)

          print ( ("ID: %7d %11d %11d %11d" % \
                   tuple([site] + \
                         [clinfo[0][-1]] + \
                         [clinfo[1][-1]] + \
                         [clinfo[2][-1]]   \
                   )), file=agg_clstats_fd)

          print ( ("%11.4f %11.4f %11d %11d %11d" % \
                   tuple(clinfo[0][:-1])),   \
                   file=agg_clstats_fd
                )

          rawnum = float(clinfo[1][-3])
          rawden = clinfo[1][-2]
          rawrat = -1.0 if rawden == 0 else rawnum / rawden
          print ( ("%11.4f %11.4f %11.4f %11.4f %11.4f" %    \
                   tuple(clinfo[1][:-3] + [rawrat])), \
                   file=agg_clstats_fd
                )

          rawnum = float(clinfo[2][-3])
          rawden = clinfo[2][-2]
          rawrat = -1.0 if rawden == 0 else rawnum / rawden
          print ( ("%11.4f %11.4f %11.4f %11.4f %11.4f" % \
                   tuple(clinfo[2][:-3] + [rawrat])), \
                   file=agg_clstats_fd
                )
          print ("", file=agg_clstats_fd)

      print ("ok.")

#    except:
#      print ("error printing agg files.")


def create_thermos_infos(benches, configs, tsizes=def_tsizes, \
  object_cut=THERMOS, site_cut=THERMOS, do_objects=True, do_sites=True, \
  do_bench=True):

  for bench in benches:
    for cfg_name in configs:
      if do_objects:
        create_object_thermos_info(bench, cfg_name, tsizes=tsizes, \
                                   cutstyle=object_cut)
      if do_sites:
        create_site_thermos_info(bench, cfg_name, tsizes=tsizes, \
                                 cutstyle=site_cut)
        create_alt_site_thermos_info(bench, cfg_name, tsizes=tsizes, \
                                     cutstyle=site_cut)
      if do_bench:
        create_bench_thermos_info(bench, cfg_name, tsizes=tsizes, \
                                  cutstyle=site_cut)

def create_object_thermos_info(bench, cfg_name, tsizes=def_tsizes, \
  cutstyle=THERMOS):

  otdict = {}
  object_thermi = []

  otzstrs = [cut_str(cutstyle, tz) for tz in tsizes]
  objlist = get_object_list(bench, cfg_name)

  cutfn = None
  if cutstyle == THERMOS:
    cutfn = object_thermos
  elif cutstyle == BANDWIDTH_PER_BYTE:
    cutfn = object_bwpb_cut
  elif cutstyle == WRITES_PER_BYTE:
    cutfn = object_wrpb_cut
  elif cutstyle == RDWR_RATIO:
    cutfn = object_rdwr_cut

  # remove objects with no bandwidth
  #
  # some objects have size, but no bandwidth -- which can easily happen
  # because of the way we profile (the objects in the cache by the time we
  # start recording their accesses)
  #
  # we also remove objects with no size in the thermos code
  #
  # We can collect the thermos with objects that generate no bandwidth, but
  # have positive size -- but that will change the results. I think this is
  # the best way to do it for comparing object / site / type clustering
  #
  objlist = [ x for x in objlist if x[3] != 0 ]
      
  print("%s-%s" % (bench, cfg_name))
  print("  doing object %s cut:" % cutstyle.lower())
  for tz,tzstr in zip(tsizes,otzstrs):
    print(("    %8.4f ... " % tz), end='')
    object_thermi.append(  (tzstr, cutfn(objlist, tz)) )
    print("done.")

  print ("  object dict ... ", end='')
  otdict[ALL] = {}
  otdict[ALL][AGG]   = [0, 0, 0, 0, 0]
  otdict[ALL][SITES] = {}
  for obj in objlist:
    site = obj[0]
    if not site in otdict[ALL][SITES]:
      otdict[ALL][SITES][site] = [0, 0, 0, 0, 0, []]

    size   = obj[1]
    bw     = obj[3]
    rdwr   = obj[5]
    writes = 0 if obj[5] < -0.01 else int((obj[2] / (obj[5]+1)))
    reads  = int((obj[2] - writes))

    otdict[ALL][AGG][0] += 1
    otdict[ALL][AGG][1] += size
    otdict[ALL][AGG][2] += bw
    otdict[ALL][AGG][3] += reads
    otdict[ALL][AGG][4] += writes

    otdict[ALL][SITES][site][0] += 1
    otdict[ALL][SITES][site][1] += size
    otdict[ALL][SITES][site][2] += bw
    otdict[ALL][SITES][site][3] += reads
    otdict[ALL][SITES][site][4] += writes
    otdict[ALL][SITES][site][5].append((size, bw))
  print ("done.")

  for tzstr,thermos in object_thermi:
    print (("  %24s ... " % tzstr), end='')
    otdict[tzstr] = {}
    otdict[tzstr][AGG]   = [0, 0, 0, 0, 0]
    otdict[tzstr][SITES] = {}
    for obj in thermos:
      site  = objlist[obj][0]
      if not site in otdict[tzstr][SITES]:
        otdict[tzstr][SITES][site] = [0, 0, 0, 0, 0, []]

      size   = objlist[obj][1]
      accs   = objlist[obj][2]
      bw     = objlist[obj][3]
      rdwr   = objlist[obj][5]
      writes = 0 if rdwr < -0.01 else int((accs / (rdwr+1)))
      reads  = int((objlist[obj][2] - writes))

      otdict[tzstr][AGG][0] += 1
      otdict[tzstr][AGG][1] += size
      otdict[tzstr][AGG][2] += bw
      otdict[tzstr][AGG][3] += reads
      otdict[tzstr][AGG][4] += writes

      otdict[tzstr][SITES][site][0] += 1
      otdict[tzstr][SITES][site][1] += size
      otdict[tzstr][SITES][site][2] += bw
      otdict[tzstr][SITES][site][3] += reads
      otdict[tzstr][SITES][site][4] += writes
      otdict[tzstr][SITES][site][5].append((size, bw))
    print ("done.")

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sobject_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(otdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

def create_site_thermos_info(bench, cfg_name, tsizes=def_tsizes, \
  cutstyle=THERMOS):

  stdict = {}
  site_thermi = []

  stzstrs = [cut_str(cutstyle, tz)   for tz in tsizes]

  otdict = get_object_cut_info(bench, cfg_name, cutstyle)
  sinfo = {}
  for site in otdict[ALL][SITES]:
    sinfo[site] = {}
    sinfo[site][TOUCHED]      = otdict[ALL][SITES][site][0]
    sinfo[site][LINE_RSS]     = otdict[ALL][SITES][site][1]
    sinfo[site][BANDWIDTH]    = otdict[ALL][SITES][site][2]
    sinfo[site][TOTAL_READS]  = otdict[ALL][SITES][site][3]
    sinfo[site][TOTAL_WRITES] = otdict[ALL][SITES][site][4]

  cutfn = None
  if cutstyle == THERMOS:
    cutfn = site_thermos
  elif cutstyle == BANDWIDTH_PER_BYTE:
    cutfn = site_bwpb_cut
  elif cutstyle == WRITES_PER_BYTE:
    cutfn = site_wrpb_cut
  elif cutstyle == RDWR_RATIO:
    cutfn = site_rdwr_cut

  # remove sites with no bandwidth
  #
  # some objects have size, but no bandwidth -- which can easily happen
  # because of the way we profile (the objects in the cache by the time we
  # start recording their accesses)
  #
  # we also remove objects with no size in the thermos code
  #
  # We can collect the thermos with objects that generate no bandwidth, but
  # have positive size -- but that will change the results. I think this is
  # the best way to do it for comparing object / site / type clustering
  #
  sinfo = { k : v for k,v in sinfo.items() if v[BANDWIDTH] != 0 }

  print("%s-%s" % (bench, cfg_name))
  print("  doing site %s cut:" % cutstyle.lower())
  for tz,tzstr in zip(tsizes,stzstrs):
    print(("    %8.3f ... " % tz), end='')
    site_thermi.append( (tzstr, cutfn(sinfo, tz)) )
    print("  done.")

  stdict[ALL]        = {}
  stdict[ALL][SITES] = {}
  stdict[ALL][AGG]   = [0, 0, 0, 0, 0]
  for site in sinfo:
    objs   = sinfo[site][TOUCHED]
    size   = sinfo[site][LINE_RSS]
    bw     = sinfo[site][BANDWIDTH]
    reads  = sinfo[site][TOTAL_READS]
    writes = sinfo[site][TOTAL_WRITES]

    stdict[ALL][AGG][0] += objs
    stdict[ALL][AGG][1] += size
    stdict[ALL][AGG][2] += bw
    stdict[ALL][AGG][3] += reads
    stdict[ALL][AGG][4] += writes
    stdict[ALL][SITES][site] = (objs, size, bw, reads, writes)

  for tzstr,thermos in site_thermi:
    stdict[tzstr] = {}
    stdict[tzstr][SITES] = {}
    stdict[tzstr][AGG]   = [0, 0, 0, 0, 0]
    for sid in thermos:
      objs   = sinfo[sid][TOUCHED]
      size   = sinfo[sid][LINE_RSS]
      bw     = sinfo[sid][BANDWIDTH]
      reads  = sinfo[sid][TOTAL_READS]
      writes = sinfo[sid][TOTAL_WRITES]

      stdict[tzstr][AGG][0] += objs
      stdict[tzstr][AGG][1] += size
      stdict[tzstr][AGG][2] += bw
      stdict[tzstr][AGG][3] += reads
      stdict[tzstr][AGG][4] += writes
      stdict[tzstr][SITES][sid] = (objs, size, bw, reads, writes)

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%ssite_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(stdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

def create_alt_site_thermos_info(bench, cfg_name, tsizes=def_tsizes,\
  cutstyle=THERMOS):

  stdict = {}
  site_thermi = []

  stzstrs = [cut_str(cutstyle, tz)   for tz in tsizes]
  sinfo   = site_info(bench, cfg_name)
  ostats  = get_object_stats(bench, cfg_name)

  cutfn = None
  if cutstyle == THERMOS:
    cutfn = site_thermos
  elif cutstyle == BANDWIDTH_PER_BYTE:
    cutfn = site_bwpb_cut
  elif cutstyle == WRITES_PER_BYTE:
    cutfn = site_wrpb_cut
  elif cutstyle == RDWR_RATIO:
    cutfn = site_rdwr_cut

  for site in sinfo:
    sinfo[site][TOTAL_READS]  = sinfo[site][READ_HITS]  + sinfo[site][READ_MISSES]
    sinfo[site][TOTAL_WRITES] = sinfo[site][WRITE_HITS] + sinfo[site][WRITE_MISSES]

  # remove sites with no bandwidth
  #
  # some objects have size, but no bandwidth -- which can easily happen
  # because of the way we profile (the objects in the cache by the time we
  # start recording their accesses)
  #
  # we also remove objects with no size in the thermos code
  #
  # We can collect the thermos with objects that generate no bandwidth, but
  # have positive size -- but that will change the results. I think this is
  # the best way to do it for comparing object / site / type clustering
  #
  sinfo   = { k : v for k,v in sinfo.items() if v[BANDWIDTH] != 0 }

  print("%s-%s" % (bench, cfg_name))
  print("  doing site %s cut:" % cutstyle.lower())
  for tz,tzstr in zip(tsizes,stzstrs):
    print(("    %8.3f ... " % tz), end='')
    site_thermi.append( (tzstr, cutfn(sinfo, tz)) )
    print("  done.")

  stdict[ALL]        = {}
  stdict[ALL][SITES] = {}
  stdict[ALL][AGG]   = [0, 0, 0, 0, 0]
  for site in sinfo:
    objs   = ostats[site][TOUCHED]
    size   = sinfo[site][LINE_RSS]
    accs   = sinfo[site][BANDWIDTH]
    reads  = sinfo[site][TOTAL_READS]
    writes = sinfo[site][TOTAL_WRITES]

    stdict[ALL][AGG][0] += objs
    stdict[ALL][AGG][1] += size
    stdict[ALL][AGG][2] += accs
    stdict[ALL][AGG][3] += reads
    stdict[ALL][AGG][4] += writes
    stdict[ALL][SITES][site] = (objs, size, accs)

  for tzstr,thermos in site_thermi:
    stdict[tzstr] = {}
    stdict[tzstr][SITES] = {}
    stdict[tzstr][AGG]   = [0, 0, 0, 0, 0]
    for sid in thermos:
      objs   = ostats[sid][TOUCHED]
      size   = sinfo[sid][LINE_RSS]
      accs   = sinfo[sid][BANDWIDTH]
      reads  = sinfo[sid][TOTAL_READS]
      writes = sinfo[sid][TOTAL_WRITES]

      stdict[tzstr][AGG][0] += objs
      stdict[tzstr][AGG][1] += size
      stdict[tzstr][AGG][2] += accs
      stdict[tzstr][AGG][3] += reads
      stdict[tzstr][AGG][4] += writes
      stdict[tzstr][SITES][sid] = (objs, size, accs)

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%salt_site_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(stdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

def create_bench_thermos_info(bench, cfg_name, tsizes=def_tsizes, \
  cutstyle=BANDWIDTH_PER_BYTE):

  btdict = {}
  bench_thermi = []

  btzstrs = [cut_str(cutstyle, tz) for tz in tsizes]

  otdict = get_object_cut_info(bench, cfg_name, cutstyle)
  sinfo = {}
  sinfo[AGG] = {}
  sinfo[AGG][TOUCHED]      = otdict[ALL][AGG][0]
  sinfo[AGG][LINE_RSS]     = otdict[ALL][AGG][1]
  sinfo[AGG][BANDWIDTH]    = otdict[ALL][AGG][2]
  sinfo[AGG][TOTAL_READS]  = otdict[ALL][AGG][3]
  sinfo[AGG][TOTAL_WRITES] = otdict[ALL][AGG][4]

  cutfn = None
  if cutstyle == THERMOS:
    cutfn = site_thermos
  elif cutstyle == BANDWIDTH_PER_BYTE:
    cutfn = site_bwpb_cut
  elif cutstyle == WRITES_PER_BYTE:
    cutfn = site_wrpb_cut
  elif cutstyle == RDWR_RATIO:
    cutfn = site_rdwr_cut

  # remove sites with no bandwidth
  #
  # some objects have size, but no bandwidth -- which can easily happen
  # because of the way we profile (the objects in the cache by the time we
  # start recording their accesses)
  #
  # we also remove objects with no size in the thermos code
  #
  # We can collect the thermos with objects that generate no bandwidth, but
  # have positive size -- but that will change the results. I think this is
  # the best way to do it for comparing object / site / type clustering
  #
  sinfo = { k : v for k,v in sinfo.items() if v[BANDWIDTH] != 0 }

  print("%s-%s" % (bench, cfg_name))
  print("  doing bench %s cut:" % cutstyle.lower())
  for tz,tzstr in zip(tsizes,btzstrs):
    print(("    %8.3f ... " % tz), end='')
    bench_thermi.append( (tzstr, cutfn(sinfo, tz)) )
    print("  done.")

  btdict[ALL]        = {}
  btdict[ALL][SITES] = {}
  btdict[ALL][AGG]   = [0, 0, 0, 0, 0]
  for site in sinfo:
    objs   = sinfo[site][TOUCHED]
    size   = sinfo[site][LINE_RSS]
    bw     = sinfo[site][BANDWIDTH]
    reads  = sinfo[site][TOTAL_READS]
    writes = sinfo[site][TOTAL_WRITES]

    btdict[ALL][AGG][0] += objs
    btdict[ALL][AGG][1] += size
    btdict[ALL][AGG][2] += bw
    btdict[ALL][AGG][3] += reads
    btdict[ALL][AGG][4] += writes

  for tzstr,thermos in bench_thermi:
    btdict[tzstr] = {}
    btdict[tzstr][SITES] = {}
    btdict[tzstr][AGG]   = [0, 0, 0, 0, 0]
    for sid in thermos:
      objs   = sinfo[sid][TOUCHED]
      size   = sinfo[sid][LINE_RSS]
      bw     = sinfo[sid][BANDWIDTH]
      reads  = sinfo[sid][TOTAL_READS]
      writes = sinfo[sid][TOTAL_WRITES]

      btdict[tzstr][AGG][0] += objs
      btdict[tzstr][AGG][1] += size
      btdict[tzstr][AGG][2] += bw
      btdict[tzstr][AGG][3] += reads
      btdict[tzstr][AGG][4] += writes

  print("pickling ... ", end='')
  results_dir = get_results_dir(bench, cfg_name)
  cut_pkl_fd = open(('%sbench_%s.pkl' % (results_dir, cutstyle.lower())), 'wb')
  pickle.dump(btdict, cut_pkl_fd)
  cut_pkl_fd.close()
  print("done.")

def site_map(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_sites.out' % results_dir), 'r')

  smap = {}
  for line in fd:
    if line.isspace():
      break

    pts  = line.split()
    site = int(pts[0])

    smap[site] = {}
    smap[site][PAGE_RSS]     = ( int(pts[1]) * PAGE_SIZE )
    smap[site][LINE_RSS]     = ( int(pts[2]) * CACHE_LINE_SIZE )
    smap[site][TOTAL_HITS]   = int(pts[3])
    smap[site][TOTAL_MISSES] = int(pts[4])
    smap[site][READ_HITS]    = int(pts[5])
    smap[site][READ_MISSES]  = int(pts[6])
    smap[site][WRITE_HITS]   = int(pts[7])
    smap[site][WRITE_MISSES] = int(pts[8])
    smap[site][WRITEBACK]    = int(pts[9])

  fd.close()
  return smap

def site_strs_map(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_site_strs.out' % results_dir), 'r')

  smap = {}
  for line in fd:
    if line.isspace():
      break

    pts  = line.split()
    site = int(pts[0])

    smap[site][PAGE_RSS]     = ( int(pts[1]) * PAGE_SIZE )
    smap[site][LINE_RSS]     = ( int(pts[2]) * CACHE_LINE_SIZE )
    smap[site][TOTAL_HITS]   = int(pts[3])
    smap[site][TOTAL_MISSES] = int(pts[4])
    smap[site][READ_HITS]    = int(pts[5])
    smap[site][READ_MISSES]  = int(pts[6])
    smap[site][WRITE_HITS]   = int(pts[7])
    smap[site][WRITE_MISSES] = int(pts[8])
    smap[site][WRITEBACK]    = int(pts[9])

    smap[site][SYMBOLS] = []
    for sline in fd:
      if sline.isspace():
        break
      addr,sym = sline.split()
      smap[site][SYMBOLS].append((addr, sym))

  fd.close()
  return smap

def obj_report_format_str(stat):
  name, style = stat
  if name in [TOUCHED, ALLOCATED]:
    return "%12d"
  elif name in int_stats:
    if style in [MEAN, STDEV]:
      return "%4.3f"
    else:
      return "%12d"
  else:
    return "%4.3f"

def get_obj_report_stat(record, stat):
  name, style = stat
  stat_val = record[name]
  if name in [TOUCHED, ALLOCATED]:
    return stat_val
  else:
    return stat_val[style_idx[style]]

def format_str(stat):
  if "ratio" in stat:
    return "%4.3f"
  else:
    return "%12d"

def get_object_list(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_obj_info.out' % results_dir), 'r')

  objlist = []
  for line in fd:
    pts = line.split()

    site     = int(pts[0])
    size     = (int(pts[1])*CACHE_LINE_SIZE)
    accs     = int(pts[2])
    bw       = int(pts[3])
    hit_rate = float(pts[4])
    rw_ratio = float(pts[5])

    objlist.append((site,size,accs,bw,hit_rate,rw_ratio))

  return objlist

def get_object_stats(bench, cfg_name):
  cfg.read_cfg(cfg_name)

  results_dir = get_results_dir(bench, cfg_name)
  fd = open(('%sagg_stats.out' % results_dir), 'r')

  obj_stats = {}
  for line in fd:
    if line.startswith("ID:"):

      pts  = line.split()
      site = int(pts[1])
      obj_stats[site] = {}

      obj_stats[site][TOUCHED]     = int(pts[2])
      obj_stats[site][ALLOCATED]   = int(pts[4])
      obj_stats[site][PAGE_RSS]    = get_int_info(next(fd))
      obj_stats[site][LINE_RSS]    = get_int_info(next(fd))
      obj_stats[site][ACCESSES]    = get_int_info(next(fd))
      obj_stats[site][MISSES]      = get_int_info(next(fd))
      obj_stats[site][BANDWIDTH]   = get_float_info(next(fd))
      obj_stats[site][BW_PER_LINE] = get_float_info(next(fd))
      obj_stats[site][PC_HIT_RATE] = get_float_info(next(fd))
      obj_stats[site][RW_RATIO]    = get_float_info(next(fd))

  fd.close()
  return obj_stats

def object_report(bench, cfg_name, report_stats=object_report_stats,
  sortkey=TOUCHED, sortstyle=RAWVAL, revsort=True, cut=50):

  obj_stats = get_object_stats(bench, cfg_name)
  if sortkey in [TOUCHED, ALLOCATED]:
    sorted_stats = sorted(obj_stats.items(), \
                          key=lambda x: x[1][sortkey], \
                          reverse=revsort)
  else:
    sorted_stats = sorted(obj_stats.items(), \
                          key=lambda x: x[1][sortkey][style_idx[sortstyle]], \
                          reverse=revsort)

  print("%-4s %-6s" % ("rank", "id"), end='')
  for stat,style in report_stats:
    x = ("%s-%s" % (stat.lower(), short_style[style].lower()))
    print(" %12s" % x, end='')
  print("")

  for i,ostats in enumerate(sorted_stats[:cut]):
    outstr = ("%-4d %-6d" % (i, ostats[0]))
    for stat in report_stats:
      outstr += (" %12s" % (obj_report_format_str(stat) % \
                (get_obj_report_stat(ostats[1], stat))))
    print(outstr)


def site_info(bench, cfg_name):

  smap = site_map(bench, cfg_name)

  total_rss    = \
  total_reads  = \
  total_writes = \
  total_hits   = \
  total_misses = \
  total_accs   = 0

  for site in smap:
    site_reads  = smap[site][READ_HITS]   + smap[site][READ_MISSES]
    site_writes = smap[site][WRITE_HITS]  + smap[site][WRITE_MISSES]
    site_hits   = smap[site][READ_HITS]   + smap[site][WRITE_HITS]
    site_misses = smap[site][READ_MISSES] + smap[site][WRITE_MISSES]
    site_accs   = site_hits               + site_misses

    total_rss    += smap[site][PAGE_RSS]
    total_reads  += site_reads
    total_writes += site_writes
    total_hits   += site_hits
    total_misses += site_misses
    total_accs   += site_accs

    smap[site][BANDWIDTH] = ( smap[site][READ_MISSES] + \
                              smap[site][WRITEBACK] ) * \
                            CACHE_LINE_SIZE

    smap[site][SITE_HITS_RATIO]      = 0.0 if site_accs == 0 else \
                                     float(site_hits)   / site_accs
    smap[site][SITE_MISSES_RATIO]    = 0.0 if site_accs == 0 else \
                                     float(site_misses) / site_accs
    smap[site][SITE_RDWR_RATIO]      = 0.0 if site_writes == 0 else \
                                     float(site_reads)  / site_writes
    smap[site][SITE_RDWR_MISS_RATIO] = 0.0 if smap[site][WRITE_MISSES] == 0 else \
                                     float(smap[site][READ_MISSES]) / smap[site][WRITE_MISSES]

  for site in smap:
    site_reads  = smap[site][READ_HITS]   + smap[site][READ_MISSES]
    site_writes = smap[site][WRITE_HITS]  + smap[site][WRITE_MISSES]
    site_hits   = smap[site][READ_HITS]   + smap[site][WRITE_HITS]
    site_misses = smap[site][READ_MISSES] + smap[site][WRITE_MISSES]
    site_accs   = site_hits               + site_misses

    smap[site][TOTAL_HITS_RATIO]     = 0.0 if total_hits == 0 else \
                                     float(site_hits)   / total_hits
    smap[site][TOTAL_MISSES_RATIO]   = 0.0 if total_misses == 0 else \
                                     float(site_misses) / total_misses
    smap[site][TOTAL_ACCS_RATIO]     = 0.0 if total_accs == 0 else \
                                     float(site_accs)   / total_accs
    smap[site][PAGE_RSS_RATIO]       = 0.0 if total_rss == 0 else \
                                     float(smap[site][PAGE_RSS]) / total_rss
 
  return smap

def site_report(bench, cfg_name, stats=site_report_stats,
  sortkey=TOTAL_MISSES, revsort=True, cut=50):

  sinfo = site_info(bench, cfg_name)
  sorted_sinfo = sorted(sinfo.items(), \
                        key=lambda x: x[1][sortkey], \
                        reverse=revsort)

  print("%-4s %6s" % ("rank", "id"), end='')
  for stat in site_report_stats:
    x = stat
    if "site_" in stat:
      x = x.replace("site","s")
    if "total_" in stat:
      x = x.replace("total","t")
    if "miss_ratio" in stat:
      x = x.replace("miss_ratio","mr")
    elif "ratio" in stat:
      x = x.replace("ratio","r")

    print(" %12s" % x, end='')
  print("")

  for i,si in enumerate(sorted_sinfo[:cut]):
    outstr = ("%-4d %-6d" % (i, si[0]))
    for stat in stats:
      outstr += (" %12s" % (format_str(stat) % (si[1][stat])))
    print(outstr)

def get_cut_sites(sinfo, rev, cutkey, cut):

  sites = []
  if rev:
    for k in sinfo:
      if sinfo[k][cutkey] < cut:
        sites.append(k)
  else:
    for k in sinfo:
      if sinfo[k][cutkey] >= cut:
        sites.append(k)

  return sites

def get_sort_sites(sinfo, sortkey, revsort, cutkey, cut):

  agg_info = {}
  agg_info[cutkey]       = \
  agg_info[NUM_SITES]    = \
  agg_info[TOTAL_READS]  = \
  agg_info[TOTAL_WRITES] = \
  agg_info[READ_MISSES]  = \
  agg_info[WRITE_MISSES] = \
  agg_info[TOTAL_HITS]   = \
  agg_info[TOTAL_MISSES] = \
  agg_info[TOTAL_ACCS]   = 0

  sorted_sinfo = sorted(sinfo.items(), \
                        key=lambda x: x[1][sortkey], \
                        reverse=revsort)

  for i,si in enumerate(sorted_sinfo):
    if cut != None:
      if cutkey == NUM_SITES:
        tmp = agg_info[NUM_SITES] + 1
      else:
        tmp = (agg_info[cutkey] + si[1][cutkey])
      if tmp > cut:
        break

    site_hits   = si[1][READ_HITS]   + si[1][WRITE_HITS]
    site_misses = si[1][READ_MISSES] + si[1][WRITE_MISSES]
    site_reads  = si[1][READ_HITS]   + si[1][READ_MISSES]
    site_writes = si[1][WRITE_HITS]  + si[1][WRITE_MISSES]
    site_read_misses  = si[1][READ_MISSES]
    site_write_misses = si[1][WRITE_MISSES]

    agg_info[TOTAL_HITS]   += site_hits
    agg_info[TOTAL_MISSES] += site_misses
    agg_info[TOTAL_ACCS]   += (site_hits + site_misses)
    agg_info[TOTAL_READS]  += site_reads
    agg_info[TOTAL_WRITES] += site_writes
    agg_info[READ_MISSES]  += site_read_misses
    agg_info[WRITE_MISSES] += site_write_misses

    agg_info[NUM_SITES] += 1
    agg_info[cutkey] += si[1][cutkey]

  return ([x[0] for x in sorted_sinfo[:i]])

def prob_info(bench, cfg_name, objcut=THERMOS, cutpct=10.0,
  pre="X", post="Y", style=OBJECTS):

  stdict  = get_thermos_dict(bench, cfg_name)
  pr_info = {}

  cond  = "%s|%s" % (pre, post)
  tzkey = thermos_str(cutpct)

  total_objects = sum ([ stdict[ALL][s][0] for s in stdict[ALL] ])
  total_data    = sum ([ stdict[ALL][s][1] for s in stdict[ALL] ])
  if total_objects == 0:
    print("no objects in stdict!")
    return None

  bad_sites = set([])

  thermos_objects = 0
  thermos_data    = 0
  for site in stdict[tzkey]:
    thermos_objects += (float(stdict[tzkey][site][0]))
    thermos_data    += (float(stdict[tzkey][site][1]))

  pr_x = (thermos_objects / total_objects)
  dp_x = (thermos_data / total_data)

  if style == OBJECTS:
    pr_info[pre] = (pr_x, thermos_objects, total_objects)
    #print("  PR(X)   : %6.4f %10d / %10d" % \
    #      (pr_x, thermos_objects, total_objects))
  elif style == DATA:
    pr_info[pre] = (dp_x, thermos_data, total_data)
    #print("  PR(X)   : %6.4f %10d / %10d" % \
    #      (dp_x, thermos_data, total_data))

  if post == "Y":

    pr_y         = 0.0
    pr_x_and_y   = 0.0
    dp_y         = 0.0
    dp_x_and_y   = 0.0

    y_objs       = 0
    x_and_y_objs = 0
    y_data       = 0
    x_and_y_data = 0
    for site in stdict[tzkey]:
      all_site_objects = stdict[ALL][site][0]
      thm_site_objects = stdict[tzkey][site][0]
      all_site_data    = stdict[ALL][site][1]
      thm_site_data    = stdict[tzkey][site][1]

      if thm_site_objects > 1:
        x_and_y_objs += thm_site_objects
        pr_x_and_y   += ( float(thm_site_objects) / total_objects)

        y_objs       += all_site_objects
        pr_y         += ( float(all_site_objects) / total_objects )

        x_and_y_data += thm_site_data
        dp_x_and_y   += ( float(thm_site_data) / total_data)

        y_data       += all_site_data
        dp_y         += ( float(all_site_data) / total_data )

      elif thm_site_objects == 1:
        y_objs       += (all_site_objects-1)
        pr_y         += ( float(all_site_objects-1) / total_objects )

        y_data       += (all_site_data-thm_site_data)
        dp_y         += ( float(all_site_data-thm_site_data) / total_data )

    if y_objs == 0:
      if x_and_y_objs != 0:
        print ("pr_y should not be 0")
      else:
        pr_x_given_y = 0.0
        dp_x_given_y = 0.0
    else:
      pr_x_given_y = ((pr_x_and_y) / pr_y)
      dp_x_given_y = ((dp_x_and_y) / dp_y)

    if style == OBJECTS:
      pr_info[cond] = (pr_x_given_y, x_and_y_objs, y_objs)
      #print("  PR(X|Y) : %6.4f %10d / %10d" % \
      #      (pr_x_given_y, x_and_y_objs, y_objs))
    elif style == DATA:
      pr_info[cond] = (dp_x_given_y, x_and_y_data, y_data)
      #print("  PR(X|Y) : %6.4f %10d / %10d" % \
      #      (dp_x_given_y, x_and_y_data, y_data))

  if post == "Z":
    pr_z         = 0.0
    pr_x_and_z   = 0.0
    dp_z         = 0.0
    dp_x_and_z   = 0.0

    z_objs       = 0
    x_and_z_objs = 0
    z_data       = 0
    x_and_z_data = 0
    cutoff       = str2tz(tzkey)
    #print(tzkey + " " + str(cutoff))
    for site in stdict[tzkey]:
      all_site_objects, all_site_data, site_objs = stdict[ALL][site]
      thm_site_objects, thm_site_data, thm_objs  = stdict[tzkey][site]

      if all_site_data == 0:
        bad_sites.add((site, all_site_objects, all_site_data))
        #print("no site data! site: %d %d %d" % \
        #      (site, all_site_objects, all_site_data))
        continue

      for dat in thm_objs:
        if (float(thm_site_data - dat) / all_site_data) > 0.1:
          x_and_z_objs += 1
          pr_x_and_z   += ( 1.0 / total_objects)

          x_and_z_data += dat
          dp_x_and_z   += ( dat / total_data)

      for dat in site_objs:
        if (float(thm_site_data - dat) / all_site_data) > 0.1:
          z_objs += 1
          pr_z   += ( 1.0 / total_objects)

          z_data += dat
          dp_z   += ( dat / total_data)

    if z_objs == 0:
      if x_and_z_objs != 0:
        print ("pr_z should not be 0")
      else:
        pr_x_given_z = 0.0
        dp_x_given_z = 0.0
    else:
      pr_x_given_z = ((pr_x_and_z) / pr_z)
      dp_x_given_z = ((dp_x_and_z) / dp_z)

    if style == OBJECTS:
      pr_info[cond] = (pr_x_given_z, x_and_z_objs, z_objs)
      #print("  PR(X|Z) : %6.4f %10d / %10d" % \
      #      (pr_x_given_z, x_and_z_objs, z_objs))
    elif style == DATA:
      pr_info[cond] = (dp_x_given_z, x_and_z_data, z_data)
      #print("  PR(X|Z) : %6.4f %10d / %10d" % \
      #      (dp_x_given_z, x_and_z_data, z_data))

  if post == "Q":
    pr_q         = 0.0
    pr_x_and_q   = 0.0
    dp_q         = 0.0
    dp_x_and_q   = 0.0

    q_objs       = 0
    x_and_q_objs = 0
    q_data       = 0
    x_and_q_data = 0
    cutoff       = str2tz(tzkey)
    #print(tzkey + " " + str(cutoff))
    for site in stdict[tzkey]:
      all_site_objects, all_site_data, site_objs = stdict[ALL][site]
      thm_site_objects, thm_site_data, thm_objs  = stdict[tzkey][site]

      if all_site_data == 0:
        bad_sites.add((site, all_site_objects, all_site_data))
        #print("no site data! site: %d %d %d" % \
        #      (site, all_site_objects, all_site_data))
        continue

      for dat in thm_objs:
        if (float(thm_site_data - dat) / all_site_data) > cutoff:
          x_and_q_objs += 1
          pr_x_and_q   += ( 1.0 / total_objects)

          x_and_q_data += dat
          dp_x_and_q   += ( dat / total_objects)

      for dat in site_objs:
        if (float(thm_site_data - dat) / all_site_data) > cutoff:
          q_objs += 1
          pr_q   += ( 1.0 / total_objects)

          q_data += dat
          dp_q   += ( dat / total_objects)

    if q_objs == 0:
      if x_and_q_objs != 0:
        print ("pr_q should not be 0")
      else:
        pr_x_given_q = 0.0
        dp_x_given_q = 0.0
    else:
      pr_x_given_q = ((pr_x_and_q) / pr_q)
      dp_x_given_q = ((dp_x_and_q) / dp_q)

    if style == OBJECTS:
      pr_info[cond] = (pr_x_given_q, x_and_q_objs, q_objs)
      #print("  PR(X|Q) : %6.4f %10d / %10d" % \
      #      (pr_x_given_q, x_and_q_objs, q_objs))
    elif style == DATA:
      pr_info[cond] = (dp_x_given_q, x_and_q_data, q_data)
      #print("  PR(X|Q) : %6.4f %10d / %10d" % \
      #      (dp_x_given_q, x_and_q_data, q_data))

  if bad_sites:
    bad_sites = set(bad_sites)
    print("bad sites:")
    for site in bad_sites:
      print("%6d %10d %10d" % site)

  return pr_info

def prob_info_report(benches, cfgs, objcut=THERMOS, cutpct=10.0, \
  pre="X", post="Y", style=OBJECTS):

  header_strs = [
    "T: total objects in the benchmark",
    "X: object is in the set",
    "Y: object from the same site is in the set",
    "Z: at least 10% of the data from the same site is in the set",
    ("Q: at least %1.2f%% of the data from the same site is in the set" % cutpct)
  ]

  print(header_strs[0])
  for s in header_strs[1:]:
    if pre == s[0] or post == s[0]:
      print(s)

  print("")
  print("%-20s %8s %10s %10s %8s %10s %10s" % \
        ( ("bench", ("P(%s)" % pre), ("%s" % pre), \
            "T", ("P(%s|%s)" % (pre, post)), \
            ("%s|%s" % (pre, post)), "T") )
       )

  cond = ("%s|%s" % (pre, post))
  for bench in benches:
    for cfg_name in cfgs:
      prob = prob_info(bench, cfg_name, pre=pre, post=post, \
                       cutpct=cutpct, style=style)

      print ("%-20s " % bench, end='')
      print ("%8.4f " % prob[pre][0], end='')
      print ("%10d "  % prob[pre][1], end='')
      print ("%10d "  % prob[pre][2], end='')
      print ("%8.4f " % prob[cond][0], end='')
      print ("%10d "  % prob[cond][1], end='')
      print ("%10d "  % prob[cond][2], end='')
      print ("")
  print("")

def site_object_cut_info(bench, cfg_name, cutstyle, cutpct):

  otdict = get_object_cut_info(bench, cfg_name, cutstyle)
  stdict = get_site_cut_info(bench, cfg_name, cutstyle)
  atdict = get_alt_site_cut_info(bench, cfg_name, cutstyle)
  btdict = get_bench_cut_info(bench, cfg_name, cutstyle)

  tz = cut_str(cutstyle, cutpct)

  info = {}
  info["sites"]     = len ( otdict[ALL][SITES].keys() )
  info["objects"]   = otdict[ALL][AGG][0]
  info["data"]      = MB ( otdict[ALL][AGG][1] )
  info["bw"]        = MB ( otdict[ALL][AGG][2] )
  info["reads"]     = MB ( otdict[ALL][AGG][3] )
  info["writes"]    = MB ( otdict[ALL][AGG][4] )

  info["aa-sites"]  = len ( atdict[ALL][SITES].keys() )
  info["aa-objs"]   = atdict[ALL][AGG][0]
  info["aa-data"]   = MB ( atdict[ALL][AGG][1] )
  info["aa-bw"]     = MB ( atdict[ALL][AGG][2] )
  info["aa-reads"]  = MB ( atdict[ALL][AGG][3] )
  info["aa-writes"] = MB ( atdict[ALL][AGG][4] )
 
  # object thermos -- using object data
  info["ot-sites"]  = len ( otdict[tz][SITES].keys() )
  info["ot-objs"]   = otdict[tz][AGG][0]
  info["ot-data"]   = MB ( otdict[tz][AGG][1] )
  info["ot-bw"]     = MB ( otdict[tz][AGG][2] )
  info["ot-reads"]  = MB ( otdict[tz][AGG][3] )
  info["ot-writes"] = MB ( otdict[tz][AGG][4] )

  # site thermos -- using object data
  info["st-sites"]  = len ( stdict[tz][SITES].keys() )
  info["st-objs"]   = stdict[tz][AGG][0]
  info["st-data"]   = MB ( stdict[tz][AGG][1] )
  info["st-bw"]     = MB ( stdict[tz][AGG][2] )
  info["st-reads"]  = MB ( stdict[tz][AGG][3] )
  info["st-writes"] = MB ( stdict[tz][AGG][4] )

  # bench thermos -- using object data
  info["bt-objs"]   = btdict[tz][AGG][0]
  info["bt-data"]   = MB ( btdict[tz][AGG][1] )
  info["bt-bw"]     = MB ( btdict[tz][AGG][2] )
  info["bt-reads"]  = MB ( btdict[tz][AGG][3] )
  info["bt-writes"] = MB ( btdict[tz][AGG][4] )

  # site thermos -- using site data -- report with site stats
  info["at-sites"]  = len ( atdict[tz][SITES].keys() )
  info["at-objs"]   = atdict[tz][AGG][0]
  info["at-data"]   = MB ( atdict[tz][AGG][1] )
  info["at-bw"]     = MB ( atdict[tz][AGG][2] )
  info["at-reads"]  = MB ( atdict[tz][AGG][3] )
  info["at-writes"] = MB ( atdict[tz][AGG][4] )

  # site thermos -- using site data -- report with object stats
  vtobjs = 0
  vtdata = vtbw = vtreads = vtwrites = 0.0
  for site in atdict[tz][SITES]:
    if not site in otdict[ALL][SITES]:
      print ("warning no objects for site: %d" % site)
      continue
    vtobjs   += otdict[ALL][SITES][site][0]
    vtdata   += MB( otdict[ALL][SITES][site][1] )
    vtbw     += MB( otdict[ALL][SITES][site][2] )
    vtreads  += MB( otdict[ALL][SITES][site][3] )
    vtwrites += MB( otdict[ALL][SITES][site][4] )

  info["vt-sites"]  = len ( atdict[tz][SITES].keys() )
  info["vt-objs"]   = vtobjs
  info["vt-data"]   = vtdata
  info["vt-bw"]     = vtbw
  info["vt-reads"]  = vtreads
  info["vt-writes"] = vtwrites

  info["rdwr"]    = float(info["reads"]) / info["writes"] \
                    if info["writes"] > 0.0 else -1.0
  info["aa-rdwr"] = float(info["aa-reads"]) / info["aa-writes"] \
                    if info["aa-writes"] > 0.0 else -1.0
  info["ot-rdwr"] = float(info["ot-reads"]) / info["ot-writes"] \
                    if info["ot-writes"] > 0.0 else -1.0
  info["st-rdwr"] = float(info["st-reads"]) / info["st-writes"] \
                    if info["st-writes"] > 0.0 else -1.0
  info["at-rdwr"] = float(info["at-reads"]) / info["at-writes"] \
                    if info["at-writes"] > 0.0 else -1.0
  info["vt-rdwr"] = float(info["vt-reads"]) / info["vt-writes"] \
                    if info["vt-writes"] > 0.0 else -1.0

  info["wrpb"]    = float(info["writes"]) / info["data"] \
                    if info["data"] > 0.0 else -1.0
  info["aa-wrpb"] = float(info["aa-writes"]) / info["aa-data"] \
                    if info["aa-data"] > 0.0 else -1.0
  info["ot-wrpb"] = float(info["ot-writes"]) / info["ot-data"] \
                    if info["ot-data"] > 0.0 else -1.0
  info["st-wrpb"] = float(info["st-writes"]) / info["st-data"] \
                    if info["st-data"] > 0.0 else -1.0
  info["at-wrpb"] = float(info["at-writes"]) / info["at-data"] \
                    if info["at-data"] > 0.0 else -1.0
  info["vt-wrpb"] = float(info["vt-writes"]) / info["vt-data"] \
                    if info["vt-data"] > 0.0 else -1.0

  ot_all_objs     = otdict[ALL][AGG][0]
  ot_all_data     = otdict[ALL][AGG][1]
  ot_all_bw       = otdict[ALL][AGG][2]
  ot_all_reads    = otdict[ALL][AGG][3]
  ot_all_writes   = otdict[ALL][AGG][4]

  total_pos_objs = total_neg_objs = 0
  total_pos_data = total_neg_data = 0
  st_tp_objs = st_fp_objs = st_tn_objs = st_fn_objs = 0
  st_tp_data = st_fp_data = st_tn_data = st_fn_data = 0
  for site in otdict[ALL][SITES]:

    pos_objs = pos_data = pos_bw = pos_reads = pos_writes = 0
    if site in otdict[tz][SITES]:
      pos_objs   = otdict[tz][SITES][site][0]
      pos_data   = otdict[tz][SITES][site][1]
      pos_bw     = otdict[tz][SITES][site][2]
      pos_reads  = otdict[tz][SITES][site][3]
      pos_writes = otdict[tz][SITES][site][4]

    neg_objs   = ( otdict[ALL][SITES][site][0] - pos_objs   )
    neg_data   = ( otdict[ALL][SITES][site][1] - pos_data   )
    neg_bw     = ( otdict[ALL][SITES][site][2] - pos_bw     )
    neg_reads  = ( otdict[ALL][SITES][site][3] - pos_reads  )
    neg_writes = ( otdict[ALL][SITES][site][4] - pos_writes )

    total_pos_objs += pos_objs
    total_neg_objs += neg_objs
    total_pos_data += pos_data
    total_neg_data += neg_data
    if site in stdict[tz][SITES]:
      st_tp_objs += pos_objs
      st_fp_objs += neg_objs

      st_tp_data += pos_data
      st_fp_data += neg_data

    else:
      st_tn_objs += neg_objs
      st_fn_objs += pos_objs

      st_tn_data += neg_data
      st_fn_data += pos_data

  info["st-otpr"]  = safediv ( st_tp_objs, total_pos_objs )
  info["st-otnr"]  = safediv ( st_tn_objs, total_neg_objs )
  info["st-ofpr"]  = safediv ( st_fp_objs, total_neg_objs )
  info["st-ofnr"]  = safediv ( st_fn_objs, total_pos_objs )
  info["st-oacc"]  = safediv ( (st_tp_objs + st_tn_objs), ot_all_objs )
  info["st-onacc"] = safediv ( (st_fp_objs + st_fn_objs), ot_all_objs )

  info["st-dtpr"]  = safediv ( st_tp_data, total_pos_data )
  info["st-dtnr"]  = safediv ( st_tn_data, total_neg_data )
  info["st-dfpr"]  = safediv ( st_fp_data, total_neg_data )
  info["st-dfnr"]  = safediv ( st_fn_data, total_pos_data )
  info["st-dacc"]  = safediv ( (st_tp_data + st_tn_data), ot_all_data )
  info["st-dnacc"] = safediv ( (st_fp_data + st_fn_data), ot_all_data )


  total_pos_objs = total_neg_objs = 0
  total_pos_data = total_neg_data = 0
  bt_tp_objs = bt_fp_objs = bt_tn_objs = bt_fn_objs = 0
  bt_tp_data = bt_fp_data = bt_tn_data = bt_fn_data = 0
  for site in otdict[ALL][SITES]:

    pos_objs = pos_data = pos_bw = pos_reads = pos_writes = 0
    if site in otdict[tz][SITES]:
      pos_objs   = otdict[tz][SITES][site][0]
      pos_data   = otdict[tz][SITES][site][1]
      pos_bw     = otdict[tz][SITES][site][2]
      pos_reads  = otdict[tz][SITES][site][3]
      pos_writes = otdict[tz][SITES][site][4]

    neg_objs   = ( otdict[ALL][SITES][site][0] - pos_objs   )
    neg_data   = ( otdict[ALL][SITES][site][1] - pos_data   )
    neg_bw     = ( otdict[ALL][SITES][site][2] - pos_bw     )
    neg_reads  = ( otdict[ALL][SITES][site][3] - pos_reads  )
    neg_writes = ( otdict[ALL][SITES][site][4] - pos_writes )

    total_pos_objs += pos_objs
    total_neg_objs += neg_objs
    total_pos_data += pos_data
    total_neg_data += neg_data

    if btdict[tz][AGG][0] > 0:
      bt_tp_objs += pos_objs
      bt_fp_objs += neg_objs

      bt_tp_data += pos_data
      bt_fp_data += neg_data

    else:
      bt_tn_objs += neg_objs
      bt_fn_objs += pos_objs

      bt_tn_data += neg_data
      bt_fn_data += pos_data


  info["bt-otpr"]  = safediv ( bt_tp_objs, total_pos_objs )
  info["bt-otnr"]  = safediv ( bt_tn_objs, total_neg_objs )
  info["bt-ofpr"]  = safediv ( bt_fp_objs, total_neg_objs )
  info["bt-ofnr"]  = safediv ( bt_fn_objs, total_pos_objs )
  info["bt-oacc"]  = safediv ( (bt_tp_objs + bt_tn_objs), ot_all_objs )
  info["bt-dnacc"] = safediv ( (bt_fp_objs + bt_fn_objs), ot_all_objs )

  info["bt-dtpr"]  = safediv ( bt_tp_data, total_pos_data )
  info["bt-dtnr"]  = safediv ( bt_tn_data, total_neg_data )
  info["bt-dfpr"]  = safediv ( bt_fp_data, total_neg_data )
  info["bt-dfnr"]  = safediv ( bt_fn_data, total_pos_data )
  info["bt-dacc"]  = safediv ( (bt_tp_data + bt_tn_data), ot_all_data )
  info["bt-dnacc"] = safediv ( (bt_fp_data + bt_fn_data), ot_all_data )

  info["ot-bwpb"]  = safediv ( info["ot-bw"]   , info["ot-data"] )
  info["st-bwpb"]  = safediv ( info["st-bw"]   , info["st-data"] )
  info["bt-bwpb"]  = safediv ( info["bt-bw"]   , info["bt-data"] )
  info["st-bwpbr"] = safediv ( info["st-bwpb"] , info["ot-bwpb"] )
  info["bt-bwpbr"] = safediv ( info["bt-bwpb"] , info["ot-bwpb"] )

  info["ot-nbw"]  = info["ot-bw"]
  info["st-nbw"]  = info["st-bw"]
  info["at-nbw"]  = info["at-bw"]
  info["vt-nbw"]  = info["vt-bw"]

  # should only be used with object_thermos and site_thermos cuts
  cutrat  = (cutpct / 100.0)
  ot_over = max ( [ (info["ot-data"] - (info["data"]*cutrat)), 0 ] )
  if ot_over > 0:
    scale = ( (info["data"]*cutrat) / info["ot-data"] )
    info["ot-nbw"]  = (info["ot-bw"] * scale)

  st_over = max ( [ (info["st-data"] - (info["data"]*cutrat)), 0 ] )
  if st_over > 0:
    scale = ( (info["data"]*cutrat) / info["st-data"] )
    info["st-nbw"]  = (info["st-bw"] * scale)

  at_over = max ( [ (info["at-data"] - (info["aa-data"]*cutrat)), 0 ] )
  if at_over > 0:
    scale = ( (info["aa-data"]*cutrat) / info["at-data"] )
    info["at-nbw"]  = (info["at-bw"] * scale)

  vt_over = max ( [ (info["vt-data"] - (info["data"]*cutrat)), 0 ] )
  if vt_over > 0:
    scale = ( (info["data"]*cutrat) / info["vt-data"] )
    info["vt-nbw"]  = (info["vt-bw"] * scale)

  info["ot-site-r"] = safediv ( info["ot-sites"]  , info["sites"]   )
  info["ot-obj-r"]  = safediv ( info["ot-objs"]   , info["objects"] )
  info["ot-data-r"] = safediv ( info["ot-data"]   , info["data"]    )
  info["ot-nbw-r"]  = safediv ( info["ot-nbw"]    , info["bw"]      )
  info["ot-bw-r"]   = safediv ( info["ot-bw"]     , info["bw"]      )
  info["ot-wr-r"]   = safediv ( info["ot-writes"] , info["writes"]  )

  info["st-site-r"] = safediv ( info["st-sites"]  , info["sites"]   )
  info["st-obj-r"]  = safediv ( info["st-objs"]   , info["objects"] )
  info["st-data-r"] = safediv ( info["st-data"]   , info["data"]    )
  info["st-nbw-r"]  = safediv ( info["st-nbw"]    , info["bw"]      )
  info["st-bw-r"]   = safediv ( info["st-bw"]     , info["bw"]      )
  info["st-wr-r"]   = safediv ( info["st-writes"] , info["writes"]  )

  info["bt-obj-r"]  = safediv ( info["bt-objs"]   , info["objects"] )
  info["bt-data-r"] = safediv ( info["bt-data"]   , info["data"]    )
  info["bt-bw-r"]   = safediv ( info["bt-bw"]     , info["bw"]      )
  info["bt-wr-r"]   = safediv ( info["bt-writes"] , info["writes"]  )

  info["at-site-r"] = safediv ( info["at-sites"]  , info["aa-sites"] )
  info["at-obj-r"]  = safediv ( info["at-objs"]   , info["aa-objs"]  )
  info["at-data-r"] = safediv ( info["at-data"]   , info["aa-data"]  )
  info["at-nbw-r"]  = safediv ( info["at-nbw"]    , info["aa-bw"]    )
  info["at-bw-r"]   = safediv ( info["at-bw"]     , info["aa-bw"]    )
  info["at-wr-r"]   = safediv ( info["at-writes"] , info["writes"]  )

  info["vt-site-r"] = safediv ( info["vt-sites"]  , info["sites"]   )
  info["vt-obj-r"]  = safediv ( info["vt-objs"]   , info["objects"] )
  info["vt-data-r"] = safediv ( info["vt-data"]   , info["data"]    )
  info["vt-nbw-r"]  = safediv ( info["vt-nbw"]    , info["bw"]      )
  info["vt-bw-r"]   = safediv ( info["vt-bw"]     , info["bw"]      )
  info["vt-wr-r"]   = safediv ( info["vt-writes"] , info["writes"]  )

  return info

def site_object_cut_compare(benches, cfgs, cutstyle=THERMOS, \
  cutpct=10.0, stats=ot_stats):

  print("")
  header_pts = ["bench"]
  format_pts = ["%-16s"]
  for hname,hfmt,dfmt in stats:
    header_pts.append(hname) 
    format_pts.append(hfmt)

  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  avgs = {}
  for stat in stats:
    avgs[stat] = []

  for bench in benches:
    for cfg_name in cfgs:
      socinfo = site_object_cut_info(bench, cfg_name, cutstyle, cutpct)

      format_str  = " ".join( ["%-16s"] + [ x[2] for x in stats ] )
      report_info = tuple ( [bench] + [ socinfo[x[0]] for x in stats ] )
      print( format_str % report_info )

      for stat in stats:
        if socinfo[stat[0]] > -0.01:
          avgs[stat].append(socinfo[stat[0]])


  for stat in avgs:
    if stat[0] in geomean_agg_stats:
      avgs[stat] = (stat[2] % safe_geo_mean(avgs[stat]))
    elif stat[0] in no_agg_stats:
      avgs[stat] = (stat[1] % "-")
    else:
      avgs[stat] = (stat[2] % mean(avgs[stat]))

  avgs_str = "%-16s"%"average"
  for stat in stats:
    avgs_str += (" " + avgs[stat])
  print( avgs_str )
  print("")

def multi_cut_compare(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrats=def_bwpb_rats, stats=bw_dtr_stats):

  print("")
  header_pts = ["ratio"]
  format_pts = ["%-12s"]
  for hname,hfmt,dfmt in stats:
    header_pts.append(hname) 
    format_pts.append(hfmt)

  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  avgs = {}
  for rat in cutrats:
    avgs[rat] = {}
    for stat in stats:
      avgs[rat][stat] = []

  for rat in cutrats:
    for bench in benches:
      for cfg_name in cfgs:
        socinfo = site_object_cut_info(bench, cfg_name, cutstyle, rat)

        #format_str  = " ".join( ["%-16s"] + [ x[2] for x in stats ] )
        #report_info = tuple ( [bench] + [ socinfo[x[0]] for x in stats ] )
        #print( format_str % report_info )

        for stat in stats:
          if socinfo[stat[0]] > -0.01:
            avgs[rat][stat].append(socinfo[stat[0]])

    for stat in avgs[rat]:
      if stat[0] in geomean_agg_stats:
        avgs[rat][stat] = (stat[2] % safe_geo_mean(avgs[rat][stat]))
      elif stat[0] in no_agg_stats:
        avgs[rat][stat] = (stat[1] % "-")
      else:
        avgs[rat][stat] = (stat[2] % mean(avgs[rat][stat]))

    ratstr = ("%-12s"%rat)
    for stat in stats:
      ratstr += (" " + avgs[rat][stat])
    print( ratstr )
  print("")

def get_packed_bw(bench, cfg_name, cuts, cutrats, utsize, style):
  bc = ("%s-%s" % (bench, cfg_name))

  if style == OBJECTS:
    pref = "ot"
  elif style == SITES:
    pref = "st"
  elif style == BENCH:
    pref = "bt"
  else:
    print ("invalid style: %s" % style)
    raise (SystemExit(1))

  datakey = ("%s-data" % pref)
  bwkey   = ("%s-bw"   % pref)

  total_data = cuts[bc][cutrats[0]]["data"]
  total_bw   = cuts[bc][cutrats[0]]["bw"]
  target = (total_data * (utsize/100.0))
  packed_bw = (total_bw * target)
  for rat in sorted(cutrats):
    if cuts[bc][rat][datakey] <= target:
      packed_bw  = cuts[bc][rat][bwkey]
      datapt     = ( (target - cuts[bc][rat][datakey]) / \
                     (cuts[bc][rat]["data"] - cuts[bc][rat][datakey] ) )
      bwpt       = ( cuts[bc][rat]["bw"] - cuts[bc][rat][bwkey] )
      packed_bw += ( datapt * bwpt )
      break
  return (safediv(packed_bw,total_bw))

def sim_2lm_report(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrats=def_bwpb_rats, styles=[BENCH, SITES, OBJECTS], \
  utsizes=def_upper_tier_sizes):

  print("")
  print ("getting cut infos")
  cuts = {}
  for bench in benches:
    for cfg_name in cfgs:
      bc = ("%s-%s" % (bench, cfg_name))
      print (("  %s ... " % bc),end='')
      cuts[bc] = {}
      for rat in cutrats:
        socinfo = site_object_cut_info(bench, cfg_name, cutstyle, rat)
        cuts[bc][rat] = socinfo
      print ("done.")

  for utsize in utsizes:
    print ("UL bandwidth with %4.2f%% UL capacity" % utsize)

    header_pts = ["benchmark"]
    format_pts = ["%-16s"]
    for style in styles:
      header_pts.append(style.lower()) 
      format_pts.append("%9s")

    print("")
    print( ((" ".join(format_pts)) % tuple(header_pts)) )

    avgs = []
    for bench in benches:
      for cfg_name in cfgs:
        bt_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, BENCH)
        st_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, SITES)
        ot_packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                       utsize, OBJECTS)

        print ("%-16s %9.3f %9.3f %9.3f" % (bench, bt_packed_bw, \
               st_packed_bw, ot_packed_bw))

        avgs.append((ot_packed_bw, st_packed_bw, bt_packed_bw))

    means = [ geo_mean ( [ x[i] for x in avgs ] ) for i in range(3) ]
    print ("%-16s %9.3f %9.3f %9.3f" % tuple ( ["average"] + means ))
    print("")

def sim_2lm_bw_summary(benches, cfgs, cutstyle=BANDWIDTH_PER_BYTE, \
  cutrats=def_bwpb_rats, styles=[BENCH, SITES, OBJECTS], \
  utsizes=def_upper_tier_sizes):

  print("")
  print ("getting cut infos")
  cuts = {}
  for bench in benches:
    for cfg_name in cfgs:
      bc = ("%s-%s" % (bench, cfg_name))
      print (("  %s ... " % bc),end='')
      cuts[bc] = {}
      for rat in cutrats:
        socinfo = site_object_cut_info(bench, cfg_name, cutstyle, rat)
        cuts[bc][rat] = socinfo
      print ("done.")

  header_pts = ["capacity"]
  format_pts = ["%-12s"]
  for style in styles:
    header_pts.append(style.lower()) 
    format_pts.append("%9s")

  print("")
  print( ((" ".join(format_pts)) % tuple(header_pts)) )

  for utsize in utsizes:
    utsize_vals = []
    for style in styles:
      style_vals = []
      for bench in benches:
        for cfg_name in cfgs:
          packed_bw = get_packed_bw(bench, cfg_name, cuts, cutrats, \
                      utsize, style)
          style_vals.append(packed_bw)
      utsize_vals.append( geo_mean(style_vals) )
    print ("%-12s %9.3f %9.3f %9.3f" % tuple ( [utsize] + utsize_vals ))
  print("")

def object_cut_report(bench, cfg_name, objcut=THERMOS, cutpct=10.0,
  sortkey=DATA, scutnum=50, scutrat=1.0, style=DATA):

  tzkey  = thermos_str(cutpct)
  stdict = get_thermos_dict(bench, cfg_name)

  all_dict = stdict[ALL]
  cut_dict = stdict[tzkey]

  print("%s %s %3.2f\n" % (bench, cfg_name, cutpct))

  if style == OBJECTS:
    print("%-5s %8s %10s %10s %8s %8s" % \
          ("rank", "obj-rat", "obj-cut", "obj-all",
            "cut-cr", "all-cr")
         )
  elif style == DATA:
    print("%-5s %8s %10s %10s %8s %8s" % \
          ("rank", "data-rat", "data-cut", "data-all",
            "cut-cr", "all-cr")
         )
  else:
    raise SystemExit(1)

  dpos        = 0 if style == OBJECTS else 1
  total_data  = sum ( [ float(all_dict[s][dpos]) for s in all_dict ])
  cutset_data = sum ( [ float(cut_dict[s][dpos]) for s in cut_dict ] )
  if total_data == 0:
    print("no data in all_dict!")
    return None

  bad_sites = set([])
  sorted_sites = [ k for k,v in \
                   sorted( cut_dict.items(), key=lambda x: x[1][1], \
                           reverse=True)
                 ]


  cum_cut_ratio = cum_all_ratio = 0.0
  for rank,site in enumerate(sorted_sites):
    all_data = float(all_dict[site][dpos])
    cut_data = float(cut_dict[site][dpos])

    if all_data == 0:
      bad_sites.append(site)
      continue

    cut_ratio = cut_data / all_data

    cum_cut_ratio += (cut_data / cutset_data)
    cum_all_ratio += (all_data / total_data)

    print ("%-5d %8.4f %10d %10d %8.4f %8.4f" % \
           ((rank+1), cut_ratio, cut_data, all_data, \
            cum_cut_ratio, cum_all_ratio))

    if (rank+1) >= scutnum or cum_all_ratio >= scutrat:
      break
 

def object_thermos(objlist, cutpct):

  sizekey = 1
  bwkey   = 3

  vals = []
  for idx,obj in enumerate(objlist):
    if obj[sizekey] != 0:
      bwpl = (obj[bwkey] / obj[sizekey])
      vals.append((bwpl, obj[bwkey], obj[sizekey], idx))

  target = ( (sum( [ v[2] for v in vals ] ) * (cutpct / 100)))
  sorted_nzv  = sorted ( [ x for x in vals if x[0] != 0 ], \
                         key=lambda tup: tup[0], reverse=True )
  sorted_zv   = sorted ( [ x for x in vals if x[0] == 0 ], \
                         key=lambda tup: tup[2] )

  # becuase of the way we profile -- some objects and sites are never touched
  # zero size objects should not be in the thermos
  #
  #sorted_vals = sorted_nzv + sorted_zv
  sorted_vals = sorted_nzv

#  thermos = []
#  thermos_bw   = 0
#  thermos_size = 0
#  for bwpl,obj_bw,obj_size,idx in sorted_vals:
#    thermos += [idx]
#    thermos_size += obj_size
#    thermos_bw   += obj_bw
#    if (thermos_size > target):
#      break

#  if cutpct == 10.0:
#    print("")
#    print(target)

  thermos = []
  thermos_bw   = 0
  thermos_size = 0
  for bwpl,obj_bw,obj_size,idx in sorted_vals:

    over = max([((thermos_size + obj_size) - target),0])
    if over > 0:
      tmp_set  = []
      tmp_size = 0
      tmp_bw   = 0
      for obj in thermos:
        tmp_set  += [obj]
        tmp_bw   += objlist[obj][bwkey]
        tmp_size += objlist[obj][sizekey]
        if tmp_size > over:
          break

      # if the new object displaces all data in the thermos -- what would be
      # the bandwidth left in the thermos
      #
      my_bw = obj_bw
      if obj_size > target:
        my_bw = (obj_bw * (target / obj_size))

#      if cutpct==10.0:
#        print ("idx: %-4d tbw: %-12d thz: %-12d obw: %-12d osz: %-12d tmp: %-12d my_: %-12d dif: %-12d" % \
#               (idx, thermos_bw, thermos_size, obj_bw, obj_size, \
#               tmp_bw, my_bw, (thermos_bw - tmp_bw + my_bw)))

      if ((thermos_bw - tmp_bw + my_bw) > thermos_bw):
        thermos      += [idx]
        thermos_size += obj_size
        thermos_bw   += obj_bw

    else:
#      if cutpct == 10.0:
#        print ("aaa: %-4d tbw: %-12d tsz: %-12d obw: %-12d osz: %-12d" % \
#               (idx, thermos_bw, thermos_size, obj_bw, obj_size))
      thermos += [idx]
      thermos_size += obj_size
      thermos_bw   += obj_bw

  return thermos


def site_thermos(sinfo, cutpct):

  vals = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS] != 0:
      bwpl = (rec[BANDWIDTH] / rec[LINE_RSS])
      vals.append((bwpl, rec[BANDWIDTH], rec[LINE_RSS], site))

  target = ( (sum( [ v[2] for v in vals ] ) * (cutpct / 100)))

  sorted_nzv  = sorted ( [ x for x in vals if x[1] != 0 ], \
                         key=lambda tup: tup[0], reverse=True )
  sorted_zv   = sorted ( [ x for x in vals if x[1] == 0 ], \
                         key=lambda tup: tup[2] )

  # becuase of the way we profile -- many objects and sites are never touched
  # zero size objects should not be in the thermos
  #
  #sorted_vals = sorted_nzv + sorted_zv
  sorted_vals = sorted_nzv

#  thermos = []
#  thermos_bw   = 0
#  thermos_size = 0
#  for bwpl,site_bw,site_size,sid in sorted_vals:
#    thermos      += [sid]
#    thermos_size += site_size
#    thermos_bw   += site_bw
#    if (thermos_size > target):
#      break

#  if cutpct == 10.0:
#    print("")
#    print(target)

  thermos = []
  thermos_bw = 0
  thermos_size = 0
  for bwpl,site_bw,site_size,sid in sorted_vals:

    over = max([((thermos_size + site_size) - target),0])
    if over > 0:
      tmp_set  = []
      tmp_size = 0
      tmp_bw   = 0
      for site in thermos:
        tmp_set  += [site]
        tmp_bw   += sinfo[site][BANDWIDTH]
        tmp_size += sinfo[site][LINE_RSS]
        if tmp_size > over:
          break

      # if the new site displaces all data in the thermos -- what would be the
      # bandwidth left in the thermos
      #
      my_bw = site_bw
      if site_size > target:
        my_bw = (site_bw * (target / site_size))

#      if cutpct==10.0:
#        print ("idx: %-4d thm: %-12d thz: %-12d sbw: %-12d ssz: %-12d tmp: %-12d my_: %-12d dif: %-12d" % \
#               (sid, thermos_bw, thermos_size, site_bw, site_size, \
#               tmp_bw, my_bw, (thermos_bw - tmp_bw + my_bw)))

      if ((thermos_bw - tmp_bw + my_bw) > thermos_bw):
        thermos      += [sid]
        thermos_size += site_size
        thermos_bw   += site_bw

    else:
#      if cutpct == 10.0:
#        print ("aaa: %-4d tbw: %-12d tsz: %-12d sbw: %-12d ssz: %-12d" % \
#               (sid, thermos_bw, thermos_size, site_bw, site_size))

      thermos      += [sid]
      thermos_size += site_size
      thermos_bw   += site_bw

  return thermos


def object_bwpb_cut(objlist, cutrat):
  sizekey = 1
  bwkey   = 3

  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[sizekey] != 0:
      bwpb = ( (obj[bwkey] / obj[sizekey] ) )
      if bwpb > cutrat:
        cutset.append(idx)

  return cutset

def site_bwpb_cut(sinfo, cutrat):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      bwpb = ( (rec[BANDWIDTH] / rec[LINE_RSS] ) )
      if bwpb > cutrat:
        cutset.append(site)
  return cutset 

def object_wrpb_cut(objlist, cutrat):
  sizekey = 1
  accskey = 2
  rdwrkey = 5

  cutset = []
  for idx,obj in enumerate(objlist):
    if obj[sizekey] != 0:
      obj_writes = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
      wrpb = ( (obj_writes / obj[sizekey] ) )
      if wrpb < cutrat:
        cutset.append(idx)

  return cutset

def site_wrpb_cut(sinfo, cutrat):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS] != 0:
      wrpb = ( (rec[TOTAL_WRITES] / rec[LINE_RSS] ) )
      if wrpb < cutrat:
        cutset.append(site)
  return cutset 


def object_rdwr_cut(objlist, cutrat):
  sizekey = 1
  rdwrkey = 5

  return [ idx for idx,obj in enumerate(objlist) \
           if (obj[sizekey] != 0) and \
              ((obj[rdwrkey] < -0.01) or (obj[rdwrkey] > cutrat)) ]

def site_rdwr_cut(sinfo, cutrat):

  cutset = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      rdwr = -1.0 if rec[TOTAL_WRITES] == 0 else \
             float(rec[TOTAL_READS]) / rec[TOTAL_WRITES]
      if (rdwr < -0.01) or (rdwr > cutrat):
        cutset.append(site)
  return cutset 

def object_rdwr_pack(objlist, cutrat):

  sizekey = 1
  accskey = 2
  rdwrkey = 5

  vals = []
  for idx,obj in enumerate(objlist):
    if obj[sizekey]:
      obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
      obj_reads   = (obj[accskey] - obj_writes)
      vals.append((obj_reads, obj_writes, obj[rdwrkey], obj[sizekey], idx))

  rdmostly     = sorted ( [ x for x in vals if ((x[2] < -0.01) or  (x[2] >= cutrat)) ], \
                          key=lambda tup: tup[3], reverse=True )
#  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
#                          key=lambda tup: tup[2], reverse=True )
  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
                          key=lambda tup: tup[3], reverse=True )

  cutset = [ x[4] for x in rdmostly ]
  reads  = sum ( [ x[0] for x in rdmostly ] )
  writes = sum ( [ x[1] for x in rdmostly ] )
  for obj in sorted_cands:
    next_reads  = (reads  + obj[0])
    next_writes = (writes + obj[1])
    rdwr_rat    = ( float(next_reads) / next_writes )

#    if obj[0] > 0 and obj[2] > 0:
#      print ("%12d %12d %12d %12d %3.4f %d" % \
#             (obj_reads, obj_writes, next_reads, next_writes, rdwr_rat, obj[3]))

    if rdwr_rat > cutrat:
      cutset += [obj[4]]
      reads   = next_reads
      writes  = next_writes

  return cutset

def site_rdwr_pack(sinfo, cutrat):

  vals = []
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      rdwr = -1.0 if rec[TOTAL_WRITES] == 0 else \
             float(rec[TOTAL_READS]) / rec[TOTAL_WRITES]
      vals.append((rec[TOTAL_READS], rec[TOTAL_WRITES], rdwr, rec[LINE_RSS], site))

  rdmostly     = sorted ( [ x for x in vals if ((x[2] < -0.01) or  (x[2] >= cutrat)) ], \
                          key=lambda tup: tup[3], reverse=True )
#  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
#                          key=lambda tup: tup[2], reverse=True )
  sorted_cands = sorted ( [ x for x in vals if ((x[2] > -0.01) and (x[2] < cutrat))  ], \
                          key=lambda tup: tup[3], reverse=True )

  cutset = [ x[4] for x in rdmostly ]
  reads  = sum ( [ x[0] for x in rdmostly ] )
  writes = sum ( [ x[1] for x in rdmostly ] )
  for site in sorted_cands:
    next_reads  = (reads  + site[0])
    next_writes = (writes + site[1])
    rdwr_rat    = ( float(next_reads) / next_writes )
    if rdwr_rat > cutrat:
      cutset += [site[4]]
      reads   = next_reads
      writes  = next_writes

  return cutset


def candidate_sites(bench, cfg_name, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  if aggtype == ALL_SITES:
    sinfo = site_info(bench, cfg_name)
    cands = sinfo.keys()
  elif aggtype == SORT:
    sinfo = site_info(bench, cfg_name)
    cands = get_sort_sites(sinfo, sortkey, revsort, cutkey, cut)
  elif aggtype == CUT:
    sinfo = site_info(bench, cfg_name)
    cands = get_cut_sites(sinfo, revsort, cutkey, cut)
  elif aggtype == THERMOS:
    sinfo = get_object_stats(bench, cfg_name)
    cands = site_thermos(sinfo, cut)
  else:
    print ("unknown aggtype: %s" % aggtype)
    return None

  return cands

def agg_site_info(bench, cfg_name, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  cands = candidate_sites(bench, cfg_name, aggtype=aggtype, \
           sortkey=sortkey, revsort=revsort, cutkey=cutkey, cut=cut)

  always_stats = [ NUM_SITES,    \
                   TOTAL_READS,  \
                   TOTAL_WRITES, \
                   READ_MISSES,  \
                   WRITE_MISSES, \
                   TOTAL_HITS,   \
                   TOTAL_MISSES, \
                   TOTAL_ACCS    \
                 ]

  agg_info = {}

  for stat in set(always_stats + stats):
    agg_info[stat] = 0

  for cand in cands:
    site_hits   = sinfo[cand][READ_HITS]   + sinfo[cand][WRITE_HITS]
    site_misses = sinfo[cand][READ_MISSES] + sinfo[cand][WRITE_MISSES]
    site_reads  = sinfo[cand][READ_HITS]   + sinfo[cand][READ_MISSES]
    site_writes = sinfo[cand][WRITE_HITS]  + sinfo[cand][WRITE_MISSES]
    site_read_misses  = sinfo[cand][READ_MISSES]
    site_write_misses = sinfo[cand][WRITE_MISSES]

    agg_info[TOTAL_HITS]   += site_hits
    agg_info[TOTAL_MISSES] += site_misses
    agg_info[TOTAL_ACCS]   += (site_hits + site_misses)
    agg_info[TOTAL_READS]  += site_reads
    agg_info[TOTAL_WRITES] += site_writes
    agg_info[READ_MISSES]  += site_read_misses
    agg_info[WRITE_MISSES] += site_write_misses

    agg_info[NUM_SITES] += 1
    for stat in [x for x in stats if not x in always_stats]:
      agg_info[stat] += sinfo[cand][stat]

  for stat in stats:
    if stat == SITE_HITS_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_ACCS]   == 0 else \
                       float(agg_info[TOTAL_HITS]) / agg_info[TOTAL_ACCS]
    elif stat == SITE_MISSES_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_ACCS]   == 0 else \
                       float(agg_info[TOTAL_MISSES]) / agg_info[TOTAL_ACCS]
    elif stat == SITE_RDWR_RATIO:
      agg_info[stat] = 0.0 if agg_info[TOTAL_WRITES] == 0 else \
                       float(agg_info[TOTAL_READS]) / agg_info[TOTAL_WRITES]
    elif stat == SITE_RDWR_MISS_RATIO:
      agg_info[stat] = 0.0 if agg_info[WRITE_MISSES] == 0 else \
                       float(agg_info[READ_MISSES]) / agg_info[WRITE_MISSES]

  return agg_info

def agg_site_report(benches, configs, stats=site_report_stats,
  aggtype=ALL_SITES, sortkey=TOTAL_MISSES, revsort=True, cutkey=NUM_SITES,
  cut=None):

  print_cfg_name = True if len(configs) > 1 else False
  if print_cfg_name:
    print("%-20s %-20s %6s" % ("bench", "config", "sites"), end='')
  else:
    print("%-20s %6s" % ("bench", "sites"), end='')

  for stat in site_report_stats:
    x = stat
    if "site_" in stat:
      x = x.replace("site","s")
    if "total_" in stat:
      x = x.replace("total","t")
    if "miss_ratio" in stat:
      x = x.replace("miss_ratio","mr")
    elif "ratio" in stat:
      x = x.replace("ratio","r")

    print(" %12s" % x, end='')
  print("")

  avg_info = {}
  avg_info[NUM_SITES] = []
  for stat in stats:
    avg_info[stat] = []

  for bench in benches:
    for cfg_name in configs:
      sinfo = agg_site_info(bench, cfg_name, stats=stats, \
              aggtype=aggtype, sortkey=sortkey, revsort=revsort,
              cutkey=cutkey, cut=cut)

      outstr = ("%-20s" % (bench))
      if print_cfg_name:
        outstr += (" %-20s" % (cfg_name))
      outstr += (" %6d" % sinfo[NUM_SITES])
      for stat in stats:
        outstr += (" %12s" % (format_str(stat) % (sinfo[stat])))
      print(outstr)

      avg_info[NUM_SITES].append(sinfo[NUM_SITES])
      for stat in stats:
        avg_info[stat].append(sinfo[stat])

  outstr = ("%-20s" % ("average"))
  if print_cfg_name:
    outstr += (" %-20s" % ("-"))

  outstr += (" %6d" % median(avg_info[NUM_SITES]))
  for stat in stats:
    if 'ratio' in stat and not stat.endswith("miss_ratio"):
      avg_stat = mean(avg_info[stat])
    else:
      avg_stat = median(avg_info[stat])
    outstr += (" %12s" % (format_str(stat) % (avg_stat)))
  print(outstr)

def parse_site_info(bench, cfg_name, it, rdict):
  cfg.read_cfg(cfg_name)

  runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])
  for copy in range(cfg.current_cfg['runcfg']['copies']):

    rdict[TOTAL_HEAP_PAGES]  = 0
    rdict[TOTAL_HEAP_LINES]  = 0
    rdict[TOTAL_HEAP_HITS]   = 0
    rdict[TOTAL_HEAP_MISSES] = 0
    rdict[TOTAL_UNKN_PAGES]  = 0
    rdict[TOTAL_UNKN_LINES]  = 0
    rdict[TOTAL_UNKN_HITS]   = 0
    rdict[TOTAL_UNKN_MISSES] = 0
    rdict[TOTAL_PAGES]       = 0
    rdict[TOTAL_LINES]       = 0
    rdict[TOTAL_HITS]        = 0
    rdict[TOTAL_MISSES]      = 0
    rdict[TOTAL_ACCS]        = 0
    rdict[HEAP_ACCS]         = 0
    rdict[UNKN_ACCS]         = 0

    err = False
    for run in runs:
      try:
        results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
        fd = open(('%sorig_ap_info.out' % results_dir), 'r')
      except:
        err = True
        continue

      for line in fd:
        pts = line.split()
        if line.startswith('HEAP'):
          rdict[TOTAL_HEAP_PAGES]  += int(pts[1])
          rdict[TOTAL_HEAP_LINES]  += int(pts[2])
          rdict[TOTAL_HEAP_HITS]   += int(pts[3])
          rdict[TOTAL_HEAP_MISSES] += int(pts[4])
        elif line.startswith('UNKN'):
          rdict[TOTAL_UNKN_PAGES]  += int(pts[1])
          rdict[TOTAL_UNKN_LINES]  += int(pts[2])
          rdict[TOTAL_UNKN_HITS]   += int(pts[3])
          rdict[TOTAL_UNKN_MISSES] += int(pts[4])
      fd.close()

    rdict[TOTAL_PAGES]        = (rdict[TOTAL_HEAP_PAGES] + rdict[TOTAL_UNKN_PAGES])
    rdict[KNOWN_PAGES_RATIO]  = 0.0 if rdict[TOTAL_PAGES] == 0 else \
                                (rdict[TOTAL_HEAP_PAGES] / rdict[TOTAL_PAGES])

    rdict[TOTAL_LINES]        = (rdict[TOTAL_HEAP_LINES] + rdict[TOTAL_UNKN_LINES])
    rdict[KNOWN_LINES_RATIO]  = 0.0 if rdict[TOTAL_LINES] == 0 else \
                                (rdict[TOTAL_HEAP_LINES] / rdict[TOTAL_LINES])

    rdict[TOTAL_HITS]         = (rdict[TOTAL_HEAP_HITS] + rdict[TOTAL_UNKN_HITS])
    rdict[KNOWN_HITS_RATIO]   = 0.0 if rdict[TOTAL_HITS] == 0 else \
                                (rdict[TOTAL_HEAP_HITS] / rdict[TOTAL_HITS])

    rdict[TOTAL_MISSES]       = (rdict[TOTAL_HEAP_MISSES] + rdict[TOTAL_UNKN_MISSES])
    rdict[KNOWN_MISSES_RATIO] = 0.0 if rdict[TOTAL_MISSES] == 0 else \
                                (rdict[TOTAL_HEAP_MISSES] / rdict[TOTAL_MISSES])

    rdict[TOTAL_ACCS]         = rdict[TOTAL_HITS] + rdict[TOTAL_MISSES]
    rdict[HEAP_ACCS]          = (rdict[TOTAL_HEAP_HITS] + rdict[TOTAL_HEAP_MISSES])
    rdict[KNOWN_ACCS_RATIO]   = 0.0 if rdict[TOTAL_ACCS] == 0 else \
                                (rdict[HEAP_ACCS] / rdict[TOTAL_ACCS])

    rdict[TOTAL_HITS_RATIO]   = 0.0 if rdict[TOTAL_ACCS] == 0 else \
                                (rdict[TOTAL_HITS] / rdict[TOTAL_ACCS])
    rdict[HEAP_HITS_RATIO]    = 0.0 if rdict[HEAP_ACCS] == 0 else \
                                (rdict[TOTAL_HEAP_HITS] / rdict[HEAP_ACCS])

    rdict[UNKN_ACCS]          = (rdict[TOTAL_UNKN_HITS] + rdict[TOTAL_UNKN_MISSES])
    rdict[UNKN_HITS_RATIO]    = 0.0 if rdict[UNKN_ACCS] == 0 else \
                                (rdict[TOTAL_UNKN_HITS] / rdict[UNKN_ACCS])

    if err:
      for key in rdict:
        rdict[key] = None
    #print(rdict)

def aggregate_results(bench, cfg_name, iters):
  avgs = dict()
  for i in range(iters):
    for key,val in results[bench][cfg_name][i].items():
      if not isinstance(val, list) and not isinstance(val, dict):
        if key in avgs:
          avgs[key] += val
        else:
          avgs[key] = val
  return avgs

def get_results_dict(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  full_parse=False):

  results = dict()
  for bench, cfg_name, i in expiter(benches, cfgs, iters):
    if not bench in results:
      results[bench] = dict()

    if not cfg_name in results[bench]:
      results[bench][cfg_name] = dict()

    results[bench][cfg_name][i] = dict()

    rdict = results[bench][cfg_name][i]
    results_dir = get_iter_results_dir(bench, cfg_name, i)
    if stat in [EXE_TIME,PAGE_RSS,RUNTIME] or full_parse:
      parse_time(bench, cfg_name, results_dir, rdict)
    elif stat in marena_stats or full_parse:
      parse_marena(bench, cfg_name, results_dir, rdict)
    elif stat in numastat_stats or full_parse:
      parse_numastat(results_dir, rdict)
    elif stat in memreserve_stats or full_parse:
      parse_memreserve(results_dir, rdict)
    elif stat in pcm_stats or full_parse:
      parse_pcm(results_dir, rdict)
    elif stat in site_info_stats or full_parse:
      parse_site_info(bench, cfg_name, i, rdict)

  return results

def get_report_strs(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  basecfg='default_noir', style=MEAN, cis=True, absolute=False):

  if cis and iters < 2:
    cis = False

  results = get_results_dict(benches, cfgs, iters, stat, False)
  report_strs = dict()

  averages = dict()
  for expcfg in cfgs:
    averages[expcfg] = []

  for bench in results.keys():
    report_strs[bench] = dict()

    if not absolute:
      basevals   = [ results[bench][basecfg][i][stat] for i in \
                     results[bench][basecfg].keys() ]
      if None in basevals:
        basemean = basemedian = basestd = None
      else:
        basemean   = mean(basevals) 
        basemedian = median(basevals) 
        basestd    = stdev(basevals) if len(basevals) > 1 else 0.0

    for expcfg in results[bench].keys():
      expvals   = [ results[bench][expcfg][i][stat] for i in \
                     results[bench][expcfg].keys() ]

      if None in expvals:
        expmean = expmedian = expstd = None
      else:
        #print (expvals)
        expmean   = mean(expvals)
        expmedian = median(expvals)
        expstd    = stdev(expvals) if len(expvals) > 1 else 0.0

      if cis:
        if absolute:
          ci = get95CI(expmean, expmean, expstd, expstd, iters)
        else:
          ci = get95CI(basemean, expmean, basestd, expstd, iters)
          ci /= expmean

      val = None
      if expmean != None:
        if not absolute:
          if style == MEAN:
            val = (float(expmean) / basemean) if basemean > 0.0 else None
          else:
            val = (float(expmedian) / basemedian) if basemedian > 0.0 else None
        else:
          val = (float(expmean)) if style == MEAN else (float(expmedian))

      if val == None:
        report_strs[bench][expcfg] = "N/A" if not cis else ("N/A", "N/A")
      else:
        if absolute:
          if "ratio" in stat:
            report_strs[bench][expcfg] = ("%4.3f" % val) if not cis else \
                                         (("%4.3f" % val), (("%4.3f" % ci)).rjust(6))
          else:
            report_strs[bench][expcfg] = ("%5.1f" % val) if not cis else \
                                         (("%5.1f" % val), (("%5.1f" % ci)).rjust(6))
        else:
          report_strs[bench][expcfg] = ("%4.3f" % val) if not cis else \
                                       (("%4.3f" % val), (("%3.2f" % ci)).rjust(6))

      if val != None:
        averages[expcfg].append(val)

  report_strs["average"] = dict()
  for expcfg in cfgs:
    val = None
    if (len(averages[expcfg]) > 0):
      if not absolute:
        val = safe_geo_mean(averages[expcfg])
      else:
        val = mean(averages[expcfg])

    if val == None:
      report_strs["average"][expcfg] = "N/A" if not cis else ("N/A", "-".rjust(6))
    else:
      if absolute:
        if "ratio" in stat:
          report_strs["average"][expcfg] = ("%4.3f" % val) if not cis else \
                                           (("%4.3f" % val), ("-".rjust(6)))
        else:
          report_strs["average"][expcfg] = ("%5.1f" % val) if not cis else \
                                           (("%5.1f" % val), ("-".rjust(6)))
      else:
        report_strs["average"][expcfg] = ("%4.3f" % val) if not cis else \
                                         (("%4.3f" % val), ("-".rjust(6)))
                                      
  return report_strs

# Gets a dict of results for the ddr_bandwidth configs
def get_ddr_bandwidth_results(benches, cfgs):
  profcfg_results = dict()
  bandwidth_profile_results = dict()
  for bench in benches:
    profcfg_results[bench] = dict()
    bandwidth_profile_results[bench] = dict()
    for cfg_name in cfgs:
      cfg.read_cfg(cfg_name)
      # Get the results for the profcfg
      profcfg_results[bench][cfg_name] = dict()
      bandwidth_profile_results[bench][cfg_name] = dict()

      profcfg_results_dir = get_results_dir(bench, cfg.current_cfg['runcfg']['profcfg'], 0)
      parse_bench(bench, cfg.current_cfg['runcfg']['profcfg'], profcfg_results_dir, profcfg_results[bench][cfg_name])
      bandwidth_profile_dir = results_dir + bench + '/' + cfg_name + '/'

      # Get the bandwidth of the baseline run
      refres = None
      idirs    = [ x+'/' for x in glob(bandwidth_profile_dir+"*") ]
      for x in idirs:
        if x.endswith('i0/'):
          refres = dict()
          parse_pcm(x, refres)
          #print("refres: avg: %5.2f max: %5.2f" % \
          #      (refres[AVG_DDR4_BANDWIDTH], refres[MAX_DDR4_BANDWIDTH]))
      for idir in idirs:
        site = tuple([int(idir.split('/')[-2].strip('i'))])
        bandwidth_profile_results[bench][cfg_name][site[0]] = dict()
        parse_pcm(idir, bandwidth_profile_results[bench][cfg_name][site[0]], refres=refres)

  return (profcfg_results, bandwidth_profile_results)


# MRJ -- this code is not useful but I'm keeping it around in case we need a
# knapsack implementation for something
#
def object_writes_knapsack(objlist, cutrat, sigfigs=5):

  sizekey = 1
  accskey = 2
  rdwrkey = 5

  vals = []
  total_accs = total_writes = 0
  for idx,obj in enumerate(objlist):
    if obj[sizekey]:
      obj_writes = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
      total_accs   += obj[accskey]
      total_writes += obj_writes
      vals.append((obj_writes, obj[sizekey], idx))

  if (len(vals)>1000000):
    sigfigs -= 1
  print (sigfigs)

  WRITES = 0
  SIZE   = 1
  IDX    = 2

  target = ( total_accs * (cutrat / 100))

  wincs = (10**sigfigs)
  cutoff = (target / total_writes)
  print( "target: %8.2f total: %d cutoff: %4.4f" % (target, total_writes, cutoff) )

  wgt_vals = [ (v[SIZE], int((float(v[WRITES]) / total_writes) * wincs), idx) \
                for v in vals ]

  wgt_vals.sort(key=lambda tup: tup[WRITES], reverse=True)

  sig_wgts = [ x for x in wgt_vals if x[WRITES] > 0 ]
  insig_wgts = wgt_vals[len(sig_wgts):]

  wgt_incs = range(int(cutoff*wincs))

  objs = {}
  for v in vals:
    objs[v[IDX]] = (v[WRITES], v[SIZE])

  m = np.zeros((len(sig_wgts)+1, len(wgt_incs)))

  for i,wgt in enumerate(sig_wgts,start=1):
    for j,inc in enumerate(wgt_incs):
      if wgt[WRITES] <= inc:
        m[i][j] = max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + objs[wgt[IDX]][SIZE])
      else:
        m[i][j] = m[i-1][j]
    if i % 1000 == 0:
      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))

  hots = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      hots.append(sig_wgts[i-1][IDX])
      j -= sig_wgts[i-1][WRITES]
    i -= 1

  for wgt in insig_wgts:
    hots.append(wgt[IDX])

  hotset_writes = 0
  for idx in hots:
    obj = objlist[idx]
    obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
    hotset_writes += obj_writes

  print ("target_writes: %3.2f" % (target        / (1024*1024)))
  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

#  m = []
#  m.append([])
#  for j in wgt_incs:
#    m[0].append(0)
#
#  for i,wgt in enumerate(sig_wgts,start=1):
#    m.append([])
#    for j,inc in enumerate(wgt_incs):
#      if wgt[WRITES] <= inc:
#        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + objs[wgt[IDX]][SIZE]))
#      else:
#        m[i].append(m[i-1][j])
#    if i % 1000 == 0:
#      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))
#
#  hots = []
#  i = len(sig_wgts)
#  j = (len(wgt_incs)-1)
#  while i > 0:
#    if m[i][j] != m[i-1][j]:
#      hots.append(sig_wgts[i-1][IDX])
#      j -= sig_wgts[i-1][WRITES]
#    i -= 1
#
#  for wgt in insig_wgts:
#    hots.append(wgt[IDX])
#
#  hotset_writes = 0
#  for idx in hots:
#    obj = objlist[idx]
#    obj_writes  = 0 if obj[rdwrkey] == -1.0 else (obj[accskey] / (obj[rdwrkey]+1))
#    hotset_writes += obj_writes
#
#  print ("target_writes: %3.2f" % (target        / (1024*1024)))
#  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

  return hots

def site_writes_knapsack(sinfo, cutrat, sigfigs=5):

  vals = []
  total_accs = total_writes = 0
  for site,rec in sinfo.items():
    if rec[LINE_RSS]:
      total_accs   += (rec[TOTAL_READS] + rec[TOTAL_WRITES])
      total_writes += rec[TOTAL_WRITES]
      vals.append((rec[TOTAL_WRITES], rec[LINE_RSS], site))

  if (len(vals)>1000000):
    sigfigs -= 1
  print (sigfigs)

  WRITES = 0
  SIZE   = 1
  SITE   = 2

  target = ( total_accs * (cutrat / 100))

  wincs = (10**sigfigs)
  cutoff = (target / total_writes)
  print( "target: %8.2f total: %d cutoff: %4.4f" % (target, total_writes, cutoff) )

  wgt_vals = [ (v[SIZE], int((float(v[WRITES]) / total_writes) * wincs), idx) \
                for v in vals ]

  wgt_vals.sort(key=lambda tup: tup[WRITES], reverse=True)

  sig_wgts = [ x for x in wgt_vals if x[WRITES] > 0 ]
  insig_wgts = wgt_vals[len(sig_wgts):]

  wgt_incs = range(int(cutoff*wincs))

  sites = {}
  for v in vals:
    sites[v[SITE]] = (v[WRITES], v[SIZE])

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[WRITES] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[WRITES]] + sites[wgt[SITE]][SIZE]))
      else:
        m[i].append(m[i-1][j])
    if i % 1000 == 0:
      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))

  hots = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      hots.append(sig_wgts[i-1][SITE])
      j -= sig_wgts[i-1][WRITES]
    i -= 1

  for wgt in insig_wgts:
    hots.append(wgt[SITE])

  hotset_writes = 0
  for site in hots:
    hotset_writes += (sinfo[site][TOTAL_WRITES])

  print ("target_writes: %3.2f" % (target        / (1024*1024)))
  print ("hotset_writes: %3.2f" % (hotset_writes / (1024*1024)))

  return hots

def object_apb_cut_old(objlist, cutpct):

  accskey    = 0
  sizekey    = 1
  apbkey     = 2
  bad_objs   = 0
  total_size = 0

  vals = []
  for idx,obj in enumerate(objlist):
    if isnan(obj[apbkey]) or isinf(obj[apbkey]):
      bad_objs += 1
    else:
      vals.append((obj[apbkey], obj[sizekey], idx))

  accesses_per_byte = []
  zero_vals     = [ x for x in vals if x[0] == 0 ]
  non_zero_vals = [ x for x in vals if x[0] != 0 ]

  for apb,size,idx in non_zero_vals:
    accesses_per_byte.append((apb, idx))

  if (len(accesses_per_byte) > 0):
    accesses_per_byte.sort(key=lambda tup: tup[0], reverse=True)

  zero_vals.sort(key=lambda tup: tup[1])
  for apb,size,idx in zero_vals:
    accesses_per_byte.append((apb, idx))

  cut    = int((len(accesses_per_byte) * (cutpct / 100)))
  cutset = [ x[1] for x in accesses_per_byte[:cut] ]

  if bad_objs:
    print("bad objects: %d" % bad_objs)

  return cutset

def object_hotset(objlist, cutpct):

  accskey    = 0
  sizekey    = 1
  apbkey     = 2
  bad_objs   = 0
  total_size = 0

  vals = []
  for idx,obj in enumerate(objlist):
    if isnan(obj[apbkey]) or isinf(obj[apbkey]):
      bad_objs += 1
    else:
      vals.append((obj[apbkey], obj[sizekey], idx))

  target = ( (sum( [ v[0] for v in vals ] ) * (cutpct / 100)))

  accesses_per_byte = []
  zero_vals     = [ x for x in vals if x[0] == 0 ]
  non_zero_vals = [ x for x in vals if x[0] != 0 ]

  for apb,size,idx in non_zero_vals:
    accesses_per_byte.append((apb, idx))

  if (len(accesses_per_byte) > 0):
    accesses_per_byte.sort(key=lambda tup: tup[0], reverse=True)

  zero_vals.sort(key=lambda tup: tup[1])
  for apb,size,idx in zero_vals:
    accesses_per_byte.append((apb, idx))

  hotset = []
  for apb,idx in accesses_per_byte:
    total_size += objlist[idx][sizekey]
    hotset.append(idx)
    if total_size > target:
      break

  print("%10d %10d" % (total_size, target))
  if bad_objs:
    print("bad objects: %d" % bad_objs)

  return hotset

