import pprint
import os

# A cfg consists of a buildcfg and a runcfg
current_cfg = {}
cfgs_dir = os.path.dirname(os.path.realpath(__file__)) + '/cfgs/'

def get_cfg_file(name):
  for root, dirs, files in os.walk(cfgs_dir):
    for filename in files:
      if filename == (name + '.py'):
        fd = open(root + '/' + name + '.py', 'r')
        lines = fd.read()
        exec(lines, globals())
        fd.close()
        return root + '/' + name + '.py'
  return ''

def read_cfg(name):
  global current_cfg
  fd = open(get_cfg_file(name), 'r')
  lines = fd.read()
  exec(lines, globals())
  fd.close()

def print_cfg(name):
  global current_cfg
  read_cfg(name)
  pprint.pprint(current_cfg)

