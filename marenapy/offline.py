import pprint
import marenapy.cfg as cfg
from marenapy.benches.bench import *
from marenapy.results import *
from operator import itemgetter

ACCS = 0
SIZE = 1
SITE = 2
APB  = 3

HOTSET        = "hotset"
HOTSET_3CXT   = "hotset_3cxt"
KNAPSACK      = "knapsack"
KNAPSACK_3CXT = "knapsack_3cxt"

MAX_BANDWIDTH_HOTSET        = "max_bandwidth_hotset"
AVG_BANDWIDTH_HOTSET        = "avg_bandwidth_hotset"
MAX_DDR_BANDWIDTH_HOTSET    = "max_ddr_bandwidth_hotset"
AVG_DDR_BANDWIDTH_HOTSET    = "avg_ddr_bandwidth_hotset"

MAX_DDR_BANDWIDTH_ORIG_HOTSET = "max_ddr_bandwidth_orig_hotset"
AVG_DDR_BANDWIDTH_ORIG_HOTSET = "avg_ddr_bandwidth_orig_hotset"

ACCS_HOTSET                 = "accs_hotset"
MAX_BANDWIDTH_ACCS_HOTSET   = "max_bandwidth_accs_hotset"
AVG_BANDWIDTH_ACCS_HOTSET   = "avg_bandwidth_accs_hotset"

KNAPSACK                    = "knapsack"
MAX_BANDWIDTH_KNAPSACK      = "max_bandwidth_knapsack"
AVG_BANDWIDTH_KNAPSACK      = "avg_bandwidth_knapsack"
MAX_DDR_BANDWIDTH_KNAPSACK  = "max_ddr_bandwidth_knapsack"
AVG_DDR_BANDWIDTH_KNAPSACK  = "avg_ddr_bandwidth_knapsack"

ACCS_KNAPSACK                   = "accs_knapsack"
MAX_BANDWIDTH_ACCS_KNAPSACK     = "max_bandwidth_accs_knapsack"
AVG_BANDWIDTH_ACCS_KNAPSACK     = "avg_bandwidth_accs_knapsack"
MAX_DDR_BANDWIDTH_ACCS_KNAPSACK = "max_ddr_bandwidth_accs_knapsack"
AVG_DDR_BANDWIDTH_ACCS_KNAPSACK = "avg_ddr_bandwidth_accs_knapsack"

PEBS_HOTSET = "pebs_hotset"

bwstats = [ AVG_MCDRAM_BANDWIDTH, MAX_MCDRAM_BANDWIDTH,
            AVG_DDR4_BANDWIDTH, MAX_DDR4_BANDWIDTH
          ]

def gen_hotset(profcfg_results, accskey, pctkey):

  vals = []
  #print(accskey)
  for site in profcfg_results['sites']:
    accesses = profcfg_results['sites'][site][accskey]
    peak_rss = profcfg_results['sites'][site][PEAK_RSS]
    vals.append((accesses, peak_rss, site))
    #print ("  %5d %12.8f %8.4f" % (site[0], peak_rss/(1024*1024*1024), accesses))

  #target = ( (sum( [ v[SIZE] for v in vals ] )) * \
  #           (cfg.current_cfg['runcfg'][pctkey] / 100) - \
  #           (125*1024*1024))

  # MBO: USE THIS ONE
  target = ( (sum( [ v[SIZE] for v in vals ] )) * \
             (cfg.current_cfg['runcfg'][pctkey] / 100))

  # Hacky hardcoded to 4GB
  #target = 4294967296 

#  target_pct = ( (4.0 * (1024*1024*1024)) / (sum( [ v[SIZE] for v in vals ] )) )
#  target = ( (sum( [ v[SIZE] for v in vals ] )) * (target_pct) )

  accesses_per_byte = []
  zero_vals     = [ x for x in vals if x[0] == 0 ]
  non_zero_vals = [ x for x in vals if x[0] != 0 ]
  for accesses,peak_rss,site in non_zero_vals:
    apb = ( ( float(accesses) / peak_rss ) if peak_rss != 0 else 0.0 )
    accesses_per_byte.append((apb, site))

  if (len(accesses_per_byte) > 0):
    accesses_per_byte.sort(key=lambda tup: tup[0], reverse=True)

  zero_vals.sort(key=lambda tup: tup[1])
  for accesses,peak_rss,site in zero_vals:
    apb = ( ( float(accesses) / peak_rss ) if peak_rss != 0 else 0.0 )
    accesses_per_byte.append((apb, site))

  for tup in accesses_per_byte:
    accesses = profcfg_results['sites'][tup[1]][accskey]
    peak_rss = profcfg_results['sites'][tup[1]][PEAK_RSS]

  hotset = []
  hotset_accs = 0
  hotset_size = 0
  for tup in accesses_per_byte:
    site_accs = profcfg_results['sites'][tup[1]][accskey]
    site_size = profcfg_results['sites'][tup[1]][PEAK_RSS]

    over = max([((hotset_size + site_size) - target),0])
    if over > 0:
      tmp_set  = []
      tmp_size = 0
      tmp_accs = 0
      for site in hotset:
        tmp_set  += [site]
        tmp_accs += profcfg_results['sites'][site][accskey]
        tmp_size += profcfg_results['sites'][site][PEAK_RSS]
        if tmp_size > over:
          break

      #print ("hot: %3.4f tmp: %3.4f site: %3.4f cmp: %3.4f" % \
      #       (hotset_accs, tmp_accs, site_accs, \
      #       (hotset_accs - tmp_accs + site_accs)))
      if ((hotset_accs - tmp_accs + site_accs) > hotset_accs):
        hotset += [tup[1]]
        hotset_size += site_size
        hotset_accs += site_accs

    else:
      hotset += [tup[1]]
      hotset_size += site_size
      hotset_accs += site_accs

    #scale  = min([(target/(hotset_size+site_size)),1.0])

    #print (str((hotset_accs+site_accs))+" "+\
    #       str((hotset_size+site_size))+" "+str(target)+" "+str(scale))

    #if (((hotset_accs+site_accs)*scale) > hotset_accs):
    #  hotset += [tup[1]]
    #  hotset_size += site_size
    #  hotset_accs += site_accs

#  hotset = []
#  hotset_size = 0
#  for tup in accesses_per_byte:
#    hotset += [tup[1]]
#    hotset_size += profcfg_results['sites'][tup[1]][PEAK_RSS]
#    if hotset_size >= target:
#      break

  print ("target_size: %3.2f" % (target  / (1024*1024)))
  print ("hotset_size: %3.2f" % (hotset_size / (1024*1024)))

  return hotset

def gen_orig_hotset(profcfg_results, accskey, pctkey):

  vals = []
  #print(accskey)
  for site in profcfg_results['sites']:
    accesses = profcfg_results['sites'][site][accskey]
    peak_rss = profcfg_results['sites'][site][PEAK_RSS]
    vals.append((accesses, peak_rss, site))
    #print ("  %5d %12.8f %8.4f" % (site[0], peak_rss/(1024*1024*1024), accesses))

  #target = ( (sum( [ v[SIZE] for v in vals ] )) * \
  #           (cfg.current_cfg['runcfg'][pctkey] / 100) - \
  #           (125*1024*1024))
  target = ( (sum( [ v[SIZE] for v in vals ] )) * \
             (cfg.current_cfg['runcfg'][pctkey] / 100))

  accesses_per_byte = []
  for accesses,peak_rss,site in vals:
    apb = ( float(accesses) / peak_rss ) if peak_rss != 0 else 0.0
    accesses_per_byte.append((apb, site))

  accesses_per_byte.sort(key=lambda tup: tup[0], reverse=True)
  for tup in accesses_per_byte:
    accesses = profcfg_results['sites'][tup[1]][accskey]
    peak_rss = profcfg_results['sites'][tup[1]][PEAK_RSS]

  hotset = []
  hotset_accs = 0
  hotset_size = 0
  for tup in accesses_per_byte:
    site_accs = profcfg_results['sites'][tup[1]][accskey]
    site_size = profcfg_results['sites'][tup[1]][PEAK_RSS]
    hotset += [tup[1]]
    hotset_size += site_size
    hotset_accs += site_accs
    if (hotset_size > target):
      break

  print ("target_size: %3.2f" % (target  / (1024*1024)))
  print ("hotset_size: %3.2f" % (hotset_size / (1024*1024)))

  return hotset


def gen_accs_hotset(profcfg_results, accskey, pctkey):

  vals = []
  for site in profcfg_results['sites']:
    accesses = profcfg_results['sites'][site][accskey]
    peak_rss = profcfg_results['sites'][site][PEAK_RSS]
    #print ("  %5d %8.1f %8.1f" % (site[0], (float(peak_rss)/(1024*1024)), accesses))
    vals.append((accesses, peak_rss, site))

  if accskey in bwstats:
    target = ( 480.0 * (cfg.current_cfg['runcfg'][pctkey] / 100))
  else:
    target = ( (sum( [ v[ACCS] for v in vals ] )) * \
               (cfg.current_cfg['runcfg'][pctkey] / 100))

  accesses_per_byte = []
  for accesses,peak_rss,site in vals:
    apb = ( float(accesses) / peak_rss ) if peak_rss != 0 else 0.0
    accesses_per_byte.append((apb, site))

  accesses_per_byte.sort(key=lambda tup: tup[0])

  colds = []
  coldset_accs = 0
  for tup in accesses_per_byte:
    colds += [tup[1]]
    coldset_accs += profcfg_results['sites'][tup[1]][accskey]
    if coldset_accs >= target:
      break

  print ("target_accs:  %3.2f" % (target       ))
  print ("coldset_accs: %3.2f" % (coldset_accs ))

  coldset_rss = 0
  for site in colds:
    coldset_rss += profcfg_results['sites'][site][PEAK_RSS]
  total_rss = sum( [ v[SIZE] for v in vals ] )

  print ("total_rss:   %3.2f" % (total_rss   / (1024.0*1024.0) ))
  print ("coldset_rss: %3.2f" % (coldset_rss / (1024.0*1024.0) ))

  hots = [ x[SITE] for x in vals if not x[SITE] in colds ]

  return hots

def gen_knapsack(profcfg_results, accskey, pctkey, sigfigs=5):

  vals = []
  print(accskey)
  for site in profcfg_results['sites']:
    accesses = profcfg_results['sites'][site][accskey]
    peak_rss = profcfg_results['sites'][site][PEAK_RSS]
    vals.append((accesses, peak_rss, site))

  target = ( (sum( [ v[SIZE] for v in vals ] )) * \
             (cfg.current_cfg['runcfg'][pctkey] / 100))
  #target = ( (sum( [ v[SIZE] for v in vals ] )) * \
  #           (cfg.current_cfg['runcfg'][pctkey] / 100) - \
  #           (125*1024*1024))

  wincs = (10**sigfigs)
  total_size = sum( [ v[SIZE] for v in vals ] )
  cutoff = (target / total_size)
  print( "target: %8.2f total: %d cutoff: %4.4f" % (target, total_size, cutoff) )

  wgt_vals = [ (v[ACCS], int((float(v[SIZE]) / total_size) * wincs), v[SITE]) \
                for v in vals ]

  wgt_vals.sort(key=lambda tup: tup[SIZE], reverse=True)

  sig_wgts = [ x for x in wgt_vals if x[SIZE] > 0 ]
  insig_wgts = wgt_vals[len(sig_wgts):]

  wgt_incs = range(int(cutoff*wincs))

  sites = {}
  for v in vals:
    sites[v[SITE]] = (v[ACCS], v[SIZE])

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[1] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[SIZE]] + sites[wgt[SITE]][ACCS]))
      else:
        m[i].append(m[i-1][j])
    if i % 10 == 0:
      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))

  hots = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      hots.append(sig_wgts[i-1][SITE])
      j -= sig_wgts[i-1][SIZE]
    i -= 1

  for wgt in insig_wgts:
    hots.append(wgt[SITE])

  hotset_size = 0
  for site in hots:
    hotset_size += profcfg_results['sites'][site][PEAK_RSS]

  print ("target_size: %3.2f" % (target      / (1024*1024)))
  print ("hotset_size: %3.2f" % (hotset_size / (1024*1024)))

  return hots

def gen_accs_knapsack(profcfg_results, accskey, pctkey, sigfigs=5):

  vals = []
  for site in profcfg_results['sites']:
    accesses = profcfg_results['sites'][site][accskey]
    peak_rss = profcfg_results['sites'][site][PEAK_RSS]
    #print ("  %5d %8.1f %8.1f" % (site[0], (peak_rss/(1024*1024)), accesses))
    vals.append((accesses, peak_rss, site))

  if accskey in bwstats:
    target = ( 480.0 * (cfg.current_cfg['runcfg'][pctkey] / 100))
  else:
    target = ( (sum( [ v[ACCS] for v in vals ] )) * \
               (cfg.current_cfg['runcfg'][pctkey] / 100))

  wincs = (10**sigfigs)
  total_accs = sum( [ v[ACCS] for v in vals ] )
  cutoff = (target / total_accs)
  print( "target: %8.2f total: %d cutoff: %4.4f" % (target, total_accs, cutoff) )

  wgt_vals = [ ( int((float(v[ACCS]) / total_accs) * wincs), v[SIZE], v[SITE]) \
                for v in vals ]

  wgt_vals.sort(key=lambda tup: tup[ACCS], reverse=True)

  sig_wgts = [ x for x in wgt_vals if x[ACCS] > 0 ]
  insig_wgts = wgt_vals[len(sig_wgts):]

  wgt_incs = range(int(cutoff*wincs))

  sites = {}
  for v in vals:
    sites[v[SITE]] = (v[ACCS], v[SIZE])

  m = []
  m.append([])
  for j in wgt_incs:
    m[0].append(0)

  for i,wgt in enumerate(sig_wgts,start=1):
    m.append([])
    for j,inc in enumerate(wgt_incs):
      if wgt[ACCS] <= inc:
        m[i].append(max(m[i-1][j], m[i-1][inc-wgt[ACCS]] + sites[wgt[SITE]][SIZE]))
      else:
        m[i].append(m[i-1][j])
    if i % 10 == 0:
      print ("  m[%d][%d] = %d" % (i,j,m[i][j]))

  colds = []
  i = len(sig_wgts)
  j = (len(wgt_incs)-1)
  while i > 0:
    if m[i][j] != m[i-1][j]:
      colds.append(sig_wgts[i-1][SITE])
      j -= sig_wgts[i-1][ACCS]
    i -= 1

  for wgt in insig_wgts:
    colds.append(wgt[SITE])

  coldset_accs = 0
  for site in colds:
    coldset_accs += profcfg_results['sites'][site][accskey]

  print ("target_accs:  %3.2f" % (target       ))
  print ("coldset_accs: %3.2f" % (coldset_accs ))

  coldset_rss = 0
  for site in colds:
    coldset_rss += profcfg_results['sites'][site][PEAK_RSS]
  total_rss = sum( [ v[SIZE] for v in vals ] )

  print ("total_rss:   %3.2f" % (total_rss   / (1024.0*1024.0) ))
  print ("coldset_rss: %3.2f" % (coldset_rss / (1024.0*1024.0) ))

  hots = [ x[SITE] for x in vals if not x[SITE] in colds ]

  return hots


hotset_helper_recs = {
  HOTSET                        : ( "hotset_percentage",                      ACCESSES,             gen_hotset),
  MAX_BANDWIDTH_HOTSET          : ( "max_bandwidth_hotset_percentage",        MAX_MCDRAM_BANDWIDTH, gen_hotset),
  AVG_BANDWIDTH_HOTSET          : ( "avg_bandwidth_hotset_percentage",        AVG_MCDRAM_BANDWIDTH, gen_hotset),
  MAX_DDR_BANDWIDTH_HOTSET      : ( "max_ddr_bandwidth_hotset_percentage",    MAX_DDR4_BANDWIDTH,   gen_hotset),
  AVG_DDR_BANDWIDTH_HOTSET      : ( "avg_ddr_bandwidth_hotset_percentage",    AVG_DDR4_BANDWIDTH,   gen_hotset),

  MAX_DDR_BANDWIDTH_ORIG_HOTSET : ( "max_ddr_bandwidth_orig_hotset_percentage", MAX_DDR4_BANDWIDTH, gen_orig_hotset),
  AVG_DDR_BANDWIDTH_ORIG_HOTSET : ( "avg_ddr_bandwidth_orig_hotset_percentage", AVG_DDR4_BANDWIDTH, gen_orig_hotset),

  ACCS_HOTSET                   : ( "accs_hotset_percentage",                 ACCESSES,             gen_accs_hotset),
  MAX_BANDWIDTH_ACCS_HOTSET     : ( "max_bandwidth_accs_hotset_percentage",   MAX_MCDRAM_BANDWIDTH, gen_accs_hotset),
  AVG_BANDWIDTH_ACCS_HOTSET     : ( "avg_bandwidth_accs_hotset_percentage",   AVG_MCDRAM_BANDWIDTH, gen_accs_hotset),

  KNAPSACK                      : ( "knapsack_percentage",                    ACCESSES,             gen_knapsack),
  MAX_BANDWIDTH_KNAPSACK        : ( "max_bandwidth_knapsack_percentage",      MAX_MCDRAM_BANDWIDTH, gen_knapsack),
  AVG_BANDWIDTH_KNAPSACK        : ( "avg_bandwidth_knapsack_percentage",      AVG_MCDRAM_BANDWIDTH, gen_knapsack),
  MAX_DDR_BANDWIDTH_KNAPSACK    : ( "max_ddr_bandwidth_knapsack_percentage",  MAX_DDR4_BANDWIDTH,   gen_knapsack),
  AVG_DDR_BANDWIDTH_KNAPSACK    : ( "avg_ddr_bandwidth_knapsack_percentage",  AVG_DDR4_BANDWIDTH,   gen_knapsack),

  ACCS_KNAPSACK                   : ( "accs_knapsack_percentage",                   ACCESSES,             gen_accs_knapsack),
  MAX_BANDWIDTH_ACCS_KNAPSACK     : ( "max_bandwidth_accs_knapsack_percentage",     MAX_MCDRAM_BANDWIDTH, gen_accs_knapsack),
  AVG_BANDWIDTH_ACCS_KNAPSACK     : ( "avg_bandwidth_accs_knapsack_percentage",     AVG_MCDRAM_BANDWIDTH, gen_accs_knapsack),
  MAX_DDR_BANDWIDTH_ACCS_KNAPSACK : ( "max_ddr_bandwidth_accs_knapsack_percentage", MAX_DDR4_BANDWIDTH,   gen_accs_knapsack),
  AVG_DDR_BANDWIDTH_ACCS_KNAPSACK : ( "avg_ddr_bandwidth_accs_knapsack_percentage", AVG_DDR4_BANDWIDTH,   gen_accs_knapsack),

  PEBS_HOTSET : ( "pebs_hotset_percentage",  ACCESSES, gen_hotset),
}


def get_hstype(runcfg):
  hstype = None

  # MRJ -- need to fix this -- no need to have to specify DDR here and in
  # hotset_helper_recs
  #
  if 'hotset_percentage' in runcfg:
    hstype = HOTSET
  elif 'max_bandwidth_hotset_percentage' in runcfg:
    hstype = MAX_BANDWIDTH_HOTSET
  elif 'avg_bandwidth_hotset_percentage' in runcfg:
    hstype = AVG_BANDWIDTH_HOTSET
  elif 'max_ddr_bandwidth_hotset_percentage' in runcfg:
    hstype = MAX_DDR_BANDWIDTH_HOTSET
  elif 'avg_ddr_bandwidth_hotset_percentage' in runcfg:
    hstype = AVG_DDR_BANDWIDTH_HOTSET
  elif 'max_ddr_bandwidth_orig_hotset_percentage' in runcfg:
    hstype = MAX_DDR_BANDWIDTH_ORIG_HOTSET
  elif 'avg_ddr_bandwidth_orig_hotset_percentage' in runcfg:
    hstype = AVG_DDR_BANDWIDTH_ORIG_HOTSET

  elif 'accs_hotset_percentage' in runcfg:
    hstype = ACCS_HOTSET
  elif 'max_bandwidth_accs_hotset_percentage' in runcfg:
    hstype = MAX_BANDWIDTH_ACCS_HOTSET
  elif 'avg_bandwidth_accs_hotset_percentage' in runcfg:
    hstype = AVG_BANDWIDTH_ACCS_HOTSET

  elif 'knapsack_percentage' in runcfg:
    hstype = KNAPSACK
  elif 'max_bandwidth_knapsack_percentage' in runcfg:
    hstype = MAX_BANDWIDTH_KNAPSACK
  elif 'avg_bandwidth_knapsack_percentage' in runcfg:
    hstype = AVG_BANDWIDTH_KNAPSACK
  elif 'max_ddr_bandwidth_knapsack_percentage' in runcfg:
    hstype = MAX_DDR_BANDWIDTH_KNAPSACK
  elif 'avg_ddr_bandwidth_knapsack_percentage' in runcfg:
    hstype = AVG_DDR_BANDWIDTH_KNAPSACK

  elif 'accs_knapsack_percentage' in runcfg:
    hstype = ACCS_KNAPSACK
  elif 'max_bandwidth_accs_knapsack_percentage' in runcfg:
    hstype = MAX_BANDWIDTH_ACCS_KNAPSACK
  elif 'avg_bandwidth_accs_knapsack_percentage' in runcfg:
    hstype = AVG_BANDWIDTH_ACCS_KNAPSACK
  elif 'max_ddr_bandwidth_accs_knapsack_percentage' in runcfg:
    hstype = MAX_DDR_BANDWIDTH_ACCS_KNAPSACK
  elif 'avg_ddr_bandwidth_accs_knapsack_percentage' in runcfg:
    hstype = AVG_DDR_BANDWIDTH_ACCS_KNAPSACK

  elif 'pebs_hotset_percentage' in runcfg:
    hstype = PEBS_HOTSET

  return hstype

def parse_bandwidth_profile(bench, cfg_name, profcfg_name, profcfg_results):
  cfg.read_cfg(cfg_name)

  # MRJ -- this is necessary to get the peak RSS of each site. I do not
  # really like this -- we should probably try to think of a better way of
  # getting this information
  #
  if 'three_cxt' in cfg.current_cfg['runcfg']['name']:
    profcfg_results_dir = get_results_dir(bench, 'marena_shared_site_profiling_three_cxt', 0)
    parse_bench(bench, 'marena_shared_site_profiling_three_cxt', profcfg_results_dir, profcfg_results)
  else:
    profcfg_results_dir = get_results_dir(bench, 'marena_shared_site_profiling', 0)
    parse_bench(bench, 'marena_shared_site_profiling', profcfg_results_dir, profcfg_results)
  cfg.read_cfg(cfg_name)

  prof_results_dir = results_dir + bench + '/' + profcfg_name + '/'
  idirs    = [ x+'/' for x in glob(prof_results_dir+"*") ]
  profdirs = [ x for x in idirs if not x.endswith('i0/') ]

  refres = dict()
  if bench in ['qmcpack','amg'] and 'three_cxt' in cfg.current_cfg['runcfg']['name']:
    refdir = results_dir + bench + '/' + 'marena_reference_bandwidth_three_cxt/'
    ref_idirs = [ x+'/' for x in glob(refdir+"*") ]
    avgvals = []
    maxvals = []
    for x in ref_idirs:
      parse_pcm(x, refres)
      avgvals.append(refres[AVG_DDR4_BANDWIDTH])
      maxvals.append(refres[MAX_DDR4_BANDWIDTH])

    refres[AVG_DDR4_BANDWIDTH] = (sum(avgvals)/len(avgvals))
    refres[MAX_DDR4_BANDWIDTH] = (sum(maxvals)/len(maxvals))

    print ("refavg: %3.4f refmax: %3.4f" % \
      (refres[AVG_DDR4_BANDWIDTH], refres[MAX_DDR4_BANDWIDTH]))

  else:
    for x in idirs:
      if x.endswith('i0/'):
        parse_pcm(x, refres)
        #print("refres: avg: %5.2f max: %5.2f" % \
        #      (refres[AVG_DDR4_BANDWIDTH], refres[MAX_DDR4_BANDWIDTH]))

  for idir in profdirs:

    site = tuple([int(idir.split('/')[-2].strip('i'))])
    bw_results = dict()
    parse_pcm(idir, bw_results, refres=refres)
    for key in bw_results:
      profcfg_results['sites'][site][key] = bw_results[key]
    cfg.read_cfg(cfg_name)

def parse_avg_pebs(bench, profcfg_name, profcfg_results):

  avgres = dict()
  for i in range(5):
    ires = dict()
    profcfg_results_dir = get_results_dir(bench, profcfg_name, i)
    parse_bench(bench, profcfg_name, profcfg_results_dir, ires, True)

    for site in ires['sites']:
      if not site in avgres:
        avgres[site] = []
      avgres[site].append(ires['sites'][site][ACCESSES])

  profcfg_results_dir = get_results_dir(bench, profcfg_name, 0)
  parse_bench(bench, profcfg_name, profcfg_results_dir, profcfg_results, True)

  for site in profcfg_results['sites']:
    profcfg_results['sites'][site][ACCESSES] = ( sum(avgres[site]) / len(avgres[site]) )
    #print (profcfg_results['sites'][site][ACCESSES])

def get_first_mems(bench, profcfg_name):
  prof_results_dir = results_dir + bench + '/' + profcfg_name + '/'
  idirs    = [ x+'/' for x in glob(prof_results_dir+"*") ]
  profdirs = [ x for x in idirs if not x.endswith('i0/') ]

  fmems = []
  for idir in profdirs:
    site = tuple([int(idir.split('/')[-2].strip('i'))])
    val = get_first_mem(idir)
    fmems.append((site[0], val))

  return fmems

def print_first_mems(bench, profcfg_name):

  fmems = get_first_mems(bench, profcfg_name)
  for s,v in sorted(fmems, key=itemgetter(1), reverse=True):
    print ("site: %6d first_mem: %3.4f" % (s,v))

def avg_pebs_cfg(cfg_name):
  return True if "avg_pebs" in cfg_name else False

# Generates a hotset from the map of arenas in profcfg_results['arenas'].
# Accesses/byte is the value unit, size in bytes is the weight.
def generate_hotset(bench, cfg_name, profcfg_results, hstype):
  cfg.read_cfg(cfg_name)

  pctkey, accidx, gen_hotset_fn = hotset_helper_recs[hstype]
  if accidx in bwstats:
    parse_bandwidth_profile(bench, cfg_name,
      cfg.current_cfg['runcfg']['profcfg'], profcfg_results)

#  print ("ayay")
#  for site in profcfg_results['sites']:
#    accesses = profcfg_results['sites'][site][ACCESSES]
#    peak_rss = profcfg_results['sites'][site][PEAK_RSS]
#    print ("  %5d %12.8f %8.4f" % (site[0], peak_rss/(1024*1024*1024), accesses))

  if avg_pebs_cfg(cfg_name):
    parse_avg_pebs(bench, cfg.current_cfg['runcfg']['profcfg'], profcfg_results)
#    print ("byby")
#    for site in profcfg_results['sites']:
#      accesses = profcfg_results['sites'][site][ACCESSES]
#      peak_rss = profcfg_results['sites'][site][PEAK_RSS]
#      print ("  %5d %12.8f %8.4f" % (site[0], peak_rss/(1024*1024*1024), accesses))
    cfg.read_cfg(cfg_name)

  hotset = gen_hotset_fn(profcfg_results, accidx, pctkey)
  return hotset

def write_hotset(hot_aps_file, hotset, all_sites):
  # Output the hot APs file
  f = open(hot_aps_file, 'w')
  for site in all_sites:
    if not site in hotset:
      f.write("%d %d unknown\n" % (site[0], 1))
  f.close()

   # MRJ -- alt output method
#  # Output the hot APs file
#  f = open(hot_aps_file, 'w')
#  for site in sorted(hotset):
#    for ap in site:
#      f.write("%d %d unknown\n" % (ap, 2))
#  f.close()

def my_gen_hotset(bench,cfg,hstype):
  x = dict()
  xdir = (results_dir + bench + '/' + 'marena_shared_site_profiling' + '/i' + str(0) + '/')
  get_peak_rss(bench, 'default_ir', x)
  parse_bench(bench, 'marena_shared_site_profiling', xdir, x)

  generate_hotset(bench, cfg, x, hstype)

