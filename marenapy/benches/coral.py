import os
import pprint
import subprocess
import re
from marenapy.benches.bench import *
import marenapy.cfg as cfg

# Root of the benchmark suite, relative to benchmark directory
coral_root = 'coral/'

########################################
# All Benches
########################################

corals = [
  'amg',
  'lulesh',
  'qmcpack',
  'qmcpack-o0',
  'qmcpack-o1',
  'qmcpack-giantir-nocontext',
  'pennant',
  'pennant-medium'
]

for bench in corals:
  bench_suite[bench] = 'coral'

########################################
# Functions
########################################

def parse_coral(bench, cfg_name, results_dir, results):
  cfg.read_cfg(cfg_name)

def get_coral_dir():
  return benches_dir + coral_root
