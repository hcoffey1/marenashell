import os
import pprint
import subprocess
import re
from marenapy.benches.bench import *
import marenapy.cfg as cfg

# Root of the SPEC benches, relative to benchmark directory
spec_root = 'cpu2017/'

specrate_int = [
  '500.perlbench_r',
  '502.gcc_r',
  '505.mcf_r',
  '520.omnetpp_r',
  '523.xalancbmk_r',
  '525.x264_r',
  '531.deepsjeng_r',
  '541.leela_r',
  '548.exchange2_r',
  '557.xz_r',
]

specspeed_int = [
  '600.perlbench_s',
  '602.gcc_s',
  '605.mcf_s',
  '620.omnetpp_s',
  '623.xalancbmk_s',
  '625.x264_s',
  '631.deepsjeng_s',
  '641.leela_s',
  '648.exchange2_s',
  '657.xz_s',
]

specrate_fp = [
  '503.bwaves_r',
  '507.cactuBSSN_r',
  '508.namd_r',
  '510.parest_r',
  '511.povray_r',
  '519.lbm_r',
  '521.wrf_r',
  '526.blender_r',
  '527.cam4_r',
  '538.imagick_r',
  '544.nab_r',
  '549.fotonik3d_r',
  '554.roms_r',
]

specspeed_fp = [
  '603.bwaves_s',
  '607.cactuBSSN_s',
  '619.lbm_s',
  '621.wrf_s',
  '627.cam4_s',
  '628.pop2_s',
  '638.imagick_s',
  '644.nab_s',
  '649.fotonik3d_s',
  '654.roms_s',
]

cpu2017_exe_filename = {
  '500.perlbench_r' : 'perlbench_r',
  '502.gcc_r'       : 'cpugcc_r',
  '503.bwaves_r'    : 'bwaves_r',
  '505.mcf_r'       : 'mcf_r',
  '507.cactuBSSN_r' : 'cactusBSSN_r',
  '508.namd_r'      : 'namd_r',
  '510.parest_r'    : 'parest_r',
  '511.povray_r'    : 'povray_r',
  '519.lbm_r'       : 'lbm_r',
  '520.omnetpp_r'   : 'omnetpp_r',
  '521.wrf_r'       : 'wrf_r',
  '523.xalancbmk_r' : 'cpuxalan_r',
  '525.x264_r'      : 'x264_r',
  '526.blender_r'   : 'blender_r',
  '527.cam4_r'      : 'cam4_r',
  '531.deepsjeng_r' : 'deepsjeng_r',
  '538.imagick_r'   : 'imagick_r',
  '541.leela_r'     : 'leela_r',
  '544.nab_r'       : 'nab_r',
  '548.exchange2_r' : 'exchange2_r',
  '544.nab_r'       : 'nab_r',
  '549.fotonik3d_r' : 'fotonik3d_r',
  '554.roms_r'      : 'roms_r',
  '557.xz_r'        : 'xz_r',
}

########################################
# All Benches
########################################

specrate    = specrate_int  + specrate_fp
specspeed   = specspeed_int + specspeed_fp
all_cpu2017 = specrate + specspeed

simint      = [x for x in specrate_int \
               if not x in ['548.exchange2_r', '531.deepsjeng_r']]
simfp       = [x for x in specrate_fp \
               if not x in ['511.povray_r', '519.lbm_r']]
simrate     = simint + simfp

for bench in all_cpu2017:
  bench_suite[bench] = 'cpu2017'

########################################
# Functions
########################################
def get_cpu2017_dir():
  return benches_dir + spec_root

def benchspec_dir(bench):
  return suites_dir + ('cpu2017/benchspec/CPU/%s/'%bench)

#########################################
## Floating Point
#########################################
#
#fp_benches = [
#  'bwaves',
#  'cactubssn',
#  'lbm',
#  'wrf',
#  'cam4',
#  'pop2',
#  'imagick',
#  'nab',
#  'fotonik3d',
#  'roms'
#]
#
#########################################
## Integer
#########################################
#
#int_benches = [
#  'perlbench',
#  'gcc',
#  'mcf',
#  'omnetpp',
#  'xalancbmk',
#  'x264',
#  'deepsjeng',
#  'leela',
#  'exchange2',
#  'xz'
#]
#
#
#########################################
## By Language
#########################################
#
#c_benches = [
#  'cactubssn',
#  'povray',
#  'lbm',
#  'wrf',
#  'blender',
#  'cam4',
#  'pop2',
#  'imagick',
#  'nab',
#  'perlbench',
#  'gcc',
#  'mcf',
#  'x264',
#  'xz'
#]
#
#cxx_benches = [
#  'cactubssn',
#  'namd',
#  'parest',
#  'povray',
#  'blender',
#  'omnetpp',
#  'xalancbmk',
#  'deepsjeng',
#  'leela'
#]
#
#fortran_benches = [
#  'bwaves',
#  'cactubssn',
#  'wrf',
#  'cam4',
#  'pop2',
#  'fotonik3d',
#  'roms'
#  'exchange2',
#]

