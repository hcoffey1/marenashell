from marenapy.paths import *
from marenapy.tools import *

from statistics import mean,stdev
from glob import glob

########################################
# Stats
########################################
EXE_TIME     = 'exe_time'
RUNTIME      = 'runtime'
ALLOCS       = 'allocs'
CHUNK_ALLOCS = 'chunk_allocs'
PAGE_RSS     = 'peak_rss'
ACCESSES     = 'accesses'
PEAK_SIZE    = 'peak_size'
NUM_SITES    = 'num_sites'

TOTAL_HEAP_PAGES     = "total_heap_pages"
TOTAL_HEAP_LINES     = "total_heap_lines"
TOTAL_HEAP_HITS      = "total_heap_hits"
TOTAL_HEAP_MISSES    = "total_heap_misses"
TOTAL_UNKN_PAGES     = "total_unkn_pages"
TOTAL_UNKN_LINES     = "total_unkn_lines"
TOTAL_UNKN_HITS      = "total_unkn_hits"
TOTAL_UNKN_MISSES    = "total_unkn_misses"
TOTAL_PAGES          = "total_pages"
TOTAL_LINES          = "total_lines"
TOTAL_HITS           = "total_hits"
TOTAL_MISSES         = "total_misses"
TOTAL_ACCS           = "total_accs"
TOTAL_READS          = "total_reads"
TOTAL_WRITES         = "total_writes"
HEAP_ACCS            = "heap_accs"
UNKN_ACCS            = "unkn_accs"
READ_HITS            = "read_hits"
READ_MISSES          = "read_misses"
WRITE_HITS           = "write_hits"
WRITE_MISSES         = "write_misses"
WRITEBACK            = "writeback"
SYMBOLS              = "symbols"

KNOWN_PAGES_RATIO    = "known_pages_ratio"
KNOWN_LINES_RATIO    = "known_lines_ratio"
KNOWN_HITS_RATIO     = "known_hits_ratio"
KNOWN_MISSES_RATIO   = "known_misses_ratio"
KNOWN_ACCS_RATIO     = "known_accs_ratio"
TOTAL_HITS_RATIO     = "total_hits_ratio"
TOTAL_MISSES_RATIO   = "total_misses_ratio"
HEAP_HITS_RATIO      = "heap_hits_ratio"
UNKN_HITS_RATIO      = "unkn_hits_ratio"

SITE_HITS_RATIO      = "site_hits_ratio"
SITE_MISSES_RATIO    = "site_misses_ratio"
SITE_RDWR_RATIO      = "site_rdwr_ratio"
SITE_RDWR_MISS_RATIO = "site_rdwr_miss_ratio"
PAGE_RSS_RATIO       = "page_rss_ratio"
TOTAL_ACCS_RATIO     = "total_accs_ratio"


ALL_SITES = "ALL_SITES"
THERMOS   = "THERMOS"
SORT      = "SORT"
CUT       = "CUT"
KNAPSACK  = "KNAPSACK"
HOTSET    = "HOTSET"

BANDWIDTH_PER_BYTE = "BANDWIDTH-PER-BYTE"
WRITES_PER_BYTE    = "WRITES-PER-BYTE"
RDWR_RATIO         = "RDWR-RATIO"

TOUCHED       = "touch"
ALLOCATED     = "alloc"
PAGE_RSS      = "pgrss"
LINE_RSS      = "clrss"
PRE_READS     = "prerds"
PRE_WRITES    = "prewrs"
POST_READS    = "postrds"
POST_WRITES   = "postwrs"
PC_ACCESSES   = "pcaccs"
MC_ACCESSES   = "mcaccs"
MISSES        = "miss"
BANDWIDTH     = "bw"
BW_PER_LINE   = "bw_line"
PC_HIT_RATE   = "pc_hits"
MC_HIT_RATE   = "mc_hits"
PRE_RW_RATIO  = "prerw"
POST_RW_RATIO = "postrw"
LIFETIME      = "lifetime"
LIFETIME_PCT  = "lifetime_pct"
AGG           = "agg"
SITES         = "sites"
SIZE          = "size"
SIZE_BUCKET   = "size_bucket"
TYPE_SIG      = "type_sig"
ACC_SIG       = "acc_sig"
ALLOC_PHASE   = "alloc_phase"
PHASE_SITES   = "ph_sites"
PHSZ_SITES    = "phsz_sites"
PHASE_SIG     = "ph_sig"
PHASE_SIZE    = "ph_size"
TYPE_SIZE     = "type_size"

pinfiles = [
  'mc_stats.out',          'site_cxt_strs.out',
  'cache_line_info.out',   'page_info.out',
  'site_cxt0_info.out',    'site_cxt1_info.out',
  'site_cxt2_info.out',    'site_cxt4_info.out',
  'site_cxt8_info.out',    'site_cxt0_stats.out',
  'site_cxt1_stats.out',   'site_cxt2_stats.out',
  'site_cxt4_stats.out',   'site_cxt8_stats.out',
  'site_cxt0_cxts.out',    'site_cxt1_cxts.out',
  'site_cxt2_cxts.out',    'site_cxt4_cxts.out',
  'site_cxt8_cxts.out',    'size_info.out',
  'size_bucket_info.out',  'type_sig_info.out',
  'acc_sig_info.out',      'size_stats.out',
  'size_bucket_stats.out', 'type_sig_stats.out',
  'acc_sig_stats.out',     'type_strs.out',
  'acc_strs.out',          'type_sigs.out',
  'acc_sigs.out',          'obj_info.out',
  'size_typesigs.out',     'size_typesig_info.out',
  'phase_size.out',        'phase_size_info.out',
  'alloc_phase_info.out',  'phase_site0_info.out',
  'phase_site1_info.out',  'phase_site2_info.out',
  'phase_site4_info.out',  'phase_site8_info.out',
  'phase_site0.out',       'phase_site1.out',
  'phase_site2.out',       'phase_site4.out',
  'phase_site8.out',       'phsz_site0_info.out',
  'phsz_site1_info.out',   'phsz_site2_info.out',
  'phsz_site4_info.out',   'phsz_site8_info.out',
  'phsz_site0.out',        'phsz_site1.out',
  'phsz_site2.out',        'phsz_site4.out',
  'phsz_site8.out',        'phase_bbvs.out',
  'phase_sigs.out',        'phase_sig_info.out',
  'phase_info.out',        'vall_phase_info.out',
  'vobj_phase_info.out',   'ins_count.out'
] 

pin_cxt_lengths = [0,1,2,4,8]
UNIQUE_PHASE_THRESHOLD = 524288

marena_stats = [
  ALLOCS,
  CHUNK_ALLOCS,
  NUM_SITES
]

site_info_stats = [
  TOTAL_HEAP_PAGES,
  TOTAL_HEAP_LINES,
  TOTAL_HEAP_HITS,
  TOTAL_HEAP_MISSES,
  TOTAL_UNKN_PAGES,
  TOTAL_UNKN_LINES,
  TOTAL_UNKN_HITS,
  TOTAL_UNKN_MISSES,
  TOTAL_PAGES,
  TOTAL_LINES,
  TOTAL_HITS,
  TOTAL_MISSES,
  TOTAL_ACCS,
  HEAP_ACCS,
  KNOWN_PAGES_RATIO,
  KNOWN_LINES_RATIO,
  KNOWN_HITS_RATIO,
  KNOWN_MISSES_RATIO,
  KNOWN_ACCS_RATIO,
  TOTAL_HITS_RATIO,
  HEAP_HITS_RATIO,
  UNKN_HITS_RATIO,
]

object_info_stats = [
  ( "SITE_CXT0_ID",  int   ),
  ( "SITE_CXT1_ID",  int   ),
  ( "SITE_CXT2_ID",  int   ),
  ( "SITE_CXT3_ID",  int   ),
  ( "SITE_CXT4_ID",  int   ),
  ( "SITE_CXT5_ID",  int   ),
  ( "SITE_CXT6_ID",  int   ),
  ( "SITE_CXT7_ID",  int   ),
  ( "SITE_CXT8_ID",  int   ),
  ( "SIZE",          int   ),
  ( "SIZE_BUCKET",   int   ),
  ( "TYPE_SIG",      int   ),
  ( "ACC_SIG",       int   ),
  ( "LINES",         int   ),
  ( "PC_ACCS",       int   ),
  ( "MC_ACCS",       int   ),
  ( "BANDWIDTH",     int   ),
  ( "LIFETIME",      int   ),
  ( "PC_HIT_RATE",   float ),
  ( "MC_HIT_RATE",   float ),
  ( "PRE_RW_RATIO",  float ),
  ( "POST_RW_RATIO", float ),
]

object_phase_info_stats = [
  ( "SITE_CXT0_ID",   int   ),
  ( "SITE_CXT1_ID",   int   ),
  ( "SITE_CXT2_ID",   int   ),
  ( "SITE_CXT4_ID",   int   ),
  ( "SITE_CXT8_ID",   int   ),
  ( "SIZE",           int   ),
  ( "SIZE_BUCKET",    int   ),
  ( "TYPE_SIG",       int   ),
  ( "ACC_SIG",        int   ),
  ( "ALLOC_PHASE",    int   ),
  ( "PHASE_SITE0_ID", int   ),
  ( "PHASE_SITE1_ID", int   ),
  ( "PHASE_SITE2_ID", int   ),
  ( "PHASE_SITE4_ID", int   ),
  ( "PHASE_SITE8_ID", int   ),
  ( "PHSZ_SITE0_ID",  int   ),
  ( "PHSZ_SITE1_ID",  int   ),
  ( "PHSZ_SITE2_ID",  int   ),
  ( "PHSZ_SITE4_ID",  int   ),
  ( "PHSZ_SITE8_ID",  int   ),
  ( "PHASE_SIG",      int   ),
  ( "TYPE_SIZE",      int   ),
  ( "PHASE_SIZE",     int   ),
  ( "LINES",          int   ),
  ( "PC_ACCS",        int   ),
  ( "MC_ACCS",        int   ),
  ( "BANDWIDTH",      int   ),
  ( "LIFETIME",       int   ),
  ( "PC_HIT_RATE",    float ),
  ( "MC_HIT_RATE",    float ),
  ( "PRE_RW_RATIO",   float ),
  ( "POST_RW_RATIO",  float ),
]

size_idx     = 5
szbucket_idx = 6
tsig_idx     = 7
asig_idx     = 8
aph_idx      = 9
phsite_idx   = 10
phszst_idx   = 15
phsig_idx    = 20
typesz_idx   = 21
phsz_idx     = 22

lines_idx    = 0
pcaccs_idx   = 1
mcaccs_idx   = 2
bw_idx       = 3
lifetime_idx = 4
pchits_idx   = 5
mchits_idx   = 6
prerw_idx    = 7
postrw_idx   = 8

site_report_stats = [
  PAGE_RSS,
  TOTAL_HITS,
  TOTAL_MISSES,
  PAGE_RSS_RATIO,
#  TOTAL_HITS_RATIO,
  TOTAL_MISSES_RATIO,
  TOTAL_ACCS_RATIO,
#  SITE_HITS_RATIO,
  SITE_MISSES_RATIO,
#  SITE_RDWR_RATIO,
  SITE_RDWR_MISS_RATIO,
]

int_stats = [
  PAGE_RSS,
  LINE_RSS,
  ACCESSES,
  MISSES,
  BANDWIDTH
]

MEAN   = "mean"
STDEV  = "stdev"
MIN    = "min"
MAX    = "max"
RAWVAL = "rawval"

ALL     = "all"
OBJECTS = "objects"
DATA    = "data"
BENCH   = "bench"

style_idx = {
  MEAN   : 0,
  STDEV  : 1,
  MIN    : 2,
  MAX    : 3,
  RAWVAL : 4
}

short_style = {
  MEAN   : "av",
  STDEV  : "st",
  MIN    : "mn",
  MAX    : "mx",
  RAWVAL : "rv"
}

object_report_stats = [
  (TOUCHED     , RAWVAL),
  (LINE_RSS    , RAWVAL),
  (LINE_RSS    , MEAN),
  (LINE_RSS    , STDEV),
  (BANDWIDTH   , RAWVAL),
  (BANDWIDTH   , MEAN),
  (BANDWIDTH   , STDEV),
#  (PC_HIT_RATE , MEAN),
#  (RW_RATIO    , MEAN)
]


base_stats = [
  ("objects",   "%9s",  "%9d"),
  ("pre-RD",    "%12s", "%12d"),
  ("pre-WR",    "%12s", "%12d"),
  ("post-RD",   "%9s",  "%9.1f"),
  ("post-WR",   "%9s",  "%9.1f"),
  ("dyninst",   "%12s", "%12d"),
  ("bw",        "%9s",  "%9.1f"),
  ("pc-accs",   "%12s", "%12d"),
  ("pc-hits",   "%12s", "%12d"),
  ("pc-rate",   "%9s",  "%9.4f"),
  ("mc-accs",   "%12s",  "%12d"),
  ("mc-hits",   "%12s",  "%12d"),
  ("mc-rate",   "%9s",  "%9.4f"),
  ("data",      "%9s",  "%9.1f"),
#  ("bwpb",      "%9s",  "%9.1f"),
#  ("wrpb",      "%9s",  "%9.1f"),
#  ("BPKI",      "%9s",  "%9.1f"),
#  ("MPKI",      "%9s",  "%9.1f"),
#  ("ot-obj-r",  "%9s", "%9.3f"),
#  ("ot-data-r", "%9s", "%9.3f"),
#  ("ot-bw-r",   "%9s", "%9.3f"),
]

class_stats = [
  ("objects",   "%9s", "%9d"),
  ("sizes",     "%9s", "%9d"),
  ("szbckts",   "%9s", "%9d"),
  ("tsigs",     "%9s", "%9d"),
  ("typesz",    "%9s", "%9d"),
  ("phsz",      "%9s", "%9d"),
  ("aphs",      "%9s", "%9d"),
  ("phsigs",    "%9s", "%9d"),
  ("asigs",     "%9s", "%9d"),
  ("scxt0",     "%9s", "%9d"),
  ("scxt1",     "%9s", "%9d"),
  ("scxt2",     "%9s", "%9d"),
  ("scxt4",     "%9s", "%9d"),
  ("scxt8",     "%9s", "%9d"),
  ("phsite0",   "%9s", "%9d"),
  ("phsite1",   "%9s", "%9d"),
  ("phsite2",   "%9s", "%9d"),
  ("phsite4",   "%9s", "%9d"),
  ("phsite8",   "%9s", "%9d"),
  ("phszst0",   "%9s", "%9d"),
  ("phszst1",   "%9s", "%9d"),
  ("phszst2",   "%9s", "%9d"),
  ("phszst4",   "%9s", "%9d"),
  ("phszst8",   "%9s", "%9d"),
]


#class_prefs = [
#  "bt", "zt", "yt", "lt", "tt", "it", "pt", "nt", "at", "c0", "c1", "c2",
#  "c4", "c8", "p0", "p1", "p2", "p4", "p8", "k0", "k1", "k2", "k4", "k8",
#]

class_prefs = [
  "bt", "zt", "yt", "lt",
  "pt", "nt", "at",
  "c0", "c1", "c2", "c4", "c8",
  "tt", "it", "p0", "k0",
]

obj_stats    = [ (pref + "-obj-r",  "%9s", "%9.3f") for pref in (["ot"] + class_prefs) ]
data_stats   = [ (pref + "-data-r", "%9s", "%9.3f") for pref in (["ot"] + class_prefs) ]
bw_stats     = [ (pref + "-bw-r",   "%9s", "%9.3f") for pref in (["ot"] + class_prefs) ]
wr_stats     = [ (pref + "-wr-r",   "%9s", "%9.3f") for pref in (["ot"] + class_prefs) ]
acc_stats    = [ (pref + "-oacc",   "%9s", "%9.3f") for pref in (class_prefs) ]
dacc_stats   = [ (pref + "-dacc",   "%9s", "%9.3f") for pref in (class_prefs) ]
bw_acc_stats = [ (pref + "-bwacc",  "%9s", "%9.3f") for pref in (class_prefs) ]
ppv_stats    = [ (pref + "-oppv",   "%9s", "%9.3f") for pref in (class_prefs) ]
dppv_stats   = [ (pref + "-dppv",   "%9s", "%9.3f") for pref in (class_prefs) ]
bw_ppv_stats = [ (pref + "-bwppv",  "%9s", "%9.3f") for pref in (class_prefs) ]
npv_stats    = [ (pref + "-onpv",   "%9s", "%9.3f") for pref in (class_prefs) ]
dnpv_stats   = [ (pref + "-dnpv",   "%9s", "%9.3f") for pref in (class_prefs) ]
bw_npv_stats = [ (pref + "-bwnpv",  "%9s", "%9.3f") for pref in (class_prefs) ]

ft_tp_obj_stats  = [ (pref + "-otps",   "%9s", "%9d") for pref in (class_prefs) ]
ft_tn_obj_stats  = [ (pref + "-otns",   "%9s", "%9d") for pref in (class_prefs) ]
ft_fp_obj_stats  = [ (pref + "-ofps",   "%9s", "%9d") for pref in (class_prefs) ]
ft_fn_obj_stats  = [ (pref + "-ofns",   "%9s", "%9d") for pref in (class_prefs) ]
ft_ap_obj_stats  = [ (pref + "-oaps",   "%9s", "%9d") for pref in (class_prefs) ]
ft_an_obj_stats  = [ (pref + "-oans",   "%9s", "%9d") for pref in (class_prefs) ]

ft_tp_data_stats = [ (pref + "-dtps",   "%9s", "%9d") for pref in (class_prefs) ]
ft_tn_data_stats = [ (pref + "-dtns",   "%9s", "%9d") for pref in (class_prefs) ]
ft_fp_data_stats = [ (pref + "-dfps",   "%9s", "%9d") for pref in (class_prefs) ]
ft_fn_data_stats = [ (pref + "-dfns",   "%9s", "%9d") for pref in (class_prefs) ]
ft_ap_data_stats = [ (pref + "-daps",   "%9s", "%9d") for pref in (class_prefs) ]
ft_an_data_stats = [ (pref + "-dans",   "%9s", "%9d") for pref in (class_prefs) ]

ft_tp_bw_stats   = [ (pref + "-bwtps",  "%9s", "%9d") for pref in (class_prefs) ]
ft_tn_bw_stats   = [ (pref + "-bwtns",  "%9s", "%9d") for pref in (class_prefs) ]
ft_fp_bw_stats   = [ (pref + "-bwfps",  "%9s", "%9d") for pref in (class_prefs) ]
ft_fn_bw_stats   = [ (pref + "-bwfns",  "%9s", "%9d") for pref in (class_prefs) ]
ft_ap_bw_stats   = [ (pref + "-bwaps",  "%9s", "%9d") for pref in (class_prefs) ]
ft_an_bw_stats   = [ (pref + "-bwans",  "%9s", "%9d") for pref in (class_prefs) ]

ft_tp_obj_rates  = [ (pref + "-otpr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_tn_obj_rates  = [ (pref + "-otnr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_fp_obj_rates  = [ (pref + "-ofpr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_fn_obj_rates  = [ (pref + "-ofnr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_ap_obj_rates  = [ (pref + "-oapr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_an_obj_rates  = [ (pref + "-oanr",   "%9s", "%9.3f") for pref in (class_prefs) ]

ft_tp_data_rates = [ (pref + "-dtpr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_tn_data_rates = [ (pref + "-dtnr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_fp_data_rates = [ (pref + "-dfpr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_fn_data_rates = [ (pref + "-dfnr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_ap_data_rates = [ (pref + "-dapr",   "%9s", "%9.3f") for pref in (class_prefs) ]
ft_an_data_rates = [ (pref + "-danr",   "%9s", "%9.3f") for pref in (class_prefs) ]

ft_tp_bw_rates   = [ (pref + "-bwtpr",  "%9s", "%9.3f") for pref in (class_prefs) ]
ft_tn_bw_rates   = [ (pref + "-bwtnr",  "%9s", "%9.3f") for pref in (class_prefs) ]
ft_fp_bw_rates   = [ (pref + "-bwfpr",  "%9s", "%9.3f") for pref in (class_prefs) ]
ft_fn_bw_rates   = [ (pref + "-bwfnr",  "%9s", "%9.3f") for pref in (class_prefs) ]
ft_ap_bw_rates   = [ (pref + "-bwapr",  "%9s", "%9.3f") for pref in (class_prefs) ]
ft_an_bw_rates   = [ (pref + "-bwanr",  "%9s", "%9.3f") for pref in (class_prefs) ]


PC_ACCS           = "pc_accs"
MC_ACCS           = "mc_accs"
PC_HITS           = "pc_hits"
MC_HITS           = "mc_hits"

LINE_RSS_SCALE    = "line_rss_scale"
PC_ACCS_SCALE     = "pc_accs_scale"
MC_ACCS_SCALE     = "mc_accs_scale"
BANDWIDTH_SCALE   = "bandwidth_scale"
POST_WRITES_SCALE = "post_writes_scale"
LIFETIME_SCALE    = "lifetime_scale"
BWPB_SCALE        = "bwpb_scale"
WRPB_SCALE        = "wrpb_scale"
PC_HITS_SCALE     = "pc_hits_scale"
MC_HITS_SCALE     = "mc_hits_scale"

raw_feature_stats = [
  OBJECTS,
  LINE_RSS,
  PC_ACCS,
  MC_ACCS,
  BANDWIDTH,
  POST_WRITES,
  BANDWIDTH_PER_BYTE,
  WRITES_PER_BYTE,
  PC_HIT_RATE,
  MC_HIT_RATE,
  LIFETIME,
]

scale_feature_stats = [
  LINE_RSS_SCALE,
  PC_ACCS_SCALE,
  MC_ACCS_SCALE,
  BANDWIDTH_SCALE,
  POST_WRITES_SCALE,
  BWPB_SCALE,
  WRPB_SCALE,
  PC_HITS_SCALE,
  MC_HITS_SCALE,
  LIFETIME_SCALE
]

feature_stats = raw_feature_stats + scale_feature_stats

short_feature_stats = {
  OBJECTS            : "objs",
  LINE_RSS           : "clrss",
  PC_ACCS            : "pc-acc",
  MC_ACCS            : "mc-acc",
  BANDWIDTH          : "bw",
  POST_WRITES        : "writes",
  BANDWIDTH_PER_BYTE : "bwpb",
  WRITES_PER_BYTE    : "wrpb",
  PC_HIT_RATE        : "pc_hit",
  MC_HIT_RATE        : "mc_hit",
  LIFETIME           : "life",
  LINE_RSS_SCALE     : "clrss-x",
  PC_ACCS_SCALE      : "pc-acc-x",
  MC_ACCS_SCALE      : "mc-acc-x",
  BANDWIDTH_SCALE    : "bw-x",
  POST_WRITES_SCALE  : "writes-x",
  BWPB_SCALE         : "bwpb-x",
  WRPB_SCALE         : "wrpb-x",
  LIFETIME_SCALE     : "life-x",
}

LOI_DIST  = 7
LOO_DIST  = 8
LOI_ERR   = 9
LOO_ERR   = 10
MATCH_LOI = 11
MATCH_LOO = 12

def_tsizes    = [1.0, 2.0, 5.0, 10.0, 20.0, 50.0]
#def_bwpb_rats = [0.001, 0.002, 0.004, 0.008, 0.016, 0.032, 0.064,
#                 0.128, 0.256, 0.512, 1.024, 2.048, 4.096, 8.912]
def_bwpb_rats = [  0.01,  0.02,  0.04, 0.08,  0.16,  0.32,  0.64,
                   0.80,  0.96,  1.12, 1.28,  1.60,  1.92,  2.24,
                   2.56,  3.84,  5.12, 7.68, 10.24, 20.48, 30.72,
                   40.96, 61.44, 81.92
                ]

def_wrpb_rats = [0.001, 0.002, 0.004, 0.008, 0.016, 0.032,
                 0.064, 0.128, 0.256, 0.512, 1.024, 2.048]

def_rdwr_rats = [1.0, 2.0, 4.0, 8.0, 16.0, 32.0, 64.0, 128.0, 256.0]
def_upper_tier_sizes = [3.125, 6.25, 12.5, 25, 50]

def_lifetime_cuts = [ (2**7), (2**10), (2**13), (2**17), (2**20), (2**21),
                      (2**22), (2**23), (2**24), (2**25), (2**26), (2**27),
                      (2**28), (2**29), (2**30), (2**31), (2**32)
                    ]

def_lifetime_pct_cuts = [ 0.99, 0.95, 0.9, 0.8, 0.5, 0.25, 0.1, 0.01 ]

PAGE_SIZE       = 4096
CACHE_LINE_SIZE = 64
OPT_IN          = "opt_in"
OPT_OUT         = "opt_out"
OPT_BENCH       = "opt_bench"

REF   = "REF"
TRAIN = "TRAIN"
DIFF  = "DIFF"

train_ref_stats = [
  ("vals",     REF,   "%12s", "%12d"),
  ("vals",     TRAIN, "%12s", "%12d"),
  ("mr-vals",  REF,   "%12s", "%12.3f"),
  ("mr-vals",  TRAIN, "%12s", "%12.3f"),
  ("objs",     REF,   "%12s", "%12d"),
  ("objs",     TRAIN, "%12s", "%12d"),
  ("mr-objs",  REF,   "%12s", "%12.3f"),
  ("mr-objs",  TRAIN, "%12s", "%12.3f"),
  ("data",     REF,   "%12s", "%12d"),
  ("data",     TRAIN, "%12s", "%12d"),
  ("mr-data",  REF,   "%12s", "%12.3f"),
  ("mr-data",  TRAIN, "%12s", "%12.3f"),
  ("bw",       REF,   "%12s", "%12d"),
  ("bw",       TRAIN, "%12s", "%12d"),
  ("mr-bw",    REF,   "%12s", "%12.3f"),
  ("mr-bw",    TRAIN, "%12s", "%12.3f"),
#  ("bwpb",     REF,   "%12s", "%12.2f"),
#  ("bwpb",     TRAIN, "%12s", "%12.2f"),
#  ("mat-bwpb", REF,   "%12s", "%12.2f"),
#  ("mat-bwpb", TRAIN, "%12s", "%12.2f"),
#  ("bwpb-avg", DIFF,  "%12s", "%12.2f"),
#  ("bwpb-abs", DIFF,  "%12s", "%12.2f"),
]

INTER_DISTANCE = "inter_distance"
INTRA_DISTANCE = "intra_distance"

scalar_features = [ BENCH, SIZE, SIZE_BUCKET, TYPE_SIG, TYPE_SIZE, \
                    ALLOC_PHASE, PHASE_SIZE, PHASE_SIG, ACC_SIG  ]
site_features   = [ SITES, PHASE_SITES, PHSZ_SITES ]
all_features    = scalar_features + site_features
phase_features  = [ ALLOC_PHASE, PHASE_SIZE, PHASE_SIG, PHASE_SITES, \
                    PHSZ_SITES ]

phase_cfgs = [ 'pin_phase_lpc_9cxt',
               'pin_phase_mpc_12_5_9cxt',
               'pin_phase_mpc_12_5_9cxt_train',
               'pin_phase2_lpc_9cxt',
               'pin_phase_lpc_9cxt_train', 
               'pin_phase_lpc_9cxt_test', 
             ]

geomean_agg_stats = [
]

no_agg_stats = [
]

########################################
# Benchmarks
########################################

# A dict relating benchmarks to the suite that they're in
bench_suite = {}

def convert_to_seconds(time):
  parts = time.split(':')
  hours = minutes = seconds = 0.0
  if len(parts) > 2:
    hours = float(parts[0])
    parts = parts[1:]

  minutes = float(parts[0])
  seconds = float(parts[1])
  return ( (hours*3600.0) + (minutes*60.0) + seconds )

def convert_to_bytes(kbytes):
  return (float(kbytes) * 1024.0)

def get_peak_rss(bench, cfg_name, results):

  rss = re.compile('\tMaximum resident set size')

  results['default_peak_rss'] = []
  for fname in glob("%s%s/%s/i*/stdout0.txt"% (results_dir, bench, cfg_name)):
    fd = open(fname)
    for line in fd:
      if rss.match(line):
        results['default_peak_rss'].append(convert_to_bytes(line.split()[-1]))
        break
  results['default_peak_rss'] = mean(results['default_peak_rss'])
  print ("peak_rss: %d" % results['default_peak_rss'])

def parse_time(bench, cfg_name, results_dir, results):
  cfg.read_cfg(cfg_name)

  results[RUNTIME] = []
  results[EXE_TIME] = []
  results[PAGE_RSS] = []
  lulesh_fom  = re.compile('FOM')
  pennant_fom = re.compile('hydro cycle')
  amg_fom     = re.compile('Figure of Merit \(FOM_2\):')
  qmcpack_fom = re.compile('  QMC Execution time')
  time = re.compile('\tElapsed \(wall clock\) time')
  rss  = re.compile('\tMaximum resident set size')

  for copy in range(cfg.current_cfg['runcfg']['copies']):
    fd = open('%sstdout%d.txt' % (results_dir, copy), 'r')
    fd.seek(0, 0)
    if bench in ['lulesh']:
      for line in fd:
        if lulesh_fom.match(line):
          results[EXE_TIME].append( ( (1.0/float(line.split()[-2])) * 1000000.0) )
          #results[EXE_TIME].append(float(line.split()[-2]))
        elif rss.match(line):
          results[PAGE_RSS].append(convert_to_bytes(line.split()[-1]))
    elif bench in ['pennant', 'pennant-medium']:
      for line in fd:
        if pennant_fom.match(line):
          results[EXE_TIME].append( ( float(line.split()[-1]) ) )
        elif rss.match(line):
          results[PAGE_RSS].append(convert_to_bytes(line.split()[-1]))
    elif bench in ['amg']:
      for line in fd:
        if amg_fom.match(line):
          results[EXE_TIME].append( (1.0/( float(line.split()[-1])) ) )
          #results[EXE_TIME].append( (float(line.split()[-1]))) 
        elif rss.match(line):
          results[PAGE_RSS].append(convert_to_bytes(line.split()[-1]))
    elif bench in ['qmcpack']:
      for line in fd:
        if qmcpack_fom.match(line):
          results[EXE_TIME].append( ( float(line.split()[-2]) ) )
          #results[EXE_TIME].append( ( 256*16/float(line.split()[-2]) ) )
        elif rss.match(line):
          results[PAGE_RSS].append(convert_to_bytes(line.split()[-1]))
    else:
      for line in fd:
        if time.match(line):
          results[EXE_TIME].append(convert_to_seconds(line.split()[-1]))
        elif rss.match(line):
          results[PAGE_RSS].append(convert_to_bytes(line.split()[-1]))
    fd.seek(0, 0)
    for line in fd:
      if time.match(line):
        results[RUNTIME].append(convert_to_seconds(line.split()[-1]))

  try:
    results[EXE_TIME] = max(results[EXE_TIME])
    results[RUNTIME] = max(results[RUNTIME])
    results[PAGE_RSS] = sum(results[PAGE_RSS])
  except:
    results[EXE_TIME] = 0.0
    results[RUNTIME] = 0.0
    results[PAGE_RSS] = 0.0

# Need to be updated
def parse_marena(bench, cfg_name, results_dir, results):
  cfg.read_cfg(cfg_name)

  #print("Parsing Marena's output.")

  # Output of the destructor
  peak_rss = re.compile('VmHWM:\s+(\d+) kB')
  for copy in range(cfg.current_cfg['runcfg']['copies']):
    fd = open('%sstdout%d.txt' % (results_dir, copy), 'r')
    for line in fd:
      peak_rss_ret = peak_rss.match(line)
      if peak_rss_ret:
        # This value is in kB, convert to bytes
        results[PAGE_RSS] = int(peak_rss_ret.group(1)) * 1024

  # Output of the profiling tools
  new_arena = re.compile('Arena (\d+):')
  peak_rss = re.compile('  Peak RSS: (\d+)')
  arena_accesses = re.compile('  Accesses: (\d+)')
  arena_peak_size = re.compile('  Peak size: (\d+)')
  arena_chunk_allocs = re.compile('  Chunks: (\d+)')
  arena_allocs = re.compile('  Num Allocs: (\d+)')
  aps = re.compile('  Sites: ([\d ]+)')

  for copy in range(cfg.current_cfg['runcfg']['copies']):
    if os.path.isfile('%sprofiling.txt' % (results_dir)):
      fd = open('%sprofiling.txt' % (results_dir), 'r')
      arena = 0
      results['sites'] = {}
      results['arenas'] = {}

      for line in fd:
        # Match the line to the regexes
        new_arena_ret = new_arena.match(line)
        peak_rss_ret = peak_rss.match(line)
        arena_accesses_ret = arena_accesses.match(line)
        arena_peak_size_ret = arena_peak_size.match(line)
        arena_chunk_allocs_ret = arena_chunk_allocs.match(line)
        arena_allocs_ret = arena_allocs.match(line)
        aps_ret = aps.match(line)

        if new_arena_ret:

          arena = int(new_arena_ret.group(1))
          results['arenas'][arena] = {}
          cur_peak_rss = None
          cur_accesses = None
          cur_peak_size = None
          cur_chunk_allocs = None
          cur_allocs = None

        elif aps_ret:

          site = tuple(set(map(int, aps_ret.group(1).split())))
          if not site in results['sites']:
            results['sites'][site] = {}
            results['sites'][site][PAGE_RSS]     = cur_peak_rss
            results['sites'][site][ACCESSES]   = cur_accesses
            results['sites'][site][PEAK_SIZE]  = cur_peak_size
            results['sites'][site][CHUNK_ALLOCS] = cur_chunk_allocs
            results['sites'][site][ALLOCS] = cur_allocs

          else:
            if cur_peak_rss is not None:
              results['sites'][site][PAGE_RSS]   += cur_peak_rss
            if cur_accesses is not None:
              results['sites'][site][ACCESSES] += cur_accesses
            results['sites'][site][PEAK_SIZE]  += cur_peak_size
            results['sites'][site][CHUNK_ALLOCS] += cur_chunk_allocs
            if cur_allocs is not None:
              results['sites'][site][ALLOCS] += cur_allocs

        elif peak_rss_ret:
          cur_peak_rss = int(peak_rss_ret.group(1))
          results['arenas'][arena][PAGE_RSS] = cur_peak_rss
        elif arena_accesses_ret:
          cur_accesses = int(arena_accesses_ret.group(1))
          results['arenas'][arena][ACCESSES] = cur_accesses
        elif arena_peak_size_ret:
          cur_peak_size = int(arena_peak_size_ret.group(1))
          results['arenas'][arena][PEAK_SIZE] = cur_peak_size
        elif arena_chunk_allocs_ret:
          cur_chunk_allocs = int(arena_chunk_allocs_ret.group(1))
          results['arenas'][arena][CHUNK_ALLOCS] = cur_chunk_allocs
        elif arena_allocs_ret:
          cur_allocs = int(arena_allocs_ret.group(1))
          results['arenas'][arena][ALLOCS] = cur_allocs
  
  results[PAGE_RSS] = 0
  if 'sites' in results:
    for site in list(results['sites']):
      if (PAGE_RSS in results['sites'][site]) and (results['sites'][site][PAGE_RSS] is not None):
        results[PAGE_RSS] += results['sites'][site][PAGE_RSS]

  # Aggregate results
  results[ALLOCS] = 0
  results[CHUNK_ALLOCS] = 0
  if 'sites' in results:
    for site in list(results['sites']):
      if results['sites'][site][ALLOCS] != None:
        results[ALLOCS] += results['sites'][site][ALLOCS]
      results[CHUNK_ALLOCS] += results['sites'][site][CHUNK_ALLOCS]

  results[NUM_SITES] = len(results['sites'])

  #print("Finished parsing Marena's output.")

# Import the individual suites
from marenapy.benches.cpu2017 import *
from marenapy.benches.coral import *

def parse_bench(bench, cfg, results_dir, results, get_profiling=True):
  parse_time(bench, cfg, results_dir, results)
  if get_profiling:
    parse_marena(bench, cfg, results_dir, results)
  func = 'parse_' + bench_suite[bench] + '(\'' + \
         bench + '\',\'' + \
         cfg + '\',\'' + \
         results_dir + '\',' + \
         'results' + '' + ')'
  eval(func)

def get_bench_dir(bench):
  func = 'get_' + bench_suite[bench] + '_dir' + '()'
  return eval(func) + bench + '/'
