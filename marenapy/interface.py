import pprint
import numpy
from operator import itemgetter
from marenapy.results import *
import marenapy.cfg as cfg
from marenapy.buildcfg import *
from marenapy.runcfg import *
from marenapy.offline import *
from marenapy.plot import *
from subprocess import check_output, STDOUT

from string import ascii_lowercase
from math import log10, floor

def geo_mean(iterable):
  a = numpy.array(iterable)
  return a.prod()**(1.0/len(a))

def runexps(benches=[], cfgs=[], runs=None, iters=1, start=0, \
  wait=True):

  for bench in benches:
    for cfg_name in cfgs:
      cfg.read_cfg(cfg_name)

      # Collect profcfg results
      profcfg_results = dict()
      if 'peak_rss_cfg' in cfg.current_cfg['runcfg']:
        get_peak_rss(bench, cfg.current_cfg['runcfg']['peak_rss_cfg'],
                     profcfg_results)

      if 'profcfg' in cfg.current_cfg['runcfg']:
        get_profile_info(bench, cfg_name)

      # Each iteration will have a different arena in the hotset
      # Note -- only works with shared_site_profiling as the profcfg
      #
      # XXX: MRJ -- some recent changes to how we setup the run and results
      # directories broke this code -- but it shouldn't be too hard to fix
      #
      if ('bandwidth_profile' in cfg.current_cfg['runcfg']):
        run_bandwidth_profile(bench, cfg_name)
 
      # Run the iterations normally
      else:
        for it in range(start, (start+iters)):
          print('Preparing %s with %s on iter %d.' % (bench, cfg_name, it))
          prepare_runcfg(bench, cfg_name, it, runs=runs)

          if not wait and cfg.current_cfg['runcfg']['tools']:
            print("Cannot do concurrent runs with tools")
            return
          else:
            run_tools(bench, cfg_name, it, profcfg_results=profcfg_results)

          print('Running %s with %s.' %(bench, cfg_name))
          run_runcfg(bench, cfg_name, it, runs=runs, wait=wait)
          kill_tools()

def validate_exps(benches=[], cfgs=[], iters=1):

  cwd = os.getcwd()
  for bench in benches:
    for cfg_name in cfgs:
      cfg.read_cfg(cfg_name)

      for it in range(iters):
        ncopies = cfg.current_cfg['runcfg']['copies']
        print("validating %d copies for %s-%s-i%d ... " % \
              (ncopies, bench, cfg_name, it), end='')

        problems = False
        for copy in range(ncopies):
          copy_dir = get_copy_dir(bench, cfg_name, it, copy)
          os.chdir(copy_dir)
          try:
            check_output('./compare.sh', stderr=STDOUT, shell=True)
          except:
            print ("problem in copy%d" % copy)
            problems = True
            break

        if not problems:
          print("ok.")

  os.chdir(cwd)

def get_profile_info(bench, cfg_name):

  # First parse the results of the profcfg for this bench
  profcfg_name = cfg.current_cfg['runcfg']['profcfg']

  do_profiling = False if 'bandwidth_profile' in profcfg_name else True
  profcfg_results_dir = get_results_dir(bench, profcfg_name)
  parse_bench(bench, profcfg_name, profcfg_results_dir, profcfg_results, do_profiling)

  cfg.read_cfg(cfg_name)
  hstype = get_hstype(cfg.current_cfg['runcfg'])
  if hstype != None:
    pctkey, accidx, gen_hotset_fn = hotset_helper_recs[hstype]

    print("Generating a %5.3f %s." % \
          (cfg.current_cfg['runcfg'][pctkey], hstype))

    hotset = generate_hotset(bench, cfg_name, profcfg_results, hstype)

    print ("hotset: %d (site, peak_rss, accs)" % len(hotset))
    for site in hotset:
      print ("  %6d %8.1f %8.1f" % (site[0],
              (float(profcfg_results['sites'][site][PEAK_RSS])/(1024*1024)),
              profcfg_results['sites'][site][accidx] ))

    buildcfg_name = cfg.current_cfg['buildcfg']['name']
    hot_aps_file = get_bench_dir(bench) + 'exe/' + buildcfg_name + '/hotset.txt'
    write_hotset(hot_aps_file, hotset, profcfg_results['sites'])

#        if 'hotset_percentage' in cfg.current_cfg['runcfg']:
#        elif 'knapsack_percentage' in cfg.current_cfg['runcfg']:
#          generate_hotset(bench, cfg_name, profcfg_results, hstype=KNAPSACK)
#        elif 'max_bandwidth_hotset_pct' in cfg.current_cfg['runcfg']:
#          generate_bandwidth_hotset(bench, cfg_name, profcfg_results, hstype=MAX_BANDWIDTH_HOTSET)
#        elif 'avg_bandwidth_hotset_pct' in cfg.current_cfg['runcfg']:
#          get_results_dict(benches=[bench], cfgs=[cfg.current_cfg['runcfg']['profcfg']], \
#            iters=len(profcfg_results['sites']), stat=AVG_MCDRAM_BANDWIDTH)
#          generate_bandwidth_hotset(bench, cfg_name, profcfg_results, hstype=AVG_BANDWIDTH_HOTSET)

def run_bandwidth_profile(bench, cfg_name):

  buildcfg_name = cfg.current_cfg['buildcfg']['name']
  hot_aps_file = get_bench_dir(bench) + 'exe/' + buildcfg_name + '/hotset.txt'

  sorted_sites = sorted( [ (x,y[PEAK_RSS]) for x,y in \
                            profcfg_results['sites'].items() ], \
                         key=itemgetter(1), reverse=True )

  all_sites = [s for s,t in sorted_sites]

  # for qmcpack:
#  fmems = get_first_mems(bench, cfg.current_cfg['runcfg']['profcfg'])
#  all_sites = [s for s,t in sorted_sites \
#               if s[0] in [p[0] for p in fmems if p[0] > 800] ]
#  print (all_sites)

  # get the baseline cfg
#  results_dir = create_results_dir(bench, cfg_name, 0)
#  f = open(hot_aps_file, 'w')
#  f.write("\n")
#  f.close()
#  print('Preparing %s with %s on iter %d.' % (bench, cfg_name, 0))
#  prepare_runcfg(bench, cfg_name)
#  run_tools(bench, cfg_name, results_dir, profcfg_results=profcfg_results)
#  print('Running %s with %s.' %(bench, cfg_name))
#  run_runcfg(bench, cfg_name, results_dir)
#  kill_tools()

  # Get the number of arenas from the profcfg
  for my_site in all_sites:
    f = open(hot_aps_file, 'w')
    iso_mem = cfg.current_cfg['runcfg']['bandwidth_profile']
    f.write("%d %d unknown\n" % (my_site[0], iso_mem))
    f.close()
    results_dir = create_results_dir(bench, cfg_name, my_site[0])
    print('Preparing %s with %s on iter %d.' % (bench, cfg_name, my_site[0]))
    prepare_runcfg(bench, cfg_name)
    run_tools(bench, cfg_name, my_site[0], profcfg_results=profcfg_results)
    print('Running %s with %s.' %(bench, cfg_name))
    run_runcfg(bench, cfg_name, my_site[0])
    kill_tools()

#  my_site = [s for s in all_sites][81]
#  f = open(hot_aps_file, 'w')
#  iso_mem = cfg.current_cfg['runcfg']['bandwidth_profile']
#  f.write("%d %d unknown\n" % (my_site[0], iso_mem))
#  f.close()
#  results_dir = create_results_dir(bench, cfg_name, my_site[0])
#  print('Preparing %s with %s on iter %d.' % (bench, cfg_name, my_site[0]))
#  prepare_runcfg(bench, cfg_name)
#  run_tools(bench, cfg_name, results_dir, profcfg_results=profcfg_results)
#  print('Running %s with %s.' %(bench, cfg_name))
#  run_runcfg(bench, cfg_name, results_dir)
#  kill_tools()
#
#  tmp_sites = [s for s in all_sites][143:]
#  for my_site in tmp_sites:
#    f = open(hot_aps_file, 'w')
#    iso_mem = cfg.current_cfg['runcfg']['bandwidth_profile']
#    f.write("%d %d unknown\n" % (my_site[0], iso_mem))
#    f.close()
#    results_dir = create_results_dir(bench, cfg_name, my_site[0])
#    print('Preparing %s with %s on iter %d.' % (bench, cfg_name, my_site[0]))
#    prepare_runcfg(bench, cfg_name)
#    run_tools(bench, cfg_name, results_dir, profcfg_results=profcfg_results)
#    print('Running %s with %s.' %(bench, cfg_name))
#    run_runcfg(bench, cfg_name, results_dir)
#    kill_tools()

# cfgs can be a list of lists for the line graph
def plot(benches=[], cfgs=[], iters=1, stats=[EXE_TIME], \
  basecfg='default_ir', style=MEAN, cis=False, absolute=False, plot_type='bandwidth_profile_line', vert_angle=30, horiz_angle=30):

  if cis and iters < 2:
    cis = False

  # Goes through cfgs array and flattens it if it's a list of lists
  cfg_list = list()
  for cfg_arr in cfgs:
    if isinstance(cfg_arr, list):
      for cfg_name in cfg_arr:
        cfg_list.append(cfg_name)
    else:
      cfg_list.append(cfg_arr)

  rstrs = get_report_strs(benches, cfg_list, iters, stats[0], \
                          basecfg, style, cis, absolute)

  ## LINE PLOTS
  if plot_type == 'line':
    if len(stats) < 2:
      print('ERROR: NOT ENOUGH STATS FOR A LINE GRAPH')
      return
    if not isinstance(cfgs[0], list):
      print('ERROR: CFGS SHOULD BE A LIST OF LISTS')
      return
    rstrs2 = get_report_strs(benches, cfg_list, iters, stats[1], \
                            basecfg, style, cis, absolute)
    line_plot(rstrs, rstrs2, cfgs, stats)

  ## 2D BAR PLOTS
  elif plot_type == 'bar':
    bar_plot(rstrs, cfg_list, stats[0]) # Only requires cfgs to get the order right

  ## 2D BANDWIDTH PROFILE LINE
  elif plot_type == 'bandwidth_profile_line':
    (profcfg_results, bandwidth_profile_results) = get_ddr_bandwidth_results(benches, cfgs)
    bandwidth_profile_line_plot(benches, bandwidth_profile_results, profcfg_results, cfgs, stats)

  elif plot_type == 'cache_bar':
    rstrs2 = get_report_strs(benches, cfg_list, iters, stats[1], \
                            basecfg, style, cis, absolute)
    cache_plot(rstrs, rstrs2, cfg_list, stats) # Only requires cfgs to get the order right

  elif plot_type == 'perf_bar':
    perf_bar_plot(rstrs, cfg_list, stats[0]) # Only requires cfgs to get the order right

  elif plot_type == 'threeway_bar':
    threeway_perf_bar_plot(rstrs, cfg_list, stats[0]) # Only requires cfgs to get the order right

  elif plot_type == 'overhead':
    overhead_plot(rstrs, cfg_list, stats[0]) # Only requires cfgs to get the order right

  elif plot_type == 'pebs':
    pebs_bar_plot(rstrs, cfg_list, stats[0]) # Only requires cfgs to get the order right

  elif plot_type == 'backtrace':
    backtrace_plot(rstrs, cfg_list, stats[0]) # Only requires cfgs to get the order right

  elif plot_type == 'double_line':
    bench = benches[0]
    cfg.read_cfg(cfgs[0])
    # Get the results for the profcfg
    profcfg_results = dict()
    profcfg_results_dir = get_results_dir(bench, cfg.current_cfg['runcfg']['profcfg'], 0)
    parse_bench(bench, cfg.current_cfg['runcfg']['profcfg'], profcfg_results_dir, profcfg_results)

    bandwidth_profile_results = dict()
    bandwidth_profile_dir = results_dir + bench + '/' + cfgs[0] + '/'

    # Get the bandwidth of the baseline run
    refres = None
    idirs    = [ x+'/' for x in glob(bandwidth_profile_dir+"*") ]
    for x in idirs:
      if x.endswith('i0/'):
        refres = dict()
        parse_pcm(x, refres)
        print("refres: avg: %5.2f max: %5.2f" % \
              (refres[AVG_DDR4_BANDWIDTH], refres[MAX_DDR4_BANDWIDTH]))
    for idir in idirs:
      site = tuple([int(idir.split('/')[-2].strip('i'))])
      bandwidth_profile_results[site[0]] = dict()
      parse_pcm(idir, bandwidth_profile_results[site[0]], refres=refres)
    double_line_plot(bench, bandwidth_profile_results, profcfg_results, cfgs, stats)

  ## 3D BAR PLOTS
  elif plot_type == '3d_bar':
    bench = benches[0]
    cfg.read_cfg(cfgs[0])
    # Get the results for the profcfg
    profcfg_results = dict()
    profcfg_results_dir = get_results_dir(bench, cfg.current_cfg['runcfg']['profcfg'], 0)
    parse_bench(bench, cfg.current_cfg['runcfg']['profcfg'], profcfg_results_dir, profcfg_results)

    bandwidth_profile_results = dict()
    bandwidth_profile_dir = results_dir + bench + '/' + cfgs[0] + '/'

    # Get the bandwidth of the baseline run
    refres = None
    idirs    = [ x+'/' for x in glob(bandwidth_profile_dir+"*") ]
    for x in idirs:
      if x.endswith('i0/'):
        refres = dict()
        parse_pcm(x, refres)
        print("refres: avg: %5.2f max: %5.2f" % \
              (refres[AVG_DDR4_BANDWIDTH], refres[MAX_DDR4_BANDWIDTH]))
    for idir in idirs:
      site = tuple([int(idir.split('/')[-2].strip('i'))])
      bandwidth_profile_results[site[0]] = dict()
      parse_pcm(idir, bandwidth_profile_results[site[0]], refres=refres)
    bar_plot_3d(bench, bandwidth_profile_results, profcfg_results, cfgs, stats, vert_angle=vert_angle, horiz_angle=horiz_angle)
    
#def report_bandwidth_profile(benches=[], cfg_name='marena_bandwidth_profile', stat=MAX_MCDRAM_BANDWIDTH):
#  for bench in benches:
#    cfg.read_cfg(cfg_name)
#    # First parse the profcfg to figure out how many iters
#    profcfg_results = dict()
#    if 'profcfg' in cfg.current_cfg['runcfg']:
#      profcfg_results_dir = get_results_dir(bench, cfg.current_cfg['runcfg']['profcfg'], 0)
#      parse_bench(bench, cfg.current_cfg['runcfg']['profcfg'], profcfg_results_dir, profcfg_results)
#      parse_pcm(profcfg_results_dir, profcfg_results)
#
#    # Parse the results from the actual config in the hackiest way possible
#    bandwidth_profile_results = dict()
#    bandwidth_profile_dir = results_dir + bench + '/' + cfg_name + '/'
#    for idir in glob(bandwidth_profile_dir + "*"):
#      mydir = idir+"/"
#      site = tuple([int(idir.split('/')[-1].strip('i'))])
#      bw_results = dict()
#      parse_pcm(mydir, bw_results)
#      print("%d: %f, %d" % (site[0], bw_results[AVG_MCDRAM_BANDWIDTH], profcfg_results['sites'][site]['accesses']))

def report(benches=[], cfgs=[], iters=1, stat=EXE_TIME, \
  basecfg='default_ir', style=MEAN, cis=True, absolute=False):

  if cis and iters < 2:
    cis = False

  rstrs = get_report_strs(benches, cfgs, iters, stat, \
                          basecfg, style, cis, absolute)

  print("%s report\n" % stat)
  print("configs:")

  if not absolute:
    mycfgs = [basecfg] + [x for x in cfgs if not x == basecfg]
    labels = ["base"] + list(ascii_lowercase)[:(len(mycfgs)-1)]
  else:
    mycfgs = cfgs
    labels = list(ascii_lowercase)[:len(mycfgs)]

  for expcfg,label in zip(mycfgs, labels):
    print("  %4s : %s" % (label, expcfg))
  print("")

  print ("".ljust(16),end='')
  for label in labels:
    if cis:
      print (("%s" % label).rjust(9),end='')
      print (("").rjust(7),end='')
    else:
      print (("%s" % label).rjust(16),end='')
  print ("")

  mybenches = benches + ['average']
  for bench in mybenches:
    print(("%s"%bench).ljust(16),end='')

    for expcfg in mycfgs:
      if cis:
        print( (("%s %s"%rstrs[bench][expcfg]).rjust(16)), end='')
      else:
        print( ("%s"%rstrs[bench][expcfg].rjust(16)), end='')
    print("")
  print("")

def prep_cpu2017(benches=[], sizes=[], fakeout=None, fakefake=True, clean=True):
  prep_cpu2017_build(benches, fakeout, fakefake=fakefake, clean=clean)
  prep_cpu2017_run(benches, sizes, fakeout, clean=clean)

def build(benches=[], cfgs=[], skip=[], verbose=False, use_ir_backup=False,
  use_transformed_backup=False, clean=False, nthreads=-1):
  for bench in benches:
    for cfg_name in cfgs:
      print('Building %s with %s.' % (bench, cfg_name))
      run_buildcfg(bench, cfg_name, skip=skip, verbose=verbose,
                   use_ir_backup=use_ir_backup,
                   use_transformed_backup=use_transformed_backup,
                   clean=clean, nthreads=nthreads)

def create_cfg(cfg_name, buildcfg_name, runcfg_name):
  new_cfg(cfg_name, buildcfg_name, runcfg_name)

def bandwidth_profile_report(bench, profcfg, accidx=AVG_DDR4_BANDWIDTH,
  sort=APB, hotcfg=None):
#  profcfg_results = {}
#  profcfg_results_dir = get_results_dir(bench, profcfg)
#  parse_bench(bench, profcfg, profcfg_results_dir, profcfg_results,\
#              get_profiling=True)
#  sites = profcfg_results['sites']
#  print ( sum( [ (float(v[PEAK_RSS])) for s,v in sites.items() ] ) )

  profcfg_results = {}
  profcfg_results_dir = get_results_dir(bench, profcfg)
  parse_bench(bench, profcfg, profcfg_results_dir, profcfg_results,\
              get_profiling=True)
#  parse_bench(bench, profcfg, profcfg_results_dir, profcfg_results,\
#              get_profiling=False)
#  parse_bandwidth_profile(bench, profcfg, profcfg, profcfg_results)

  sites = profcfg_results['sites']

  lst = None
  if (sort == APB):
    lst = [ ( ( (float(v[accidx])/v[PEAK_RSS] if v[PEAK_RSS] > 0 else 0.0)\
                if v[accidx] != 0 else 0.0 ), s)
            for s,v in sites.items() ]
  elif (sort == ACCS):
    lst = [ (float(v[accidx]), s) for s,v in sites.items() ]
  elif (sort == SIZE):
    lst = [ (float(v[PEAK_RSS]), s) for s,v in sites.items() ]
  else:
    print ("bad sort style: %s" % str(sort))
    return

  hotset = None
  if hotcfg:
    hotcfg_results = {}

    hotcfg_results_dir = get_results_dir(bench, profcfg)
    parse_bench(bench, profcfg, hotcfg_results_dir, hotcfg_results, get_profiling=True)
    #parse_bench(bench, profcfg, hotcfg_results_dir, hotcfg_results, get_profiling=False)

    cfg.read_cfg(hotcfg)
    hstype = hstype=get_hstype(cfg.current_cfg['runcfg'])
    hotset = generate_hotset(bench, hotcfg, hotcfg_results, hstype)

  print("site".rjust(8) + "rss".rjust(9) + "accs".rjust(9), end='')
  print("hot?".rjust(6)) if hotset != None else print("")

  sorted_sites = [ s for v,s in sorted(lst,reverse=True) ]
  for s in sorted_sites:
    print ("  %6d %8.4f %8.4f" % (s[0],
          (float(hotcfg_results['sites'][s][PEAK_RSS])/(1024*1024*1024)),
          hotcfg_results['sites'][s][accidx] ), end='')
    print ("") if hotset == None else print ("+".rjust(6)) \
      if s in hotset else print ("-".rjust(6))
#  for s in sorted_sites:
#    print ("  %6d %8.4f %8.4f" % (s[0],
#          (float(sites[s][PEAK_RSS])/(1024*1024*1024)),
#          sites[s][accidx] ), end='')
#    print ("") if hotset == None else print ("+".rjust(6)) \
#      if s in hotset else print ("-".rjust(6))

def progress():
  benches = ['imagick', 'fotonik3d', 'roms', 'lulesh', 'qmcpack', 'amg']
  cfgs = [
    'static_firsttouch_12.5',
    'static_firsttouch_25',
    'static_firsttouch_50',
    'marena_avg_ddr_bandwidth_hotset_12.5_12.5',
    'marena_avg_ddr_bandwidth_hotset_25_25',
    'marena_avg_ddr_bandwidth_hotset_50_50',
    'marena_avg_ddr_bandwidth_knapsack_12.5_12.5',
    'marena_avg_ddr_bandwidth_knapsack_25_25',
    'marena_avg_ddr_bandwidth_knapsack_50_50',
    'marena_avg_ddr_bandwidth_hotset_three_cxt_12.5_12.5',
    'marena_avg_ddr_bandwidth_hotset_three_cxt_25_25',
    'marena_avg_ddr_bandwidth_hotset_three_cxt_50_50',
    'marena_avg_ddr_bandwidth_knapsack_three_cxt_12.5_12.5',
    'marena_avg_ddr_bandwidth_knapsack_three_cxt_25_25',
    'marena_avg_ddr_bandwidth_knapsack_three_cxt_50_50',
    'marena_shared_site_profiling_128',
    'marena_shared_site_profiling_256',
    'marena_shared_site_profiling_512',
    'marena_shared_site_profiling_1024',
    'marena_shared_site_profiling_2048',
    'marena_shared_site_profiling_4096',
  ]
  letters = list(ascii_lowercase)
  for letter in ascii_lowercase:
    for letter2 in ascii_lowercase:
      letters.append(letter + letter2)
  labels = list(letters)[:len(cfgs)]
    
  for expcfg, label in zip(cfgs, labels):
    print("  %4s : %s" % (label, expcfg))
  print("")

  print ("".ljust(16),end='')
  for label in labels:
    print (("%s" % label).rjust(3),end='')
  print ("")

  for bench in benches:
    print(("%s"%bench).ljust(16),end='')

    for expcfg in cfgs:
      is_run = True
      if 'profile' in expcfg:
        results_dir = get_results_dir(bench, expcfg, i=0)
        if not os.path.isdir(results_dir):
          is_run = False
      else:
        for i in range(0, 5):
          results_dir = get_results_dir(bench, expcfg, i=i)
          if not os.path.isdir(results_dir):
            is_run = False
            break
      if(is_run):
        print(("X".rjust(3)), end='')
      else:
        print((" ".rjust(3)), end='')
    print("")
  print("")

def round_to_sigfig(x):
  if x < 0.1:
    return 0.0
  else:
    return round(x, -int(floor(log10(abs(x)))))

def compare_mbi_pebs(bench, mbi_cfg, pebs_cfg, pebs2_cfg):
  # Collect pebs_results and mbi_results
  pebs_results = dict()
  pebs2_results = dict()
  parse_bench(bench, pebs_cfg, get_results_dir(bench, pebs_cfg, 0), pebs_results)
  parse_bench(bench, pebs2_cfg, get_results_dir(bench, pebs2_cfg, 0), pebs2_results)
  (profcfg_results, mbi_results) = get_ddr_bandwidth_results([bench], [mbi_cfg])

  mbi_vals = []
  mbi_percentages = []
  pebs_vals = []
  pebs_percentages = []
  pebs2_vals = []
  pebs2_percentages = []
  diffs = []
  diffs2 = []
  sites = []
  for site in mbi_results[bench][mbi_cfg]:
    if site is 0:
      continue
    mbi_vals.append(mbi_results[bench][mbi_cfg][site]['avg_ddr4_bandwidth'])
    pebs_vals.append(pebs_results['sites'][(site,)]['accesses'])
    pebs2_vals.append(pebs2_results['sites'][(site,)]['accesses'])
    sites.append(site)
  mbi_tot = sum(mbi_vals)
  pebs_tot = sum(pebs_vals)
  pebs2_tot = sum(pebs2_vals)
  for val in mbi_vals:
    mbi_percentages.append(val / mbi_tot * 100)
  for val in pebs_vals:
    pebs_percentages.append(val / pebs_tot * 100)
  for val in pebs2_vals:
    pebs2_percentages.append(val / pebs2_tot * 100)
  for i in range(0, len(mbi_percentages)):
    print(str(sites[i]) + ': ' + str(round(mbi_percentages[i], 2)) + ' ' + str(round(pebs_percentages[i], 2)) + ' ' + str(round(pebs2_percentages[i], 2)))
    if round(mbi_percentages[i], 2) is not 0.0:
      diffs.append(abs(mbi_percentages[i] - pebs_percentages[i]))
      diffs2.append(abs(mbi_percentages[i] - pebs2_percentages[i]))
  print(sum(diffs))
  print(sum(diffs2))


#def chkcfgs(benches=[], cfgs=[]):
#  for bench in benches:
#    for cfg_name in cfgs:
#      cfg.read_cfg(cfg_name)
#
#      # Collect profcfg results
#      profcfg_results = dict()
#      if 'peak_rss_cfg' in cfg.current_cfg['runcfg']:
#        get_peak_rss(bench, cfg.current_cfg['runcfg']['peak_rss_cfg'],
#                     profcfg_results)
#
#      if 'profcfg' in cfg.current_cfg['runcfg']:
#        # First parse the results of the profcfg for this bench
#        profcfg_name = cfg.current_cfg['runcfg']['profcfg']
#
#        do_profiling = False if 'bandwidth_profile' in profcfg_name else True
#        profcfg_results_dir = get_results_dir(bench, profcfg_name)
#        parse_bench(bench, profcfg_name, profcfg_results_dir, profcfg_results, do_profiling)
#
#        cfg.read_cfg(cfg_name)
#        buildcfg_name = cfg.current_cfg['buildcfg']['name']
#        hot_aps_file = get_bench_dir(bench) + 'exe/' + buildcfg_name + '/hotset.txt'
#        hotset = generate_hotset(bench, cfg_name, profcfg_results, \
#                   hstype=get_hstype(cfg.current_cfg['runcfg']))
#        write_hotset(hot_aps_file, hotset, profcfg_results['sites'])
#
#      #print ("cfg: %-40s OK" % (cfg.current_cfg['name']))
