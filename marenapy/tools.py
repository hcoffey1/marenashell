import subprocess
import os
import pexpect # Used to launch a sudo command in a shell, then be able to kill it
import sys
import re
import time
import threading # To capture the output in parallel
import multiprocessing
import resource
import marenapy.cfg as cfg
from marenapy.paths import *
from marenapy.results import *

# some weirdness here -- not sure why this is necessary
import marenapy.utils as mpu

########################################
# Stats
########################################

# PCM stats
SUM_TOTAL_BANDWIDTH  = 'sum_total_bandwidth'
AVG_TOTAL_BANDWIDTH  = 'avg_total_bandwidth'
MAX_TOTAL_BANDWIDTH  = 'max_total_bandwidth'
AVG_MCDRAM_BANDWIDTH = 'avg_mcdram_bandwidth'
MAX_MCDRAM_BANDWIDTH = 'max_mcdram_bandwidth'
AVG_DDR4_BANDWIDTH   = 'avg_ddr4_bandwidth'
MAX_DDR4_BANDWIDTH   = 'max_ddr4_bandwidth'

TOTAL_BANDWIDTH  = 'total_bandwidth'
DDR4_BANDWIDTH   = 'ddr4_bandwidth'
MCDRAM_BANDWIDTH = 'mcdram_bandwidth'

pcm_stats = [
  SUM_TOTAL_BANDWIDTH,
  AVG_TOTAL_BANDWIDTH,
  MAX_TOTAL_BANDWIDTH,
  AVG_MCDRAM_BANDWIDTH,
  MAX_MCDRAM_BANDWIDTH,
  AVG_DDR4_BANDWIDTH,
  MAX_DDR4_BANDWIDTH,
]

# Numastat stats
AVG_NODE0_MEMORY = 'avg_node0_memory'
AVG_NODE1_MEMORY = 'avg_node1_memory'
MAX_NODE0_MEMORY = 'max_node0_memory'
MAX_NODE1_MEMORY = 'max_node1_memory'
BASELINE_NODE0_MEMORY = 'baseline_node0_memory'
BASELINE_NODE1_MEMORY = 'baseline_node1_memory'
NUMASTAT_INTERVALS = 'numastat_intervals'
numastat_stats = [
  AVG_NODE0_MEMORY,
  AVG_NODE1_MEMORY,
  MAX_NODE0_MEMORY,
  MAX_NODE1_MEMORY,
  BASELINE_NODE0_MEMORY,
  BASELINE_NODE1_MEMORY,
  NUMASTAT_INTERVALS
]
memreserve_stats = [
]


########################################
# Globals
########################################

sudo_dir = tool_dir + 'sudo-1.8.20p2/'
pcm_dir = tool_dir + 'pcm/'
memreserve_dir = tool_dir + 'memreserve/'
pebs_dir = tool_dir + 'simple-pebs/'
running_tools = {} # Stores the processes
output_threads = {}
tool_type = {} # Determines which function to launch the tool with
pcm_tools = [
  'core',
  'memory',
  'msr',
  'numa',
  'pcie',
  'power',
  'sensor',
  'tsx',
  'query'
]
for tool in pcm_tools:
  tool_type[tool] = 'pcm'

numastat_tools = [
  'numastat -m',
]
for tool in numastat_tools:
  tool_type[tool] = 'numastat'

memreserve_tools = [
  'memreserve-50',
  'memreserve-38',
  'memreserve-30',
  'memreserve-25',
  'memreserve-15',
  'memreserve-12.5',
  'memreserve-6.25',
  'memreserve-3.125',
  'memreserve-1.5625'
]
for tool in memreserve_tools:
  tool_type[tool] = 'memreserve'

numactl_tools = [
  'numactl --preferred=0',
  'numactl --preferred=1'
]
for tool in numactl_tools:
  tool_type[tool] = 'numactl'

pebs_tools = [
  'pebs-128-4096-32768',
  'pebs-256-4096-32768',
  'pebs-512-4096-32768',
  'pebs-1024-4096-32768',
  'pebs-2048-4096-32768',
  'pebs-4096-4096-32768',
  'pebs-8192-4096-32768',
  'pebs-16384-4096-32768',
  'pebs-32768-4096-32768',
]
for tool in pebs_tools:
  tool_type[tool] = 'pebs'

########################################
# Utility functions
########################################

def check_tool(tool):
  if tool not in tool_type:
    print('The tool ' + tool + ' does not exist.')
    return False;
  else:
    return True;

class CaptureThread(threading.Thread):
  def __init__(self, tool):
    super(CaptureThread, self).__init__()
    self._stop_event = threading.Event()
    self.tool = tool
  def stop(self):
    self._stop_event.set()
    self.tool.sendcontrol('c')
    time.sleep(1)
    self.tool.terminate(force=True)
  def stopped(self):
    return self._stop_event.is_set()
  def run(self):
    while (not self.stopped()) and self.tool.isalive():
      line = self.tool.readline()


########################################
# Run functions
########################################

def run_pebs(tool, path, bench=None, profcfg_results=None):
  # Open the output file of the compile, rmmod, and insmod
  fd = open(path + tool + '.txt', 'w')
  # Split the tool name on dashes to figure out the period and buffer sizes
  tool_args = tool.split('-')
  period = tool_args[1]
  pebs_buf = tool_args[2]
  out_buf = tool_args[3]
  command = 'sudo bash make.sh --period ' + period + ' --pebs-buffer-size ' + pebs_buf + ' --out-buffer-size ' + out_buf
  # Compile the kernel module with the given arguments and install it
  pexpect.run(command, cwd=pebs_dir, encoding='utf-8', logfile=fd)

def run_pcm(tool, path, bench=None, profcfg_results=None):
  # Construct the command
  command = 'sudo ' + pcm_dir + 'pcm-' + tool + '.x'

  # Open the output file
  fd = open(path + tool + '.txt', 'w')
  
  # Spawn the process, store it in running_tools
  proc = pexpect.spawnu(command, timeout=None)
  proc.logfile_read = fd
  running_tools[tool] = proc
    
def run_numastat(tool, path, bench=None, profcfg_results=None):
  # Construct the command
  command = 'bash -c "while sleep 1; do '
  command += tool
  command += '; done"'

  # Open the output file
  fd = open(path + tool + '.txt', 'w')

  # Spawn the process, store it in running_tools
  proc = pexpect.spawnu(command, timeout=None)
  proc.logfile_read = fd
  running_tools[tool] = proc
  time.sleep(2)

def run_memreserve(tool, path, bench=None, profcfg_results=None):
  command = memreserve_dir + 'memreserve'
  pagesize = resource.getpagesize()

  # Run numastat -m to see how much free space is on the MCDRAM right now
  numastat = pexpect.spawn('numastat -m', timeout=None)
  numastat_re = re.compile('MemFree\s+([\d\.]*)\s+([\d\.]*)\s+([\d\.]*)')
  free_mem = 0
  for byte_line in numastat:
    line = byte_line.decode('utf_8')
    numastat_ret = numastat_re.match(line)
    if numastat_ret:
      free_mem = (float(numastat_ret.group(2)) * 1024 * 1024)
  numastat.close()

  # Get the ratio from the tool name, figure out
  # how many pages we need to reserve to only have that percentage
  # of pages available on the MCDRAM
  #needed_pages = int(4294967296 / pagesize) #int((float(tool.split('-')[1]) / 100) * ((profcfg_results['default_peak_rss']) / pagesize))
  needed_pages = int((float(tool.split('-')[1]) / 100) * ((profcfg_results['default_peak_rss']) / pagesize))
  mcdram_pages = int(free_mem / pagesize)
  reserve_pages = ''
  if needed_pages > mcdram_pages:
    reserve_pages = str(mcdram_pages)
  else:
    reserve_pages = str(mcdram_pages - needed_pages)
  command += ' 2 256 ' + reserve_pages
  #print('Starting memreserve with ' + str(reserve_pages) + \
  #      ' pages, peak profiling RSS is ' + \
  #      str(profcfg_results['default_peak_rss']) + ' bytes')
  
  free_gbs = (free_mem / (1024*1024*1024))
  reserved_gbs = (((mcdram_pages-needed_pages)*4) / (1024*1024))
  leftover_gbs = (free_gbs - reserved_gbs)

  print("  free:     %3.2f" % free_gbs )
  print("  reserved: %3.2f" % reserved_gbs )
  print("  leftover: %3.2f" % leftover_gbs )


  # Open the file and start the thread
  fd = open(path + tool + '.txt', 'w')
  proc = pexpect.spawnu(command, timeout=None)
  proc.logfile_read = fd
  running_tools[tool] = proc
  time.sleep(5)

def run_tools(bench, cfg_name, it, profcfg_results=None):
  cfg.read_cfg(cfg_name)

  # just using get_results_dir breaks here ... not sure why
  results_dir = mpu.get_iter_results_dir(bench, cfg_name, it)

  # Ensure that memreserve gets started first
  tools = cfg.current_cfg['runcfg']['tools']
  for tool in tools:
    if tool in memreserve_tools:
      tools.insert(0, tools.pop(tools.index(tool)))
  for tool in tools:
    func = 'run_' + tool_type[tool] + '(\'' + \
           tool + '\',\'' + \
           results_dir + '\',' + \
           'bench=\'' + str(bench) + '\',' + \
           'profcfg_results=' + str(profcfg_results) + ')'
    eval(func)
  for tool, tool_proc in running_tools.items():
    t = CaptureThread(tool_proc)
    output_threads[tool] = t
    output_threads[tool].start()

def kill_tools():
  pexpect.run('sudo rmmod simple-pebs') # Just in case it's still running
  for tool_name, thread in output_threads.items():
    thread.stop()
    thread.join()

########################################
# Parsing
########################################

def parse_memreserve(results_dir, results):
  pass

def parse_numastat(results_dir, results):
  node0 = 0
  node1 = 0
  results['node0_memory'] = []
  results['node1_memory'] = []
  fd = open(results_dir + 'numastat -m.txt', 'r')
  first = True

  used = re.compile('MemUsed\s+([\d\.]+)\s+([\d\.]+)\s+([\d\.]+)')
  for line in fd:
    used_ret = used.match(line)
    if used_ret:
      if first:
        results[BASELINE_NODE0_MEMORY] = float(used_ret.group(1))
        results[BASELINE_NODE1_MEMORY] = float(used_ret.group(2))
        first = False
      else:
        results['node0_memory'].append(float(used_ret.group(1)) - results[BASELINE_NODE0_MEMORY])
        results['node1_memory'].append(float(used_ret.group(2)) - results[BASELINE_NODE1_MEMORY])
  results[AVG_NODE0_MEMORY] = sum(results['node0_memory']) / len(results['node0_memory'])
  results[AVG_NODE1_MEMORY] = sum(results['node1_memory']) / len(results['node1_memory'])
  results[MAX_NODE0_MEMORY] = max(results['node0_memory'])
  results[MAX_NODE1_MEMORY] = max(results['node1_memory'])
  results[NUMASTAT_INTERVALS] = len(results['node0_memory'])

def get_first_mem(results_dir):
  fd = open(results_dir + 'memory.txt', 'r')
  sys_bandwidth = re.compile('[\r\s]*\|--\s+System Memory Throughput\(MB/s\):\s+([\d\.]+)')
  vals = []
  for line in fd:
    line = line.rstrip()
    sys_bandwidth_ret = sys_bandwidth.match(line)
    if sys_bandwidth_ret:
      vals.append(float(sys_bandwidth_ret.group(1)))
    if len(vals) > 2:
      break
  return (sum(vals) / len(vals))

def parse_pcm(results_dir, results, refres=None):
  results[TOTAL_BANDWIDTH] = []
  results[DDR4_BANDWIDTH] = []
  results[MCDRAM_BANDWIDTH] = []
  fd = open(results_dir + 'memory.txt', 'r')
  sys_bandwidth = re.compile('[\r\s]*\|--\s+System Memory Throughput\(MB/s\):\s+([\d\.]+)')
  node_bandwidth = re.compile('[\r\s]*\|--\s+DDR4 Memory \(MB/s\)\s+:\s+([\d\.]+)\s+--\|\|--\s+MCDRAM \(MB/s\)\s+:\s+([\d\.]+)\s+--\|')

  startddr    = []
  startmcdram = []
  for line in fd:
    line = line.rstrip()
    sys_bandwidth_ret = sys_bandwidth.match(line)
    if sys_bandwidth_ret:
      results[TOTAL_BANDWIDTH].append(float(sys_bandwidth_ret.group(1)))
      # MRJ -- why is this done twice?
      #results[TOTAL_BANDWIDTH].append(float(sys_bandwidth_ret.group(1)))
      continue
    node_bandwidth_ret = node_bandwidth.match(line)
    if node_bandwidth_ret:
      results[DDR4_BANDWIDTH].append(float(node_bandwidth_ret.group(1)))
      results[MCDRAM_BANDWIDTH].append(float(node_bandwidth_ret.group(2)))
      if (len(startddr) < 3):
        startddr.append((float(node_bandwidth_ret.group(1))/1024.0))
      if (len(startmcdram) < 3):
        startmcdram.append((float(node_bandwidth_ret.group(2))/1024.0))
      continue
  results[SUM_TOTAL_BANDWIDTH]   = ( (sum(results[TOTAL_BANDWIDTH]) / 1024.0 ) )
  results[AVG_TOTAL_BANDWIDTH]   = ( (sum(results[TOTAL_BANDWIDTH]) / len(results[TOTAL_BANDWIDTH])) / 1024.0 )
  results[MAX_TOTAL_BANDWIDTH]   = ( max(results[TOTAL_BANDWIDTH]) / 1024.0 )
  results[AVG_MCDRAM_BANDWIDTH]  = ( (sum(results[MCDRAM_BANDWIDTH]) / len(results[MCDRAM_BANDWIDTH])) / 1024.0)
  results[MAX_MCDRAM_BANDWIDTH]  = ( max(results[MCDRAM_BANDWIDTH]) / 1024.0)
  results[AVG_DDR4_BANDWIDTH]    = ( (sum(results[DDR4_BANDWIDTH]) / len(results[DDR4_BANDWIDTH])) / 1024.0 )
  results[MAX_DDR4_BANDWIDTH]    = ( max(results[DDR4_BANDWIDTH]) / 1024.0 )
  if refres != None:
    refavg = refres[AVG_DDR4_BANDWIDTH]
    refmax = refres[MAX_DDR4_BANDWIDTH]
    adj_avg = results[AVG_DDR4_BANDWIDTH] - refavg
    adj_max = results[MAX_DDR4_BANDWIDTH] - refmax
    results[AVG_DDR4_BANDWIDTH] = max([0.0, adj_avg])
    results[MAX_DDR4_BANDWIDTH] = max([0.0, adj_max])
    #print ("%3.4f %3.4f" % (results[AVG_DDR4_BANDWIDTH], results[MAX_DDR4_BANDWIDTH]))
