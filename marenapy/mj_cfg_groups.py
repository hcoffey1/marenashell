
mybenches = ['imagick', 'fotonik3d', 'roms', 'lulesh', 'qmcpack', 'amg']
mycfgs = [
# DONE for all benchmarks
#  'default_ir',
#  'default_ir_mcdram',
#  'marena_shared_site_profiling'
#  'marena_shared_site_profiling_three_cxt'
#  'marena_ddr_bandwidth_profile'
#  'marena_ddr_bandwidth_profile_three_cxt'
#  'static_firsttouch_12.5',
#  'static_firsttouch_25',
#  'static_firsttouch_50',
#  'marena_avg_ddr_bandwidth_knapsack_12.5_12.5',
#  'marena_avg_ddr_bandwidth_orig_hotset_12.5_12.5',
#  'marena_avg_ddr_bandwidth_hotset_12.5_12.5',
#  'marena_avg_ddr_bandwidth_hotset_12.5_12.5',
#  'marena_avg_ddr_bandwidth_hotset_25_25',
#  'marena_avg_ddr_bandwidth_hotset_50_50',
#  'marena_avg_ddr_bandwidth_knapsack_12.5_12.5',
#  'marena_avg_ddr_bandwidth_knapsack_25_25',
#  'marena_avg_ddr_bandwidth_knapsack_50_50',
  'marena_avg_ddr_bandwidth_hotset_three_cxt_12.5_12.5',
#  'marena_avg_ddr_bandwidth_hotset_three_cxt_25_25',
#  'marena_avg_ddr_bandwidth_hotset_three_cxt_50_50',
#  'marena_avg_ddr_bandwidth_knapsack_three_cxt_12.5_12.5',
#  'marena_avg_ddr_bandwidth_knapsack_three_cxt_25_25',
#  'marena_avg_ddr_bandwidth_knapsack_three_cxt_50_50',

# NEEDED for AMG and QMCPACK
#  'marena_shared_site_arenas',
#  'marena_shared_site_profiling_128',
#  'marena_shared_site_profiling_256',
#  'marena_shared_site_profiling_512',
#  'marena_shared_site_profiling_1024',
#  'marena_shared_site_profiling_2048',
#  'marena_shared_site_profiling_4096',
#  'marena_shared_site_profiling_8192',
#  'marena_shared_site_profiling_16384',
#  'marena_shared_site_profiling_three_cxt_128',
#  'marena_shared_site_profiling_three_cxt_256',
#  'marena_shared_site_profiling_three_cxt_512',
#  'marena_shared_site_profiling_three_cxt_1024',
#  'marena_shared_site_profiling_three_cxt_2048',
#  'marena_shared_site_profiling_three_cxt_4096',
#  'marena_shared_site_profiling_three_cxt_8192',
#  'marena_shared_site_profiling_three_cxt_16384',

#  'marena_hotset_pebs_128_12.5_12.5',
#  'marena_hotset_pebs_256_12.5_12.5',
#  'marena_hotset_pebs_512_12.5_12.5',
#  'marena_hotset_pebs_1024_12.5_12.5',
#  'marena_hotset_pebs_2048_12.5_12.5',
#  'marena_hotset_pebs_4096_12.5_12.5',
#  'marena_hotset_pebs_8192_12.5_12.5',
#  'marena_hotset_three_cxt_pebs_128_12.5_12.5',
#  'marena_hotset_three_cxt_pebs_256_12.5_12.5',
#  'marena_hotset_three_cxt_pebs_512_12.5_12.5',
#  'marena_hotset_three_cxt_pebs_1024_12.5_12.5',
#  'marena_hotset_three_cxt_pebs_2048_12.5_12.5',
#  'marena_hotset_three_cxt_pebs_4096_12.5_12.5',
#  'marena_hotset_three_cxt_pebs_8192_12.5_12.5',
#  'marena_hotset_three_cxt_pebs_16384_12.5_12.5',
#
#  'marena_hotset_three_cxt_avg_pebs_128_12.5_12.5',
#  'marena_hotset_three_cxt_avg_pebs_256_12.5_12.5',
#  'marena_hotset_three_cxt_avg_pebs_512_12.5_12.5',
#  'marena_hotset_three_cxt_avg_pebs_1024_12.5_12.5',
#  'marena_hotset_three_cxt_avg_pebs_2048_12.5_12.5',
  'marena_hotset_three_cxt_avg_pebs_4096_12.5_12.5',
#  'marena_hotset_three_cxt_avg_pebs_8192_12.5_12.5',
#  'marena_hotset_three_cxt_avg_pebs_16384_12.5_12.5',

#  'marena_hotset_pebs_128_25_25',
#  'marena_hotset_pebs_256_25_25',
#  'marena_hotset_pebs_512_25_25',
#  'marena_hotset_pebs_1024_25_25',
#  'marena_hotset_pebs_2048_25_25',
#  'marena_hotset_pebs_4096_25_25',
#  'marena_hotset_pebs_8192_25_25',
#  'marena_hotset_three_cxt_pebs_128_25_25',
#  'marena_hotset_three_cxt_pebs_256_25_25',
#  'marena_hotset_three_cxt_pebs_512_25_25',
#  'marena_hotset_three_cxt_pebs_1024_25_25',
#  'marena_hotset_three_cxt_pebs_2048_25_25',
#  'marena_hotset_three_cxt_pebs_4096_25_25',
#  'marena_hotset_three_cxt_pebs_8192_25_25',
#  'marena_hotset_three_cxt_pebs_16384_25_25',
#  'marena_hotset_three_cxt_pebs_32768_25_25',

#  'marena_hotset_three_cxt_avg_pebs_128_25_25',
#  'marena_hotset_three_cxt_avg_pebs_256_25_25',
#  'marena_hotset_three_cxt_avg_pebs_512_25_25',
#  'marena_hotset_three_cxt_avg_pebs_1024_25_25',
#  'marena_hotset_three_cxt_avg_pebs_2048_25_25',
#  'marena_hotset_three_cxt_avg_pebs_4096_25_25',
#  'marena_hotset_three_cxt_avg_pebs_8192_25_25',
#  'marena_hotset_three_cxt_avg_pebs_16384_25_25',

#  'marena_hotset_pebs_128_50_50',
#  'marena_hotset_pebs_256_50_50',
#  'marena_hotset_pebs_512_50_50',
#  'marena_hotset_pebs_1024_50_50',
#  'marena_hotset_pebs_2048_50_50',
#  'marena_hotset_pebs_4096_50_50',
#  'marena_hotset_pebs_8192_50_50',
#  'marena_hotset_three_cxt_pebs_128_50_50',
#  'marena_hotset_three_cxt_pebs_256_50_50',
#  'marena_hotset_three_cxt_pebs_512_50_50',
#  'marena_hotset_three_cxt_pebs_1024_50_50',
#  'marena_hotset_three_cxt_pebs_2048_50_50',
#  'marena_hotset_three_cxt_pebs_4096_50_50',
#  'marena_hotset_three_cxt_pebs_8192_50_50',
#  'marena_hotset_three_cxt_pebs_16384_50_50',
#  'marena_hotset_three_cxt_pebs_32768_50_50',

#  'marena_hotset_three_cxt_avg_pebs_128_50_50',
#  'marena_hotset_three_cxt_avg_pebs_256_50_50',
#  'marena_hotset_three_cxt_avg_pebs_512_50_50',
#  'marena_hotset_three_cxt_avg_pebs_1024_50_50',
#  'marena_hotset_three_cxt_avg_pebs_2048_50_50',
#  'marena_hotset_three_cxt_avg_pebs_4096_50_50',
#  'marena_hotset_three_cxt_avg_pebs_8192_50_50',
#  'marena_hotset_three_cxt_avg_pebs_16384_50_50',

#  'marena_hotset_three_cxt_avg_pebs_4096_12.5_12.5',
#  'marena_hotset_three_cxt_avg_pebs_4096_25_25',
#  'marena_hotset_three_cxt_avg_pebs_4096_50_50',

#  'marena_hotset_pebs_1024_25_25',
#  'marena_hotset_pebs_1024_50_50',
#  'marena_hotset_pebs_2048_12.5_12.5',
#  'marena_hotset_pebs_2048_25_25',
#  'marena_hotset_pebs_2048_50_50',

# NEEDED for all benchmarks
#  new configs guided by pebs


# finish running these for lulesh
#     c : marena_shared_site_profiling_512
#     d : marena_shared_site_profiling_1024
#     e : marena_shared_site_profiling_2048
#     f : marena_shared_site_profiling_4096
#
#  'marena_avg_ddr_bandwidth_orig_hotset_12.5_12.5',
#  'marena_avg_ddr_bandwidth_orig_hotset_25_25',
#  'marena_avg_ddr_bandwidth_orig_hotset_50_50',
#  'marena_avg_ddr_bandwidth_orig_hotset_three_cxt_12.5_12.5',
#  'marena_avg_ddr_bandwidth_orig_hotset_three_cxt_25_25',
#  'marena_avg_ddr_bandwidth_orig_hotset_three_cxt_50_50',
]

def mj_cfgs(val, x=False):
  cfgs = []
  cfgs += ['default_ir_mcdram']
  cfgs += ['static_firsttouch_%s' % str(val)]
  if not x:
    cfgs += [ ('marena_avg_ddr_bandwidth_knapsack_%s_%s'    % (str(val),str(val))) ]
    cfgs += [ ('marena_avg_ddr_bandwidth_orig_hotset_%s_%s' % (str(val),str(val))) ]
    cfgs += [ ('marena_avg_ddr_bandwidth_hotset_%s_%s'      % (str(val),str(val))) ]
  else:
    cfgs += [ ('marena_avg_ddr_bandwidth_knapsack_three_cxt_%s_%s'    % (str(val),str(val))) ]
    cfgs += [ ('marena_avg_ddr_bandwidth_orig_hotset_three_cxt_%s_%s' % (str(val),str(val))) ]
    cfgs += [ ('marena_avg_ddr_bandwidth_hotset_three_cxt_%s_%s'      % (str(val),str(val))) ]
  return cfgs

def ft_cfgs(limits=[12.5,25,50]):
  return [ "static_firsttouch_%s" % str(l) for l in limits ]

def guided_cfgs(styles=[HOTSET, HOTSET_3CXT, KNAPSACK, KNAPSACK_3CXT],
  limits=[(12.5,12.5),(25,25),(50,50)]):

  cfgs = []
  if HOTSET in styles:
    cfgs += [ ("marena_avg_ddr_bandwidth_hotset_%s_%s" % (str(x), str(y))) for x,y in limits ]
  if HOTSET_3CXT in styles:
    cfgs += [ ("marena_avg_ddr_bandwidth_hotset_three_cxt_%s_%s" % (str(x), str(y))) for x,y in limits ]
  if KNAPSACK in styles:
    cfgs += [ ("marena_avg_ddr_bandwidth_knapsack_%s_%s" % (str(x), str(y))) for x,y in limits ]
  if KNAPSACK_3CXT in styles:
    cfgs += [ ("marena_avg_ddr_bandwidth_knapsack_three_cxt_%s_%s" % (str(x), str(y))) for x,y in limits ]

  return cfgs

def pebs_cfgs(styles=[HOTSET, HOTSET_3CXT], limits=[(12.5,12.5),(25,25),(50,50)],
  rates=[128,512,2048]):
  cfgs = []
  if HOTSET in styles:
    cfgs += [ ("marena_hotset_pebs_%s_%s_%s" % (str(r), str(x), str(y))) \
              for x,y in limits for r in rates ]
  if HOTSET_3CXT in styles:
    cfgs += [ ("marena_hotset_three_cxt_pebs_%s_%s_%s" % (str(r), str(x), str(y))) \
              for x,y in limits for r in rates ]
  return cfgs
