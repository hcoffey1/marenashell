import pickle
import os
import stat
import sys
from shutil import *
import pexpect
import threading
import marenapy.cfg as cfg
from marenapy.benches.bench import *
from marenapy.utils import *

running_benches = {}
output_threads = {}

def capture_output(proc, bench, cfg_name, it, copy, runs):
  proc.expect(pexpect.EOF, timeout=None)
  cfg.read_cfg(cfg_name)

  # Unset all of the environment variables
  for var in cfg.current_cfg['runcfg']['env_variables']:
    if var.upper() in os.environ:
      del(os.environ[var.upper()])

  # Copy the config for safekeeping
  copy_dir = get_copy_results_dir(bench, cfg_name, it, copy)
  copyfile(cfg.get_cfg_file(cfg_name), copy_dir + 'cfg.py');
  # Copy user-supplied files
  for file in cfg.current_cfg['runcfg']['inputfiles']:
    try:
      copyfile(get_exe_dir(bench, cfg.current_cfg['buildcfg']['name']) + file, \
               copy_dir + file)
      #print( "Copied   " + (("%s" % file).ljust(24)) + " to results."  )
    except FileNotFoundError:
      print( "Warning: " + (("%s " % bench).ljust(25)) + 
            (("%s" % file).ljust(25)) + " file not found.")

  # Copy files output from this config
  for copy in range(cfg.current_cfg['runcfg']['copies']):
    for run in runs:
      run_dir = get_run_dir(bench, cfg_name, it, copy, run)
      results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
      for file in cfg.current_cfg['runcfg']['outputfiles']:
        try:
          copyfile(run_dir + file, results_dir + file)
          #print( "Copied   " + (("%s" % file).ljust(24)) + " to results." )
        except FileNotFoundError:
          print( "Warning: " + (("%s " % bench).ljust(25)) +
                (("%s" % file).ljust(25)) + " not found.")

def get_pin_cmd(bench, cfg_name, it, copy, run):
  cfg.read_cfg(cfg_name)

  pinparts =  [(pin_dir + "pin")]
  pinparts += [("-t %s" % (pin_dir + cfg.current_cfg['pincfg']['pintool']))]

  cxt_size = None
  if 'XPS_CxtSize' in cfg.current_cfg['runcfg']['env_variables']:
    cxt_size = cfg.current_cfg['runcfg']['env_variables']['XPS_CxtSize']

  if 'cache_args' in cfg.current_cfg['pincfg']:
    pinparts += [cfg.current_cfg['pincfg']['cache_args']]
  if 'window_shift' in cfg.current_cfg['pincfg']:
    pinparts += [("-w %d" % cfg.current_cfg['pincfg']['window_shift'])]
  if 'slice_shift' in cfg.current_cfg['pincfg']:
    pinparts += [("-s %d" % cfg.current_cfg['pincfg']['slice_shift'])]
  if 'mc_sizepct' in cfg.current_cfg['pincfg']:

    mc_sizefile = get_run_results_dir(bench, \
                  cfg.current_cfg['pincfg']['mc_sizecfg'], \
                  it, copy, run) + 'ins_count.out'

    mc_sizepct  = (cfg.current_cfg['pincfg']['mc_sizepct']/100.0)

    mc_size  = None
    sizecfgf = open(mc_sizefile)
    for line in sizecfgf:
      if line.startswith("ALL"):
        mc_size = (float(line.split()[6]) * mc_sizepct)
    pinparts += [("-C %d" % mc_size)]

  #if cxt_size != None:
  #  pinparts += [("-X %d" % cxt_size)]

  return " ".join(pinparts)

def write_seq_run_file(bench, cfg_name, it, copy):
  cfg.read_cfg(cfg_name)
  run_ref_dir = get_run_ref_dir(bench, cfg.current_cfg['runcfg']['size'])
  copy_dir = get_copy_dir(bench, cfg_name, it, copy)

  sfile = (run_ref_dir + "run.sh")
  dfile = (copy_dir + "run-seq.sh")

  srcf = open(sfile)
  dstf = open(dfile, 'w')
  for line in srcf:
    if line.startswith('cd ./run'):
      c1, c2, c3  = line.split(';')
      run = int(c1.split('-')[-1].strip('/'))

      results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
      outfile = results_dir + 'stdout.txt'
      c2 += (" > %s" % outfile)
      dcmd = ";".join([c1, c2, c3])
      dstf.write(dcmd)
    else:
      dstf.write(line)
  srcf.close()
  dstf.close()

  st = os.stat(dfile)
  os.chmod(dfile, st.st_mode | stat.S_IXUSR | stat.S_IXGRP)

def write_run_file(bench, cfg_name, it, copy, run):
  run_dir  = get_run_dir(bench, cfg_name, it, copy, run)

  src = (run_dir + ("run-%d.sh" % run))
  dst = (run_dir + "run.sh")
  command = open(src).read().split('\n')[2]

  dstf = open(dst, 'w')
  dstf.write("#!/bin/bash\n\n")
  if 'pincfg' in cfg.current_cfg:
    pincmd = get_pin_cmd(bench, cfg_name, it, copy, run)
    dstf.write(pincmd + " -- " + command + "\n")
  else:
    dstf.write(command + "\n")
  dstf.write("\n")
  dstf.close()
 
def prepare_runcfg(bench, cfg_name, it, runs=None):
  # Load the configuration from the file
  cfg.read_cfg(cfg_name)

  if runs == None:
    runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])

  bench_dir = get_bench_dir(bench)
  run_ref_dir = get_run_ref_dir(bench, cfg.current_cfg['runcfg']['size'])
  iter_dir = get_iter_dir(bench, cfg_name, it)

  # Set all of the environment variables for the scripts
  for var in cfg.current_cfg['runcfg']['env_variables']:
    print('Setting ' + var.upper())
    os.environ[var.upper()] = str(cfg.current_cfg['runcfg']['env_variables'][var])
  os.environ['BENCH'] = bench
  os.environ['OMP_NUM_THREADS'] = str(cfg.current_cfg['runcfg']['threads'])
  os.environ['KMP_NUM_THREADS'] = str(cfg.current_cfg['runcfg']['threads'])
  os.environ['KMP_AFFINITY'] = 'none'
  os.environ['THREADS'] = str(cfg.current_cfg['runcfg']['threads'])

  # Copy the executable into the iter directory for this size
  # Also make it executable
  cfg_exe = get_cfg_exe_file(bench, cfg.current_cfg['buildcfg']['name'])
  run_exe = run_ref_dir + exe_name(bench)
  copyfile(cfg_exe, run_exe)
  st = os.stat(run_exe)
  os.chmod(run_exe, st.st_mode | stat.S_IXUSR | stat.S_IXGRP)

  if bench in extra_exes:
    for exe in extra_exes[bench]:
      cfg_extra_exe = (get_exe_dir(bench, cfg.current_cfg['buildcfg']['name'])+exe)
      run_extra_exe = run_ref_dir + exe
      copyfile(cfg_extra_exe, run_extra_exe)
      st = os.stat(run_extra_exe)
      os.chmod(run_extra_exe, st.st_mode | stat.S_IXUSR | stat.S_IXGRP)

  mkdir_p(iter_dir)
  
  # Create multiple copies of the iter directory
  for copy in range(cfg.current_cfg['runcfg']['copies']):
    copy_dir = get_copy_dir(bench, cfg_name, it, copy)
    mkdir_p(copy_dir)
    write_seq_run_file(bench, cfg_name, it, copy)

    for run in runs:
      run_dir = get_run_dir(bench, cfg_name, it, copy, run)
      rm_rf(run_dir, root=True)

      copytree(run_ref_dir, run_dir)
      write_run_file(bench, cfg_name, it, copy, run)

      for file in cfg.current_cfg['runcfg']['inputfiles']:
        print('Copying ' + file + ' along with the executable.')
        copyfile(get_exe_dir(bench, cfg.current_cfg['buildcfg']['name']) + file, \
                 run_dir + file)

      create_run_results_dir(bench, cfg_name, it, copy, run)

#      if file == 'post-contexts.txt':
#        x = open(run_dir + file)
#        cnt = 0
#        for line in x:
#          if re.match("(\d)+ ben_", line):
#            cnt += 1
#        x.close()
#        if cnt > 4096:
#          os.environ['XPS_EXTRA_SITES'] = 'true'
#          print ("%d unique sites! Using extra sites" % cnt)

  # Drop caches
  os.system('echo 3 > sudo tee /proc/sys/vm/drop_caches')

# Runs a benchmark with the given runcfg
def run_runcfg(bench, cfg_name, it, runs=None, wait=True):
  cfg.read_cfg(cfg_name)

  if runs == None:
    runs = get_runs(bench, cfg.current_cfg['runcfg']['size'])

  if wait \
  and 'runseq' in cfg.current_cfg['runcfg'] \
  and cfg.current_cfg['runcfg']['runseq']:
    # Run all copies
    for copy in range(cfg.current_cfg['runcfg']['copies']):
      copy_dir = get_copy_dir(bench, cfg_name, it, copy)
      results_dir = get_copy_results_dir(bench, cfg_name, it, copy)
      fd = open(results_dir + 'stdout.txt', 'wb')
      command = ""
      if cfg.current_cfg['runcfg']['wrapper']:
        command += cfg.current_cfg['runcfg']['wrapper'] + " "
      command += "/usr/bin/sudo -E bash " + copy_dir + "run-seq.sh"
      print('Starting ' + command)
      proc = pexpect.spawn(command, timeout=None, cwd=copy_dir, logfile=fd)
      t = threading.Thread(target=capture_output,
          args=(proc, bench, cfg_name, it, copy, runs))
      t.start()
      running_benches[(copy)] = proc
      output_threads[(copy)] = t
    
    # Wait on them all to finish
    for copy in range(cfg.current_cfg['runcfg']['copies']):
      for run in runs:
        output_threads[(copy)].join()
        running_benches[(copy)].wait()

  else:
    # Run all runs
    for copy in range(cfg.current_cfg['runcfg']['copies']):
      for run in runs:
        run_dir = get_run_dir(bench, cfg_name, it, copy, run)
        results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
        fd = open(results_dir + 'stdout.txt', 'wb')

        command = ""
        if cfg.current_cfg['runcfg']['wrapper']:
          command += cfg.current_cfg['runcfg']['wrapper'] + " "
        command += "/usr/bin/sudo -E bash " + run_dir + "run.sh"

        print('Starting ' + command)
        proc = pexpect.spawn(command, timeout=None, cwd=run_dir, logfile=fd)
        t = threading.Thread(target=capture_output, \
            args=(proc, bench, cfg_name, it, copy, [run]))
        t.start()
        running_benches[(copy,run)] = proc
        output_threads[(copy,run)] = t
    
    if wait:
      # Wait on them all to finish
      for copy in range(cfg.current_cfg['runcfg']['copies']):
        for run in runs:
          output_threads[(copy,run)].join()
          running_benches[(copy,run)].wait()

  # Unset all of the environment variables
  for var in cfg.current_cfg['runcfg']['env_variables']:
    if var.upper() in os.environ:
      del(os.environ[var.upper()])

#  # Copy the config for safekeeping
#  copy_dir = get_copy_results_dir(bench, cfg_name, it, copy)
#  copyfile(cfg.get_cfg_file(cfg_name), copy_dir + 'cfg.py');
#  # Copy user-supplied files
#  for file in cfg.current_cfg['runcfg']['inputfiles']:
#    try:
#      copyfile(get_exe_dir(bench, cfg.current_cfg['buildcfg']['name']) + file, \
#               copy_dir + file)
#      print( "Copied   " + (("%s" % file).ljust(24)) + " to results." )
#    except FileNotFoundError:
#      print( "Warning: " + (("%s" % file).ljust(25)) + " not found.")
#
#  # Copy files output from this config
#  for copy in range(cfg.current_cfg['runcfg']['copies']):
#    for run in get_runs(bench, cfg.current_cfg['runcfg']['size']):
#      run_dir = get_run_dir(bench, cfg_name, it, copy, run)
#      results_dir = get_run_results_dir(bench, cfg_name, it, copy, run)
#      for file in cfg.current_cfg['runcfg']['outputfiles']:
#        try:
#          copyfile(run_dir + file, results_dir + file)
#          print( "Copied   " + (("%s" % file).ljust(24)) + " to results." )
#        except FileNotFoundError:
#          print( "Warning: " + (("%s" % file).ljust(25)) + " not found.")

