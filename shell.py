#!/usr/bin/env python
from IPython.terminal.embed import InteractiveShellEmbed
from traitlets.config.loader import Config
from IPython.terminal.prompts import Prompts, Token

from marenapy.interface import *

########################################
# iPython Shell
########################################

class CustomPrompt(Prompts):
  def in_prompt_tokens(self, cli=None):
    return [
      (Token.Prompt, 'In <'),
      (Token.PromptNum, str(self.shell.execution_count)),
      (Token.Prompt, '>: '),
    ]

  def out_prompt_tokens(self):
    return [
      (Token.OutPrompt, 'Out<'),
      (Token.OutPromptNum, str(self.shell.execution_count)),
      (Token.OutPrompt, '>: '),
    ]

nested = 0
cfg = Config()
cfg.TerminalInteractiveShell.prompts_class=CustomPrompt
ipshell = InteractiveShellEmbed(config=cfg)
ipshell('WELCOME TO MARENAPY\n')
