MARENASHELL DOCUMENTATION
=========================

Marenashell is used to compile and run benchmarks in order to test Marena. Its goals are to
implement a very simple interface and provide consistency for all of the experiments run.
To simply use the scripts without knowing many details, all you need is the functions defined in
`interface.py`.

`runexps`
---------
This is used for running experiments once you've already got the benchmarks built. 
It takes a list of benchmark names, a list of configuration names, and a number of iterations, 
and it runs iters iterations of every combination of benchmarks and configurations. For example:
```
runexps(benches=['wrf', 'xz'], cfgs=['default-clang'], iters=5)
```
would run 5 iterations of WRF and XZ with the `default-clang` configuration.
It'll look in `marenapy/cfgs/default-clang/cfg.py` for the definition of the configuration,
and use the `runcfg` section of that configuration to run the experiments.

`report`
--------
This is used for reporting the results of your experiments, such as its runtime, average memory
bandwidth, or any other metric that you've added to the scripts to be parsed. Currently it simply
calls the `parse` function for each benchmark that you've run, collecting this information in a
dict, and then prints the dict. More will be implemented later.

`build`
-------
This is where the bulk of the work has been done in Marenashell. It accepts a list of benchmarks and
configurations, similar to `runexps`, except that it uses the `buildcfg` section of the given configuration
to compile the application into an executable. As an example:
```
build(benches=['wrf', 'xz'], cfgs=['default-clang', 'marena'], verbose=True, clean=True, use_ir_backup=False, use_transformed_backup=False, nthreads=8)
```
would compile WRF and XZ into executables with both the `default-clang` and `marena` configurations.
If you set `verbose` to `True`, it will print the `stdout` and `stderr` of the compilation of each file to
your screen, which can get extremely cluttered if there are a lot of warnings or errors. However, it is useful
for debugging. Setting `clean` to `True` makes marenashell clean out all IR files that it finds in the `build/` directory,
then run `build/clean.sh` if it finds such a script. This script is usually used to remove object files, executables, etc.
`use_ir_backup` makes marenashell simply copy the IR from the `ir/` directory, instead of generating it from the source code.
`use_transformed_backup` makes marenashell simply copy the transformed IR from `transformed/` instead of generating it from the
IR. `nthreads` limits the number of concurrent processes spawned for the build. Any value less than 1 turns off thread limiting. The default is -1.

Moving to a Different Machine
-----------------------------
After cloning the `marenashell` repo, there are a few things that you'll have to set up. First, edit `paths.py`, which contains all
absolute paths that are used in the scripts. These paths tell `marenashell` where to find your benchmarks, your tools, your compiler, etc.
Next, construct a configuration file using these paths.
